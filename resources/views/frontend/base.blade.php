<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SMKN 2 PAYAKUMBUH</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
 	
	<link rel="icon" href="{{asset('assets/backend/img/favicon.ico')}}" type="image/x-icon">
    
    <title>Freelancer - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/css/freelancer.min.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('page-styles')

    @yield('inline-style')
</head>

<body id="page-top" class="index">
    @yield('content')


    <!-- jQuery -->
    <script src="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/vendor/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/js/jqBootstrapValidation.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/js/contact_me.js')}}"></script>

    <!-- Theme JavaScript -->
    <script src="{{asset('assets/backend/plugins/startbootstrap-freelancer-gh-pages/js/freelancer.min.js')}}"></script>

</body>

</html>
