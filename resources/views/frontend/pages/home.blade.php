@extends('frontend.base')

@section('content')
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">SMKN 2 PAYAKUMBUH</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#home">Home</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">About</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header id="home">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="{{asset('assets/backend/img/logo_img.png')}}" alt="">
                    <div class="intro-text">
                        <span class="name">SMKN 2 PAYAKUMBUH</span>
                        <span class="skills">Student Management System And Committee</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section class="success" style="padding-top:0px;" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                    <h1>Visi</h1>
                    <p>Menjadi Lembaga pendidikan dan latihan berkualitas dibidang Teknologi dan Rekayasa yang berstandar Internasional.</p>
                    <h1>Tujuan</h1>
                    <ol>
                        <li>Mempersiapkan peserta didik agar menjadi manusia produktif yang mampu bekerja mandiri, mengisi lowongan pekerjaan yang ada di Dunia Usaha dan Industri sebagai tenaga kerja tingkat menengah sesuai dengan kompetensi dalam Kompetensi Keahlian yang dipilihnya.</li>
                        <li>Mempersiapkan peserta didik agar mampu berkarir, ulet dan gigih, berkompetensi, beradaptasi di lingkungan kerja dan mengembangkan sikap professional dalam bidang keahlian yang dipilihnya.</li>
                        <li>Membekali peserta didik dengan ilmu pengetahuan, teknologi dan seniagar mampu mengembangkan diri di kemudian hari, baik secara mandiri maupun melalui jenjang pendidikan yang lebih tinggi.</li>
                        <li>Membekali peserta didik dengan kompetensi-kompetensi lain, sebagai pendukung kompetensi keahliannya</li>    
                    </ol>
                </div>
                <div class="col-lg-4">
                    <h1>Misi</h1>
                    <ol>
                        <li>Menjadikan tamatan yang mempunyai kecerdasan spritual, emosional,dan berakhlak mulia.</li>
                        <li>Meningkatkan mutu tamatan sesuai dengan kebutuhan DU/DI secara Nasional maupun Internasional.</li>
                        <li>Menghasilkan tamatan yang berjiwa Enterpreneurship sehingga mampu menjadi insane yang mandiri.</li>
                        <li>Meningkatkan tamatan yang terampil yang berbahasa asing, sehinggamampu beradaptasi pada komunitas Internasional.</li>
                        <li>Meningkatkan keterampilan lulusan dalam penguasaan teknologi komunikasi dan informasi.</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Login As</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 ">
                    <a href="{{ url('komite')}}" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                        <center><h1>Komite</h1></center>
                        </div>
                        <center><img style="max-width:200px;" src="{{asset('assets/backend/img/komite.png')}}" class="img-responsive" alt=""></center>
                    </a>
                </div>
                <div class="col-sm-4 ">
                    <a href="{{ url('tu-user')}}" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                        <center><h1>Tata Usaha</h1></center>
                        </div>
                        <center><img style="max-width:200px;" src="{{asset('assets/backend/img/tu.png')}}" class="img-responsive" alt=""></center>
                    </a>
                </div>
                <div class="col-sm-4 ">
                    <a href="{{ url('admin')}}" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                        <center><h1>Admin</h1></center>
                        </div>
                        <center><img style="max-width:200px;" src="{{asset('assets/backend/img/admin.png')}}" class="img-responsive" alt=""></center>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>Jl. Soekarno - Hatta, Gang Anggrek 1, Bulakan Balai Kandi
                            <br>Payakumbuh Barat, Payakumbuh, Sumatera Barat 26225</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Social Media</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Student Management System And Committee</h3>
                        <p>Cetak absen, management siswa, kelola keuangan siswa Dll.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; smkn2payakumbuh.com<br/>  
                        Dev by : <a href="https://www.facebook.com/aldifa0512" target="_blank">Aldi Fajrin</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
@stop