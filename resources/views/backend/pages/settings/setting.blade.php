@extends('backend.base')

@section('title', 'Pengaturan Aplikasi')

@section('content')
	<ol class="breadcrumb">
	    <li class=""><a href="{{ url('admin') }}">Dashboard</a></li>
	    <li class="">Pengaturan Aplikasi</li>
	</ol>
	<div class="container-fluid">
		<div data-widget-group="group1">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-green">
						<div class="panel-heading">
							<h2>
								<span data-toggle="modal" data-target="#LM" style="padding-right: 10px;" class="btn btn-default tooltips" role="button" title="Tambah data guru" id="tambahData"><i class="fa fa-plus"></i> Tambah</span> <span style="padding-left: 10px;" >Daftar Nama Guru</span>
							</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-bordered table-hover">
								<thead>
									<tr class="danger">
										<th>No</th>
										<th>Nama Depan</th>
										<th>Nama Belakang</th>
										<th>NIP</th>
										<th>Jabatan Bidang</th>
										<th>Jabatan</th>
										<th width="120">Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php
                                    	$no = 1;
							            foreach ($gurus as $key => $value){
								            echo "<tr>";
	                                    	echo "<td> $no </td>";
	                                    	echo "<td> $value->namaDepan </td>";
	                                    	echo "<td> $value->namaBelakang </td>";
	                                    	echo "<td> $value->nip </td>";
	                                    	echo "<td> $value->jabatanBidang </td>";
	                                    	echo "<td> $value->jabatanId </td>";
											echo '<td>
												<a href="#" data-toggle="modal" value="' . $value->id . '" data-target="#LM" class="btn btn-primary-alt btn-sm editDataGuru" role="button" title="Edit data guru"><i class="ti ti-pencil"></i>&nbsp;Edit</a>
		                                        <a class="btn btn-danger btn-sm tooltips" href="' . url("admin/rombels/drop", $value->id) . '" role="button" title="Hapus data guru"><i class="ti ti-trash"></i></a>
		                                        </td>';
		                                    echo '</tr>';         
								            $no++;
							            }
                                    ?>	
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							<?php
								if(isset($render) && $render == false)
									echo "";
								else
									echo $gurus->render();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>

	<div class="bs-example">
	    <!-- Large modal -->
	    <form action="{{ url('admin/setting/guru/add') }}" method="POST" id="formDataGuru">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div id="LM" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
		        <div class="modal-dialog modal-lg">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span style="font-size: 40px !important;">×</span></button>
		                	<h2  id="titleBarAjax"><i class="fa fa-plus"></i> Tambah Data Guru</h2>
		                </div>
		                <div class="modal-body">
		                    <div data-widget-group="group1" class="ui-sortable">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-info" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
											<div class="panel-heading">
												<h2>
													<i class="fa fa-th-list"></i><span style="">Form Isian Biodata Guru</span>
												</h2>
												<div class="panel-ctrls"></div>
											</div>
											<div class="panel-body">
												<table id="listings-list" class="table table-striped table-bordered table-hover">
													<thead>
														<tr class="danger">
															<th style="text-align: center;">Nama Depan</th>
															<th style="text-align: center;">Nama Belakang</th>
															<th style="text-align: center;">NIP</th>
															<th style="text-align: center;">Jabatan Bidang</th>
															<th style="text-align: center;">Jabatan</th>
														</tr>
													</thead>
													<tbody>
					                                    <tr>
						                                    <td> <input type="text" id="dataAjaxNamaDepan" placeholder="Nama depan" class="form-control" name="namaDepan"> </td>
						                                    <td> <input type="text" id="dataAjaxNamaBelakang" placeholder="Nama belakang" class="form-control" name="namaBelakang"> </td>
						                                    <td> <input type="text" id="dataAjaxNip" placeholder="Nip" class="form-control" name="nip"> </td>
						                                    <td> <input type="text" id="dataAjaxJabatanBidang" placeholder="Jabatan Bidang" class="form-control" name="jabatanBidang"> </td>
						                                    <td> <input type="text" id="dataAjaxJabatanId" placeholder="Jabatan" class="form-control" name="jabatanId"> </td>
							                            </tr>	
													</tbody>
												</table>
											</div>
											<div class="panel-footer text-right">
			                        			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												<button type="submit" id="submitAjax" name="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
							</div>
		                </div>
		            </div>
		        </div>
		    </div>
	    </form>
	</div>

@endsection

@section('page-styles')

@endsection

@section('page-scripts')

@endsection

@section('inline-script')
	<script type="text/javascript">
	$('.editDataGuru').on('click', function(e) {
		var dataId = $(this).attr('value');
		$.ajax({
          	url: '{{ url('admin/setting/guruSearchAjax/') }}',
          	data: {
				_token: '{{ csrf_token() }}',
				dataId: dataId
			},
          	type: "POST",
          	dataType: 'json',
          	success: function(res){
	          	console.log(res);
	          	var editDataGuruUrl = "{{ url('admin/setting/guru/edit') }}/" + res.id;

	          	$("#dataAjaxNamaDepan").val(res.namaDepan);
	          	$("#dataAjaxNamaBelakang").val(res.namaBelakang);
	          	$("#dataAjaxNip").val(res.nip);
	          	$("#dataAjaxJabatanBidang").val(res.jabatanBidang);
	          	$("#dataAjaxJabatanId").val(res.jabatanId);
	          	
	          	$("#formDataGuru").attr("action", editDataGuruUrl);
       			$("#submitAjax").html('<i class="fa fa-check"></i> Update');
       			$("#titleBarAjax").html('<i class="fa fa-pencil"></i> Edit Data Guru');
          	},
          	error: function(jqXHR, textStatus, errorThrown){
	            console.log( jqXHR);
	            console.log( textStatus);
	            console.log( errorThrown);
        	}
    	});
    });

    $('#tambahData').on('click', function(e) {
      	var editDataGuruUrl = "{{ url('admin/setting/guru/add') }}";
       	
       	$("#dataAjaxNamaDepan").val('');
      	$("#dataAjaxNamaBelakang").val('');
      	$("#dataAjaxNip").val('');
      	$("#dataAjaxJabatanBidang").val('');
      	$("#dataAjaxJabatanId").val('');

       	$("#formDataGuru").attr("action", editDataGuruUrl);
       	$("#submitAjax").html('<i class="fa fa-check"></i> Submit');
       	$("#titleBarAjax").html('<i class="fa fa-plus"></i> Tambah Data Guru');

    });

	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop
