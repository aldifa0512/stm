@extends('backend.base')

@section('title', 'Pengaturan Teacher\'s Roles')

@section('content')
<?php

	function getNextPageNo($pageNo, $paginate){
		if($pageNo == 1){
			return 1;
		}else{
			return ((($pageNo -1) * $paginate ) + 1);
		}
	}

?>
	<ol class="breadcrumb">
	    <li class=""><a href="{{ url('admin') }}">Dashboard</a></li>
	    <li class="">Pengaturan Teacher's Roles</li>
	</ol>
	<div class="container-fluid">
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
						</div>
					</div>
				@endif
				@if ($errors->has())
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<div class="panel panel-green">
						<div class="panel-heading">
							<h2>
								<span data-toggle="modal" data-target="#LM" style="padding-right: 10px;" class="btn btn-default tooltips" role="button" title="Tambah Teacher's Role" id="tambahData"><i class="fa fa-plus"></i> Tambah</span> <span style="padding-left: 10px;" >Daftar Teacher's Roles</span>
							</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-bordered table-hover">
								<thead>
									<tr class="danger">
										<th width="10">No</th>
										<th>Role's Name</th>
										<th>Value</th>
										<th width="120">Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php
                                    	$no = 1;
                                    	if(isset($_GET['page'])){
                                    		$no = getNextPageNo($_GET['page'], $paginate);
                                    	}
							            foreach ($settings as $key => $value){
								            echo "<tr>";
	                                    	echo "<td> $no </td>";
	                                    	echo "<td> $value->key </td>";
	                                    	echo "<td> $value->value </td>";
											echo '<td>
												<a href="#" data-toggle="modal" value="' . $value->id . '" data-target="#LM" class="btn btn-primary-alt btn-sm editRole" role="button" title="Edit Teacher\'s Roles"><i class="ti ti-pencil"></i>&nbsp;Edit</a>
		                                        <a class="btn btn-danger btn-sm tooltips deleteRole" data-toggle="modal"  href="#" data-target="#LM2" role="button" title="Hapus Teacher\'s Roles"><i class="ti ti-trash"></i></a>
		                                        </td>';
		                                    echo '</tr>';         
								            $no++;
							            }
                                    ?>	
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							<?php
								if(isset($render) && $render == false)
									echo "";
								else
									echo $settings->render();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>

	<div class="bs-example">
	    <form action="{{ url('admin/setting/roles/add') }}" method="POST" id="formDataGuru">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div id="LM" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
		        <div class="modal-dialog modal-lg">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span style="font-size: 40px !important;">×</span></button>
		                	<h2  id="titleBarAjax"><i class="fa fa-plus"></i> Tambah Teacher's Role</h2>
		                </div>
		                <div class="modal-body">
		                    <div data-widget-group="group1" class="ui-sortable">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-info" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
											<div class="panel-heading">
												<h2>
													<i class="fa fa-th-list"></i><span style="">Form Teacher's Role</span>
												</h2>
												<div class="panel-ctrls"></div>
											</div>
											<div class="panel-body">
												<table id="listings-list" class="table table-striped table-bordered table-hover">
													<thead>
														<tr class="danger">
															<th style="text-align: center;">Teacher's Role</th>
														</tr>
													</thead>
													<tbody>
					                                    <tr>
						                                    <td> <input type="text" id="dataAjaxKey" placeholder="Teacher's Role" class="form-control" name="key"> </td>
							                            </tr>	
													</tbody>
												</table>
											</div>
											<div class="panel-footer text-right">
			                        			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												<button type="submit" id="submitAjax" name="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
							</div>
		                </div>
		            </div>
		        </div>
		    </div>
	    </form>
	</div>

	<div class="bs-example">
	    <form action="" method="POST" id="formDeleteRoleDelete">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div id="LM2" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
		        <div class="modal-dialog modal-lg">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span style="font-size: 40px !important;">×</span></button>
		                	<h2 id="titleBarAjaxDelete"><i class="fa fa-trash"></i> Tambah Teacher's Role</h2>
		                </div>
		                <div class="modal-body">
		                    <div data-widget-group="group1" class="ui-sortable">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-info" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
											<div class="panel-heading">
												<h2>
													<i class="fa fa-th-list"></i><span style="">Form Teacher's Role</span>
												</h2>
												<div class="panel-ctrls"></div>
											</div>
											<div class="panel-body">
												<table id="listings-list" class="table table-striped table-bordered table-hover">
													<thead>
														<tr class="danger">
															<th style="text-align: center;">Teacher's Role</th>
														</tr>
													</thead>
													<tbody>
					                                    <tr>
						                                    <td> <input type="text" id="dataAjaxKeyDelete" placeholder="Teacher's Role" class="form-control" name="key"> </td>
							                            </tr>	
													</tbody>
												</table>
											</div>
											<div class="panel-footer text-right">
			                        			<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												<button type="submit" id="submitAjaxDelete" name="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
											</div>
										</div>
									</div>
								</div>
							</div>
		                </div>
		            </div>
		        </div>
		    </div>
	    </form>
	</div>
@endsection

@section('page-styles')

@endsection

@section('page-scripts')

@endsection

@section('inline-script')
	<script type="text/javascript">
	$('.editRole').on('click', function(e) {
		var dataId = $(this).attr('value');
		$.ajax({
          	url: "{{ url('admin/setting/roles/guruSearchRoleAjax/') }}",
          	data: {
				_token: '{{ csrf_token() }}',
				dataId: dataId
			},
          	type: "POST",
          	dataType: 'json',
          	success: function(res){
	          	console.log(res);
	          	var editDataGuruRoleUrl = "{{ url('admin/setting/roles/edit') }}/" + res.id;

	          	$("#dataAjaxKey").val(res.key);
	          	
	          	$("#formDataGuru").attr("action", editDataGuruRoleUrl);
       			$("#submitAjax").html('<i class="fa fa-check"></i> Update');
       			$("#titleBarAjax").html('<i class="fa fa-pencil"></i> Edit Teacher\'s Role');
          	},
          	error: function(jqXHR, textStatus, errorThrown){
	            console.log( jqXHR);
	            console.log( textStatus);
	            console.log( errorThrown);
        	}
    	});
    });

    $('.deleteRole').on('click', function(e) {
		var dataId = $(this).attr('value');
		$.ajax({
          	url: "{{ url('admin/setting/roles/guruSearchRoleAjax/') }}",
          	data: {
				_token: '{{ csrf_token() }}',
				dataId: dataId
			},
          	type: "POST",
          	dataType: 'json',
          	success: function(res){
	          	console.log(res);
	          	var editDataGuruRoleUrl = "{{ url('admin/setting/roles/delete') }}/" + res.id;

	          	$("#dataAjaxKeyDelete").val(res.key);
	          	
	          	$("#formDataGuruDelete").attr("action", editDataGuruRoleUrl);
	          	$("#dataAjaxKeyDelete").attr("disabled", "disabled");
	          	$("#dataAjaxKeyDelete").append("disabled", "disabled");
       			$("#submitAjaxDelete").html('<i class="fa fa-check"></i> Update');
       			$("#titleBarAjaxDelete").html('<i class="fa fa-trash"></i> Delete Teacher\'s Role');
          	},
          	error: function(jqXHR, textStatus, errorThrown){
	            console.log( jqXHR);
	            console.log( textStatus);
	            console.log( errorThrown);
        	}
    	});
    });

    $('#tambahData').on('click', function(e) {
      	var editDataGuruRoleUrl = "{{ url('admin/setting/roles/create') }}";
       	
       	$("#dataAjaxNamakey").val('');

       	$("#formDataGuru").attr("action", editDataGuruRoleUrl);
       	$("#submitAjax").html('<i class="fa fa-check"></i> Submit');
       	$("#titleBarAjax").html('<i class="fa fa-plus"></i> Tambah Teacher\'s Role');

    });

	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop
