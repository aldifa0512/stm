@extends('backend.base')

@section('title', 'Hapus Madding')

@section('content')
	<h3 class="page-title">Hapus Madding</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('admin') }}">Dashboard</a></li>
	    <li><a href="{{ url('admin/madding') }}">Madding</a></li>
	    <li class="active"><span>Hapus Madding</span></li>
	</ol>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				@endif
				
				<div class="panel panel-blue" data-widget='{"draggable": "false"}'>
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Hapus Madding</h2>
					</div>
					<!-- ./End panel heading -->

					<!-- ./End panel heading -->
					<div class="panel-body">
						<p>Yakin ingin menghapus madding dibawah ini ?</p>
						<div class="row form-group">
							<label class="col-sm-2 control-label">Description</label>
							<div class="col-sm-6">
								<input type="text" name="description" class="form-control" value="{{ $madding->description }}" disabled>
							</div>
							<a class="btn btn-primary" href="<?php echo url('admin/madding/delete/'. $madding->id . '/batal') ?>" role="button">Batal</a>&nbsp;&nbsp;
							<a class="btn btn-danger" href="<?php echo url('admin/madding/delete/'. $madding->id . '/hapus') ?>" role="button">Hapus</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
	$(function(){
		$('#expire-date').datepicker({
			todayHighLight: true,
			startDate: "+0d",
			format: 'yyyy-mm-dd',
			endDate: "+" + $('input[name="days_to_show"]').val() + "d"
		});

		$('input[name="days_to_show"]').change(function(){
			var val = $(this).val();

			$('#expire-date').datepicker('setEndDate', '+' + val + 'd');
		});

		$('#customer-id').change(function(){
			if ($(this).val() == 'non-customer') {
				$('#set-addpass').css('display', 'block');
			} else {
				$('#set-addpass').css('display', 'none');
			}
		});

		if ($('#customer-id').val() == 'non-customer') {
			$('#set-addpass').css('display', 'block');
		} else {
			$('#set-addpass').css('display', 'none');
		}
	});
	</script>
@stop