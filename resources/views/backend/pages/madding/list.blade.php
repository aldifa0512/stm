@extends('backend.base')

@section('title', 'Madding')

@section('content')
	<h3 class="page-title">Madding</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('admin') }}">Dashboard</a></li>
	    <li class="active">Madding</li>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif

		@if (Session::has('error'))
			<div class="alert alert-dismissable alert-danger">
				<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif

		@if ($errors->has())
			<div class="alert alert-dismissable alert-danger">
				<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif
		
		<div class="row">
			<div class="action-menu col-md-12">
				<a class="btn btn-primary" href="{{ url('admin/madding/create') }}" role="button"><i class="ti ti-plus"></i> Tambah Madding</a>
			</div>
		</div>

		<div data-widget-group="group1">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Daftar Madding</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body no-padding">
							<table id="example" style="" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Description</th>
										<th>Status</th>
										<th width="170px">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$no = 1;
									foreach ($maddings as $madding): ?>
										<td>{{ $no }}</td>
										<td>{{ $madding->description }}</td>
										<td>{{ $madding->status }}</td>
										<td> 
											<a class="btn btn-primary btn-sm tooltips" title="Jadikan aktif" href="<?php echo url('admin/madding/active', $madding->id) ?>" role="button">set as active</a>
											<a class="btn btn-primary btn-sm tooltips" title="View Details Madding" href="<?php echo url('admin/madding/edit', $madding->id) ?>" role="button"><i class="ti ti-eye"></i></a>
											<a class="btn btn-danger btn-sm tooltips" href="<?php echo url('admin/madding/delete', $madding->id) ?>" role="button" title="Delete Madding"><i class="ti ti-trash"></i></a>
										</td>
									</tr>
								<?php 
								$no++;
								endforeach ?>
								</tbody>
							</table>
						</div>
						<div class="panel-footer"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')

	<style type="text/css">
	table tr td {
		vertical-align: middle;
	}
	</style>
@stop

@section('page-scripts')
@stop