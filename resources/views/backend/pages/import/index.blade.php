@extends('backend.base')

@section('title', 'Import Data Siswa')

@section('content')
	<ol class="breadcrumb">
	    <li class="">Import Data Siswa</li>
	</ol>
	<div class="container-fluid">
		<!-- Action menu -->
		<center>
		<span style="text-align: left"><b><a href="http://localhost/phpmyadmin/db_export.php?db=smk2payakumbuh" target="_blank">BACKUP</a> DATA SEBELUM IMPORT SISWA.</b> Masukan file data siswa yang ingin diimport (Data dengan format dari .xlsx DATA GLOBAL SISWA dari website DAPODIK). <br>
		Sebelum upload data siswa ganti header tabel dengan header yang telah disediakan. download <a href="{{ asset('assets/header.xlsx') }}"><b>disini</b></a><br>
		Proses Import mungkin akan memakan waktu bergantung kepada jumlah data yang akan diimport, mohon bersabar dan jangan menghentikan proses import data...</span><br><br>
		<form action="{{ url('admin/import') }}" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<table width='620' border='0' align='center' cellpadding='5px' cellspacing='5px' bgcolor='#FFFFFF' style='border-radius: 5px;'>
		<tr>
			<td>
				<input type="file" name="file">
			</td>
		</tr>
		<tr>
			<td>
				<input  type="submit" name="submit">
			</td>
		</tr>
		</table>
		</center>
		</form>  
		<h3>Proses Import data berhasil apabila muncul tulisan "Berhasil" disebelah kiri atas halaman.<br> Hubungi developer jika terjadi gangguan <span style="color:blue">081372087940 (Aldi)</span>. </h3>
	</div>
@endsection

@section('inline-style')
<style type="text/css">
th, td {
    padding: 7px;
    text-align: left;
}
</style>justify-content: 
@stop