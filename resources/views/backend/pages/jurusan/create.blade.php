@extends('backend.base')

@section('title', 'Tambah Jurusan Baru')

@section('content')
	<h3 class="page-title">Tambah Jurusan Baru</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('admin') }}">Dashboard</a></li>
	    <li><a href="{{ url('admin/jurusan') }}">Jurusan</a></li>
	    <li class="active"><span>Tambah jurusan Baru</span></li>
	</ol>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				@endif
				<form action="{{ url('admin/jurusan/create') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="col-md-12">
					<div class="panel panel-blue">
						<div class="panel-heading">
							<h2>Form Edit Rombel</h2>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Jurusan</label>
								<div class="col-sm-6">
									<input type="text" name="nama_jurusan" class="form-control" value="{{ old('jurusan->nama_jurusan') }}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tata Usaha (Pengelola)</label>
								<div class="col-sm-6">
								    <select class="form-control" name="tu" required>
									    <?php
									    	$tus = App\Models\Customer::all();
									    	foreach ($tus as $tu) {
                                                $selected = "";
									    		echo "<option value='$tu->customer_id' $selected >$tu->customer_name</option>";
									    	}
									    ?>
									</select>
								</div>
							</div>
						</div>
						<!-- ./End panel body -->

						<!-- Panel Footer -->
						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									<a href="{{ url('admin/jurusan') }}" class="btn-default btn">Cancel</a>&nbsp;&nbsp;&nbsp;
									<button class="btn-primary btn">Submit</button>
								</div>
							</div>
						</div>
						<!-- ./End Panel Footer -->
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-select2/select2.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-select2/select2.min.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
	$(function(){

		$('select[name="country"]').change(function(){
			var thisEl = $(this);
			var thisVal = thisEl.find(':selected').val();

			$.ajax({
				method: 'get',
				url: '{{ url('app-admin/geo/getZone') }}',
				data: {
					_token: '{{ csrf_token() }}',
					country_id: thisVal
				},
				success: function(res) {
					if (res.status === 'success') {
						var output = '';
						var results = res.results;
						var inputProvince = $('select[name="province"]');

						for (var i = 0; i < results.length; i++) {
							output += '<option value="' + results[i].zone_id + '">' + results[i].name + '</option>';
						}
						inputProvince.find('option').remove();
						inputProvince.append(output);
					}
				}
			});
		});

		$('select[name="country"]').select2({width: '100%'});
	});
	</script>
@stop