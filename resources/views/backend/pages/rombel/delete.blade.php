@extends('backend.base')

@section('title', 'Hapus Rombel')

@section('content')
	<h3 class="page-title">Hapus Rombel</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('admin') }}">Dashboard</a></li>
	    <li><a href="{{ url('admin/rombels') }}">Rombongan Belajar</a></li>
	    <li class="active"><span>Hapus Rombel</span></li>
	</ol>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				@endif

                <div class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-12">
                        <div class="panel panel-blue">
                            <div class="panel-heading">
                                <h2>Form Edit Rombel</h2>
                            </div>
                            <div class="panel-body" style="padding-bottom:5px !important;">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Rombel</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="nama" value="{{ $rombel->nama }}" class="form-control" disabled>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Jurusan</label>
                                    <div class="col-sm-6">
                                            <select class="form-control" name="jurusan" disabled>
                                            <?php
                                                $jurusans = App\Models\Jurusan::all();
                                                foreach ($jurusans as $jurusan) {
                                                    if($jurusan->id == $rombel->jurusan_id)
                                                        $selected = "selected";
                                                    else
                                                        $selected = "";
                                                    echo "<option ".$selected.">$jurusan->nama_jurusan</option>";
                                                }
                                            ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kurikulum</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="kurikulum" value="{{ $rombel->kurikulum }}" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pengajar</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="pengajar" value="{{ $rombel->pengajar }}" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <!-- ./End panel body -->

                            <!-- Panel Footer -->
                            <div class="panel-footer" style="padding-top:5px !important;">
                                <div class="row">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <a class="btn btn-primary" href="<?php echo url('admin/rombels/drop/'. $rombel->id . '/batal') ?>" role="button">Batal</a>&nbsp;&nbsp;
                                        <a class="btn btn-danger" href="<?php echo url('admin/rombels/drop/'. $rombel->id . '/hapus') ?>" role="button">Hapus</a>
                                    </div>
                                </div>
                            </div>
                            <!-- ./End Panel Footer -->
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
	$(function(){
		$('#expire-date').datepicker({
			todayHighLight: true,
			startDate: "+0d",
			format: 'yyyy-mm-dd',
			endDate: "+" + $('input[name="days_to_show"]').val() + "d"
		});

		$('input[name="days_to_show"]').change(function(){
			var val = $(this).val();

			$('#expire-date').datepicker('setEndDate', '+' + val + 'd');
		});

		$('#customer-id').change(function(){
			if ($(this).val() == 'non-customer') {
				$('#set-addpass').css('display', 'block');
			} else {
				$('#set-addpass').css('display', 'none');
			}
		});

		if ($('#customer-id').val() == 'non-customer') {
			$('#set-addpass').css('display', 'block');
		} else {
			$('#set-addpass').css('display', 'none');
		}
	});
	</script>
@stop
