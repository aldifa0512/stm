@extends('backend.base')

@section('title', 'Edit Rombel')

@section('content')
	<ol class="breadcrumb">
	    <li class=""><a href="{{ url('admin') }}">Dashboard</a></li>
	    <li class=""><a href="{{ url('admin/rombels') }}">Rombongan Belajar</a></li>
	    <li>Edit Rombel</li>
	</ol>
	<div class="container-fluid">
		<div class="row">
			@if (Session::has('error'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if ($errors->has())
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif

			<form method="post" action="{{ url('admin/rombels/edit', $rombel->id) }}" class="form-horizontal">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="jurusanSingkatan" id="jurusanSingkatan" value="">
				<div class="col-md-12">
					<div class="panel panel-blue">
						<div class="panel-heading">
							<h2>Form Edit Rombel</h2>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">Jurusan</label>
								<div class="col-sm-4">
								    <select class="form-control" name="jurusan" required id="jurusan">
								    	<option value="none" selected disabled> ---- </option>
									    <?php
									    	$jurusans = App\Models\Jurusan::all();
									    	foreach ($jurusans as $jurusan) {
                                                if($jurusan->id == $rombel->jurusan_id)
                                                    $selected = "selected";
                                                else
                                                    $selected = "";
									    		echo "<option value='$jurusan->id' $selected >$jurusan->nama_jurusan</option>";
									    	}
									    ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tingkat</label>
								<div class="col-sm-3">
									<select class="form-control" name="tingkat" id="tingkat">
										<?php $tingkats = array(1 => '1 (Satu)', 2 => '2 (Dua)', 3 => '3 (Tiga)', 4 => '4 (Empat)');
											foreach ($tingkats as $key => $value) {
												if($rombel->tingkat == $key)
													echo '<option value="' . $key . '" selected> ' . $value . ' </option>';
												else
													echo '<option value="' . $key . '"> ' . $value . ' </option>';
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Kelas Ke</label>
								<div class="col-sm-3">
									<input type="number" name="kelasKe" value="{{ $rombel->kelasKe }}" class="form-control" id="kelasKe"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenjang</label>
								<div class="col-sm-3">
									<select class="form-control" name="jenjang" id="jenjang">
										<?php
											$satuan = 'Tahun'; 
											$tingkats = array(3 => '3 (Tiga)', 4 => '4 (Empat)');
											foreach ($tingkats as $key => $value) {
												if($rombel->tingkat == $key)
													echo '<option value="' . $key . '" selected> ' . $value .  ' ' . $satuan . ' </option>';
												else
													echo '<option value="' . $key . '"> ' . $value .  ' ' . $satuan . ' </option>';
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Rombel Yang Telah Ada</label>
								<div class="col-sm-3">
									<ul id="namaRombelYangAda">
									<?php foreach ($existedRombels as $key => $value) { ?>
											<li>{{ $value->nama }}</li>
									<?php } ?>
									</ul>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Rombel Yang Direkomendasikan</label>
								<div class="col-sm-3">
									<input type="text" name="nama" value="{{ $rombel->nama }}" class="form-control" id="namaRombel"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Pengajar</label>
								<div class="col-sm-3">
									<input type="text" name="pengajar" value="{{ $rombel->pengajar }}" class="form-control">
								</div>
							</div>
						</div>
						<!-- ./End panel body -->

						<!-- Panel Footer -->
						<div class="panel-footer">
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									<a href="{{ url('admin/rombels') }}" class="btn-default btn">Cancel</a>&nbsp;&nbsp;&nbsp;
									<button class="btn-primary btn">Update</button>
								</div>
							</div>
						</div>
						<!-- ./End Panel Footer -->
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('page-styles')
<!-- Summernote -->
<link type="text/css" href="{{ asset('assets/backend/plugins/summernote/dist/summernote.css') }}" rel="stylesheet">
@endsection

@section('page-scripts')
<!-- Summernote -->
<script type="text/javascript" src="{{ asset('assets/backend/plugins/summernote/dist/summernote.js') }}"></script>
@endsection

@section('inline-script')
	<script type="text/javascript">
		function getAngkaRomawi(angka){
			var romawi = '';
			if (angka == '1') {
				romawi = 'X';
			} else if (angka == '2') {
				romawi = 'XI';
			} else if (angka == '3') {
				romawi = 'XII';
			} else if (angka == '4') {
				romawi = 'XIII';
			}
			return romawi;
		}

		function callJurusanAjax(jurusanId){
			$.ajax({
	            url: "{{ url('admin/rombels/getJurusanAjax') }}",
	            data: {
					_token: '{{ csrf_token() }}',
					jurusanId: jurusanId
				},
              	type: "POST",
             	dataType: 'json',
              	success: function(res){
	              	// console.log(res);
	              	var tingkat = $("#tingkat").val();
	              	var kelasKe = $("#kelasKe").val();
	              	$("#jurusanSingkatan").val(res.singkatan);
					callKelasKeAjax(res.singkatan, tingkat, kelasKe);
                },
                error: function(jqXHR, textStatus, errorThrown){
	                console.log( jqXHR);
	                console.log( textStatus);
	                console.log( errorThrown);
                }
	        });
		}

		function callTingkatAjax(tingkat, jurusanId){
			var tingkat = $("#tingkat").val();
			var kelasKe = $("#kelasKe").val();
	        var jurusanSingkatan = $("#jurusanSingkatan").val();
			callKelasKeAjax(jurusanSingkatan, tingkat, kelasKe);
			$.ajax({
	            url: "{{ url('admin/rombels/getExistedRombelsAjax') }}",
	            data: {
					_token: '{{ csrf_token() }}',
					jurusanId: jurusanId,
					tingkat: tingkat
				},
              	type: "POST",
             	dataType: 'json',
              	success: function(res){
	              	var option = '';
	              	for (var i = 0; i < res.length; i++) {
	              		option += "<li>" + res[i].nama + "</li>";
	              		// console.log(res[i]);
	              	}
	              	$("#namaRombelYangAda").html(option)
                },
                error: function(jqXHR, textStatus, errorThrown){
	                console.log( jqXHR);
	                console.log( textStatus);
	                console.log( errorThrown);
              }
            });

			if(tingkat == '4'){
				$("#jenjang").val(tingkat);
			}
		}

		function callKelasKeAjax(jurusanSingkatan, tingkat, kelasKe){
          	var kelasKe = $("#kelasKe").val();
          	if(kelasKe == 0) kelasKe = '';
			$("#namaRombel").val(getAngkaRomawi(tingkat) + ' ' + jurusanSingkatan + ' ' + kelasKe);
		}

		$("#jurusan").on('keyup keypress blur change', function(e){
			var jurusanId = $("#jurusan").val();
			var tingkat = $("#tingkat").val();

			callJurusanAjax(jurusanId);
			callTingkatAjax(tingkat, jurusanId);
		});
		
		$("#tingkat").on('keyup keypress blur change', function(e){
			var jurusanId = $("#jurusan").val();
			var tingkat = $("#tingkat").val();
			
			callTingkatAjax(tingkat, jurusanId);
		});

		$("#kelasKe").on('keyup keypress blur change', function(e){
			var tingkat = $("#tingkat").val();
			var kelasKe = $("#kelasKe").val();
	        var jurusanSingkatan = $("#jurusanSingkatan").val();

	        callKelasKeAjax(jurusanSingkatan, tingkat, kelasKe);			
		});
	</script>
@endsection

@section('inline-style')
<style type="text/css">

</style>
@stop
