@extends('backend.base')

@section('title', 'Rombongan Belajar')

@section('content')
<?php
	function getNextPageNo($pageNo, $paginate){
		if($pageNo == 1){
			return 1;
		}else{
			return ((($pageNo -1) * $paginate ) + 1);
		}
	}

	function formatStringTingkatRombel($tingkatRombel){
		switch ($tingkatRombel) {
			case '1':
				$tingkat = '1 (Satu)';
				break;
			
			case '2':
				$tingkat = '2 (Dua)';
				break;

			case '3':
				$tingkat = '3 (Tiga)';
				break;

			case '4':
				$tingkat = '4 (Empat)';
				break;

			default:
				$tingkat = ' --- ';
				break;
		}
		return $tingkat;
	}

	function formatStringJenjangRombel($tingkatRombel){
		switch ($tingkatRombel) {
			case '1':
				$tingkat = '1 (Satu) tahun';
				break;
			
			case '2':
				$tingkat = '2 (Dua) tahun';
				break;

			case '3':
				$tingkat = '3 (Tiga) tahun';
				break;

			case '4':
				$tingkat = '4 (Empat) tahun';
				break;

			default:
				$tingkat = ' --- ';
				break;
		}
		return $tingkat;
	}
?>

	<ol class="breadcrumb">
	    <li class=""><a href="{{ url('admin/rombels') }}">Dashboard</a></li>
	    <li class="">Rombongan Belajar</li>
	</ol>
	<div class="container-fluid">
		<!-- Action menu -->
		<div class="row">
			<div class="action-menu col-md-12">
				<a class="btn btn-primary" href="{{ url('admin/rombels/create') }}" role="button"><i class="ti ti-plus"></i> Tambah Rombel</a>      
			</div>
		</div>

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Daftar Rombel</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body">
							<table id="listings-list" class="table table-striped table table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Rombel</th>
										<th>Tingkat</th>
										<th>Jenjang</th>
										<th>Jurusan</th>
										<th>Jumlah Siswa</th>
										<th>Pengajar</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php
                                    	$no = 1;
                                    	if(isset($_GET['page'])){
                                    		$no = getNextPageNo($_GET['page'], $paginate);
                                    	}
								        foreach ($jurusans as $value){
								            echo "<tr>";
	                                    	foreach ($value->rombels as $value2){
		                                    	echo "<td> $no </td>";
		                                    	echo "<td> $value2->nama </td>";
		                                    	echo "<td> " . formatStringTingkatRombel($value2->tingkat) . " </td>";
		                                    	echo "<td> " . formatStringJenjangRombel($value2->jenjang) . " </td>";
									            echo "<td> " . $value2->jurusan . "</td>";
									            echo "<td> $value2->jumlahSiswa orang </td>";            
									            echo "<td> $value2->pengajar </td>";
								    ?>
											<td>
												<a href="{{ url('admin/rombels/edit', $value2->id) }}" class="btn btn-primary-alt btn-sm" title="Edit rombel ini"><i class="ti ti-pencil"></i>&nbsp;Edit</a>
		                                        <a class="btn btn-danger btn-sm tooltips" href="<?php echo url('admin/rombels/drop', $value2->id) ?>" role="button" title="Hapus rombel ini"><i class="ti ti-trash"></i></a>
											</td>
										</tr>
									<?php    
											$no++;          
											}
								        }
                                    ?>	
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop
