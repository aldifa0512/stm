@extends('backend.base')

@section('title', 'Master SPP Tahunan')

@section('content')
	<h3 class="page-title">Master SPP Tahunan</h3>
	<ol class="breadcrumb">
		<li><a href="{{ url('admin') }}">Dashboard</a></li>
		<li class="active">Master SPP Tahunan</li>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif

		@if (Session::has('error'))
			<div class="alert alert-dismissable alert-danger">
				<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif

		@if ($errors->has())
			<div class="alert alert-dismissable alert-danger">
				<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif
		
		<div class="row">
			<div class="action-menu col-md-12">
				<a class="btn btn-primary" href="{{ url('admin/spp/create') }}" role="button"><i class="ti ti-plus"></i> Masukan Master SPP</a>
			</div>
		</div>

		<div data-widget-group="group1">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Daftar Master SPP</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body no-padding">
							<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>TP</th>
										<th>SPP</th>
										<th>Pembangunan</th>
										<th>Pramuka</th>
										<th>Lain-lain</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($spps as $spp)
										<tr>
											<td>{{ $spp->tp }}/{{ ($spp->tp+1) }}</td>
											<td>Rp. {{ number_format($spp->spp,2,',','.') }}</td>
											<td>Rp. {{ number_format($spp->pembangunan,2,',','.') }}</td>
											<td>Rp. {{ number_format($spp->pramuka,2,',','.') }}</td>
											<td>Rp. {{ number_format($spp->lain_lain,2,',','.') }}</td>
											<td>
												<a class="btn btn-primary btn-sm tooltips" title="Edit Master SPP" href="<?php echo url('admin/spp/edit', $spp->id) ?>" role="button"><i class="ti ti-eye"></i></a>
												<a class="btn btn-danger btn-sm tooltips" href="<?php echo url('admin/spp/delete', $spp->id) ?>" role="button" title="Delete Master SPP"><i class="ti ti-trash"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="panel-footer"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop