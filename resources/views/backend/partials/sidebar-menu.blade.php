<div class="static-sidebar-wrapper sidebar-midnightblue">
    <div class="static-sidebar">
    	<div class="sidebar">

    		<div class="widget stay-on-collapse" id="widget-sidebar">
    			<nav role="navigation" class="widget-body">
    				<ul class="acc-menu">
    					<li class="nav-separator"><span></span></li>
    					<li class="{{ Request::is('admin') ? 'active ' : null }}"><a href="{{ url('admin') }}"><i class="ti ti-home"></i><span>Dashboard</span></a></li>

                            <li class="{{ Request::is('admin/users*') || Request::is('admin/roles*') ? 'active' : null }}"><a href="javascript:;"><i class="ti ti-user"></i><span>Users</span></a>
                                <ul class="acc-menu"{!! Request::is('admin/users*') || Request::is('admin/roles*') ? ' style="display: block;"' : null !!}>
                                    <li class="{{ Request::is('admin/users/komite') ? ' active' : null }}"><a href="{{ url('admin/users/komite') }}">Komite Users</a></li>
                                    <li class="{{ Request::is('admin/users/tu') ? ' active' : null }}"><a href="{{ url('admin/users/tu') }}">Tata Usaha Users</a></li>
                                    <li class="{{ Request::is('admin/users/admin') ? ' active' : null }}"><a href="{{ url('admin/users/admin') }}">Administrator Users</a></li>
                                </ul>
                            </li>

                            <li class="{{ Request::is('admin/madding*') ? ' active' : null }}"><a href="{{ url('admin/madding') }}"><i class="ti ti-email"></i><span>Madding</span></a></li>

                            <li class="{{ Request::is('admin/tp*') ? ' active' : null }}"><a href="{{ url('admin/tp') }}"><i class="ti ti-layout-cta-btn-right"></i><span>TP</span></a></li>

                            <li class="{{ Request::is('admin/spp*') ? ' active' : null }}"><a href="{{ url('admin/spp') }}"><i class="ti ti-id-badge"></i><span>SPP</span></a></li>

                            <li class="{{ Request::is('admin/rombels*') ? 'active' : null }}"><a href="{{ url('admin/rombels') }}"><i class="ti ti-layout-list-thumb"></i><span>Rombongan Belajar</span></a>
                            </li>

                            <li class="{{ Request::is('admin/jurusan*') ? ' active' : null }}"><a href="{{ url('admin/jurusan') }}"><i class="ti ti-layout-accordion-list"></i><span>Jurusan</span></a></li>

                            <li class="{{ Request::is('admin/import*') ? ' active' : null }}"><a href="{{ url('admin/import') }}"><i class="ti ti-import"></i><span>Import</span></a></li>

                            <li class="{{ Request::is('admin/setting*') ? ' active' : null }}"><a href="javascript:;"><i class="ti ti-settings"></i><span>Setting</span></a>
                                <ul class="acc-menu"{!! Request::is('admin/users*') || Request::is('admin/roles*') ? ' style="display: block;"' : null !!}>
                                    <li class="{{ Request::is('admin/users/komite') ? ' active' : null }}"><a href="{{ url('admin/setting') }}">Teachers</a></li>
                                    <li class="{{ Request::is('admin/users/tu') ? ' active' : null }}"><a href="{{ url('admin/setting/roles') }}">Teacher's Roles</a></li>
                                </ul>
                            </li>
    				</ul>
    			</nav>
    		</div>
    	</div>
    </div>
</div>
