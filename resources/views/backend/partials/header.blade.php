<header id="topnav" class="navbar navbar-default navbar-fixed-top" role="banner">
    <div class="logo-area">
        <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
            <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                <span class="icon-bg">
                    <i class="ti ti-menu"></i>
                </span>
            </a>
        </span>

        <div class="toolbar-icon-bg hidden-xs" id="toolbar-search">

            <div class="input-group row">
                <img  style="margin-top:7px;margin-right:10px;float:left; max-width:40px;" src="{{ asset('assets/backend/img/favicon.ico') }}"><h3>ADMIN</h3>
                <span class="input-group-btn"><button class="btn" type="button"><i class="ti ti-close"></i></button></span>
            </div>
        </div>

    </div>

    <ul class="nav navbar-nav toolbar pull-right">

        <li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
            <a href="#" class="toggle-fullscreen"><span class="icon-bg"><i class="ti ti-fullscreen"></i></span></i></a>
        </li>

        <li class="dropdown toolbar-icon-bg">
            <a href="#" class="dropdown-toggle username" data-toggle="dropdown">
                <img class="img-circle" src="{{ asset('assets/backend/img/komite.png') }}" alt="" />
            </a>
            <ul class="dropdown-menu userinfo arrow">
                <li><a href="{{ url('admin/auth/logout') }}"><i class="ti ti-shift-right"></i><span>Sign Out</span></a></li>
            </ul>
        </li>

    </ul>
</header>