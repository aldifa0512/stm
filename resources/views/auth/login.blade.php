@extends('auth.base')

@section('title', 'Login')

@section('content')
	<div class="container" id="login-form">
		<a href="{{ url('/') }}" class="login-logo">
		<center><h2 style="margin-top:150px; font-family:arial; font-size:27px; font-weight:bold;">SMKN 2 PAYAKUMBUH</h2></center><!--<img src="{{ asset('assets/backend/img/logo.png') }}">--></a>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<p>{{ Session::get('error') }}</p>
					</div>
				@endif
				@if (count($errors) > 0)
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<?php
				$action = '';

				if (Request::is('auth-tu/login')) {
					$action = url('auth-tu/login');
					$who = 'Tata Usaha';
				}
				elseif (Request::is('auth-komite/login')) {
					$action = url('auth-komite/login');
					$who = 'Komite';
				}
				else {
					$action = url('admin/auth/login');
					$who = 'Admin';
				}
				?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3>Login {{ $who }}</h3>
					</div>
					<form method="post" action="{{ $action }}" class="form-horizontal" id="validate-form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="panel-body">
							<div class="form-group mb-md">
		                        <div class="col-xs-12">
		                        	<div class="input-group">							
										<span class="input-group-addon">
											<i class="ti ti-user"></i>
										</span>
										<input type="text" name="<?php echo $action == url('noncust-ads/login') ? 'ads_id' : 'email'; ?>" class="form-control" placeholder="<?php echo $action == url('noncust-ads/login') ? 'Ads ID' : 'Email'; ?>" data-parsley-minlength="6" placeholder="At least 6 characters" required value="{{ old('email') }}">
									</div>
		                        </div>
							</div>

							<div class="form-group mb-md">
		                        <div class="col-xs-12">
		                        	<div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-key"></i>
										</span>
										<input type="password" name="password" class="form-control" placeholder="Password">
									</div>
		                        </div>
							</div>

							<!--
							<div class="form-group mb-md">
		                        <div class="col-xs-12">
		                        	<div class="input-group">
										{!//! app('captcha')->display(); !!}
										@//if ($errors->has('g-recapchta-response'))
											<div class="alert alert-dismissable alert-danger">
												<p>{//{ $errors->first('g-recapchta-response') }}</p>
											</div>
										@//endif
									</div>
		                        </div>
							</div>
							-->

							@if ($action !== url('noncust-ads/login'))
								<div class="form-group mb-n">
									<div class="col-xs-12">
										<div class="checkbox-inline icheck p-n">
											<label for="">
												<input type="checkbox" name="remember"></input>
												Remember me
											</label>
										</div>
									</div>
								</div>
							@endif
						</div>
						<div class="panel-footer">
							<div class="clearfix">
								<button type="submit" class="btn btn-primary pull-right">Login</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop