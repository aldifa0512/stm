<!DOCTYPE html>
<html lang="en" class="coming-soon">
<head>
<meta charset="utf-8">
<title>@yield('title') - SMKN 2 PAYAKUMBUH</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<link rel="icon" href="{{asset('assets/backend/img/favicon.ico')}}" type="image/x-icon">

<!--<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet' type='text/css'>-->

<link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('assets/backend/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link type="text/css" href="{{ asset('assets/backend/fonts/themify-icons/themify-icons.css') }}" rel="stylesheet">               <!-- Themify Icons -->

<link type="text/css" href="{{ asset('assets/backend/css/style-blessed3.css') }}" rel="stylesheet">

</head>

<body class="focused-form animated-content">
	
	@yield('content')
    
	<script type="text/javascript" src="{{ asset('assets/backend/js/jquery-1.10.2.min.js') }}"></script> 							<!-- Load jQuery -->
	<script type="text/javascript" src="{{ asset('assets/backend/js/jqueryui-1.10.3.min.js') }}"></script> 							<!-- Load jQueryUI -->
	<script type="text/javascript" src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script> 								<!-- Load Bootstrap -->
	<script type="text/javascript" src="{{ asset('assets/backend/js/enquire.min.js') }}"></script> 									<!-- Load Enquire -->

	<script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.min.js') }}"></script>					<!-- Load Velocity for Animated Content -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.ui.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/backend/plugins/wijets/wijets.js') }}"></script>     						<!-- Wijet -->

	<script type="text/javascript" src="{{ asset('assets/backend/plugins/codeprettifier/prettify.js') }}"></script> 				<!-- Code Prettifier  -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-switch/bootstrap-switch.js') }}"></script> 		<!-- Swith/Toggle Button -->

	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}"></script>  <!-- Bootstrap Tabdrop -->

	<script type="text/javascript" src="{{ asset('assets/backend/plugins/iCheck/icheck.min.js') }}"></script>     					<!-- iCheck -->

	<script type="text/javascript" src="{{ asset('assets/backend/plugins/nanoScroller/js/jquery.nanoscroller.min.js') }}"></script> <!-- nano scroller -->

	<script type="text/javascript" src="{{ asset('assets/backend/js/application.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/backend/demo/demo.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/backend/demo/demo-switcher.js') }}"></script>

</body>
</html>