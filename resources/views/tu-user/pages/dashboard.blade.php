@extends('tu-user.base')

@section('title', 'Dashboard')

@section('content')
	<ol class="breadcrumb">
	    <li>Dashboard</li>
	</ol>
	<div class="container-fluid">
		<div class="col-md-12">
				<div class="panel panel-gray">

					<div class="panel-body">
					<span style="font-size:24px; color:red;">{{ $madding->description }}</span>
					<?php 
					$jurusanBlmClaim = App\Models\Student::where('jurusan', NULL)->count();
					$tpBlmClaim = App\Models\Student::where('tp', NULL)->count();

					if($tpBlmClaim != 0){
						echo "<br/>";
						echo "<span style=\"font-size:24px; color:red;\">Siswa belum diperbaharui tahun masuk ada <b>".$tpBlmClaim."</b> orang</span>";
						echo "<br/>";
					}
					if($jurusanBlmClaim != 0){
						echo "<span style=\"font-size:24px; color:red;\">Siswa belum diperbaharui jurusan ada <b>".$jurusanBlmClaim."</b> orang</span>";
					}
					?>
					</div>
					<!-- ./End panel body -->
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="panel panel-bluegray">
					<div class="panel-heading">
						<h2>Historis Pembayaran</h2>
					</div>
					<div class="panel-body">
					<table id="listings-list" class="table table-striped table-bordered">
						<thead>
							<tr class="success">
								<th><a href="">Waktu Transaksi</a></th>
								<th width=""><a href="">Staff Penerima</a></th>
								<th width=""><a href="">Nama Siswa</a></th>
								<th width=""><a href="">NISN Siswa</a></th>
								<th width="150"><a href="">No Invoice</a></th>
								<th width="150"><a href="">Action</a></th>
							</tr>
						</thead>
						<tbody>
						<?php
						foreach ($historis as $histori) {
							echo "<tr>";
							echo "<td>" . $histori->updated_at . "</td>";
							echo "<td>";
							echo  $histori->oleh;
							echo "</td>";
							echo "<td>" . $histori->nama . "</td>";
							echo "<td>" .  str_pad($histori->nisn, 10, '0', STR_PAD_LEFT)  . "</td>";
							echo "<td>" . $histori->no_kwitansi . "</td>";
							echo "<td>";
							echo "<a href='" . url('tu-user/laporan/invoice/'.$histori->nisn. '/' .$histori->tp. '/' .$histori->semester. '/' .$histori->no_kwitansi) . "' target='_blank' class='btn btn-primary-alt btn-sm' title='Lihat Histori Pembayaran'><i class='ti ti-eye'></i>&nbsp;Lihat</a>";
							echo "</td>";
							echo "</tr>";
						}
						?>
						</tbody>
					</table>						

					</div>
				
				</div>
			</div>
	</div> <!-- .container-fluid -->
@stop

@section('page-styles')
	<!-- FullCalendar -->
	<link type="text/css" href="{{ asset('assets/backend/plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
	<!-- jVectorMap -->
	<link type="text/css" href="{{ asset('assets/backend/plugins/jvectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet">
	<!-- Switchery -->
	<link type="text/css" href="{{ asset('assets/backend/plugins/switchery/switchery.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Load page level scripts-->
	<!-- Charts -->
	<!-- Flot Main File -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/charts-flot/jquery.flot.min.js') }}"></script>
	<!-- Flot Pie Chart Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/charts-flot/jquery.flot.pie.min.js') }}"></script>
	<!-- Flot Stacked Charts Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/charts-flot/jquery.flot.stack.min.js') }}"></script>
	<!-- Flot Ordered Bars Plugin-->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/charts-flot/jquery.flot.orderBars.min.js') }}"></script>
	<!-- Flot Responsive -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/charts-flot/jquery.flot.resize.min.js') }}"></script>
	<!-- Flot Tooltips -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/charts-flot/jquery.flot.tooltip.min.js') }}"></script>
	<!-- Flot Curved Lines -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/charts-flot/jquery.flot.spline.js') }}"></script>
	<!-- Sparkline -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/sparklines/jquery.sparklines.min.js') }}"></script>

	<!-- jVectorMap -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
	<!-- jVectorMap -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

	<!-- Switchery -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/switchery/switchery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/easypiechart/jquery.easypiechart.js') }}"></script>
	<!-- Moment.js Dependency -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/fullcalendar/moment.min.js') }}"></script>
	<!-- Calendar Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
	<!-- Initialize scripts for this page-->
	<script type="text/javascript" src="{{ asset('assets/backend/demo/demo-index.js') }}"></script>
	<!-- End loading page level scripts-->
@stop