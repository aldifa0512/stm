@extends('tu-user.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <?php
	    	$tp = get_tp_aktif();
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $kelas)->first()->toArray();//dd($jurusan);
			$jurusan = getJurusanById($rombel['jurusan_id']);
	    ?>
	    <li><a href="{{ url('tu-user/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('tu-user/students/rombel') }}">Rombongan Belajar</a></li>
	    <li>Kelas {{$rombel['nama']}}</li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="col-md-12">
					<?php
					if (isset($rombelM) && $rombelM == true)
						echo "";
					else{
					?>
					<div class="row">
						<div class="action-menu col-md-12">
						<form action="{{ url('tu-user/students/filter') }}" method="post" id="buy-listing-wizard" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div style="padding-bottom:5px;">
									<label class="col-sm-1">NISN</label>
									<div class="row col-sm-3">
										<input type="text" name="nisn" placeholder="NISN atau NAMA" class="form-control" value="{{ old('nisn') }}">
									</div>
									<button style="margin-left:15px;" class="btn-primary btn">Cari !</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					}
					?>
					
					<div class="panel panel-default">
						<div class="panel-heading">
						<?php 
						$tpAktif = get_tp_aktif();
						$bulan = Bulan_laporan();

						if(isset($rombelM)){ ?>
						<div style="float: right; padding-left: 10px; color: black;">
							<span style="padding-right: 5px; color:#03a9f4; cursor: pointer;"><i style="" id="hideButton" class="fa fa-chevron-up" style="float: right;">Hide</i></span>
							<span style="padding-right: 5px; color:#03a9f4; cursor: pointer;"><i style="" id="showButton" class="fa fa-chevron-up" style="float: right;">Show</i></span>	
						</div>
						
						<?php } ?>
							<?php
								if(isset($recap) && $recap == true){
									echo "<h2 style='color:#03a9f4;'>Daftar Siswa Keseluruhan (Aktif dan Tidak Aktif)</h2> ";
									$satuan = " Siswa Aktif dan Tidak Aktif ";
								}
								elseif(isset($pasif) && $pasif == true){
									echo "<h2 style='color:#03a9f4;'>Daftar Siswa Tidak Aktif</h2> ";
									$satuan = " Siswa Tidak Aktif ";
								}
								elseif(isset($rombelM) && $rombelM == true){
									echo "<h2 style='color:#03a9f4;'>Daftar Siswa Kelas ". $rombel['nama'] ." / " . number_format($jumlahSiswa,0,',','.') . " " .strtoupper('siswa') . "</h2> ";
									$satuan = ' Siswa '
							?>
									<!-- <a style="float:right; " href="{{ url('tu-user/students/spp/rombel/'.$kelas.'/lap') }}" class=""><u>Cetak Laporan</u></a> -->
							<?php
								}
								else{
									echo "<h2 style='color:#03a9f4;'>Daftar Siswa Aktif </h2> ";
									$satuan = 'Siswa Aktif ';
								} 
							?>    
								<?php if(isset($jumlahSiswa) && !isset($rombelM)) : ?>
									<span style="float: right;" >{{ number_format($jumlahSiswa,0,',','.') }} {{ strtoupper($satuan) }}</span> 
								<?php endif; ?>
						</div>

						<div class="panel-body">
						<?php 
						if(isset($rombelM)){ 
						$naikKelas = getNaikKelas($rombel['id']);
						?>
						<form action="{{ url('tu-user/students/rombel/action/'.$rombel['id']) }}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="idRombel" value="{{ $rombel['id'] }}">
						<input type="hidden" name="rombelTingkat" value="{{ $rombel['tingkat'] }}">
						<input type="hidden" name="jurusanJenjang" value="{{ $jurusan['jenjang'] }}">
						<input type="hidden" name="idJurusan" value="{{ $rombel['jurusan_id'] }}">
							<div style="padding-bottom: 20px;">
								<div id="actionBar" style="background-color: ;">
									<div style=" padding-bottom: 10px;">
			                            <a href="#" data-toggle="modal" data-target="#LM"><span style="font-size: 16px; cursor: pointer; color:#03a9f4;"><i class="fa fa-list-ul"></i> Cetak Absen</span></a> 
			                            <span style="padding-left: 10px; padding-right: 10px;">|</span> 
			                             <a href="#" data-toggle="modal" data-target="#LM2"><span style="font-size: 16px; cursor: pointer; color:#03a9f4;"><i class="fa fa-envelope-o"></i> Cetak Surat</span></a>
			                        </div>
			                        <hr>
		                            <b>Action : </b>
		                            <select name="action" id="action">
		                            	<option disabled selected>--Pilih--</option>
		                            	<option value="lulus">Lulus</option>
		                            	<option value="naik">Naik ke Kelas</option>
		                            	<option value="tinggal">Tinggal ke Kelas</option>
		                            </select>
		                            <select name="actionVal" id="actionVal">
		                            	<option id="actionValDefault">Silahkan pilih action terlebih dahulu</option>
		                            </select>
		                            <span style="padding-top: -5px; float: right;"><input type="submit" name="submit" value="submit" class="btn btn-sm"></span>
		                        </div>
							</div>
							
	                    <?php } ?>
	                        <table id="listings-list" class="table table-striped table-bordered">
								<thead>
									<tr class="success">
										<th width="">Nama Lengkap</th>
										<th width="">Foto</th>
										<th width="">NISN</th>
										<th width="90">Tanggal Lahir</th>
										<?php
											if(isset($pasif))
												echo "<th>Alasan Keluar</th>";
											else
												echo "<th>Jurusan</th>";
										?>
										<th width="80">Kelas</th>
										<th width="150">Tunggakan</th>
										<th width="150">Action
										<?php 
											if(isset($rombelM)){
												echo ' <span style="float:right;"> <input name="selectAll" type="checkbox" id="select_all"></th></span>';
											}
										?>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($students as $student)
									<?php 
										$assets = json_decode($student->pas_foto);//var_dump($assets);die();
										$filename = substr($assets[0], strrpos($assets[0], '/') + 1);
										$img_entry = str_replace($filename, 'thumb-admin-'.$filename, $assets[0]);
									?>
									<?php
										if($student->status == "pasif"){
											$disableBtn = "disabled";
											$class = "class='danger'";
										}else{
											$disableBtn = "";
											$class = "class=''";
										}
									?>
									<tr <?php echo $class ?>>
										<?php 
										$require = "";
										if($student->jurusan == NULL & $student->tp != NULL)
											$require = "style=color:blue;";
										if($student->jurusan != NULL & $student->tp == NULL)
											$require = "style=color:green;";
										if($student->jurusan == NULL & $student->tp == NULL)
											$require = "style=color:red;";
										?>
										<td class=""><span {{$require}}>{{ $student->nama_lengkap }}</span></td>
										<td class="text-center">
											<?php if ($img_entry != ''): ?>
												<img class="img-thumbnail" src="{{ asset($img_entry) }}" width="70" height="70">
											<?php endif ?>
											<?php
												if($student->pas_foto == null){
													echo "Foto tidak tersedia";
												}
											?>
										</td>
										<td>{{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }}</td>
										<td>
											<?php echo $student->tanggal_lahir ?>
											
										</td>
										<td>
											<?php
												if(isset($pasif)){
													$alasan = App\Models\DropOut::where('nisn', $student->nisn)->first()->toArray();
													echo $alasan['keluar_karena'];
												}else{
													if($student->jurusan == null){
														echo "<span style='color:blue;'>Jurusan belum diisi</span>";
													}else{
														$jurusan = App\Models\Jurusan::where('id', $student->jurusan)->first()->toArray();//dd($jurusan);
														echo $jurusan['nama_jurusan'];
													}
												}
											?>
										</td>
										<td>
											<?php 
												if($student->kelas == null){
													echo "<span style='color:blue;'>Kelas belum diisi</span>";
												}else{
													$rombel = App\Models\Rombel::where('id', $student->kelas)->first();
													if($rombel != null){
														$rombel = $rombel->toArray();
														echo $rombel['nama'];
													}
												}
											?>
										</td>
										<td>Rp. {{number_format(tunggakan_total_tahun_aktif($student->nisn, 9, $tp->tp, $student->tp),2,',','.')}}</td>
										<td class="row">
											<?php if(!isset($rombelM)){ ?>
	                                        <div class="btn-group">
		                                        <button disabled type="button" class="btn btn-midnightblue-alt dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		                                            <i class="ti ti-settings"></i> <span class="caret"></span>
		                                        </button>
		                                        <ul class="dropdown-menu" role="menu">
		                                            <li><a href="{{ url('tu-user/students/edit', $student->nisn) }}">Edit</a></li>
		                                            <li><a href="{{ url('tu-user/students/drop/'.$student->nisn) }}" target="_blank">Non Aktifkan</a></li>
		                                        </ul>
		                                    </div>
		                                    <?php }else{ ?>
		                                    	<div class="btn-group">
		                                        <button type="button" class="btn btn-midnightblue-alt dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		                                            <i class="ti ti-settings"></i> <span class="caret"></span>
		                                        </button>
		                                        <ul class="dropdown-menu" role="menu">
		                                            <li><a href="{{ url('tu-user/students/edit', $student->nisn) }}">Edit</a></li>
		                                            <li><a href="{{ url('tu-user/students/drop/'.$student->nisn) }}" target="_blank">Non Aktifkan</a></li>
		                                        </ul>
		                                    </div>
		                                    <?php } ?>
		                                    <?php
		                                    if(isset($rombelM)){
		                                    	echo '<input style="float:right;" class="checkbox" type="checkbox" value="'. $student->nisn . '" name="check[]">';
		                                    }
		                                    ?>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							</form>
						</div>

						<div class="panel-footer text-right">
							<?php
								if(isset($render) && $render == false)
									echo "";
								else
									echo $students->render();
							?>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
<?php if(isset($rombelM) && $rombelM == true){ ?>
<div class="bs-example">
    <!-- Large modal -->
    <form action="{{ url('tu-user/students/rombel/absen/'.$rombel['id']) }}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="idRombel" value="{{ $rombel['id'] }}">
		<input type="hidden" name="jurusan" value="{{ $jurusan['nama_jurusan'] }}">
		<div id="LM" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
	        <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span style="font-size: 40px !important;">×</span></button>
	                	<h2>Cetak Absen <i class="fa fa-print"></i></h2>
	                </div>
	                <div class="modal-body">
	                    <div class="panel panel-info">
				            <div class="panel-heading">
				              <h4 class="modal-title" style="color:black;" id='namaSiswa'></h4>
				            </div>
				            <div class="panel-body">
				              <div class="row">
				                <div class=" col-md-12 col-lg-12 "> 
				                  <fieldset class="form-group">
								    <legend><i class="fa fa-th"></i> Silahkan Pilih Jenis Absen</legend>
								    <div class="form-check">
								      <label class="form-check-label">
								        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="siswaAktif" checked>
								        Absen berdasarkan status aktif siswa. <em><b>(Untuk kebutuhan sehari-hari)</b></em>
								      </label>
								    </div>
								    <div class="form-check">
								    <label class="form-check-label">
								        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="tunggakan">
								        Absen berdasarkan tunggakan siswa terhadap batas tunggakan. <em><b>(pendisiplinan saat pendaftaran ulang)</b></em>
								      </label>
								    </div>
								    <div id="pilihBulan"></div>
								  </fieldset>
								  <hr>
								  <legend><i class="fa fa-th"></i> Data Absen Untuk Rombel {{ $rombel['nama'] }}</legend>
								    <div class="form-check">
								      <label class="form-check-label">
								        <table class="table table-bordered">
					                      <tbody>
					                        <tr class="warning">
					                          <td>Kelas</td>
					                          <td>Jurusan</td>
					                          <td>Tahun Ajaran</td>
					                          <td>Bulan</td>
					                          <td>Tanggal</td>
					                        </tr>
					                        <tr>
					                          <td>{{ $rombel['nama'] }}</td>
					                          <td>{{ $jurusan['nama_jurusan'] }}</td>
					                          <td>{{ $tpAktif->tp }}/{{ ($tpAktif->tp+1) }}</td>
					                          <td>{{ $bulan[date('m')] }}</td>
					                          <td><center> --- </center></td>
					                        </tr>
					                      </tbody>
					                    </table>
								      </label>
								    </div>   
			                    </div>
				              </div>
				            </div>
			                <div class="panel-footer">
		                        <div style="float:right;">
			                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		                    		<button type="submit" name="submit" class="btn btn-primary">OK</button>
		                    	</div>
			                </div>
				        </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </form>
</div>

<div class="bs-example">
    <!-- Large modal -->
    <form action="{{ url('tu-user/students/rombel/surat/'.$rombel['id']) }}" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="idRombel" value="{{ $rombel['id'] }}">
		<input type="hidden" name="jurusan" value="{{ $jurusan['nama_jurusan'] }}">
		<div id="LM2" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
	        <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span style="font-size: 40px !important;">×</span></button>
	                	<h2>Cetak Surat <i class="fa fa-print"></i></h2>
	                </div>
	                <div class="modal-body">
	                    <div class="panel panel-info">
				            <div class="panel-heading">
				              <h4 class="modal-title" style="color:black;" id='namaSiswa'></h4>
				            </div>
				            <div class="panel-body">
				              <div class="row">
				                <div class=" col-md-12 col-lg-12 "> 
				                <fieldset class="form-group">
								    <legend><i class="fa fa-th"></i> Silahkan Pilih Jenis Surat</legend>
								    <div class="form-check">
								      <label class="form-check-label">
								        <input type="radio" class="form-check-input" name="optionsRadiosSurat" id="optionsRadiosSurat1" value="1" checked>
								        Surat rapat tipe 1. <em><b>(Undangan dari Bendahara)</b></em>
								      </label>
								    </div>
								    <div class="form-check">
								    <label class="form-check-label">
								        <input type="radio" class="form-check-input" name="optionsRadiosSurat" id="optionsRadiosSurat2" value="2">
								        Surat rapat tipe 2. <em><b>(Undangan dari Kaprog)</b></em>
								      </label>
								    </div>
								    <div id="pilihBulan"></div>
								  </fieldset>
								  <hr>
			                    </div>
				              </div>
				            </div>
			                <div class="panel-footer">
		                        <div style="float:right;">
			                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		                    		<button type="submit" name="submit" class="btn btn-primary">OK</button>
		                    	</div>
			                </div>
				        </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </form>
</div>
<?php } ?>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('inline-script')
	<script type="text/javascript">
<?php if(isset($rombelM) && $rombelM == true){ ?>
	
	$('#action').on('keyup keypress blur change', function(e) {
		var action = $('#action').val();
            $.ajax({
              url: '{{ url('tu-user/students/rombelAjax/') }}',
              data: {
					_token: '{{ csrf_token() }}',
					action: action,
					rombelTingkat: '{{ $rombel['tingkat'] }}',
					rombelId: '{{ $rombel['id'] }}',
					jurusanId: '{{ $rombel['jurusan_id'] }}'
				},
              type: "POST",
              dataType: 'json',
              success: function(res){
              	console.log(res);
              	$("#actionValDefault").remove();
              	$(".addedOption").remove();
              	$("#actionVal").append(res);
              },
              error: function(jqXHR, textStatus, errorThrown){
                console.log( jqXHR);
                console.log( textStatus);
                console.log( errorThrown);
              }
            });      
        });

	$('input[name=optionsRadios]').on('change', function(e) {
		var radioVal = $('input[name=optionsRadios]:checked').val();
		var pilihBulan;
		console.log(radioVal);

		if(radioVal == 'tunggakan'){
			pilihBulan = '<div class="form-check col-md-12" id="pilihBulan">';
			pilihBulan += '<label class="form-check-label"><span style="color:#704403;"><b>Pilih Batas Bulan Tunggakan : </b></span> ';
			pilihBulan += '</label>';
			pilihBulan += '<div class="row">';
			pilihBulan += '<div class="col-md-4"><select name="pilihBulan" class="form-control">';
			<?php 
				foreach ($bulan as $key => $value) {
					echo "pilihBulan += '<option value=\"" . ($key + 3) . "\">" . $value . "</option>';\n";
				}
			?>
			pilihBulan += '</select></div>';
			pilihBulan += '<div class="col-md-4"><select name="pilihJenisTunggakan" class="form-control">';
			pilihBulan += '<option value="semesterAktif">Semester Aktif</option>';
			pilihBulan += '<option value="kumulatif">Kumulatif</option>';
			pilihBulan += '</select></div>';
			pilihBulan += 'atau <span style="padding-left:10px"><a href="{{ url('tu-user/students/rombel/tunggakan/'.$rombel['id']) }}"><i class="fa fa-file-excel-o"></i> TUNGG - </button>';
			pilihBulan += '{{ $rombel['nama'] }}';
			pilihBulan += '.xls</a></span>';
			pilihBulan += '</div>';
			pilihBulan += '</div>';
		}else{
			pilihBulan = '<div id="pilihBulan"></div>';
		}

		$("#pilihBulan").html(pilihBulan);
                
        });
<?php } ?>
	//select all checkboxes
	$("#select_all").change(function(){  //"select all" change 
	    var status = this.checked; // "select all" checked status
	    $('.checkbox').each(function(){ //iterate all listed checkbox items
	        this.checked = status; //change ".checkbox" checked status
	    });
	});

	$('.checkbox').change(function(){ //".checkbox" change 
	    //uncheck "select all", if one of the listed checkbox item is unchecked
	    if(this.checked == false){ //if this item is unchecked
	        $("#select_all")[0].checked = false; //change "select all" checked status to false
	    }
	    
	    //check "select all" if all checkbox items are checked
	    if ($('.checkbox:checked').length == $('.checkbox').length ){ 
	        $("#select_all")[0].checked = true; //change "select all" checked status to true
	    }
	});

	//HIDE AND SHOW ACTION BAR/TOGGLE
	$(document).ready(function(){
		$("#actionBar").hide();
	    $("#hideButton").click(function(){
	        $("#actionBar").hide();
	    });
	    $("#showButton").click(function(){
	        $("#actionBar").show();
	    });
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
	    .bs-example{
    	margin: 20px;
    }
</style>
@stop