@extends('tu-user.base')

@section('title', 'Rombongan Belajar')

@section('content')
<?php
	function getNextPageNo($pageNo, $paginate){
		if($pageNo == 1){
			return 1;
		}else{
			return ((($pageNo -1) * $paginate ) + 1);
		}
	}

	function formatStringTingkatRombel($tingkatRombel){
		switch ($tingkatRombel) {
			case '1':
				$tingkat = '1 (Satu)';
				break;
			
			case '2':
				$tingkat = '2 (Dua)';
				break;

			case '3':
				$tingkat = '3 (Tiga)';
				break;

			case '4':
				$tingkat = '4 (Empat)';
				break;

			default:
				$tingkat = ' --- ';
				break;
		}
		return $tingkat;
	}

	function formatStringJenjangRombel($tingkatRombel){
		switch ($tingkatRombel) {
			case '1':
				$tingkat = '1 (Satu) tahun';
				break;
			
			case '2':
				$tingkat = '2 (Dua) tahun';
				break;

			case '3':
				$tingkat = '3 (Tiga) tahun';
				break;

			case '4':
				$tingkat = '4 (Empat) tahun';
				break;

			default:
				$tingkat = ' --- ';
				break;
		}
		return $tingkat;
	}
?>
	<ol class="breadcrumb">
		<li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li><a href="{{ url('tu-user/students') }}">Siswa</a></li>
	    <li class="">Rombongan Belajar</li>
	</ol>
	<div class="container-fluid">
			@if (Session::has('error'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if (Session::has('success'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-success">
						<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if ($errors->has())
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 style="color:blue;">Daftar Rombel</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body">
							<table id="listings-list" class="table table-striped table table-hover">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Rombel</th>
										<th>Tingkat</th>
										<th>Jenjang</th>
										<th>Jurusan</th>
										<th>Jmlh Siswa</th>
										<th>Pengajar</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php
                                    	$no = 1;
                                    	if(isset($_GET['page'])){
                                    		$no = getNextPageNo($_GET['page'], $paginate);
                                    	}
								        foreach ($jurusans as $value){
								            echo "<tr>";
	                                    	foreach ($value->rombels as $value2){
		                                    	echo "<td> $no </td>";
		                                    	echo "<td> $value2->nama </td>";
		                                    	echo "<td> " . formatStringTingkatRombel($value2->tingkat) . " </td>";
		                                    	echo "<td> " . formatStringJenjangRombel($value2->jenjang) . " </td>";
									            echo "<td> " . $value2->jurusan . "</td>";
									            echo "<td> $value2->jumlahSiswa <span style='float:right;'>orang</span></td>";            
									            echo "<td> $value2->pengajar </td>";
								    ?>
											<td>
												<div class="btn-group">
			                                        <button type="button" class="btn btn-midnightblue-alt dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			                                            <i class="ti ti-settings"></i> <span class="caret"></span>
			                                        </button>
			                                        <ul class="dropdown-menu" role="menu">
			                                            <li><a href="{{ url('tu-user/students/rombel/lihat', $value2->id) }}">Lihat</a></li>
			                                            <li><a href="{{ url('tu-user/students/export/'.$value2->id) }}">Absen</a></li>
			                                            <li class="divider"></li>
			                                            <li><a href="{{ url('tu-user/students/rombel/claim', $value2->id) }}">Claim Jurusan</a></li>
			                                            <li><a href="{{ url('tu-user/students/rombel/claim-tahun-masuk', $value2->id) }}">Claim Tahun Masuk</a></li>
			                                        </ul>
			                                    </div>
											</td>
										</tr>
									<?php    
											$no++;          
											}
								        }
                                    ?>	
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop
