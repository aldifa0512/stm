@extends('tu-user.base')

@section('title', 'Imported Students')

@section('content')
<?php
	function checkClassExists($existedClass){
		if ($existedClass == '<span style="color:black">Kelas telah tersedia</span>'){
			return TRUE;
		}
		
		return FALSE;
	}

	function checkZeroStudents($existedStudents){
		if($existedStudents == 0){
			return TRUE;
		}

		return FALSE;
	}
	
	function checkBeforeProcess($existedStudents, $existedClass){
		$existedStudents = checkZeroStudents($existedStudents);
		$existedClass = checkClassExists($existedClass);

		if($existedStudents == TRUE && $existedClass == TRUE){
			return TRUE;
		}

		return FALSE;
	}
?>
	<ol class="breadcrumb">
	    <li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li class="active"><span>Imported Students</span></li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				<div class="col-md-12">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				</div>
				<div class="col-md-12">
					<table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Imported Class</th>
                                <th>Existed Class</th>
                                <th>Pilihan Class</th>
                                <th>Imported Students</th>
                                <th>Existed Students</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
                        		$no = 1;
                        		foreach ($rombels as $key => $value){ 
                        		$existedClass = checkExistedClassByName($value->importedClass);
                        	?>
                        		<tr>
                        			<form action="{{ url('tu-user/students/importedStudents/' . $value->importedClass) }}" method="POST">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="className" value="{{ $value->importedClass }}">
	                        		<td>{{ $no }}</td>
	                        		<td>{{ $value->importedClass }}</td>
	                        		<td><?php echo $existedClass ?></td>
	                        		<td>
	                        			<?php 
	                        			if (checkClassExists($existedClass)) {
	                        				echo '<select name="namaKelas" class="form-control" disabled>';
	                        				foreach ($existedRombels as $key => $value2) {
	                        					if($value2->nama == $value->importedClass){
	                        						echo '<option selected>' . $value2->nama . '</option>';
	                        					}else{
	                        						echo '<option>' . $value2->nama . '</option>';
	                        					}
	                        				}
	                        				echo '</select>';
	                        			}else{
	                        				echo 'Kelas belum ada';
	                        			}
	                        		 	?>
	                        		 </td>
	                        		<td>{{ $value->jumlahSiswa }} orang</td>
	                        		<td>
	                        			<?php 
	                        				if(checkZeroStudents($value->existedStudents)){
	                        					echo '<span style="color:black">' . $value->existedStudents . ' orang</span>';
	                        				}else{
	                        					echo '<span style="color:red">' . $value->existedStudents . ' orang</span>';
	                        				}
	                        			?>
	                        		</td>
	                        		<td>
	                        			<?php 
	                        				if(checkBeforeProcess($value->existedStudents, $existedClass)){ 
	                        					echo '<button type="button-primary" class="btn btn-success btn-sm">Proses Kelas</button>'; 
	                        				}else{
	                        					echo '<button type="button" class="btn btn-danger btn-sm" disabled>Proses Kelas</button>';
	                        				}  
	                        			?>
	                        		</td>
	                        		</form>
	                        	</tr>	
                        	<?php 
	                        	$no++;
	                        	} 
                        	?>
                        	<tr class="danger">
                        		<td colspan="4">Total Siswa</td>
                        		<td colspan="3">{{ $jumlahSiswaTotal }} orang</td>
                        	</tr>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('inline-script')
@endsection

@section('inline-style')
@stop