@extends('tu-user.base')

@section('title', 'Data Siswa Keluar')

@section('content')
	<h3 class="page-title">Data Siswa Keluar</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li><a href="{{ url('tu-user/students') }}">Siswa</a></li>
	    <li class="active"><span>Data Siswa Keluar</span></li>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-blue" data-widget='{"draggable": "false"}'>
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Data Siswa Keluar</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<form action="{{ url('tu-user/students/drop/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<!--<fieldset title="DATA PRIBADI">
								<legend>Rincian Data Pribadi Siswa </legend>-->
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Lengkap</label>
								<div class="col-sm-6">
									<input type="text" name="nama_lengkap" class="form-control" value="{{ $student->nama_lengkap }}" disabled>
									<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">NISN</label>
								<div class="col-sm-4">
									<input type="number" name="nisn" class="form-control" value="{{ $student->nisn }}" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-4">
									    <select class="form-control" name="jenis_kelamin" value="{{ $student->nama_lengkap }}" disabled>
										    <option>Laki-laki</option>
										    <option>Perempuan</option>
										</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tanggal Lahir</label>
								<div class="col-sm-4">
									<input type="text" name="tanggal_lahir" value="{{ $student->tanggal_lahir }}" class="form-control" disabled>
								</div>
								<em>yyyy-mm-dd</em>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Keluar Karena</label>
								<div class="col-sm-4">
									    <select class="form-control" name="keluar_karena" value="{{ old('keluar_karena') }}" required>
										    <option>Lulus</option>
										    <option>Mutasi</option>
										    <option>Dikeluarkan</option>
										    <option>Mengundurkan Diri</option>
										    <option>Putus Sekolah</option>
										    <option>Wafat</option>
										    <option>Hilang</option>
										    <option>Lainnya</option>
										</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tanggal Keluar</label>
								<div class="col-sm-4">
									<input type="text" name="tanggal_keluar" value="{{ date('Y-n-d') }}" class="form-control" required>
								</div>
								<em>yyyy-mm-dd</em>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alasan</label>
								<div class="col-sm-6">
									<input type="text" name="alasan" class="form-control" value="{{ old('alasan') }}" required>
								</div>
							</div>
							<input type="submit" class="finish btn-success btn" value="Submit" />
						</form>
					</div>
					<!-- ./End panel body -->
						<div class="panel-body" style="padding: 40px 16px;">
							
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
	$(function(){
		var dataPrice = parseInt($('#package-id').find(':selected').data('price'));
		var daysTotal = parseInt($('#count-days').val());
		var billTotal = dataPrice * daysTotal;

		$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));

		$('#package-id').change(function(){

			$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));
		});

		$('input[name="tanggal_lahir"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tanggal_keluar"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
	});

	Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	</script>

	<script type="text/javascript">
	$(function(){
		$('#data-siswa').stepy(
			{finishButton: true,
				titleClick: true,
				block: true,
				validate: true
			});

	    //Add Wizard Compability - see docs
	    $('.stepy-navigator').wrapInner('<div class="pull-right"></div>');

	    //Make Validation Compability - see docs
	    /*$('#buy-listing-wizard').validate({
	        errorClass: "help-block",
	        validClass: "help-block",
	        highlight: function(element, errorClass,validClass) {
	           $(element).closest('.form-group').addClass("has-error");
	        },
	        unhighlight: function(element, errorClass,validClass) {
	            $(element).closest('.form-group').removeClass("has-error");
	        }
	    });

	    $('body').on('change', '.select-package', function(){
			var selectedOpt = $(this).find(':selected');
			var selectVal = selectedOpt.val();

			if (selectVal !== '') {
				$(this).closest('.listing-entry').find('.listing-package-info').html(selectedOpt.data('info'));
			}
			$(this).parents('.form-group').next('.listing-package-info').addClass('active');
		});

		$('body').on('click', '.add-listing-entry', function(){
			var parent = $(this).parent();

			parent.find('.tooltip').remove();

			var cloned = parent.clone();

			$(this).css('display', 'none');

			cloned.find('.listing-package-info').text('').removeClass('active');
			cloned.find('.remove-listing-entry').css('display', 'block');
			parent.after(cloned);

			renameFieldEntry();
		});

		$('body').on('click', '.remove-listing-entry', function(){
			var prevEntry = $(this).parent().prev();
			var nextEntry = $(this).parent().next();
			$(this).parent().remove();

			// show the remove button and add button to the prev of parent
			// if any entry after
			if (nextEntry.hasClass('listing-entry') === false) {
				prevEntry.find('.add-listing-entry').css('display', 'block');
			}

			renameFieldEntry();
		});

		function renameFieldEntry() {
			var selectPackage = $('.select-package');

			for (var i = 0; i < selectPackage.length; i++) {
				selectPackage.eq(i).attr('name', 'listings[' + i + '][package_id]');
			}
		}*/
	});
	/**
	 * Number.prototype.format(n, x)
	 * 
	 * @param integer n: length of decimal
	 * @param integer x: length of sections
	 */
	Number.prototype.format = function(n, x) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
	};
	</script>
@endsection

@section('inline-style')
<style type="text/css">
.package-info-wrapper {
    display: block;
    margin-top: 10px;
    background: #f6f6f6;
    padding: 7px 10px;
    font-size: 12px;
    font-style: italic;
    color: #888;
}
.package-info-wrapper strong {
	color: #000;
}
</style>
@stop