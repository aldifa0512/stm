@extends('tu-user.base')

@section('title', 'Pembayaran')

@section('content')
	<?php
		$sme = $tp['semester'];
	?>
	<h3 class="page-title">Pembayaran</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('tu-user') }}">Dashboard</a></li>
		<?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('tu-user/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('tu-user/students/rombel') }}">Rombongan Belajar</a></li>
	    <li><a href="{{ url('tu-user/students/rombel/lihat/'. $student->kelas) }}">Kelas {{$rombel['nama']}}</a></li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
			<script type="text/javascript">
			function openInvoice() {
			    window.open("{{ url('tu-user/students/spp/invoice/'.$student->nisn.'/'.$tp['tp'].'/'.$tp['semester'].'/'.Session::get('no_kuitansi')) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=900,height=500");
			}
				openInvoice();
			</script>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-gray" data-widget='{"draggable": "false"}' >
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Pembayaran</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<form action="{{ url('tu-user/students/spp/bayar/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<center>
								<h2>Pembayaran TP  {{$tp['tp']}}/{{($tp['tp']+1)}}  </h2><br/>
							</center>
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<?php
										$size1 = 10/2;
										$size2 = 10/3;
										$style = 'width=15%';
										$style2 = 'width=35%';
									?>
									<td {{$style}}>Nama Lengkap</td>
									<td {{$style2}}><b>{{ $student->nama_lengkap }} / <u><a href="" onclick="openHistori()">Histori Pembayaran</a></u></b></td>
									<td {{$style}}>Kelas</td>
									<td {{$style2}}>{{ $rombel['nama'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>NISN</td>
									<td {{$style2}}>{{ $student->nisn }}</td>
									<td {{$style}}>Wali Kelas</td>
									<td {{$style2}}>{{ $rombel['pengajar'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>TP</td>
									<td {{$style2}}>{{ $tp['tp'] }}/{{ ($tp['tp']+1) }}</td>
									<td {{$style}}>Tanggal Bayar</td>
									<td {{$style2}}><div ">
										<input type="text" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-md" disabled></td>
										<input type="hidden" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-sm" required></td>
									</div>
								</tr>
							</table>
					
							<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}" >
							<input type="hidden" name="kelas" class="form-control" value="{{ $student->kelas }}" >
							<input type="hidden" size="50" name="tp" class="form-control" value="{{ $tp['tp'] }}" >

							<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<td>SPP <span class='masterbiaya'>(Rp. {{ number_format($Mspp->spp,2,',','.') }})</span></td>
										<td>
											<select class="form-control" name="spp" ng-model="spp" >
												<option value='none'>---</option>
												<?php 
													for($i= 4; $i < 16; $i++ ) {
														if($spps[$i] == 0 && $spps[$i] == null){
															echo "<option value='$i'> $bln[$i] </option>";
														}else{
															echo "<option disabled> $bln[$i] </option>";
														}
													}
												?>
										    </select>
										</td>
									</tr>
									<tr>
										<?php
										$tbpem = 0;
										for($i=4; $i<16; $i++){
								            $tbpem += $pembangunans[$i];
								        }//dd($pembangunans);
										?>
										<td>Pembangunan (<span style="color: red;">Rp. {{ number_format((($Mspp->pembangunan) - $tbpem),2,',','.') }}</span> / <span class='masterbiaya'>Rp. {{ number_format($Mspp->pembangunan,2,',','.') }} / Thn )</td>
										<td>
											<input class="form-control" type="number" name="pem" ng-model="pem" >
										</td>
									</tr>
									<tr>
										<td>Pramuka  <span class='masterbiaya'>(Rp. {{ number_format($Mspp->pramuka,2,',','.') }})</td>
										<td>
											<select class="form-control" name="pramuka" ng-model="pra">
												<option value='none'>---</option>
										    	<?php 
													for($i= 4; $i < 16; $i++ ) {
														if($pramukas[$i] == 0 && $pramukas[$i] == null){
															echo "<option value='$i'> $bln[$i] </option>";
														}else{
															echo "<option disabled> $bln[$i] </option>";
														}
													}
												?>
										    </select>
										</td>
									</tr>
									<tr>
										<td>Lain-lain  <span class='masterbiaya'>(Rp. {{ number_format($Mspp->lain_lain,2,',','.') }})</td>
										<td>
											<select class="form-control" name="lain_lain" ng-model="lain" >
												<option value='none'>---</option>
										    	<?php 
													for($i= 4; $i < 16; $i++ ) {
														if($lains[$i] == 0 ){
															echo "<option value='$i'> $bln[$i] </option>";
														}else{
															echo "<option disabled> $bln[$i] </option>";
														}
													}
												?>
										    </select>
										</td>
									</tr>
								</tbody>
							</table>

							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<td width="70%">
										<textarea name="note" placeholder="Note" class="form-control"></textarea>
									</td>
									<td width="30%">
										<input type="text" placeholder="" name="total" class="form-control input-lg" value="" disabled>
										<input type="hidden" placeholder="Rp." name="total" class="form-control" value="" >
									</td>
								</tr>
							</table>

							<center><input type="submit" style="" class="finish btn-success btn-block btn btn-lg" value="Bayar!" /></center>
					</div>

						<!-- Panel Footer -->
						<!--<div class="panel-footer">
							<div class="row">
								<div class="col-sm-10 col-sm-offset-2">
									<a href="{//{ url('tu-user/students') }}" class="btn-default btn">Batal</a>&nbsp;&nbsp;&nbsp;
									<button class="btn-primary btn">Simpan</button>
								</div>
							</div>
						</div>-->
						<!-- ./End Panel Footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
	@stop



@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script>
		var app = angular.module('SPP', [], function($interpolateProvider){
			$interpolateProvider.startSymbol('<%');
	        $interpolateProvider.endSymbol('%>');
		});
		app.controller('sppCtrl', function($scope) {
			// $scope.spp = 'none';
			// $scope.pem = 0;
			// $scope.pra = 'none';
			// $scope.lain = 'none';
			// $scope.mspp = {{ $Mspp->spp }};
			// $scope.mpem = {{ $Mspp->pembangunan }};
			// $scope.mpra = {{ $Mspp->pramuka }};
			// $scope.mlain = {{ $Mspp->lain_lain }};
			$scope.count = function(){
				// $scope.total = 0;
				// // $scope.tspp = none;
				// // $scope.tpem = none;
				// // $scope.tpra = none;
				// // $scope.tlain = none;
				// if($scope.spp == 'none' || $scope.spp == null){
				// 	$scope.tspp = 0;
				// 	console.log($scope.tspp);
				// }else{
				// 	$scope.tspp = {{ $Mspp->spp }};
				// }
				// if($scope.spp == 'none' || $scope.spp == null){
				// 	$scope.tspp = 0;
				// }else{
				// 	$scope.tpem = $scope.pem;
				// }
				// if($scope.pra == 'none' || $scope.spp == null){
				// 	$scope.tpra = 0;
				// }else{
				// 	$scope.tpra = {{ $Mspp->pramuka }};
				// }
				// if($scope.lain == 'none' || $scope.spp == null){
				// 	$scope.tlain = 0;
				// }else{
				// 	$scope.tlain = {{ $Mspp->lain_lain }};
				// }
				// $scope.total = $scope.tspp + $scope.tpem + $scope.tpra + $scope.tlain;
				// console.log($scope.total);
			}
		});
	</script>

	<script>
	function openHistori() {
	    window.open("{{ url('tu-user/students/spp/histori/'.$student->nisn) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=1200,height=500");
	}
	</script>
	<script type="text/javascript">
	Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	</script>


<script>
  $(document).ready(function () {
    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
      mySelect.find('option:selected').prop('disabled', true);
      mySelect.selectpicker('refresh');
    });

    $('#special2').on('click', function () {
      mySelect.find('option:disabled').prop('disabled', false);
      mySelect.selectpicker('refresh');
    });

    $('#basic2').selectpicker({
      liveSearch: true,
      maxOptions: 1
    });
  });
</script>
@endsection

@section('inline-style')
<style type="text/css">
	.package-info-wrapper {
	    display: block;
	    margin-top: 10px;
	    background: #f6f6f6;
	    padding: 7px 10px;
	    font-size: 12px;
	    font-style: italic;
	    color: #888;
	}
	.package-info-wrapper strong {
		color: #000;
	}
	.nominal{
		font-size: 20px !important;
		color: red;
		float: right;
	}
	.masterbiaya{
		color:blue;
	}
</style>
@stop