@extends('tu-user.base')

@section('title', 'Rombongan Belajar')

@section('content')
	<ol class="breadcrumb">
		<li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li><a href="{{ url('tu-user/students') }}">Siswa</a></li>
	    <li class="">Rombongan Belajar</li>
	</ol>
	<div class="container-fluid">
		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 style="color:blue;">Daftar Rombel</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body">
							<table id="listings-list" class="table table-striped table table-bordered">
								<thead>
									<tr class="danger">
										<th>No</th>
										<th>Nama Rombel</th>
										<th>Jurusan</th>
										<th>Pengajar</th>
										<th>Jumlah Siswa</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php
                                    	$no = 1;
                                        foreach ($rombels as $rombel){
								            echo "<tr>";
                                        	echo "<td>$no</td>";
								            echo "<td> $rombel->nama </td>";
											$jurusan = App\Models\Jurusan::where('id', $rombel->jurusan_id)->first()->toArray();//dd($jurusan);
											echo "<td>". $jurusan['nama_jurusan']. "</td>";
								            echo "<td> $rombel->pengajar </td>";
											$jml_siswa = App\Models\Student::where('kelas', $rombel->id)->where('status', 'aktif')->count();
											echo "<td>". $jml_siswa. " org</td>";
								    ?>
											<td>
												<a href="{{ url('tu-user/students/rombel/lihat', $rombel->id) }}" class="btn btn-primary-alt btn-sm" title="Lihat Anggota rombel ini"><i class="ti ti-pencil"></i>&nbsp;Lihat</a>
											</td>
											</tr>
									<?php    
                                        	$no++;
								        }
                                    ?>	
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop
