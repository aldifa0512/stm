@extends('tu-user.base')

@section('title', 'Cetak Laporan Keuangan')

@section('content')
	<ol class="breadcrumb">
	    <li class=""><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li class=""><a href="{{ url('tu-user/laporan/invoice') }}">Bukti Pembayaran</a></li>
	    <li>Cetak Laporan Keuangan</li>
	</ol>
	<div class="container-fluid">
		<div class="row">
			@if (Session::has('error'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if (Session::has('success'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-success">
						<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if ($errors->has())
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			<div class="col-md-12">
				<div class="panel panel-blue" >
					<div class="panel-heading">
						<h2>Form Cetak Laporan Keuangan</h2>
					</div>
					<div class="panel-body">
						<p>Silahkan pilih metode penyusunan dan laporan keuangan yang ingin dicetak.</p>
						<form action="{{ url('tu-user/laporan/invoice/cetak') }}" method="post" class="form-horizontal">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<table class="table table-striped table-bordered" cellspacing="0" width="100%">
									<tr>
										<td width="6px">1. </td>
										<td><input type="radio" name="metode" ng-model="metode.harian" value="harian" ><b> Harian </b></td>
										<td>
										<div class="col-xs-3">
										<input type="text" placeholder="yyyy-mm-dd" name="tanggal_harian" value="<?php echo date('Y-m-d') ?>" class="form-control input-md">
										</div>
										</td>
									</tr>
									<tr>
										<td width="6px">2. </td>
										<td><input type="radio" name="metode" ng-model="metode.rentang_hari" value="rentang_hari"><b> Rentang Hari</b></td>
										<td>
										<div class="col-xs-3">
										<input type="text"  " placeholder="yyyy-mm-dd" name="tanggal_rentang_dari" value="<?php echo date('Y-m-d') ?>" class="form-control input-md">
										</div>
										<div class="col-xs-1">
										sampai
										</div>
										<div class="col-xs-3">
										<input type="text" placeholder="yyyy-mm-dd" name="tanggal_rentang_sampai" value="<?php echo date('Y-m-d') ?>" class="form-control input-md">
										</div>
										</td>
									</tr>
									<tr>
										<td width="6px">3. </td>
										<td><input type="radio" name="metode" ng-model="metode.bulanan" value="bulanan"><b> Bulanan</b></td>
										<td>
										<div class="col-xs-3">
										<input type="text" placeholder="mm" name="tanggal_bulanan" value="<?php echo date('n') ?>" class="form-control input-md">
										</div>
										</td>
									</tr>
									<tr>
										<td width="6px">4. </td>
										<td><input type="radio" name="metode" ng-model="metode.tahunan" value="tahunan"><b> Tahunan</b></td>
										<td>
										<div class="col-xs-3">
										<input type="text" placeholder="yyyy" name="tanggal_tahunan" value="<?php echo date('Y') ?>" class="form-control input-md">
										</div></td>
									</tr>
								</table>
							<input type="submit" class="finish btn-success btn" value="Cetak" />
						</form>
					</div>
					<!-- ./End panel body -->
				</div>
			</div>
		</div>
	</div>
@endsection

@section('page-styles')
 <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@endsection

@section('page-scripts')
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@endsection

@section('inline-script')
<script type="text/javascript">
	$(function(){
		$('input[name="tanggal_harian"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tanggal_rentang_dari"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tanggal_rentang_sampai"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tanggal_bulanan"]').datepicker({format: 'm', autoclose:true,  viewMode: "months", minViewMode: "months" });
		$('input[name="tanggal_tahunan"]').datepicker({format: 'yyyy', autoclose:true,  viewMode: "years", minViewMode: "years" });
		
	});
</script>
@endsection

@section('inline-style')
</style>
@stop