@extends('komite.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
		<li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li><?php echo $kelas ?></li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif				
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Daftar Siswa Kelas <?php echo $kelas ?></h2>
							<a style="float:right; margin:10px;" href="{{ url('komite/students/rombel/absen/'. $kelas) }}" class="secondary"><u>Cetak Absen</u></a>
						</div>							

						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-hover">
								<thead>
									<tr>
										<th width=""><a href="">Nama Lengkap</a></th>
										<th width=""><a href="">Foto</a></th>
										<th width=""><a href="">NISN</a></th>
										<th width=""><a href="">Tanggal Lahir</a></th>
										<th width=""><a href="">Nama Ibu</a></th>
										<th width=""><a href="">Jenis Pendaftaran</a></th>
										<th width=""><a href="">Jurusan</a></th>
										<th width=""><a href="">Kelas</a></th>
										<th width="120"><a href="">Action</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($students as $student)
									<?php 
										$assets = json_decode($student->pas_foto);//var_dump($assets);die();
										$filename = substr($assets[0], strrpos($assets[0], '/') + 1);
										$img_entry = str_replace($filename, 'thumb-admin-'.$filename, $assets[0]);
									?>
									<tr>
										<td class="text-center">{{ $student->nama_lengkap }}</td>
										<td class="text-center">
											<?php if ($img_entry != ''): ?>
												<img class="img-thumbnail" src="{{ asset($img_entry) }}" width="70" height="70">
											<?php endif ?>
										</td>
										<td><a href="{{ url('tu-user/students/edit', $student->id) }}">{{ $student->nisn }}</a></td>
										<td>
											<?php echo $student->tanggal_lahir ?>
											
										</td>
										<td>{{ $student->nama_ibu }}</td>
										<td>{{ $student->jenis_pendaftaran }}</td>
										<td>{{ $student->jurusan }}</td>
										<td>
											<?php echo $student->kelas ?>
										</td>
										<td class="row">
	                                        <a href="{{ url('tu-user/students/edit', $student->id) }}" class="btn btn-primary-alt btn-sm" title="Edit this student"><i class="ti ti-pencil"></i>&nbsp;Edit</a>
	                                        <a class="btn btn-danger btn-sm tooltips" href="<?php echo url('tu-user/students/drop', $student->id) ?>" role="button" title="Delete this student"><i class="ti ti-trash"></i></a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop