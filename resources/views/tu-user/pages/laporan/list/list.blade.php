@extends('komite.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li>Kelas {{$rombel['nama']}}</li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<?php
					if (isset($rombelM) && $rombelM == true)
						echo "";
					else{
					?>
					<div class="row">
						<div class="action-menu col-md-12">
						<form action="{{ url('komite/students/filter') }}" method="post" id="buy-listing-wizard" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div style="padding-bottom:5px;">
									<label class="col-sm-1">NISN</label>
									<div class="row col-sm-3">
										<input type="number" name="nisn" placeholder="NISN" class="form-control" value="{{ old('nisn') }}">
									</div>
									<button style="margin-left:15px;" class="btn-primary btn">Cari !</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					}
					?>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php
								if(isset($recap) && $recap == true)
									echo "<h2 style='color:blue;'>Daftar Siswa Keseluruhan (Aktif dan Tidak Aktif)</h2>";
								elseif(isset($pasif) && $pasif == true)
									echo "<h2 style='color:blue;'>Daftar Siswa Tidak Aktif</h2>";
								elseif(isset($rombelM) && $rombelM == true){
									echo "<h2 style='color:blue;'>Daftar Siswa Kelas ". $rombel['nama'] ." </h2>";
							?>
									<a style="float:right; " href="{{ url('komite/students/spp/rombel/'.$kelas.'/lap') }}" class=""><u>Cetak Laporan</u></a>
							<?php
								}
								else
									echo "<h2 style='color:blue;'>Daftar Siswa Aktif</h2>";
							?>
							
						</div>

						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-bordered">
								<thead>
									<tr class="success">
										<th width=""><a href="">Nama Lengkap</a></th>
										<th width=""><a href="">Foto</a></th>
										<th width=""><a href="">NISN</a></th>
										<th width="90"><a href="">Tanggal Lahir</a></th>
										<?php
											if(isset($pasif))
												echo "<th><a href=''>Alasan Keluar</a></th>";
											else
												echo "<th><a href=''>Jurusan</a></th>";
										?>
										<th width="80"><a href="">Kelas</a></th>
										<th width="150"><a href="">Tunggakan</a></th>
										<th width="150"><a href="">Action</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($students as $student)
									<?php 
										$assets = json_decode($student->pas_foto);//var_dump($assets);die();
										$filename = substr($assets[0], strrpos($assets[0], '/') + 1);
										$img_entry = str_replace($filename, 'thumb-admin-'.$filename, $assets[0]);
									?>
									<?php
										if($student->status == "pasif"){
											$disableBtn = "disabled";
											$class = "class='danger'";
										}else{
											$disableBtn = "";
											$class = "class=''";
										}
									?>
									<tr <?php echo $class ?>>
										<td class="">{{ $student->nama_lengkap }}</td>
										<td class="text-center">
											<?php if ($img_entry != ''): ?>
												<img class="img-thumbnail" src="{{ asset($img_entry) }}" width="70" height="70">
											<?php endif ?>
										</td>
										<td><a href="{{ url('komite/spp/bayar', $student->nisn) }}">{{ $student->nisn }}</a></td>
										<td>
											<?php echo $student->tanggal_lahir ?>
											
										</td>
										<td>
											<?php
												if(isset($pasif)){
													$alasan = App\Models\DropOut::where('nisn', $student->nisn)->first()->toArray();
													echo $alasan['keluar_karena'];
												}else{
													$jurusan = App\Models\Jurusan::where('id', $student->jurusan)->first()->toArray();//dd($jurusan);
													echo $jurusan['nama_jurusan'];
												}
											?>
										</td>
										<td>
											<?php 
												$rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();//dd($jurusan);
												echo $rombel['nama'];
											?>
										</td>
										<td>Rp. </td>
										<td class="row">
	                                        <a href="{{ url('komite/students/spp/bayar', $student->nisn) }}" class="btn btn-primary-alt btn-sm" title="Lakukan Pembayaran" {{$disableBtn}}><i class="ti ti-pencil"></i>&nbsp;Bayar</a>
	                                        <a href="{{ url('komite/students/spp/histori/'.$student->nisn) }}" target="_blank" class="btn btn-primary-alt btn-sm" title="Lihat Histori Pembayaran"><i class="ti ti-eye"></i>&nbsp;Lihat</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							<?php
								if(isset($render) && $render == false)
									echo "";
								else
									echo $students->render();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
	
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop