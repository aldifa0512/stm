@extends('tu-user.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li class="active"><span>Siswa</span></li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<div class="row">
						<div class="action-menu col-md-12">
						<p><b>Cari Berdasarkan :</b></p>
						<form action="{{ url('tu-user/students/filter') }}" method="post" id="buy-listing-wizard" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="row" style="padding-bottom:5px;">
									<label class="col-sm-2">Tahun Masuk</label>
									<div class="col-sm-2">
										    <select class="form-control" name="thn_masuk" value="{{ old('thn_masuk') }}">
											    <option>none</option>
											    <option>2015</option>
											    <option>2016</option>
											</select>
									</div>
								</div>
								<div class="row" style="padding-bottom:5px;">
									<label class="col-sm-2">NISN</label>
									<div class="col-sm-2">
										<input type="number" name="nisn" placeholder="NISN" class="form-control" value="{{ old('nisn') }}">
									</div>
								</div>
								<div class="row" style="padding-bottom:5px;">
									<label class="col-sm-2">Nama</label>
									<div class="col-sm-6">
										<input type="text" name="nama" placeholder="Masukkan nama" class="form-control" value="{{ old('nama') }}">
									</div>
								</div>
								<div class="row" style="padding-bottom:5px;">
									<label class="col-sm-2">Jurusan</label>
									<div class="col-sm-6">
										    <select class="form-control" name="jurusan" required>
										    <option value='select'>none</option>
										    <?php 
										    	$jurusans = App\Models\Jurusan::all();
										    	foreach ($jurusans as $jurusan) {
										    		echo "<option value='$jurusan->id_jurusan'>$jurusan->nama_jurusan</option>";
										    	}
										    ?>
											</select>
									</div>
								</div>
								<div class="col-sm-8">
									<button style="float:right;" class="btn-primary btn">Cari !</button>
								</div>
							</form>
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Daftar Siswa Aktif</h2>
							<div class="panel-ctrls">
								{!! $students->render() !!}
							</div>
						</div>

						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-hover">
								<thead>
									<tr>
										<th width=""><a href="{{ url('tu-user/students/orderby/nama_lengkap/ASC') }}">Nama Lengkap</a></th>
										<th width=""><a href="">Foto</a></th>
										<th width=""><a href="{{ url('tu-user/students/orderby/nisn/ASC') }}">NISN</a></th>
										<th width=""><a href="{{ url('tu-user/students/orderby/tanggal_lahir/ASC') }}">Tanggal Lahir</a></th>
										<th width=""><a href="{{ url('tu-user/students/orderby/nama_ibu/ASC') }}">Nama Ibu</a></th>
										<th width=""><a href="{{ url('tu-user/students/orderby/jenis_pendaftaran/ASC') }}">Jenis Pendaftaran</a></th>
										<th width=""><a href="{{ url('tu-user/students/orderby/jurusan/ASC') }}">Jurusan</a></th>
										<th width=""><a href="{{ url('tu-user/students/orderby/kelas/ASC') }}">Kelas</a></th>
										<th width=""><a href="">Action</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($students as $student)
									<?php 
										$assets = json_decode($student->pas_foto);//var_dump($assets);die();
										$filename = substr($assets[0], strrpos($assets[0], '/') + 1);
										$img_entry = str_replace($filename, 'thumb-admin-'.$filename, $assets[0]);
									?>
									<tr>
										<td class="text-center">{{ $student->nama_lengkap }}</td>
										<td class="text-center">
											<?php// if ($img_entry != ''): ?>
												<img class="img-thumbnail" src="{{ asset($img_entry) }}" width="70" height="70">
											<?php// endif ?>
										</td>
										<td><a href="{{ url('tu-user/students/edit', $student->id) }}">{{ $student->nisn }}</a></td>
										<td>
											<?php echo $student->tanggal_lahir ?>
											
										</td>
										<td>{{ $student->nama_ibu }}</td>
										<td>{{ $student->jenis_pendaftaran }}</td>
										<td>
											<?php 
												$jurusan = App\Models\Jurusan::where("id_jurusan", $student->jurusan)->select("nama_jurusan");var_dump($jurusan);die();
												echo $jurusan;
											?>												
										</td>
										<td>
											<?php echo $student->kelas ?>
										</td>
										<td class="row">
	                                        <a href="{{ url('tu-user/students/edit', $student->id) }}" class="btn btn-primary-alt btn-sm" title="Edit this student"><i class="ti ti-pencil"></i>&nbsp;Edit</a>
	                                        <a class="btn btn-danger btn-sm tooltips" href="<?php echo url('tu-user/students/drop', $student->id) ?>" role="button" title="Delete this student"><i class="ti ti-trash"></i></a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							{!! $students->render() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop