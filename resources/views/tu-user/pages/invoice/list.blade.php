@extends('tu-user.base')

@section('title', 'Bukti Pembayaran')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li>Kelas {{$rombel['nama']}}</li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Bukti Pembayaran</li>";
	    ?>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<?php
					if (isset($rombelM) && $rombelM == true)
						echo "";
					else{
					?>
					<div class="row">
						<div class="action-menu col-md-12">
						<form action="{{ url('tu-user/laporan/filter') }}" method="post" id="buy-listing-wizard" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div style="padding-bottom:5px;">
									<div class="row col-sm-3">
										<input type="number" name="no_kuitansi" placeholder="No. Kuitansi" class="form-control" value="{{ old('no_kuitansi') }}">
									</div>
									<button style="margin-left:15px;" class="btn-primary btn">Cari !</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					}
					?>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php
								if(isset($recap) && $recap == true)
									echo "<h2 style='color:blue;'>Daftar Siswa Keseluruhan (Aktif dan Tidak Aktif)</h2>";
								elseif(isset($pasif) && $pasif == true)
									echo "<h2 style='color:blue;'>Daftar Siswa Tidak Aktif</h2>";
								elseif(isset($rombelM) && $rombelM == true){
									echo "<h2 style='color:blue;'>Daftar Siswa Kelas ". $rombel['nama'] ." </h2>";
							?>
									<a style="float:right; " href="{{ url('komite/spp/rombel/'.$kelas.'/lap') }}" class=""><u>Cetak Laporan</u></a>
							<?php
								}
								else
									echo "<h2 style='color:blue;'>Bukti Pembayaran</h2>";
							?>
							<a style="float:right; " href="{{ url('tu-user/laporan/invoice/cetak') }}" class=""><u>Cetak Laporan</u></a>
							
						</div>

						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-bordered">
								<thead>
									<tr class="info">
										<th width=""><a href="">No Kuitansi</a></th>
										<th width=""><a href="">Nama Siswa</a></th>
										<th width=""><a href="">NISN</a></th>
										<th width=""><a href="">Spp</a></th>
										<th width=""><a href="">Pembangunan</a></th>
										<th width=""><a href="">Pramuka</a></th>
										<th width=""><a href="">Lain-lain</a></th>
										<th width=""><a href="">Note</a></th>
										<th width="100"><a href="">Action</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($invoices as $invoice)
									
									<tr>
										<td class="">{{ $invoice->no_kwitansi }}</td>
										<?php $student = App\Models\Student::where('nisn', $invoice->nisn)->select('nama_lengkap')->first(); ?>
										<td class="">{{ $student->nama_lengkap }}</td>
										<td class="">{{ str_pad($invoice->nisn, 10, '0', STR_PAD_LEFT) }}</td>
										<td class="">Rp. {{ number_format($invoice->spp,2,',','.') }}</td>
										<td class="">Rp. {{ number_format($invoice->pembangunan,2,',','.') }}</td>
										<td class="">Rp. {{ number_format($invoice->pramuka,2,',','.') }}</td>
										<td class="">Rp. {{ number_format($invoice->lain_lain,2,',','.') }}</td>
										<td class="">{{ $invoice->note }}</td>
										
										<td class="row">
	                                        <a href="{{ url('tu-user/laporan/invoice/'.$invoice->nisn. '/' .$invoice->tp. '/' .$invoice->semester. '/' .$invoice->no_kwitansi) }}" target="_blank" class="btn btn-primary-alt btn-sm" title="Lihat Bukti Pembayaran"><i class="ti ti-eye"></i>&nbsp;Lihat</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							<?php
								if(isset($render) && $render == false)
									echo "";
								else
									echo $invoices->render();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
	
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop