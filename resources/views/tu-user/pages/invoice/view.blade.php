<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bukti Pembayaran - SMKN 2 PAYAKUMBUH</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="icon" href="{{asset('assets/backend/img/favicon.ico')}}" type="image/x-icon">
</head>
<body>
<?php
$bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
?>
    <div bgcolor='#e4e4e4' text='#ff6633' link='#666666' vlink='#666666' alink='#ff6633' style='margin:0;font-family:Arial,Helvetica,sans-serif;border-bottom:1'>
    <table background='' bgcolor='#e4e4e4' width='100%' style='padding:20px 0 20px 0' cellspacing='0' border='0' align='center' cellpadding='0'>
        <tbody>
            <tr>
                <td>
                    <table width='620' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF' style='border-radius: 5px;'>
                        <tbody>
                            <tr>
                                <td>
                                    <table width='620' border='0' cellspacing='0' cellpadding='0'>
                                        <tbody>
                                            <tr>
                                                <td align='left' valign='top' style='padding:0px 5px 0px 5px' >
                                                    <table height='20px' width='100%' border='0' cellpadding='0' cellspacing='0'>
                                                        <tbody>
                                                            <tr>
                                                                <td height='10px' valign='top' style='color:#404041;font-size:13px;padding:5px 5px 0px 20px'><br/>
                                                                        <span style=' font-family: arial,sans-serif; font-weight: 200;'><b>SMKN 2 PAYAKUMBUH</b></span><br/>
                                                                        Jl. Soekarno - Hatta, Gang Anggrek 1<br/> 
                                                                        Bulakan Balai Kandi Payakumbuh Barat<br/>
                                                                        Payakumbuh, Sumatera Barat 26225
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                
                                                <td align='left' valign='top' style='padding:0px 5px 0px 5px'>
                                                    <table height='' width='100%' border='0' cellpadding='3' cellspacing='3' >
                                                        <tbody>
                                                            <tr>
                                                                <td height='16' valign='top' style='color:#404041;font-size:13px;padding:15px 5px 0px 5px'>
                                                                    <table style="float:right;">
                                                                        <tr>
                                                                            <td><strong>No. Kuitansi</strong></td>
                                                                            <td>: {{ $histori->no_kwitansi }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Tanggal</strong></td>
                                                                            <td>: {{ $histori->tgl_bayar }}</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td valign='top' style='color:#404041;line-height:16px;padding:10px 20px 0px 20px'>
                                    <section style='position: relative;clear: both;margin: 5px 0;height: 1px;border-top: 1px solid #cbcbcb;margin-bottom: 0px;margin-top: 10px;text-align: center;'>
                                            <h4 align='center' style='margin-top: -12px;background-color: #FFF;clear: both;width: 180px;margin-right: auto;margin-left: auto;padding-left: 15px;padding-right: 15px; font-family: arial,sans-serif;'>
                                                <span>BUKTI PEMBAYARAN</span>
                                            </h4>
                                    </section>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width='620' border='0' cellspacing='0' cellpadding='0'>
                                        <tbody>
                                            <tr>
                                                <td align='left' valign='top' style='padding:0px 5px 0px 5px' >
                                                    <table height='20px' width='100%' border='0' cellpadding='0' cellspacing='0'>
                                                        <tbody>
                                                            <tr>
                                                                <td height='10px' valign='top' style='color:#404041;font-size:13px;padding:5px 5px 0px 20px'><br/>
                                                                        <table>
                                                                            <tr>
                                                                                <td><strong>Nama siswa</strong></td>
                                                                                <td>: {{ $histori->nama}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>NISN</strong></td>
                                                                                <td>: {{ str_pad($histori->nisn, 10, '0', STR_PAD_LEFT) }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>Kelas</strong></td>
                                                                                <td>: {{ $histori->kelas }}</td>
                                                                            </tr>
                                                                        </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                
                                                <td align='left' valign='top' style='padding:0px 5px 0px 5px'>
                                                    <table height='' width='100%' border='0' cellpadding='3' cellspacing='3' >
                                                        <tbody>
                                                            <tr>
                                                                <td height='16' valign='top' style='color:#404041;font-size:13px;padding:15px 5px 0px 5px'>
                                                                    <table>
                                                                        <tr>
                                                                            <td><strong>TP</strong></td>
                                                                            <td>: {{ $histori->tp}}/{{ $histori->tp+1}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><strong>Semester</strong></td>
                                                                            <td>: {{ $histori->semester}} <?php if($histori->semester == 1) echo '(satu)'; else echo '(dua)'; ?></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        
                            <tr>
                                <td style='color:#404041;font-size:12px;line-height:16px;padding:10px 16px 20px 18px'>
                                    <table width='100%' border='0' cellpadding='0' cellspacing='0' style='border-radius:5px; padding-bottom:10px; border:solid 1px #e5e5e5'>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table width='570' border='0' cellspacing='0' cellpadding='0'>
                                                        <tbody>
                                                            <tr style="border-bottom:3px;">
                                                                <td width='15'> 
                                                                </td>
                                                                <td width='85' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>           
                                                                    <strong>Tanggungan</strong>
                                                                </td>
                                                                <td width='85' align='right' style='text-align:left;color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <strong>Bulan Dibayar</strong>
                                                                </td>
                                                                <td width='50' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <strong>Amount</strong>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <strong>Total</strong>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width='15'> 
                                                                </td>
                                                                <td width='85' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>           
                                                                    <strong>SPP</strong>
                                                                </td>
                                                                <td width='85' align='right' style='text-align:left; color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php 
                                                                    $bln_dibayar = json_decode($histori->bln_dibayar);
                                                                    $bln_dibayar = $bln_dibayar[0] == 0 ? '-' : $bln[$bln_dibayar[0]];
                                                                    echo "<strong> $bln_dibayar </strong>";
                                                                    ?>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->spp != 0) echo "<strong>Rp. " . number_format($histori->spp,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                                
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->spp != 0) echo "<strong>Rp. ". number_format($histori->spp,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                            </tr><tr>
                                                                <td width='15'> 
                                                                </td>
                                                                <td width='85' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>           
                                                                    <strong>Pembangunan</strong>
                                                                </td>
                                                                <td width='85' align='right' style='text-align:left; color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php //dd(json_decode($histori->bln_dibayar));
                                                                    $bln_dibayar = json_decode($histori->bln_dibayar);
                                                                    $bln_dibayar = $bln_dibayar[1] == 0 ? '-' : $bln[$bln_dibayar[1]];
                                                                    echo "<strong> $bln_dibayar </strong>";
                                                                    ?>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->pembangunan != 0)echo "<strong>Rp. " . number_format($histori->pembangunan,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->pembangunan != 0) echo "<strong>Rp. ". number_format($histori->pembangunan,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                            </tr><tr>
                                                                <td width='15'> 
                                                                </td>
                                                                <td width='85' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>           
                                                                    <strong>Pramuka</strong>
                                                                </td>
                                                                <td width='85' align='right' style='text-align:left; color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php 
                                                                    $bln_dibayar = json_decode($histori->bln_dibayar);
                                                                    $bln_dibayar = $bln_dibayar[2] == 0 ? '-' : $bln[$bln_dibayar[2]];
                                                                    echo "<strong> $bln_dibayar </strong>";
                                                                    ?>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->pramuka != 0) echo "<strong>Rp. " . number_format($histori->pramuka,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->pramuka != 0) echo "<strong>Rp. ". number_format($histori->pramuka,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width='15'> 
                                                                </td>
                                                                <td width='85' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>           
                                                                    <strong>Lain-lain</strong>
                                                                </td>
                                                                <td width='85' align='right' style='text-align:left; color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php 
                                                                    $bln_dibayar = json_decode($histori->bln_dibayar);
                                                                    $bln_dibayar = $bln_dibayar[3] == 0 ? '-' : $bln[$bln_dibayar[3]];
                                                                    echo "<strong> $bln_dibayar </strong>";
                                                                    ?>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->lain_lain != 0) echo "<strong>Rp. " . number_format($histori->lain_lain,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <?php if($histori->lain_lain != 0) echo "<strong>Rp. ". number_format($histori->lain_lain,2,',','.') . "</strong>"; else echo "<strong> - </strong>"; ?>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width='15'> 
                                                                </td>
                                                                <td width='85' align='left' valign='top' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>           
                                                                    <strong>TOTAL</strong>
                                                                </td>
                                                                <td width='85' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <strong></strong>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <strong></strong>
                                                                </td>
                                                                <td width='60' align='right' style='color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5'>
                                                                    <strong>Rp. {{ number_format($histori->nominal,2,',','.') }}</strong>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width='620' border='0' cellspacing='0' cellpadding='0'>
                                        <tbody>
                                            <tr>
                                                <td align='left' valign='top' style='padding:0px 5px 0px 5px' >
                                                    <table height='20px' width='100%' border='0' cellpadding='0' cellspacing='0'>
                                                        <tbody>
                                                            <tr>
                                                                <td height='10px' valign='top' style='color:#404041;font-size:13px;padding:0px 5px 0px 20px'>
                                                                    <span style="padding-top:10px;">Simpan kuitansi ini sebagai tanda bukti pembayaran yang sah.</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                                    
                            <tr>
                                <td>
                                    <table width='620' border='0' cellspacing='0' cellpadding='0'>
                                        <tbody>
                                            <tr>
                                                <td align='left' valign='top' style='padding:0px 5px 0px 5px' >
                                                    <table height='20px' width='100%' border='0' cellpadding='0' cellspacing='0'>
                                                        <tbody>
                                                            <tr>
                                                                <td height='10px' valign='top' style='color:#404041;font-size:13px;padding:5px 5px 0px 20px'><br/>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                
                                                <td align='left' valign='top' style='padding:0px 5px 10px 5px'>
                                                    <table height='' width='100%' border='0' cellpadding='3' cellspacing='3' >
                                                        <tbody>
                                                            <tr>
                                                                <td height='16' valign='top' style='color:#404041;font-size:13px;padding:15px 5px 0px 5px'>
                                                                    <span style="float:right;">
                                                                    <b>Diterima oleh</b><br/><br/><br/>
                                                                    {{ $histori->oleh }}
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>
    
                            

    <!-- Load site level scripts -->

    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

    <!-- Load jQuery -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/jquery-1.10.2.min.js') }}"></script>
    <!-- Load jQueryUI -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/jqueryui-1.10.3.min.js') }}"></script>
    <!-- Load Bootstrap -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script>
    <!-- Load Enquire -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/enquire.min.js') }}"></script>

    <!-- Load Velocity for Animated Content -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.ui.min.js') }}"></script>

    <!-- Wijet -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/wijets/wijets.js') }}"></script>

    <!-- Code Prettifier  -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/codeprettifier/prettify.js') }}"></script>
    <!-- Swith/Toggle Button -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-switch/bootstrap-switch.js') }}"></script>

    <!-- Bootstrap Tabdrop -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}"></script>

    <!-- iCheck -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/iCheck/icheck.min.js') }}"></script>

    <!-- nano scroller -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/nanoScroller/js/jquery.nanoscroller.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/backend/js/application.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/demo/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/demo/demo-switcher.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/backend/js/angular.min.js') }}"></script>
    
    <!-- End loading site level scripts -->
    @yield('page-scripts')

    @yield('inline-script')
</body>
</html>
