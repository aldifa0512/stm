@extends('komite.base')

@section('title', 'Penyesuaian Pembayaran')

@section('content')
	<?php
		$sme = $tp['semester'];
	?>
	<h3 class="page-title">Penyesuaian Pembayaran</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
		<?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li><a href="{{ url('komite/students/rombel/lihat/'. $student->kelas) }}">Kelas {{$rombel['nama']}}</a></li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
			<script type="text/javascript">
			function openInvoice() {
			    window.open("{{ url('komite/spp/invoice/'.$student->nisn.'/'.$tp['tp'].'/'.$tp['semester'].'/'.Session::get('no_kuitansi')) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=900,height=500");
			}
				openInvoice();
			</script>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-gray" data-widget='{"draggable": "false"}' ng-app="SPP" ng-controller="sppCtrl">
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Penyesuaian Pembayaran</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<form action="{{ url('komite/spp/bayar/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<center>
								<h2>Pembayaran Semester {{$tp['semester']}} TP  {{$tp['tp']}}/{{($tp['tp']+1)}}  </h2><br/>
							</center>
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<?php
										$size1 = 10/2;
										$size2 = 10/3;
										$style = 'width=15%';
										$style2 = 'width=35%';
									?>
									<td {{$style}}>Nama Lengkap</td>
									<td {{$style2}}><b>{{ $student->nama_lengkap }} / <u><a href="" onclick="openHistori()">Histori Pembayaran</a></u></b></td>
									<td {{$style}}>Kelas</td>
									<td {{$style2}}>{{ $rombel['nama'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>NISN</td>
									<td {{$style2}}>{{ $student->nisn }}</td>
									<td {{$style}}>Wali Kelas</td>
									<td {{$style2}}>{{ $rombel['pengajar'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>TP</td>
									<td {{$style2}}>{{ $tp['tp'] }}/{{ ($tp['tp']+1) }}</td>
									<td {{$style}}>Tanggal Bayar</td>
									<td {{$style2}}><div ">
										<input type="text" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="{{ old('tanggal_bayar') }}" class="form-control input-sm" required></td>
									</div>
								</tr>
							</table>
					
							<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}" >
							<input type="hidden" name="kelas" class="form-control" value="{{ $student->kelas }}" >
							<input type="hidden" size="50" name="tp" class="form-control" value="{{ $tp['tp'] }}" >

							<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="danger">
										<th></th>
										<?php 
											if($sme == 1){
										?>
										<th width="<?php echo (100/6) ?>%">Jan</th>
										<th width="<?php echo (100/6) ?>%">Feb</th>
										<th width="<?php echo (100/6) ?>%">Mar</th>
										<th width="<?php echo (100/6) ?>%">Apr</th>
										<th width="<?php echo (100/6) ?>%">Mei</th>
										<th width="<?php echo (100/6) ?>%">Jun</th>
										</tr>
										<?php
											}else{
										?>
										<th width="<?php echo (100/6) ?>%">Jul</th>
										<th width="<?php echo (100/6) ?>%">Agu</th>
										<th width="<?php echo (100/6) ?>%">Sep</th>
										<th width="<?php echo (100/6) ?>%">Okt</th>
										<th width="<?php echo (100/6) ?>%">Nov</th>
										<th width="<?php echo (100/6) ?>%">Des</th>
										<?php 
											}
										?>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="success">SPP<br/><span class='masterbiaya'>Rp. {{ $Mspp->spp }}</span></td>
										<?php 
											if($sme == 1){
												for($i= 4; $i < 10; $i++ ) {
													if($spps[$i] == 0){
														echo "<td><input type='number' name='spp[$i]' class='form-control' value='".old('spp')[$i]."' ng-click='spp".$i."'()'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $spps[$i] </span></td>";
														echo "<input type='hidden' name='spp[$i]' value='p'>";
													}
												}
											}else{
												for($i= 10; $i < 16; $i++ ) {
													if($spps[$i] == ""){
														echo "<td><input type='number' name='spp[$i]' class='form-control' value='".old('spp')[$i]."' ng-click='spp".$i."'()'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $spps[$i] </span></td>";
														echo "<input type='hidden' name='spp[$i]' value='p'>";
													}
												}
											}
										?>
									</tr>
									<tr>
										<td class="success">Pembangunan<br/><span class='masterbiaya'>Rp. {{ $Mspp->pembangunan }}</span></td>
										<?php
											if($sme == 1){
												for($i= 4; $i < 10; $i++ ) {
													if($pembangunans[$i] == 0){
														echo "<td><input type='number' name='pem[$i]' value='".old('pem')[$i]."' class='form-control'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $pembangunans[$i] </span></td>";
														echo "<input type='hidden' name='pem[$i]' value='p'>";
													}
												}
											}else{
												for($i= 10; $i < 16; $i++ ) {
													if($pembangunans[$i] == ""){
														echo "<td><input type='number' name='pem[$i]' value='".old('pem')[$i]."' class='form-control'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $pembangunans[$i] </span></td>";
														echo "<input type='hidden' name='pem[$i]' value='p'>";
													}
												}
											}
										?>
									</tr>
									<tr>
										<td class="success">Pramuka<br/><span class='masterbiaya'>Rp. {{ $Mspp->pramuka }}</span></td>
										<?php
											if($sme == 1){
												for($i= 4; $i < 10; $i++ ) {
													if($pramukas[$i] == 0){
														echo "<td><input type='number' name='pra[$i]' value='".old('pra')[$i]."' class='form-control'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $pramukas[$i] </span></td>";
														echo "<input type='hidden' name='pra[$i]' value='p'>";
													}
												}
											}else{
												for($i= 10; $i < 16; $i++ ) {
													if($pramukas[$i] == ""){
														echo "<td><input type='number' name='pra[$i]' value='".old('pra')[$i]."' class='form-control'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $pramukas[$i] </span></td>";
														echo "<input type='hidden' name='pra[$i]' value='p'>";
													}
												}
											}
										?>
									</tr>
									<tr>
										<td class="success">Lain-lain<br/><span class='masterbiaya'>Rp. {{ $Mspp->lain_lain }}</span></td>
										<?php
											if($sme == 1){
												for($i= 4; $i < 10; $i++ ) {
													if($lains[$i] == 0){
														echo "<td><input type='number' name='lain[$i]' value='".old('lain')[$i]."' class='form-control'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $lains[$i] </span></td>";
														echo "<input type='hidden' name='lain[$i]' value='p'>";
													}
												}
											}else{
												for($i= 10; $i < 16; $i++ ) {
													if($lains[$i] == ""){
														echo "<td><input type='number' name='lain[$i]' value='".old('lain')[$i]."' lass='form-control'></td>";
													}
													else{ 	
														echo "<td><span class='nominal'>Rp. $lains[$i] </span></td>";
														echo "<input type='hidden' name='lain[$i]' value='p'>";
													}
												}
											}
										?>
									</tr>
									<tr>
										<td class="success">Total</td>
										<?php
											for ($i=0; $i < 6 ; $i++) { 
												echo "<td><input type='text' placeholder='Rp. 'name='total' class='form-control' value='' disabled></td>";
											}
										?>
									</tr>
								</tbody>
							</table>

							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<td width="70%">
										<textarea name="note" placeholder="Note" class="form-control"></textarea>
									</td>
									<td width="30%">
										<input type="text" placeholder="Rp." name="total" class="form-control input-lg" value="<%  %>" disabled>
										<input type="hidden" placeholder="Rp." name="total" class="form-control" value="<% count %>" >
										<span style="color:red;"><% msg %></span>
									</td>
								</tr>
							</table>

							<center><input type="submit" style="" class="finish btn-success btn-block btn btn-lg" value="Bayar!" /></center>
					</div>

						<!-- Panel Footer -->
						<!--<div class="panel-footer">
							<div class="row">
								<div class="col-sm-10 col-sm-offset-2">
									<a href="{//{ url('tu-user/students') }}" class="btn-default btn">Batal</a>&nbsp;&nbsp;&nbsp;
									<button class="btn-primary btn">Simpan</button>
								</div>
							</div>
						</div>-->
						<!-- ./End Panel Footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script>
		var app = angular.module('SPP', [], function($interpolateProvider){
			$interpolateProvider.startSymbol('<%');
	        $interpolateProvider.endSymbol('%>');
		});
		app.controller('sppCtrl', function($scope) {


		});
	</script>

	<script>
	function openHistori() {
	    window.open("{{ url('komite/spp/histori/'.$student->nisn) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=1200,height=500");
	}
	</script>
	<script type="text/javascript">
	$(function(){
		var dataPrice = parseInt($('#package-id').find(':selected').data('price'));
		var daysTotal = parseInt($('#count-days').val());
		var billTotal = dataPrice * daysTotal;

		$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));

		$('#package-id').change(function(){

			$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));
		});

		$('input[name="tanggal_bayar"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		
	});

	Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	</script>

	<script type="text/javascript">
	</script>
@endsection

@section('inline-style')
<style type="text/css">
.package-info-wrapper {
    display: block;
    margin-top: 10px;
    background: #f6f6f6;
    padding: 7px 10px;
    font-size: 12px;
    font-style: italic;
    color: #888;
}
.package-info-wrapper strong {
	color: #000;
}
.nominal{
	font-size: 20px !important;
	color: red;
	float: right;
}
.masterbiaya{
	color:blue;
}
</style>
@stop