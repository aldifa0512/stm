<header id="topnav" class="navbar navbar-default navbar-fixed-top" role="banner">
    <div class="logo-area">
        <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
            <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                <span class="icon-bg">
                    <i class="ti ti-menu"></i>
                </span>
            </a>
        </span>


        <div class="toolbar-icon-bg hidden-xs" id="toolbar-search">

            <div class="input-group row">
                <img  style="margin-top:7px;margin-right:10px;float:left; max-width:40px;" src="{{ asset('assets/backend/img/favicon.ico') }}"><h3 style="width: 400px;">{{ Auth::komite()->get()->komite_name }}</h3>
                <span class="input-group-btn"><button class="btn" type="button"><i class="ti ti-close"></i></button></span>
            </div>
        </div>

    </div><!-- logo-area -->
            

    <ul class="nav navbar-nav toolbar pull-right">

        <li class="dropdown toolbar-icon-bg">
            <a href="#" class="dropdown-toggle username" data-toggle="dropdown">
                @if (!Request::is('noncust-ads*'))
                <img class="img-circle" src="{{ asset('assets/backend/img/komite.png') }}" alt="" />
                @else
                <span class="icon-bg"><i class="ti ti-fullscreen"></i>
                @endif
            </a>
            <ul class="dropdown-menu userinfo arrow">
                @if (!Request::is('noncust-ads*'))
                    <li><a href="{{ url('auth-komite/logout') }}"><i class="ti ti-shift-right"></i><span>Sign Out</span></a></li>
                @else
                    <li><a href="{{ url('noncust-ads/logout') }}"><i class="ti ti-shift-right"></i><span>Sign Out</span></a></li>
                @endif
            </ul>
        </li>

    </ul>
</header>