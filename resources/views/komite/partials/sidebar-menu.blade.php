<div class="static-sidebar-wrapper sidebar-default" >
    <div class="static-sidebar">
    	<div class="sidebar">

    		<!-- Main menu -->
    		<div class="widget stay-on-collapse" id="widget-sidebar">
    			<nav role="navigation" class="widget-body">
    				<ul class="acc-menu">
    					<li class="nav-separator"><span style=" color:#3c4377;"></span></li>
    					<li class="{{ Request::is('komite') ? 'active ' : null }}"><a href="{{ url('komite') }}"><i class="ti ti-home"></i><span>Dashboard</span></a></li>

    					<!-- Listing -->
                        <li><a href="javascript:;"><i class="ti ti-layout-list-thumb"></i><span>Daftar Siswa</span></a>
                        	<ul class="acc-menu">
                        		<li><a href="<?php echo url('komite/students/') ?>"><i class="fa fa-check"></i> Siswa Aktif</a></li>
                                <li><a href="<?php echo url('komite/students/recap') ?>"><i class="fa fa-align-justify"></i> Rekap Siswa</a></li>
                                <li><a href="<?php echo url('komite/students/pasif') ?>"><i class="fa fa-warning"></i> Tidak Aktif</a></li>
                                <li><a href="<?php echo url('komite/students/rombel') ?>"><i class="fa fa-th-list"></i> Rombongan Belajar</a></li>
                        	</ul>
                        </li>

                        <li><a href="javascript:;"><i class="ti ti-receipt"></i><span>Laporan DLL</span></a>
                            <ul class="acc-menu">
                                <!-- <li><a href="<?php echo url('komite/laporan/rombel') ?>">Rombongan Belajar</a></li> -->
                                <li><a href="<?php echo url('komite/laporan/invoice') ?>"><i class="fa fa-file-text-o"></i> Bukti Pembayaran</a></li>
                                <!-- <li><a href="<?php echo url('komite/laporan/penyesuaian') ?>"><i class="fa fa-edit"></i> Edit Pembayaran</a></li> -->
                                <li><a href="<?php echo url('komite/laporan/tunggakan') ?>"><i class="fa fa-calendar"></i> Batas Tunggakan</a></li>
                                <li><a href="<?php echo url('komite/laporan/cetakSuratTunggakan') ?>"><i class="fa fa-print"></i> Cetak Surat Tunggakan</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-bar-chart-o"></i><span>Analisis Keuangan</span></a>
                            <ul class="acc-menu">
                                <li><a href="<?php echo url('komite/analisis') ?>"><i class="fa fa-wrench"></i> Main Analysist</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:;"><i class="fa fa-upload"></i><span>Downloads</span></a>
                            <ul class="acc-menu">
                                <li><a href="<?php echo url('komite/databasesBackups') ?>"> <i class="fa fa-inbox"></i> Databases Backups</a></li>
                            </ul>
                        </li>

                        <!-- Sub User -->
                        <!-- <li><a href="javascript:;"><i class="ti ti-user"></i><span>Account</span></a>
                        	<ul class="acc-menu">
                        		<li><a href="<?php echo url('komite/edit_info') ?>">Edit Account Info</a></li>
                        	</ul>
                        </li> -->

    				</ul>
    			</nav>
    		</div>
    	</div>
    </div>
</div>