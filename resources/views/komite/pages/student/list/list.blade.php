@extends('komite.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <?php
	    	$tp = get_tp_aktif();
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li>Kelas {{$rombel['nama']}}</li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="col-md-12">
					<?php
					if (isset($rombelM) && $rombelM == true)
						echo "";
					else{
					?>
					<div class="row">
						<div class="action-menu col-md-12">
						<form action="{{ url('komite/students/filter') }}" method="post" id="buy-listing-wizard" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div style="padding-bottom:5px;">
									<label class="col-sm-1">NISN</label>
									<div class="row col-sm-3">
										<input type="text" name="nisn" placeholder="NISN atau NAMA" class="form-control" value="{{ old('nisn') }}">
									</div>
									<button style="margin-left:15px;" class="btn-primary btn">Cari !</button>
								</div>
							</form>
						</div>
					</div>
					<?php
					}
					?>
					
					<div class="panel panel-default">
						<div class="panel-heading"><h2><i class="fa fa-ellipsis-v"></i></h2>
						<?php if(isset($rombelM)){ ?>
						<!-- <i style="" id="hideButton" class="fa fa-chevron-up" style="float: right;">Hide</i>
						<i style="" id="showButton" class="fa fa-chevron-up" style="float: right;">Show</i> -->
						<?php } ?>
							<?php
								if(isset($recap) && $recap == true){
									echo "<h2 style='color:blue;'>Daftar Siswa Keseluruhan (Aktif dan Tidak Aktif)</h2> ";
									$satuan = " Siswa Aktif dan Tidak Aktif ";
								}
								elseif(isset($pasif) && $pasif == true){
									echo "<h2 style='color:blue;'>Daftar Siswa Tidak Aktif</h2> ";
									$satuan = " Siswa Tidak Aktif ";
								}
								elseif(isset($rombelM) && $rombelM == true){
									echo "<h2 style='color:blue;'>Daftar Siswa Kelas ". $rombel['nama'] ." / " . number_format($jumlahSiswa,0,',','.') . ' ' . strtoupper('siswa') . "</h2> ";
									$satuan = ' Siswa '
							?>
									<!-- <a style="float:right; " href="{{ url('komite/students/spp/rombel/'.$kelas.'/lap') }}" class=""><u>Cetak Laporan</u></a> -->
							<?php
								}
								else{
									echo "<h2 style='color:blue;'>Daftar Siswa Aktif </h2> ";
									$satuan = 'Siswa Aktif ';
								} 
							?>    
								<?php if(isset($rombelM)) : ?>
									<span style="float: right;" ><b><a href="{{ url('komite/students/rombel/tunggakan/' . $rombel['id']) }}"><i class="fa fa-print"> Print Tunggakan</i></a></b></span>
								<?php endif; ?>
						</div>

						<div class="panel-body">
						<?php 
						if(isset($rombelM)){ 
						$jurusan = getJurusanById($rombel['jurusan_id']);
						$naikKelas = getNaikKelas($rombel['id']);
						?>
						<form action="{{ url('komite/students/rombel/action/'.$rombel['id']) }}" method="POST">
						<?php } ?>
	                        <table id="listings-list" class="table table-striped table-bordered">
								<thead>
									<tr class="success">
										<th width="">Nama Lengkap</th>
										<th width="">Foto</th>
										<th width="">NISN</th>
										<th width="90">Tanggal Lahir</th>
										<?php
											if(isset($pasif))
												echo "<th>Alasan Keluar</th>";
											else
												echo "<th>Jurusan</th>";
										?>
										<th width="80">Kelas</th>
										<th width="150">Tunggakan TP Aktif</th>
										<th width="120">Action
										<?php 
											if(isset($rombelM)){
												echo '/Select All <input name="selectAll" type="checkbox" id="select_all"></th>';
											}
										?>
										
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($students as $student)
									<?php 
										$assets = json_decode($student->pas_foto);//var_dump($assets);die();
										$filename = substr($assets[0], strrpos($assets[0], '/') + 1);
										$img_entry = str_replace($filename, 'thumb-admin-'.$filename, $assets[0]);
									?>
									<?php
										if($student->status == "pasif"){
											$disableBtn = "disabled";
											$class = "class='danger'";
										}else{
											$disableBtn = "";
											$class = "class=''";
										}
									?>
									<tr <?php echo $class ?>>
										<?php 
										$require = "";
										if($student->jurusan == NULL & $student->tp != NULL)
											$require = "style=color:blue;";
										if($student->jurusan != NULL & $student->tp == NULL)
											$require = "style=color:green;";
										if($student->jurusan == NULL & $student->tp == NULL)
											$require = "style=color:red;";
										?>
										<td class=""><span {{$require}}>{{ $student->nama_lengkap }}</span></td>
										<td class="text-center">
											<?php if ($img_entry != ''): ?>
												<img class="img-thumbnail" src="{{ asset($img_entry) }}" width="70" height="70">
											<?php endif ?>
											<?php
												if($student->pas_foto == null){
													echo "Foto tidak tersedia";
												}
											?>
										</td>
										<td><a href="{{ url('komite/students/spp/bayar', $student->nisn) }}">{{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }}</a></td>
										<td>
											<?php echo $student->tanggal_lahir ?>
											
										</td>
										<td>
											<?php
												if(isset($pasif)){
													$alasan = App\Models\DropOut::where('nisn', $student->nisn)->first()->toArray();
													echo $alasan['keluar_karena'];
												}else{
													if($student->jurusan == null){
														echo "<span style='color:blue;'>Jurusan belum diisi</span>";
													}else{
														$jurusan = App\Models\Jurusan::where('id', $student->jurusan)->first()->toArray();//dd($jurusan);
														echo $jurusan['nama_jurusan'];
													}
												}
											?>
										</td>
										<td>
											<?php 
												if($student->kelas == null){
													echo "<span style='color:blue;'>Kelas belum diisi</span>";
												}else{
													$rombel = App\Models\Rombel::where('id', $student->kelas)->first();
													if($rombel != null){
														$rombel = $rombel->toArray();
														echo "<a href='" . url('komite/students/rombel/lihat', $student->kelas) . "'>" . $rombel['nama'] . "</a>";
													}
												}
											?>
										</td>
										<td>Rp. {{number_format(tunggakan_total_tahun_aktif($student->nisn, 9, $tp->tp, $student->tp),2,',','.')}}</td>
										<td class="row">
	                                        <div class="btn-group">
		                                        <button type="button" class="btn btn-midnightblue-alt dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		                                            <i class="ti ti-settings"></i> <span class="caret"></span>
		                                        </button>
		                                        <ul class="dropdown-menu" role="menu">
		                                            <li><a href="{{ url('komite/students/spp/bayar', $student->nisn) }}"> <i class="fa fa-money"></i> Bayar</a></li>
		                                            <li><a href="{{ url('komite/students/spp/histori/'.$student->nisn) }}" target="_blank"><i class="fa fa-folder-o"></i> Histori Bayar</a></li>
		                                            <li><a href="{{ url('komite/students/profile/'.$student->nisn) }}" target="_blank"><i class="fa fa-user"></i> Profile</a></li>
		                                        </ul>
		                                    </div>
		                                    <?php
		                                    if(isset($rombelM)){
		                                    	echo '<input style="float:right;" class="checkbox" type="checkbox" value="'. $student->nisn . '" name="check[]">';
		                                    }
		                                    ?>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							</form>
						</div>

						<div class="panel-footer text-right">
							<?php
								if(isset($render) && $render == false)
									echo "";
								else
									echo $students->render();
							?>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>

@endsection

@section('page-styles')
@endsection

@section('page-scripts')
@endsection

@section('inline-script')
	<script type="text/javascript">
	
	//select all checkboxes
	$("#select_all").change(function(){  //"select all" change 
	    var status = this.checked; // "select all" checked status
	    $('.checkbox').each(function(){ //iterate all listed checkbox items
	        this.checked = status; //change ".checkbox" checked status
	    });
	});

	$('.checkbox').change(function(){ //".checkbox" change 
	    //uncheck "select all", if one of the listed checkbox item is unchecked
	    if(this.checked == false){ //if this item is unchecked
	        $("#select_all")[0].checked = false; //change "select all" checked status to false
	    }
	    
	    //check "select all" if all checkbox items are checked
	    if ($('.checkbox:checked').length == $('.checkbox').length ){ 
	        $("#select_all")[0].checked = true; //change "select all" checked status to true
	    }
	});

	//HIDE AND SHOW ACTION BAR/TOGGLE
	$(document).ready(function(){
		$("#actionBar").hide();
	    $("#hideButton").click(function(){
	        $("#actionBar").hide();
	    });
	    $("#showButton").click(function(){
	        $("#actionBar").show();
	    });
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
	    .bs-example{
    	margin: 20px;
    }
</style>
@stop