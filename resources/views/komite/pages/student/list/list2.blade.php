@extends('komite.base2')

@section('title', 'Daftar Siswa Aktif')

@section('content')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{$page_name}}</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="alert alert-danger">
                <span class="alert-link">Perhatian!</span> halaman ini memuat semua data siswa pada tahun aktif saat di load, disarankan untuk membuka hal ini pada tab yang terpisah. 
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data {{$page_name}}
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <!-- <th>Foto</th> -->
                                        <th>NISN</th>
                                        <th>Tanggal Lahir</th>
                                        <?php
                                            if(isset($pasif)) echo "<th>Alasan Keluar</th>";
                                            else echo "<th>Jurusan</th>";
                                        ?>
                                        <th>Kelas</th>
                                        <th>Tunggakan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @foreach ($students as $student)
                                    <?php 
                                        $assets = json_decode($student->pas_foto);//var_dump($assets);die();
                                        $filename = substr($assets[0], strrpos($assets[0], '/') + 1);
                                        $img_entry = str_replace($filename, 'thumb-admin-'.$filename, $assets[0]);
                                    ?>
                                    <?php
                                        if($student->status == "pasif"){
                                            $disableBtn = "disabled";
                                            $class = "class='danger'";
                                        }else{
                                            $disableBtn = "";
                                            $class = "class=''";
                                        }
                                    ?>
                                    <tr <?php echo $class ?>>
                                        <?php 
                                        $require = "";
                                        if($student->jurusan == NULL & $student->tp != NULL)
                                            $require = "style=color:blue;";
                                        if($student->jurusan != NULL & $student->tp == NULL)
                                            $require = "style=color:green;";
                                        if($student->jurusan == NULL & $student->tp == NULL)
                                            $require = "style=color:red;";
                                        ?>
                                        <td class=""><span {{$require}}>{{ $student->nama_lengkap }}</span></td>
                                        <!-- <td class="text-center">
                                            <?php// if ($img_entry != ''): ?>
                                                <img class="img-thumbnail" src="{//{ asset($img_entry) }}" width="70" height="70">
                                            <?php// endif ?>
                                            <?php
                                                //if($student->pas_foto == null){
                                                //    echo "Foto tidak tersedia";
                                                //}
                                            ?>
                                        </td> -->
                                        <td><a href="{{ url('komite/students/spp/bayar', $student->nisn) }}">{{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }}</a></td>
                                        <td>
                                            <?php echo $student->tanggal_lahir ?>
                                            
                                        </td>
                                        <td>
                                            <?php
                                                if(isset($pasif)){
                                                    $alasan = App\Models\DropOut::where('nisn', $student->nisn)->first()->toArray();
                                                    echo $alasan['keluar_karena'];
                                                }else{
                                                    if($student->jurusan == null){
                                                        echo "<span style='color:blue;'>Jurusan belum diisi</span>";
                                                    }else{
                                                        $jurusan = App\Models\Jurusan::where('id', $student->jurusan)->first()->toArray();//dd($jurusan);
                                                        echo $jurusan['nama_jurusan'];
                                                    }
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if($student->kelas == null){
                                                    echo "<span style='color:blue;'>Kelas belum diisi</span>";
                                                }else{
                                                    $rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();
                                                    echo $rombel['nama'];
                                                }
                                            ?>
                                        </td>
                                        <td>Rp. </td>
                                        <td class="row">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-midnightblue-alt dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                    <i class="ti ti-settings"></i> <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{ url('komite/students/spp/bayar', $student->nisn) }}">Bayar</a></li>
                                                    <li><a href="{{ url('komite/students/spp/histori/'.$student->nisn) }}" target="_blank">Lihat</a></li>
                                                    <li><a href="#" data-toggle="modal" data-target="#LM" class="profile" name="{{$student->nisn}}" target="">Profile</a></li>
                                                    <li><a href="{{ url('komite/students/surat-tunggakan/'.$student->nisn) }}">Surat Tunggakan</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Siswa dengan data tahun dasar perhitungan biaya sekolah belum diisi
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Siswa dengan data jurusan belum diisi
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@endsection

@section('page-scripts')
<!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('assets/sbadmin/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- DataTables JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/sbadmin/vendor/datatables-plugins/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/sbadmin/vendor/datatables-responsive/dataTables.responsive.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/sbadmin/dist/js/sb-admin-2.js') }}"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
@endsection
