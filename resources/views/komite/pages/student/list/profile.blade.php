@extends('komite.base')

@section('title', 'Profile Siswa')

@section('content')
	<h3 class="page-title">Profile Siswa</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class="active"><span>Profile Siswa</span></li>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-blue" data-widget='{"draggable": "false"}'>
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Profile Siswa</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<table id="listings-list" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<td>Nama Lengkap</td><td><h2>{{$student->nama_lengkap}}</h2></td>
									</tr>
									<tr>
										<td>NISN</td><td><b>{{$student->nisn}}</b></td>
									</tr>
									<tr>
										<td>Kelas</td><td>{{$student->kelas}}</td>
									</tr>
									<tr>
										<td>Jurusan</td><td>{{$student->jurusan}}</td>
									</tr>
									<tr>
										<td>NIK</td><td>{{$student->nik}}</td>
									</tr>
									<tr>
										<td>Tahun Masuk</td><td>{{$student->tp}}</td>
									</tr>
									<tr>
										<td>Tanggal Lahir</td><td>{{$student->tanggal_lahir}}</td>
									</tr>
									<tr>
										<td>Tempat Lahir</td><td>{{$student->tempat_lahir}}</td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td><td>{{$student->jenis_kelamin}}</td>
									</tr>
									<tr>
										<td>Agama</td><td>{{$student->agama}}</td>
									</tr>
									<tr>
										<td>Jenis Tinggal</td><td>{{$student->jenis_tinggal}}</td>
									</tr>
									<tr>
										<td>Alamat Jalan</td><td>{{$student->alamat_jln}}</td>
									</tr>
									<tr>
										<td>Kode Pos</td><td>{{$student->kode_pos}}</td>
									</tr>
									<tr>
										<td>RT</td><td>{{$student->rt}}</td>
									</tr>
									<tr>
										<td>RW</td><td>{{$student->rw}}</td>
									</tr>
									<tr>
										<td>Dusun</td><td>{{$student->dusun}}</td>
									</tr>
									<tr>
										<td>Kecamatan</td><td>{{$student->kelurahan}}</td>
									</tr>
									<tr>
										<td>Transport</td><td>{{$student->transport}}</td>
									</tr>
									<tr>
										<td>Pas Foto</td><td>{{$student->pas_foto}}</td>
									</tr>
									<tr>
										<td>NPSN SMP</td><td>{{$student->npsn_smp}}</td>
									</tr>
									<tr>
										<td>Nama Ayah</td><td>{{$student->nama_ayah}}</td>
									</tr>
									<tr>
										<td>Nama Ibu</td><td>{{$student->nama_ibu}}</td>
									</tr>
									<tr>
										<td>Nama Wali</td><td>{{$student->nama_wali}}</td>
									</tr>
									<tr>
										<td>Tanggal Lahir Ayah</td><td>{{$student->tl_ayah}}</td>
									</tr>
									<tr>
										<td>Tanggal Lahir Ibu</td><td>{{$student->tl_ibu}}</td>
									</tr>
									<tr>
										<td>Tanggal Lahir Wali</td><td>{{$student->tl_wali}}</td>
									</tr>
									<tr>
										<td>Pekerjaan Ayah</td><td>{{$student->pekerjaan_ayah}}</td>
									</tr>
									<tr>
										<td>Pekerjaan Ibu</td><td>{{$student->pekerjaan_ibu}}</td>
									</tr>
									<tr>
										<td>Pekerjaan Wali</td><td>{{$student->pekerjaan_wali}}</td>
									</tr>
									<tr>
										<td>Penghasilan Ayah</td><td>{{$student->penghasilan_ayah}}</td>
									</tr>
									<tr>
										<td>Penghasilan Ibu</td><td>{{$student->penghasilan_ibu}}</td>
									</tr>
									<tr>
										<td>Penghasilan Wali</td><td>{{$student->penghasilan_wali}}</td>
									</tr>
									<tr>
										<td>NIK Ayah</td><td>{{$student->nik_ayah}}</td>
									</tr>
									<tr>
										<td>NIK Ibu</td><td>{{$student->nik_ibu}}</td>
									</tr>
									<tr>
										<td>NIK Wali</td><td>{{$student->nik_wali}}</td>
									</tr>
									<tr>
										<td>Pendidikan Ayah</td><td>{{$student->pendidikan_ayah}}</td>
									</tr>
									<tr>
										<td>Pendidikan Ibu</td><td>{{$student->pendidikan_ibu}}</td>
									</tr>
									<tr>
										<td>Pendidikan Wali</td><td>{{$student->pendidikan_wali}}</td>
									</tr>
									<tr>
										<td>Jenis Pendaftaran</td><td>{{$student->jenis_pendaftaran}}</td>
									</tr>
									<tr>
										<td>No Ujian SMP</td><td>{{$student->no_ujian_smp}}</td>
									</tr>
									<tr>
										<td>No Ijazah</td><td>{{$student->no_ijazah}}</td>
									</tr>
									<tr>
										<td>No SKHUS</td><td>{{$student->no_skhus}}</td>
									</tr>
									<tr>
										<td>Telepon</td><td>{{$student->telepon}}</td>
									</tr>
									<tr>
										<td>Handphone</td><td>{{$student->hp}}</td>
									</tr>
									<tr>
										<td>Email</td><td>{{$student->email}}</td>
									</tr>
									<tr>
										<td>Penerima KPS</td><td>{{$student->penerima_kps}}</td>
									</tr>
									<tr>
										<td>No KPS</td><td>{{$student->no_kps}}</td>
									</tr>
									<tr>
										<td>Penerima KIP</td><td>{{$student->penerima_kip}}</td>
									</tr>
									<tr>
										<td>No KIP</td><td>{{$student->no_kip}}</td>
									</tr>
									<tr>
										<td>Layak PIP</td><td>{{$student->layak_pip}}</td>
									</tr>
									<tr>
										<td>Alasan PIP</td><td>{{$student->alasan_pip}}</td>
									</tr>
									<tr>
										<td>NIPD</td><td>{{$student->nipd}}</td>
									</tr>
									<tr>
										<td>skhun</td><td>{{$student->skhun}}</td>
									</tr>
									<tr>
										<td>Nama di KIP</td><td>{{$student->nama_di_kip}}</td>
									</tr>
									<tr>
										<td>No Registrasi Akta Lahir</td><td>{{$student->no_registrasi_akta_lahir}}</td>
									</tr>
									<tr>
										<td>Layak PIP Usulan dari Sekolah</td><td>{{$student->layak_pip_usulan_dari_sekolah}}</td>
									</tr>
									<tr>
										<td>Alasan Layak PIP</td><td>{{$student->alasan_layak_pip}}</td>
									</tr>
									<tr>
										<td>Bank</td><td>{{$student->bank}}</td>
									</tr>
									<tr>
										<td>No Rek Bank</td><td>{{$student->nomor_rekening_bank}}</td>
									</tr>
									<tr>
										<td>Rekening Atas Nama</td><td>{{$student->rekening_atas_nama}}</td>
									</tr>
									<tr>
										<td>Nomor KKS</td><td>{{$student->nomor_kks}}</td>
									</tr>
								</thead>
								<tbody>
								</tbody>
						</table>
				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

