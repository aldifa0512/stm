@extends('komite.base2')

@section('title', 'Analisis Keuangan')

@section('content')
<?php
//----------------------------------------------------------------------------------------------------------
//Local functions just for this page    

    function formatTp($tp){
        return ($tp . '/' . ($tp + 1));
    }

    function pilihanTahun($tps, $tpAktif, $name, $id){
        echo '<select name="' . $name . '" id="' . $id . '">';
            foreach ($tps as $key => $value) { 
                if($tpAktif == $value->tp){
                    echo '<option value="' . $value->tp . '" selected>' . formatTp($value->tp) . '</option>';    
                }else{
                    echo '<option value="' . $value->tp . '">' . formatTp($value->tp) . '</option>';    
                }
                
            }
        echo '</select>';
    }

//----------------------------------------------------------------------------------------------------------
//Local variables just for this page 

    $tpAktifObj = get_tp_aktif();
    $tpAktif = $tpAktifObj->tp;
    $tps = get_all_tp();

//----------------------------------------------------------------------------------------------------------
?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-wrench"></i> Analisis Keuangan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> GRAFIK PEMBAYARAN UANG SEKOLAH SISWA PADA <span id="grafikTahun">{{ formatTp($tpAktif) }}</span>
                            <div class="pull-right">
                                {{ pilihanTahun($tps, $tpAktif, "pilihTahunGrafikBalok", "pilihTahunGrafikBalok") }}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12" id="barAjax">
                                    <div id="morris-bar-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> KOMPOSISI PEMBAYARAN UANG SEKOLAH SISWA PADA {{ formatTp($tpAktif) }}
                            <div class="pull-right">
                                <div class="pull-right">
                                    {{ pilihanTahun($tps, $tpAktif, "pilihTahunKomposisiBayar", "pilihTahunKomposisiBayar") }}
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Kelas</th>
                                            <th>Siswa Baru</th>
                                            <th>Beasiswa</th>
                                            <th>Penyesuaian</th>
                                            <th>Alumni</th>
                                            <th>Tunai</th>
                                            <th>Total Penerimaan</th>
                                            <th>Total Keseluruhan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($bayar['detail'] as $key => $value): ?>
                                            <tr>
                                                <td>{{ $key }}</td>
                                                <td>Rp. {{ number_format($value['sb'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['bea'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['pn'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['al'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['tu'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['total'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format(($value['total'] - $value['sb'] - $value['bea'] - $value['pn']),2,',','.') }}</td>
                                            </tr>
                                        <?php endforeach ?>
                                        <tr class="success">
                                            <td><b>TOTAL</b></td>
                                            <td>Rp. {{ number_format($bayar['sb'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['bea'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['pn'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['al'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['tu'],2,',','.') }}</td>
                                            <td><b>Rp. {{ number_format(($bayar['tu'] + $bayar['al']),2,',','.') }}</b></td>
                                            <td>Rp. {{ number_format($bayar['total'],2,',','.') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> KOMPOSISI PEMBAYARAN PERBULAN PADA {{ formatTp($tpAktif) }}
                            <div class="pull-right">
                                <div class="pull-right">
                                    {{ pilihanTahun($tps, $tpAktif, "pilihTahunKomposisiBulanan", "pilihTahunKomposisiBulanan") }}
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Bulan</th>
                                            <th>Siswa Baru</th>
                                            <th>Beasiswa</th>
                                            <th>Penyesuaian</th>
                                            <th>Alumni</th>
                                            <th>Tunai</th>
                                            <th>Total Penerimaan</th>
                                            <th>Total Keseluruhan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($bayar2['detail'] as $key => $value): ?>
                                            <tr>
                                                <td>{{ $key }}</td>
                                                <td>Rp. {{ number_format($value['sb'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['bea'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['pn'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['al'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['tu'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format(($value['tu'] + $value['al']),2,',','.') }}</td>
                                                <td>Rp. {{ number_format($value['total'],2,',','.') }}</td>
                                            </tr>
                                        <?php endforeach ?>
                                        <tr class="success">
                                            <td><b>TOTAL</b></td>
                                            <td>Rp. {{ number_format($bayar['sb'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['bea'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['pn'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['al'],2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['tu'],2,',','.') }}</td>
                                                <td>Rp. {{ number_format(($bayar['tu'] + $value['al']),2,',','.') }}</td>
                                            <td>Rp. {{ number_format($bayar['total'],2,',','.') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <!-- jQuery -->
    <script src="{{ asset('assets/sbadmin/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/sbadmin/vendor/morrisjs/morris.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/sbadmin/data/morris-data.js') }}"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/sbadmin/dist/js/sb-admin-2.js') }}"></script>
@endsection
@section('inline-script')
<script type="text/javascript">
    Morris.Bar({
      element: 'morris-bar-chart',
      data: [
        <?php foreach ($data_bar as $key => $value) { ?>
            {
                y: '{{ $value['bulan'] }}',
                a: {{ $value['satu'] }},
                b: {{ $value['dua'] }},
                c: {{ $value['tiga'] }}
            }, 
        <?php } ?>
      ],
      xkey: 'y',
      ykeys: ['a', 'b', 'c'],
      labels: ['Kelas X', 'Kelas XI', 'Kelas XII']
    });

    $("#pilihTahunGrafikBalok").on('keyup keypress blur change', function(e) {
            var value = $( this ).val();
            $("#grafikTahun").html(value + '/' + ( Number(value) + 1 ));
            $.ajax({
                method: 'POST',
                url: "{{ url('komite/analisis/pilihTahunGrafikBalok') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    value: value
                },
                success: function(res) {
                    var result = [];
                    for (var i = 0; i < res.length; i++) {
                        var bulanObj = {
                            y: res[i].bulan,
                            a: res[i].satu,
                            b: res[i].dua,
                            c: res[i].tiga
                        }
                        result.push(bulanObj);
                    }
                    $("#morris-bar-chart").remove();
                    $("#barAjax").html('<div id="morris-bar-chart"></div>');
                    ShowGrpah(result);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log( jqXHR);
                    console.log( textStatus);
                    console.log( errorThrown);
                }
            });
        });

        function ShowGrpah(data) {
            Morris.Bar({
                element: 'morris-bar-chart',
                data: data,
                xkey: 'y',
                ykeys: ['a', 'b', 'c'],
                labels: ['Kelas X', 'Kelas XI', 'Kelas XII']
            });
        }
</script>
@endsection
