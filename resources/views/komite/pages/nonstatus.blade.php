 @extends('komite.base2')

@section('title', 'Dashboard')

@section('content')
        <div id="page-wrapper">
        <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> SISWA DENGAN TANPA STATUS
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Action</a>
                                        </li>
                                        <li><a href="#">Another action</a>
                                        </li>
                                        <li><a href="#">Something else here</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>NISN</th>
                                            <th>NAMA</th>
                                            <th>IMPORTED CLASS</th>
                                            <th>KELAS</th>
                                            <th>JURUSAN</th>
                                            <th>STATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no =1;
                                        foreach ($siswas as $siswa) { ?>
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ nisn_format($siswa->nisn) }}</td>
                                            <td>{{ $siswa->nama_lengkap}}</td>
                                            <td>{{ $siswa->importedClass}}</td>
                                            <td>{{ $siswa->kelas == '' ? 'Belum Diisi' : get_class_name($siswa->kelas) }}</td>
                                            <td>{{ $siswa->jurusan == '' ? 'Belum Diisi' : get_jurusan_name($siswa->jurusan) }}</td>
                                            <td>{{ $siswa->status == 'new_i' ? 'Imported' : 'Unknown' }}</td>
                                        </tr>
                                        <?php  
                                        $no++ ;} ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
 <!-- jQuery -->
    <script src="{{ asset('assets/sbadmin/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/sbadmin/vendor/morrisjs/morris.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/sbadmin/data/morris-data.js') }}"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/sbadmin/dist/js/sb-admin-2.js') }}"></script>
@endsection
@section('inline-script')
<script type="text/javascript">
    /*
     * Play with this code and it'll update in the panel opposite.
     *
     * Why not try some of the options above?
     */
    Morris.Bar({
      element: 'morris-bar-chart',
      data: [
        { y: 'JAN', a: 100, b: 90, c: 50 },
        { y: 'FEB', a: 75,  b: 65, c: 50 },
        { y: 'MAR', a: 50,  b: 40, c: 50 },
        { y: 'APR', a: 75,  b: 65, c: 50 },
        { y: 'MEI', a: 50,  b: 40, c: 50 },
        { y: 'JUN', a: 75,  b: 65, c: 50 },
        { y: 'JUL', a: 100, b: 90, c: 50 }
      ],
      xkey: 'y',
      ykeys: ['a', 'b', 'c'],
      labels: ['Kelas X', 'Kelas XI', 'Kelas XII']
    });
</script>
@endsection
