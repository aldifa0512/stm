 @extends('komite.base2')

@section('title', 'Surat Pemberitahuan Tunggakan')

@section('content')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Surat Pemberitahuan Tunggakan <i class="glyphicon glyphicon-print"></i></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
            <div class="alert alert-danger">
                Mohon bersabar, proses dapat memakan waktu beberapa menit (1-2 menit).
            </div>
            @if (Session::has('error'))
                    <div class="alert alert-dismissable alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="ti ti-check"></i>&nbsp; <strong>Sepertinya ada yang salah.</strong> {{ Session::get('error') }}.
                    </div>
                @endif
                @if ($errors->has())
                    <div class="alert alert-dismissable alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="ti ti-close"></i>&nbsp; <strong>Sepertinya ada yang salah.</strong>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
               <form role="form" action="{{ url('komite/laporan/cetakSuratTunggakan') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                   <fieldset>
                        <div class="col-lg-12 row">
                            <div class="col-lg-6">
                                <select class="form-control" id="jurusan" name="jurusan" type="text" placeholder="SIlahkan Pilih Jurusan" >
                                    <option disabled selected="">Pilih Jurusan</option>
                                    <?php
                                        foreach ($jurusan as $key => $value) {
                                            echo "<option value='". $value->id . "'>" . $value->nama_jurusan . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <select class="form-control" id="kelas" name="kelas" type="text" placeholder="SIlahkan Pilih Kelas" >
                                    <option id="optionKelas" disabled selected>Pilih Kelas</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h3>Bagian Kepala Surat</h3><hr>
                            <div class="form-group">
                                <label for="nomorSurat">Batas Tunggakan</label>
                                <select class="form-control" id="batasTunggakan" name="batasTunggakan" type="text" placeholder="" >
                                    <?php
                                        $blns = Bulan();
                                        foreach ($blns as $key => $value) {
                                            echo "<option value='". $key . "'>" . $value . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="nomorSurat">Nomor Surat</label>
                                <input class="form-control" id="nomorSurat" name="nomorSurat" type="text" placeholder="Nomor Surat" >
                                <p class="help-block">Default biarkan kosong.</p>
                            </div>
                            <div class="form-group">
                                <label for="sifatSurat">Sifat Surat</label>
                                <input class="form-control" id="sifatSurat" name="sifatSurat" type="text" placeholder="Sifat Surat" >
                            </div>
                            <div class="form-group">
                                <label for="lampiranSurat">Lampiran Surat</label>
                                <input class="form-control" id="lampiranSurat" name="lampiranSurat" type="text" placeholder="Lampiran Surat" >
                            </div>
                            <div class="form-group">
                                <label for="perihalSurat">Perihal Surat</label>
                                <input class="form-control" id="perihalSurat" name="perihalSurat" type="text" placeholder="Perihal Surat" >
                            </div>
                            <button type="submit" value="submit" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Surat Pemberitahuan Tunggakan</button>
                            <br><br><br>
                        </div>
                        <div class="col-lg-6">
                            <h3>Bagian Badan Surat</h3><hr>
                            <div class="form-group">
                                <label for="tahunPelajaran">Tahun Pelajaran</label>
                                <input class="form-control" id="tahunPelajaran" name="tahunPelajaran" type="text" placeholder="{{ get_tp_aktif()->tp}}/{{(get_tp_aktif()->tp+1)}}" disabled>
                                <input class="form-control" id="tahunPelajaran" name="tahunPelajaran" type="hidden" placeholder="Tahun Pelajaran" value="{{ get_tp_aktif()->tp}}/{{(get_tp_aktif()->tp+1)}}" >
                                <p class="help-block">Nilainya sesuai dengan TP Aktif Admin</p>
                            </div>
                            <div class="form-group">
                                <label for="latarBelakangSurat">Latar Belakang Surat</label>
                                <input class="form-control" id="latarBelakangSurat" name="latarBelakangSurat" type="text" placeholder="Latar Belakang Surat" >
                                <p class="help-block" style="color: red;"><em>Sehubungan dengan akan ..... tahun pelajaran {{ get_tp_aktif()->tp}}/{{(get_tp_aktif()->tp+1)}}</em></p>
                            </div>
                            <div class="form-group">
                                <label for="batasPelunasan">Batas Pelunasan Tunggakan</label>
                                <input class="form-control" id="batasPelunasan" name="batasPelunasan" type="text" placeholder="Batas Pelunasan Tunggakan" >
                                <p class="help-block" style="color: red;"><em>Selambat-lambatnya tanggal ....</em></p>
                            </div>
                            <div class="form-group">
                                <label for="tanggalCetak">Tanggal Cetak</label>
                                <input class="form-control" id="tanggalCetak" name="tanggalCetak" type="text" placeholder="{{ date('d') }} {{ get_bulan_noup(date('n')) }} {{ date('Y') }}" disabled>
                                <input class="form-control" id="tanggalCetak" name="tanggalCetak" type="hidden" placeholder="Tahun Pelajaran" value="{{ date('d') }} {{ get_bulan_noup(date('n')) }} {{ date('Y') }}" >
                            </div>
                            <div class="form-group">
                                <label for="suratOleh">Surat Oleh (Tertanda)</label>
                                <input class="form-control" id="suratOleh" name="suratOleh" type="text" placeholder="{{ Auth::komite()->get()->komite_name }}" disabled>
                                <input class="form-control" id="suratOleh" name="suratOleh" type="hidden" placeholder="Tahun Pelajaran" value="{{ Auth::komite()->get()->komite_name }}" >
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
 <!-- jQuery -->
    <script src="{{ asset('assets/sbadmin/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/sbadmin/vendor/morrisjs/morris.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/sbadmin/data/morris-data.js') }}"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/sbadmin/dist/js/sb-admin-2.js') }}"></script>
@endsection

@section('inline-script')
<script type="text/javascript">
    $('#jurusan').on('change', function(e) {
        var value = $('#jurusan').val();
            $.ajax({
              url: '{{ url('komite/laporan/ajaxSuratTunggakan/') }}',
              data: {
                    _token: '{{ csrf_token() }}',
                    value: value
                },
              type: "POST",
              dataType: 'json',
              success: function(res){
                $('.addedOptionClass').remove();
                var optionKelas = '';
                for(var x in res){
                    console.log(res[x]);
                    optionKelas += "<option class='addedOptionClass' value='" + res[x].id + "'>" + res[x].nama + "</option>"; 
                }
                $('#optionKelas').after(optionKelas);
              },
              error: function(jqXHR, textStatus, errorThrown){
                console.log( jqXHR);
                console.log( textStatus);
                console.log( errorThrown);
              }
            });      
        });
</script>
@endsection
