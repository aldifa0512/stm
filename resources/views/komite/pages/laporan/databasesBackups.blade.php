@extends('komite.base')

@section('title', 'Databases Backups')

@section('content')
	<ol class="breadcrumb">
	    <li class=""><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li>Databases Backups</li>
	</ol>
	<div class="container-fluid">
		<div class="row">
			@if (Session::has('error'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if (Session::has('success'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-success">
						<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if ($errors->has())
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			<div class="col-md-12">
				<div class="panel panel-blue">
					<div class="panel-heading">
						<h2><i class="fa fa-th-list"></i> Databases Backups</h2>
					</div>
					<div class="panel-body">
						<a href="{{ url('komite/databasesBackups/run') }}"><button>Run Backup</button></a><br>
						Klik tombol "Run Backup" jika backup otomatis tidak berjalan.
						<hr>
						<table id="listings-list" class="table table-striped table-bordered">
							<tr class="danger">
								<th style="text-align: center;">Nama File</th>
								<th style="text-align: center;" width="170">Action</th>
							</tr>
							<?php 
							foreach ($files as $key => $value) { 
							if($value == '.' || $value == '..') continue;
							?>
							<tr>
								<td>{{ $value }}</td>
								<td>
									<a href="{{ url('komite/databasesBackups/download', $value) }}"><i class="fa fa-upload"></i> Download</a>
									<a href="{{ url('komite/databasesBackups/delete', $value) }}"><i class="fa fa-times"></i> Delete</a>
								</td>
							</tr>
							<?php } ?>
						</table>
					</div>
					<!-- ./End panel body -->
				</div>
			</div>
		</div>
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
    <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@endsection

@section('inline-script')
@endsection

@section('inline-style')
@stop