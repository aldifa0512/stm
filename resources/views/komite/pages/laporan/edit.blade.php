@extends('tu-user.base')

@section('title', 'Edit Data Siswa')

@section('content')
	<h3 class="page-title">Edit Data Siswa</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li><a href="{{ url('tu-user/students') }}">Siswa</a></li>
	    <li class="active"><span>Edit Data Siswa</span></li>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-blue" data-widget='{"draggable": "false"}'>
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Edit Data Siswa</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<p>Untuk mengubah data siswa silahkan ikuti langkah-langkah berikut ini</p>
						<form action="{{ url('tu-user/students/edit/'.$student->id) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<fieldset title="DATA PRIBADI">
								<?php
								//$packages = App\Models\Package::all();
								?>
								<legend>Rincian Data Pribadi Siswa </legend>
								<!--<div class="listing-entry first">
									<div class="form-group">
										<label for="fieldname" class="col-md-3 control-label">Select Package</label>
										<div class="col-md-6">
											<select class="select-package form-control" name="listings[0][package_id]" required>
												<option selected disabled>--- SELECT PACKAGE ---</option>
												@//forelse ($packages as $package)
													<option value="{//{ $package->id }}" data-info="{//{ $package->notes }}" data-price="{//{ $package->price }}" data-days="{//{ $package->days }}" data-discount="{//{ $package->discount }}">{//{ $package->name }}</option>
												@//empty
													{//{-- empty expr --}}
												@//endforelse
											</select>
										</div>
									</div>
									<p class="listing-package-info"></p>
									<button type="button" class="add-listing-entry btn tooltips" title="Add New Listing Slot"><i class="fa fa-plus"></i></button>
									<button type="button" class="remove-listing-entry btn tooltips" title="Remove Slot"><i class="fa fa-close"></i></button>
								</div>-->
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Lengkap</label>
								<div class="col-sm-6">
									<input type="text" name="nama_lengkap" class="form-control" value="{{ $student->nama_lengkap }}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">NISN</label>
								<div class="col-sm-4">
									<input type="number" name="nisn" class="form-control" value="{{ $student->nisn }}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Kelamin</label>
								<div class="col-sm-4">
									    <select class="form-control" name="jenis_kelamin" value="{{ $student->nama_lengkap }}">
										    <option>Laki-laki</option>
										    <option>Perempuan</option>
										</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">NIK/No. KITAS(Untuk WNA)</label>
								<div class="col-sm-4">
									<input type="number" name="nik" class="form-control" value="{{ $student->nik }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Tempat Lahir</label>
								<div class="col-sm-6">
									<input type="text" name="tempat_lahir" class="form-control" value="{{ $student->tempat_lahir }}">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tanggal Lahir</label>
								<div class="col-sm-4">
									<input type="text" placeholder="yyyy-mm-dd" name="tanggal_lahir" value="{{ $student->tanggal_lahir }}" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Agama</label>
								<div class="col-sm-6">
									<input type="text" name="agama" class="form-control" value="{{ $student->agama }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alamat Jalan</label>
								<div class="col-sm-6">
									<input type="text" name="alamat_jln" class="form-control" value="{{ $student->alamat_jln }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">RT</label>
								<div class="col-sm-2">
									<input type="text" name="rt" class="form-control" value="{{ $student->rt }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">RW</label>
								<div class="col-sm-2">
									<input type="text" name="rw" class="form-control" value="{{ $student->rw }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Dusun</label>
								<div class="col-sm-6">
									<input type="text" name="dusun" class="form-control" value="{{ $student->dusun }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Kelurahan/Desa</label>
								<div class="col-sm-6">
									<input type="text" name="kelurahan" class="form-control" value="{{ $student->kelurahan }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Kecamatan</label>
								<div class="col-sm-6">
									<input type="text" name="kecamatan" class="form-control" value="{{ $student->kecamatan }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nomor KIP</label>
								<div class="col-sm-4">
									<input type="number" name="kip" class="form-control" value="{{ $student->kip }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Tertera di KIP</label>
								<div class="col-sm-6">
									<input type="text" name="kip_nama" class="form-control" value="{{ $student->kip_nama }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nomor KKS</label>
								<div class="col-sm-4">
									<input type="number" name="kks" class="form-control" value="{{ $student->kks }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nomor Registrasi Akta Lahir</label>
								<div class="col-sm-4">
									<input type="number" name="akta_lahir" class="form-control" value="{{ $student->akta_lahir }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">NPSN SMP</label>
								<div class="col-sm-6">
									<input type="number" name="npsn_smp" class="form-control" value="{{ $student->npsn_smp }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Pas Foto</label>
								<div class="col-sm-10">
									<input type="file" name="pas_foto">
								</div>
							</div>

							</fieldset>
							<fieldset title="DATA ORANG TUA / WALI">
								<legend>Informasi Umum Orang Tua / Wali</legend>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Ayah Kandung</label>
								<div class="col-sm-6">
									<input type="text" name="nama_ayah" class="form-control" value="{{ $student->nama_ayah }}">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tahun Lahir Ayah</label>
								<div class="col-sm-4">
									<input type="text" placeholder="yyyy-mm-dd" name="tl_ayah" class="form-control" value="{{ $student->tl_ayah }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Pekerjaan Ayah</label>
								<div class="col-sm-6">
									<input type="text" name="pekerjaan_ayah" class="form-control" value="{{ $student->pekerjaan_ayah }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Ibu Kandung</label>
								<div class="col-sm-6">
									<input type="text" name="nama_ibu" class="form-control" value="{{ $student->nama_ibu }}">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tahun Lahir Ibu</label>
								<div class="col-sm-4">
									<input type="text" placeholder="yyyy-mm-dd" name="tl_ibu" class="form-control" value="{{ $student->tl_ibu }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Pekerjaan Ibu</label>
								<div class="col-sm-6">
									<input type="text" name="pekerjaan_ibu" value="{{ $student->pekerjaan_ibu }}" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Wali</label>
								<div class="col-sm-6">
									<input type="text" name="nama_wali" class="form-control" value="{{ $student->nama_wali }}">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tahun Lahir Wali</label>
								<div class="col-sm-4">
									<input type="text" placeholder="yyyy-mm-dd" name="tl_wali" class="form-control" value="{{ $student->tl_wali }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Pekerjaan Wali</label>
								<div class="col-sm-6">
									<input type="text" name="pekerjaan_wali" value="{{ $student->pekerjaan_wali }}" class="form-control">
								</div>
							</div>
							</fieldset>
							<fieldset title="REGISTRASI SISWA">
								<legend>Data Registrasi Siswa (Masuk)</legend>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Pendaftaran</label>
								<div class="col-sm-6">
									<input type="text" name="jenis_pendaftaran" class="form-control" value="{{ $student->jenis_pendaftaran }}" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jurusan</label>
								<div class="col-sm-6">
									    <select class="form-control" name="jurusan" required>
									    <?php 
									    	$jurusans = App\Models\Jurusan::all();
									    	foreach ($jurusans as $jurusan) {
									    		echo "<option>$jurusan->nama_jurusan</option>";
									    	}
									    ?>
										</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tanggal Masuk Sekolah</label>
								<div class="col-sm-4">
									<input type="text" placeholder="yyyy-mm-dd" name="tgl_masuk" class="form-control" value="{{ $student->tgl_masuk }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">NIS</label>
								<div class="col-sm-4">
									<input type="number" name="nis" class="form-control" value="{{ $student->nis }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nomor Peserta Ujian SMP</label>
								<div class="col-sm-4">
									<input type="number" name="no_ujian_smp" class="form-control" value="{{ $student->no_ujian_smp }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">No. Seri Ijazah SMP</label>
								<div class="col-sm-6">
									<input type="text" name="no_ijazah" class="form-control" value="{{ $student->no_ijazah }}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">No. Seri SKHUS</label>
								<div class="col-sm-6">
									<input type="text" name="no_skhus" value="{{ $student->no_skhus }}" class="form-control">
								</div>
							</div>
							</fieldset>
							<input type="submit" class="finish btn-success btn" value="Submit" />
						</form>
					</div>
					<!-- ./End panel body -->
						<div class="panel-body" style="padding: 40px 16px;">
							
						</div>
						<!-- ./End panel body -->

						<!-- Panel Footer -->
						<!--<div class="panel-footer">
							<div class="row">
								<div class="col-sm-10 col-sm-offset-2">
									<a href="{//{ url('tu-user/students') }}" class="btn-default btn">Batal</a>&nbsp;&nbsp;&nbsp;
									<button class="btn-primary btn">Simpan</button>
								</div>
							</div>
						</div>-->
						<!-- ./End Panel Footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
	$(function(){
		var dataPrice = parseInt($('#package-id').find(':selected').data('price'));
		var daysTotal = parseInt($('#count-days').val());
		var billTotal = dataPrice * daysTotal;

		$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));

		$('#package-id').change(function(){

			$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));
		});

		$('input[name="tanggal_lahir"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tl_ayah"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tl_ibu"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tl_wali"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tgl_masuk"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
	});

	Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	</script>

	<script type="text/javascript">
	$(function(){
		$('#data-siswa').stepy(
			{finishButton: true,
				titleClick: true,
				block: true,
				validate: true
			});

	    //Add Wizard Compability - see docs
	    $('.stepy-navigator').wrapInner('<div class="pull-right"></div>');

	    //Make Validation Compability - see docs
	    /*$('#buy-listing-wizard').validate({
	        errorClass: "help-block",
	        validClass: "help-block",
	        highlight: function(element, errorClass,validClass) {
	           $(element).closest('.form-group').addClass("has-error");
	        },
	        unhighlight: function(element, errorClass,validClass) {
	            $(element).closest('.form-group').removeClass("has-error");
	        }
	    });

	    $('body').on('change', '.select-package', function(){
			var selectedOpt = $(this).find(':selected');
			var selectVal = selectedOpt.val();

			if (selectVal !== '') {
				$(this).closest('.listing-entry').find('.listing-package-info').html(selectedOpt.data('info'));
			}
			$(this).parents('.form-group').next('.listing-package-info').addClass('active');
		});

		$('body').on('click', '.add-listing-entry', function(){
			var parent = $(this).parent();

			parent.find('.tooltip').remove();

			var cloned = parent.clone();

			$(this).css('display', 'none');

			cloned.find('.listing-package-info').text('').removeClass('active');
			cloned.find('.remove-listing-entry').css('display', 'block');
			parent.after(cloned);

			renameFieldEntry();
		});

		$('body').on('click', '.remove-listing-entry', function(){
			var prevEntry = $(this).parent().prev();
			var nextEntry = $(this).parent().next();
			$(this).parent().remove();

			// show the remove button and add button to the prev of parent
			// if any entry after
			if (nextEntry.hasClass('listing-entry') === false) {
				prevEntry.find('.add-listing-entry').css('display', 'block');
			}

			renameFieldEntry();
		});

		function renameFieldEntry() {
			var selectPackage = $('.select-package');

			for (var i = 0; i < selectPackage.length; i++) {
				selectPackage.eq(i).attr('name', 'listings[' + i + '][package_id]');
			}
		}*/
	});
	/**
	 * Number.prototype.format(n, x)
	 * 
	 * @param integer n: length of decimal
	 * @param integer x: length of sections
	 */
	Number.prototype.format = function(n, x) {
	    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
	};
	</script>
@endsection

@section('inline-style')
<style type="text/css">
.package-info-wrapper {
    display: block;
    margin-top: 10px;
    background: #f6f6f6;
    padding: 7px 10px;
    font-size: 12px;
    font-style: italic;
    color: #888;
}
.package-info-wrapper strong {
	color: #000;
}
</style>
@stop