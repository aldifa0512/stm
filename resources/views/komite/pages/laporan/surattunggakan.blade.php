<?php

for ($i=0; $i < count($data['students']); $i++) { 

?>
<!DOCTYPE html>
<html>
<head>
	<title>Surat Tunggakan</title>
	<style type="text/css">
		/*table, tr, td{
			border: 1px solid black;
		}*/
	</style>

	<style>
	.page-break {
	    page-break-after: always;
	}
	</style>

</head>
<body>
	<table>
		<tr class="header">
			<td style="width: 15%; vertical-align: middle;" class="header float-kiri">
				<center><img  style="max-width: 70px;" src="{{ asset('assets/backend/img/logo_img.jpg') }}"></center>		
			</td>

			<td style="width: 70%;" class="header float-center">
				<center>
					<span style="font-size: 16px;">
						PEMERINTAH KOTA PAYAKUMBUH<br>
						DINAS PENDIDIKAN<br>
						SEKOLAH MENENGAH KEJURUAN NEGERI 2 PAYAKUMBUH<br>
					</span>
					<span style="font-size: 12px;">
						<em>Jl.Soekarno Hatta / Anggrek I Payakumbuh Telp/Fax. 0752-92123</em><br>
						Fax. 0752 91500 email: Smk2Pyk@yahoo.id, Website : www.smk2payakumbuh.sch.id
					</span>	
				</center>		
			</td>

			<td style="width: 15%; vertical-align: middle;" class="header float-kanan">
				<center><img  style="max-width: 70px; height: 70px;" src="{{ asset('assets/backend/img/ukas.png') }}"></center>		
			</td>	
		</tr>
		<tr>
			<td colspan="3">
				<hr>
			</td>
		</tr>

		<tr class="body">
			<td colspan="3">
			<table  style="width: 100%;">
				<tr>
					<td style="width:10%;">Nomor</td>
					<td style="">: {{ $data['nomorSurat'] }}</td>
				</tr>
				<tr>
					<td style="width:10%;">Sifat</td>
					<td style="">: {{ $data['sifatSurat'] }}</td>
				</tr>
				<tr>
					<td style="width:10%;">Lampiran</td>
					<td style="">: {{ $data['lampiranSurat'] }}</td>
				</tr>
				<tr>
					<td style="width:10%;">Perihal</td>
					<td style="">: {{ $data['perihalSurat'] }}</td>
				</tr>
				<tr>
					<td colspan="2"><br></td>
				</tr>
				<tr>
					<td colspan="2">
						Kepada : Yth Bapak/Ibu Wali Murid<br>
						<span style="margin-left: 40px;">Ditempat,</span>
					</td>
				</tr>
				<tr>
					<td colspan="2"><br></td>
				</tr>
				<tr>
					<td colspan="2">
						Sehubungan dengan akan {{ $data['latarBelakangSurat'] }} tahun pelajaran {{ $data['tahunPelajaranSurat'] }} maka kami selaku bendahara sekolah memberitahukan bahwa siswa yang namanya tersebut dibawah ini:
					</td>
				</tr>
				<tr>
					<td style="">Nama</td>
					<td style="">: {{ $data['students'][$i]['nama_lengkap'] }}</td>
				</tr>
				<tr>
					<td style="">Nisn</td>
					<td style="">: {{ nisn_format($data['students'][$i]['nisn']) }}</td>
				</tr>
				<tr>
					<td style="">Kelas</td>
					<td style="">: {{ get_rombel_byid($data['students'][$i]['nisn'])->nama }}</td>
				</tr>
				<tr>
					<td colspan="2"><br></td>
				</tr>
				<tr>
					<td colspan="2">
						Mempunyai tanggungan hingga bulan {{ $data['students'][$i]['bulanTunggakan'] }} sebagai berikut:
						<table style="margin-left: 40px;margin-top: 5px;border-collapse: collapse;">
							<tr>
								<td style="border:1px solid black;width: 20px;"><center>No</center></td>
								<td style="border:1px solid black;width: 150px;"><center>Iuran</center></td>
								<td style="border:1px solid black;width: 100px;"><center>Jumlah</center></td>
							</tr>
							<tr>
								<td style="border:1px solid black;"><center>1</center></td>
								<td style="border:1px solid black;padding:3px;">Spp</td>
								<td style="border:1px solid black;padding:3px;"><span style="text-align: left;">Rp.</span> <span style="text-align: right;">
								{{ number_format($data['students'][$i]['tunggakan']['spp'],2,',','.') }}</span></td>
							</tr>
							<tr>
								<td style="border:1px solid black;"><center>2</center></td>
								<td style="border:1px solid black;padding:3px;">Pembangunan</td>
								<td style="border:1px solid black;padding:3px;"><span style="text-align: left;">Rp.</span> <span style="text-align: right;">
								{{ number_format($data['students'][$i]['tunggakan']['pembangunan'],2,',','.') }}</span></td>
							</tr>
							<tr>
								<td style="border:1px solid black;"><center>3</center></td>
								<td style="border:1px solid black;padding:3px;">Pramuka</td>
								<td style="border:1px solid black;padding:3px;"><span style="text-align: left;">Rp.</span> <span style="text-align: right;">
								{{ number_format($data['students'][$i]['tunggakan']['pramuka'],2,',','.') }}</span></td>
							</tr>
							<tr>
								<td style="border:1px solid black;" colspan="2"><center>TOTAL</center></td>
								<td style="border:1px solid black;padding:3px;"><span style="text-align: left;">Rp.</span> <span style="text-align: right;">
								{{ number_format($data['students'][$i]['tunggakan']['total'],2,',','.') }}</span></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><br></td>
				</tr>
				<tr>
					<td colspan="2">
						Data diatas belum termasuk tunggakan uji kompetensi dan prakerin (jika ada). Untuk kelancaran proses administrasi, agar sekiranya bapak/ibu wali murid siswa yang bersangkutan untuk dapat melunasi tunggakan. Selambat-lambatnya tanggal {{ $data['batasBayar'] }}. <br>
						Demikian surat pemberitahuan ini dibuat agar dapat digunakan seperlunya.
					</td>
				</tr>
				<tr>
					<td colspan="2"><br></td>
				</tr>
				<tr>
					<table style="width: 100%">
						<tr>
							<td style="width: 70%"></td>
							<td style="width: 30%">
								Payakumbuh, {{ $data['tanggalSurat'] }}<br>
								<span style="margin-left: 20px">Tertanda</span><br>
								Staff Bendahara Komite<br><br><br><br>
								{{ $data['suratOleh'] }}<br>
								Nip : 
							</td>
						</tr>
					</table>
				</tr>
			</table>
			</td>
		</tr>
	</table>

<div class="page-break"></div>

<?php } ?>

</body>
</html>



