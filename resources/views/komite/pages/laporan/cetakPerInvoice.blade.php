@extends('komite.base')

@section('title', 'Cetak Laporan Keuangan')

@section('content')
	<ol class="breadcrumb">
	    <li class=""><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li class=""><a href="{{ url('komite/laporan/invoice') }}">Bukti Pembayaran</a></li>
	    <li>Cetak Laporan Keuangan</li>
	</ol>
	<div class="container-fluid">
		<div class="row">
			@if (Session::has('error'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if (Session::has('success'))
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-success">
						<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			@if ($errors->has())
				<div class="col-md-12">
					<div class="alert alert-dismissable alert-danger">
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
				</div>
			@endif
			<div class="col-md-12">
				<div class="panel panel-blue" >
					<div class="panel-heading">
						<h2>Form Cetak Laporan Keuangan</h2>
					</div>
					<div class="panel-body">
						<center><h1>Cetak Laporan Pembayaran & Tunggakan <i class="glyphicon glyphicon-print"></i></h1></center><hr><br>

						<form class="form-horizontal" action="{{ url('komite/laporan/invoice/cetak') }}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group" id='typeAdd'>
						    <label class="control-label col-sm-2" for="type">Pilih Lingkup Cetak</label>
						    <div class="col-sm-3">
						      <select name="type" id="type" class="form-control"> 
									<option value='none' disabled selected>None</option>
									<option value='semua'>Seluruh Siswa</option>
									<option value='perjurusan'>Perjurusan</option>
									<option value='perkelas'>Perkelas</option>
									<!-- <option value='persiswa'>Persiswa</option> -->
								</select>
						    </div>
						</div>
						<div class="form-group">
						    <label class="control-label col-sm-2">Pilih Jenis Pembayaran</label>
						    <div class="col-sm-3">
								<select name="jenisPembayaran" class="form-control">
									<option value="tu">Tunai</option>
									<option value="al">Alumni</option>
									<option value="be">Beasiswa</option>
									<option value="sb">Siswa Baru</option>
									<option value="pn">Penyesuaian</option>               			
								</select>
							</div>
						</div>
						  <div class="form-group" id='waktuAdd'>
						    <label class="control-label col-sm-2" for="waktu">Jangka Waktu</label>
						    <div class="col-sm-3">
						      <select name="waktu" id="waktu" class="form-control">
										<option value='none' disabled selected>None</option>
											<option value='tahunan'>Tahunan</option>
											<option value='bulanan'>Bulanan</option>
											<option value='rentang'>Rentang Waktu</option>
											<option value='harian'>Harian</option>
										</select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-sm-2" for="staff">Pilih Staff</label>
						    <div class="col-sm-3">
						      <select name="staff" class="form-control">
											<option value='semua'>keseluruhan</option>
											<?php
											$users = App\Models\Customer::select('customer_id', 'customer_name')->get();//dd($users);
											foreach ($users as $user) {
												echo "<option value='$user->customer_id' class='form-control'>TU - $user->customer_name </option>";
											}
											$users2 = App\Models\Komite::select('komite_id', 'komite_name')->get();//dd($users);
											foreach ($users2 as $user) {
												echo "<option value='$user->komite_id' class='form-control'> KOMITE - $user->komite_name </option>";
											}
											?>
										</select>
						  </div>
						  </div>
						  <div class="form-group" id='typeLap'>
						    <label class="control-label col-sm-2" for="type">Pilih Keluaran</label>
						    <div class="col-sm-3">
						      <select name="typeLap" id="typeLap" class="form-control"> 
									<option value='none' disabled selected>None</option>
									<option value='pembayaran'>Pembayaran</option>
									<!-- <option value='tunggakan'>Tunggakan</option> -->
								</select>
						    </div>
						  </div>
						  <div class="form-group">
						    <label class="control-label col-sm-2"></label>
						    <div class="col-sm-3">
						      <button type="submit" class="btn btn-primary form-control"><i class="fa fa-print"></i> Submit</button>
						  </div>
						</form>
					</div>
					<!-- ./End panel body -->
				</div>
			</div>
		</div>
	</div>
@endsection

@section('page-styles')
 <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@endsection

@section('page-scripts')
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@endsection

@section('inline-script')
<script type="text/javascript">
	//untuk filter laporan
	$('#type').on('change', function(e) {
		var value = $('#type').val();
		var type = $('#type').attr('name');
            $.ajax({
              url: '{{ url('komite/laporan/ajax/') }}',
              data: {
					_token: '{{ csrf_token() }}',
					type: type,
					value: value
				},
              type: "POST",
              dataType: 'json',
              success: function(response){
              	$('.added').remove();
                $('#typeAdd').after(response);
              },
              error: function(jqXHR, textStatus, errorThrown){
                console.log( jqXHR);
                console.log( textStatus);
                console.log( errorThrown);
              }
            });      
        });

	$('#waktu').on('change', function(e) {
		var value = $('#waktu').val();
		var type = $('#waktu').attr('name');
            $.ajax({
              url: '{{ url('komite/laporan/ajax/') }}',
              data: {
					_token: '{{ csrf_token() }}',
					type: type,
					value: value
				},
              type: "POST",
              dataType: 'json',
              success: function(response){
              	$('.added-waktu').remove();
                $('#waktuAdd').after(response);
              },
              error: function(jqXHR, textStatus, errorThrown){
                console.log( jqXHR);
                console.log( textStatus);
                console.log( errorThrown);
              }
            });      
        });

	$(function(){
		$('input[name="waktu_value_harian"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="waktu_value_rentang"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="waktu_value_rentang"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="waktu_value_bulanan"]').datepicker({format: 'm', autoclose:true,  viewMode: "months", minViewMode: "months" });
		$('input[name="waktu_value_tahunan"]').datepicker({format: 'yyyy', autoclose:true,  viewMode: "years", minViewMode: "years" });
		
	});
</script>
@endsection

@section('inline-style')
<style type="text/css">
	.added, .added-waktu{
		color:blue;
	}
</style>
@stop