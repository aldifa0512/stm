@extends('tu-user.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('tu-user') }}">Dashboard</a></li>
	    <li class="active"><span>Siswa</span></li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Daftar Siswa (Recap)</h2>
							<div class="panel-ctrls">
								{!! $students->render() !!}
							</div>
						</div>
						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-hover">
								<thead>
									<tr>
										<th width="80"><a href="{{ url('tu-user/students/recap/orderby/nama_lengkap/ASC') }}">Nama Lengkap</a></th>
										<th width="100"><a href="">Foto</a></th>
										<th width="100"><a href="{{ url('tu-user/students/recap/orderby/nisn/ASC') }}">NISN</a></th>
										<th width="100"><a href="{{ url('tu-user/students/recap/orderby/tanggal_lahir/ASC') }}">Tanggal Lahir</a></th>
										<th width="100"><a href="{{ url('tu-user/students/recap/orderby/nama_ibu/ASC') }}">Nama Ibu</a></th>
										<th width="100"><a href="{{ url('tu-user/students/recap/orderby/jenis_pendaftaran/ASC') }}">Jenis Pendaftaran</a></th>
										<th width="100"><a href="{{ url('tu-user/students/recap/orderby/jurusan/ASC') }}">Jurusan</a></th>
										<th width="100"><a href="{{ url('tu-user/students/recap/orderby/kelas/ASC') }}">Kelas</a></th>
										<th width="50"><a href="">Action</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($students as $student)
									<?php 
										$assets = json_decode($student->pas_foto);//var_dump($assets);die();
										$filename = substr($assets[0], strrpos($assets[0], '/') + 1);
										$img_entry = str_replace($filename, 'thumb-admin-'.$filename, $assets[0]);
									?>
									<tr>
										<td class="text-center">{{ $student->nama_lengkap }}</td>
										<td class="text-center">
											<?php// if ($img_entry != ''): ?>
												<img class="img-thumbnail" src="{{ asset($img_entry) }}" width="70" height="70">
											<?php// endif ?>
										</td>
										<td><a href="{{ url('tu-user/students/edit', $student->id) }}">{{ $student->nisn }}</a></td>
										<td>
											<?php echo $student->tanggal_lahir ?>
											
										</td>
										<td>{{ $student->nama_ibu }}</td>
										<td>{{ $student->jenis_pendaftaran }}</td>
										<td>
											<?php echo $student->jurusan ?>
												
										</td>
										<td>
											<?php echo $student->kelas ?>
										</td>
										<td>
	                                        <a href="{{ url('tu-user/students/edit', $student->id) }}" class="btn btn-primary-alt btn-sm"><i class="ti ti-pencil"></i>&nbsp;Edit</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							{!! $students->render() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop