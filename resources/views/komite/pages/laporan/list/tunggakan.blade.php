@extends('komite.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li>Batas Tunggakan</li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<!-- <div class="panel panel-default">
						<div class="panel-heading">
							<h2 style='color:blue;'>Setting Tunggakan Dari Awal Tahun Masuk</h2>
						</div>

						<div class="panel-body">
							<form action="{{ url('komite/laporan/tunggakan/') }}" method="post" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<table id="listings-list" class="table table-striped table-bordered">
								<thead>
									<tr class="success">
										<th  style="text-align: center; vertical-align: middle;"><a href="">SPP</a></th>
										<th  style="text-align: center; vertical-align: middle;"><a href="">Pembangunan</a></th>
										<th  style="text-align: center; vertical-align: middle;"><a href="">Pramuka</a></th>
										<th  style="text-align: center; vertical-align: middle;"><a href="">Lain-lain</a></th>
										<th  style="text-align: center; vertical-align: middle;"><a href="">Action</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									<tr>
										<td>
											<select name="thn_spp" class="form-control">
												<?php
													echo "<option value='0'/>Not defined</option>";
													foreach ($tp as $value) {
														if($tungg->thn_spp == $value->id){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$value->id."' ".$selected."/>".$value->tp."</option>";
												}
												?>
											</select>
										</td>
										<td>
											<select name="thn_pembangunan" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($tp as $value) {
														if($tungg->thn_pembangunan == $value->id){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$value->id."' ".$selected."/>".$value->tp."</option>";
													}
												?>
											</select>
										</td>
										<td>
											<select name="thn_pramuka" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($tp as $value) {
														if($tungg->thn_pramuka == $value->id){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$value->id."' ".$selected."/>".$value->tp."</option>";
													}
												?>
											</select>
										</td>
										<td>
											<select name="thn_lain2" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($tp as $value) {
														if($tungg->thn_lain2 == $value->id){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$value->id."' ".$selected."/>".$value->tp."</option>";
													}
												?>
											</select>
										</td>
										<td style="text-align: center;" rowspan="2">
	                                        <button class="btn btn-primary"><i class="ti ti-check"></i>&nbsp;Save</button>
										</td>
									</tr>
									<tr>
											<input value='15' type="hidden" name="tungg_spp" class="form-control">
											<input value='15' type="hidden" name="tungg_pembangunan" class="form-control">
											<input value='15' type="hidden" name="tungg_pramuka" class="form-control">
											<input value='15' type="hidden" name="tungg_lain2" class="form-control">
										<td><select disabled name="tungg_spp" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tungg->spp == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
												}
												?>
											</select>
										</td>
										<td>
											<select disabled name="tungg_pembangunan" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tungg->pembangunan == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
													}
												?>
											</select>
										</td>
										<td>
											<select disabled name="tungg_pramuka" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tungg->pramuka == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
													}
												?>
											</select>
										</td>
										<td>
											<select disabled name="tungg_lain2" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tungg->lain2 == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
													}
												?>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							</form>
						</div>

						<div class="panel-footer text-right">
							
						</div>
					</div> -->

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 style='color:blue;'>Daftar Biaya Sekolah Dan Batas Bulan Tunggakannya</h2>
						</div>

						<div class="panel-body">
							<table id="listings-list" class="table table-striped table-bordered">
								<thead>
									<tr class="success">
										<th  style="text-align: center; vertical-align: middle;" rowspan="2"><a href="">TP</a></th>
										<th  style="text-align: center; vertical-align: middle;" rowspan="2"><a href="">SPP</a></th>
										<th  style="text-align: center; vertical-align: middle;" rowspan="2"><a href="">Pembangunan</a></th>
										<th  style="text-align: center; vertical-align: middle;" rowspan="2"><a href="">Pramuka</a></th>
										<th  style="text-align: center; vertical-align: middle;" rowspan="2"><a href="">Lain-lain</a></th>
										<th  style="text-align: center; vertical-align: middle;" colspan="4"><a href="">Batas Tunggakan</a></th>
										<th  style="text-align: center; vertical-align: middle;" rowspan="2"><a href="">Action</a></th>
									</tr>
									<tr class="success">
										<th style="text-align: center; vertical-align: middle;" ><a href="">SPP</a></th>
										<th style="text-align: center; vertical-align: middle;" ><a href="">Pembangunan</a></th>
										<th style="text-align: center; vertical-align: middle;" ><a href="">Pramuka</a></th>
										<th style="text-align: center; vertical-align: middle;" ><a href="">Lain-lain</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($tunggakan as $row)
									<tr>
										<td>{{$row->tp}}/{{$row->tp+1}}</td>
										<td>{{number_format($row->spp,2,',','.')}}</td>
										<td>{{number_format($row->pembangunan,2,',','.')}}</td>
										<td>{{number_format($row->pramuka,2,',','.')}}</td>
										<td>{{number_format($row->lain_lain,2,',','.')}}</td>
										<td>{{$row->batas_bln_spp == 0 ?'Not Defined' : $bln[$row->batas_bln_spp]}}</td>
										<td>{{$row->batas_bln_pembangunan == 0 ?'Not Defined' : $bln[$row->batas_bln_pembangunan]}}</td>
										<td>{{$row->batas_bln_pramuka == 0 ?'Not Defined' : $bln[$row->batas_bln_pramuka]}}</td>
										<td>{{$row->batas_bln_lain2 == 0 ?'Not Defined' : $bln[$row->batas_bln_lain2]}}</td>
										<td class="row">
	                                        <a href="{{ url('komite/laporan/tunggakan/edit', $row->id) }}" class="btn btn-primary-alt btn-sm" title="Lakukan Pembayaran"><i class="ti ti-pencil"></i>&nbsp;Edit</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>

						<div class="panel-footer text-right">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
	
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop