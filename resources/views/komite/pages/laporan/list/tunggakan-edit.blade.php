@extends('komite.base')

@section('title', 'Daftar Siswa Aktif')

@section('content')
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li><a href="{{ url('komite/laporan/tunggakan/') }}">Batas Tunggakan</a></li>
	    <li>{{$tunggakan->tp}}/{{$tunggakan->tp+1}}</li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="tunggakan">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				<div class="col-md-12">
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 style='color:blue;'>Daftar Biaya Sekolah {{$tunggakan->tp}}/{{$tunggakan->tp+1}}</h2>
						</div>

						<div class="panel-body">
							<form action="{{ url('komite/laporan/tunggakan/edit', $tunggakan->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="id" class="form-control" value="{{ $tunggakan->id }}" >
							<table id="listings-list" class="table table-striped table-bordered">
								<thead>
									<tr class="success">
										<th  rowspan="2" style="text-align: center; vertical-align: middle;" tunggakanspan="2"><a href="">TP</a></th>
										<th  rowspan="2" style="text-align: center; vertical-align: middle;" tunggakanspan="2"><a href="">SPP</a></th>
										<th  rowspan="2" style="text-align: center; vertical-align: middle;" tunggakanspan="2"><a href="">Pembangunan</a></th>
										<th  rowspan="2" style="text-align: center; vertical-align: middle;" tunggakanspan="2"><a href="">Pramuka</a></th>
										<th  rowspan="2" style="text-align: center; vertical-align: middle;" tunggakanspan="2"><a href="">Lain-lain</a></th>
										<th  style="text-align: center; vertical-align: middle;" colspan="4"><a href="">Batas Tunggakan</a></th>
										<th  rowspan="2" style="text-align: center; vertical-align: middle;" tunggakanspan="2"><a href="">Action</a></th>
									</tr>
									<tr class="success">
										<th style="text-align: center; vertical-align: middle;" ><a href="">SPP</a></th>
										<th style="text-align: center; vertical-align: middle;" ><a href="">Pembangunan</a></th>
										<th style="text-align: center; vertical-align: middle;" ><a href="">Pramuka</a></th>
										<th style="text-align: center; vertical-align: middle;" ><a href="">Lain-lain</a></th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									<tr>
										<td class="danger">{{$tunggakan->tp}}/{{$tunggakan->tp+1}}</td>
										<td class="warning">{{$tunggakan->spp}}</td>
										<td class="warning">{{$tunggakan->pembangunan}}</td>
										<td class="warning">{{$tunggakan->pramuka}}</td>
										<td class="warning">{{$tunggakan->lain_lain}}</td>
										<td>
											<select name="tungg_spp" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tunggakan->batas_bln_spp == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
												}
												?>
											</select>
										</td>
										<td>
											<select name="tungg_pembangunan" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tunggakan->batas_bln_pembangunan == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
													}
												?>
											</select>
										</td>
										<td>
											<select name="tungg_pramuka" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tunggakan->batas_bln_pramuka == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
													}
												?>
											</select>
										</td>
										<td>
											<select name="tungg_lain2" class="form-control">
												<?php 
													echo "<option value='0'/>Not defined</option>";
													foreach ($bln as $key => $value) {
														if($tunggakan->batas_bln_lain2 == $key){
															$selected = "selected";
														}else{ $selected = " ";}
														echo "<option value='".$key."' ".$selected."/>".$value."</option>";
													}
												?>
											</select>
										</td>
										<td class="tunggakan">
	                                        <button class="btn btn-primary"><i class="ti ti-check"></i>&nbsp;Save</button>
										</td>
									</tr>
								</tbody>
							</table>
							</form>
						</div>

						<div class="panel-footer text-right">
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
	
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop