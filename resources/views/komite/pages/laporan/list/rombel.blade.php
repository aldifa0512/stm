@extends('komite.base')

@section('title', 'Laporan Rombongan Belajar')

@section('content')
	<ol class="breadcrumb">
		<li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class="">Rombongan Belajar</li>
	</ol>
	<div class="container-fluid">
		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
			@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 style="color:blue;">Daftar Rombel</h2>
							<div class="panel-ctrls"></div>
						</div>
						<div class="panel-body">
							<table id="listings-list" class="table table-striped table table-bordered">
								<thead>
									<tr class="danger">
										<th>No</th>
										<th>Nama Rombel</th>
										<th>Jurusan</th>
										<th>Kurikulum</th>
										<th>Pengajar</th>
										<th width="80">Jumlah Siswa</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php
                                    	$no = 1;
                                        foreach ($rombels as $rombel){
								            echo "<tr>";
                                        	echo "<td>$no</td>";
								            echo "<td> $rombel->nama </td>";
											$jurusan = App\Models\Jurusan::where('id', $rombel->jurusan_id)->first()->toArray();//dd($jurusan);
											echo "<td>". $jurusan['nama_jurusan']. "</td>";
								            echo "<td> $rombel->kurikulum </td>";            
								            echo "<td> $rombel->pengajar </td>";
											$jml_siswa = App\Models\Student::where('kelas', $rombel->id)->where('status', 'aktif')->count();
											echo "<td>". $jml_siswa. " org</td>";
								    ?>
											<td>
												<div class="btn-group">
			                                        <button type="button" class="btn btn-midnightblue-alt dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			                                            <i class="ti ti-agenda"></i> <span class="caret"></span>
			                                        </button>
			                                        <ul class="dropdown-menu" role="menu">
			                                            <li><a href="{{ url('komite/laporan/rombel/lengkap', $rombel->id) }}">Laporan Lengkap</a></li>
			                                            <li><a href="{{ url('komite/laporan/rombel/tunggakan', $rombel->id) }}">Laporan Tunggakan</a></li>
			                                        </ul>
			                                    </div>
											</td>
											</tr>
									<?php    
                                        	$no++;
								        }
                                    ?>	
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
@endsection

@section('page-styles')
@endsection

@section('page-scripts')
	<!-- Load page level scripts-->
@endsection

@section('inline-script')
	<script type="text/javascript">
	$(function(){
	});
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
</style>
@stop
