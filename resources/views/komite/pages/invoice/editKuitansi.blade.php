@extends('komite.base')

@section('title', 'Edit Pembayaran')

@section('content')
	<h3 class="page-title">Edit Pembayaran</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
	    <li><a href="{{ url('komite/laporan/invoice') }}">Bukti Pembayaran</a></li>
	    <li>Edit Pembayaran</li>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-primary" data-widget='{"draggable": "false"}' >
					<div class="panel-heading">
						<h2>No. Kuitansi: {{ $data['kuitansi']['no_kwitansi'] }} </h2>
					</div>

					<div class="panel-body">
						<button id="tatacara" >Toogle Tata Cara</button>
						<div class="" id="konten" style="z-index: -1000">
									Keterangan : 
									<ul>
										<li>Bulan januari diwakili dengan angka 4</li>
										<li>Bulan Juni diwakili dengan angka 9</li>
										<li>Bulan Juli diwakili dengan angka 10</li>
										<li>Bulan Desember diwakili dengan angka 15</li>
										<li>Semster I dumulai dari bulan Juli</li>
										<li>Urutan pembayaran dari semester 1 sampai selesai semster 2 sbg berikut : [4,5,6,7,8,9,10,11,12,13,14,15]</li>
									</ul>
									<hr>
								</div>
						<form action="{{ url('komite/laporan/invoice/edit/'.$data['kuitansi']['no_kwitansi']) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<div style="overflow-x:auto;">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<h3>Data Kuitansi</h3> 
							  	<table class="table table-bordered">
							    	<tr>
							    		<?php foreach ($data['kuitansi'] as $key => $value) { ?>
								    		<th class="header-th">{{ $key }}</th>	
								    	<?php } ?>	
							    	</tr>
							    	<tr>
								    	<?php $readonly = ['id', 'jurusan_id', 'kelas_id', 'nama', 'nisn', 'no_kwitansi', 'tgl_bayar', 'bln_bayar', 'thn_bayar', 'tp', 'semester', 'kelas', 'note', 'note2', 'oleh_id', 'oleh', 'updated_at', 'created_at']; ?>
								    	<?php foreach ($data['kuitansi'] as $key => $value) { ?>
								    		<?php 
								    			$ron = '';
									    		foreach ($readonly as $ro) { 
									    			if($key == $ro){
									    				$ron = 'readonly';
									    			}
									    		} 
								    		?>
								    		<td><input type="text" name="k-{{ $key }}" value="{{ $value }}" {{ $ron }} ></td>	
								    	<?php } ?>
								    </tr>
							  	</table>
							</div>	
							<hr>

							<div style="overflow-x:auto;">
								<h3>Data SPP</h3>
							  	<table class="table table-bordered">
							    	<tr>
							    		<?php foreach ($data['spp'] as $key => $value) { ?>
								    		<th class="header-th">{{ $key }}</th>	
								    	<?php } ?>	
							    	</tr>
							    	<tr>
							    		<?php $readonly = ['id', 'nisn', 'kelas', 'tp', 'updated_at', 'created_at']; ?>
								    	<?php foreach ($data['spp'] as $key => $value) { ?>
								    		<?php 
								    			$ron = '';
									    		foreach ($readonly as $ro) { 
									    			if($key == $ro){
									    				$ron = 'readonly';
									    			}
									    		} 
								    		?>
								    		<td><input type="text" name="s-{{ $key }}" value="{{ $value }}" {{ $ron }}></td>	
								    	<?php } ?>
								    </tr>
							  	</table>
							</div>	
							<hr>

							<div style="overflow-x:auto;">
								<h3>Data Pembangunan</h3>
							  	<table class="table table-bordered">
							    	<tr>
							    		<?php foreach ($data['pembangunan'] as $key => $value) { ?>
								    		<th class="header-th">{{ $key }}</th>	
								    	<?php } ?>	
							    	</tr>
							    	<tr>
								    	<?php $readonly = ['id', 'nisn', 'kelas', 'tp', 'updated_at', 'created_at']; ?>
								    	<?php foreach ($data['pembangunan'] as $key => $value) { ?>
								    		<?php 
								    			$ron = '';
									    		foreach ($readonly as $ro) { 
									    			if($key == $ro){
									    				$ron = 'readonly';
									    			}
									    		} 
								    		?>
								    		<td><input type="text" name="pe-{{ $key }}" value="{{ $value }}" {{ $ron }}></td>	
								    	<?php } ?>
								    </tr>
							  	</table>
							</div>	
							<hr>

							<div style="overflow-x:auto;">
								<h3>Data Pramuka</h3>
							  	<table class="table table-bordered">
							    	<tr>
							    		<?php foreach ($data['pramuka'] as $key => $value) { ?>
								    		<th class="header-th">{{ $key }}</th>	
								    	<?php } ?>	
							    	</tr>
							    	<tr>
								    	<?php $readonly = ['id', 'nisn', 'kelas', 'tp', 'updated_at', 'created_at']; ?>
								    	<?php foreach ($data['pramuka'] as $key => $value) { ?>
								    		<?php 
								    			$ron = '';
									    		foreach ($readonly as $ro) { 
									    			if($key == $ro){
									    				$ron = 'readonly';
									    			}
									    		} 
								    		?>
								    		<td><input type="text" name="pr-{{ $key }}" value="{{ $value }}" {{ $ron }}></td>	
								    	<?php } ?>
								    </tr>
							  	</table>
							</div>	
							<hr>

							<div style="overflow-x:auto;">
								<h3>Data Lain-Lain</h3>
							  	<table class="table table-bordered">
							    	<tr>
							    		<?php foreach ($data['lain2'] as $key => $value) { ?>
								    		<th class="header-th">{{ $key }}</th>	
								    	<?php } ?>	
							    	</tr>
							    	<tr>
								    	<?php $readonly = ['id', 'nisn', 'kelas', 'tp', 'updated_at', 'created_at']; ?>
								    	<?php foreach ($data['lain2'] as $key => $value) { ?>
								    		<?php 
								    			$ron = '';
									    		foreach ($readonly as $ro) { 
									    			if($key == $ro){
									    				$ron = 'readonly';
									    			}
									    		} 
								    		?>
								    		<td><input type="text" name="l-{{ $key }}" value="{{ $value }}" {{ $ron }}></td>	
								    	<?php } ?>
								    </tr>
							  	</table>
							</div>	
							<hr>
							<h4>
								Hapus Kuitansi <input type="checkbox" name="hapus" style="">
							</h4>
							<center>
								<button type="submit" class="btn btn-primary btn-lg active">Simpan Perubahan</button>
							</center>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
<script type="text/javascript">
	// Extend the default Number object with a formatMoney() method:
	// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
	// defaults: (2, "$", ",", ".")
	Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
		places = !isNaN(places = Math.abs(places)) ? places : 2;
		symbol = symbol !== undefined ? symbol : "$";
		thousand = thousand || ",";
		decimal = decimal || ".";
		var number = this, 
		    negative = number < 0 ? "-" : "",
		    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
		    j = (j = i.length) > 3 ? j % 3 : 0;
		return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
	};

	//total biaya two ways binding
	$('#spp, #pembangunan, #pramuka, #lain2').on("keypress keyup",function(){
	  var spp = $("#spp").val();
	  var pembangunan = $("#pembangunan").val();
	  var pramuka = $("#pramuka").val();
	  var lain2 = $("#lain2").val();
	  totalBiaya = Number(spp) + Number(pembangunan) + Number(pramuka) + Number(lain2);
	  $("#total").val(totalBiaya);
	  $("#total-disabled").val(totalBiaya.formatMoney(0, "Rp "));
	});
</script>

<script>
$(document).ready(function(){
    $("#tatacara").click(function(){
        $("#konten").toggle();
    });
});
</script>

@endsection

@section('inline-style')
<style type="text/css">
	th, td { min-width: 150px; }
	.header-th{
		background-color: #e6ebf4;
	}
</style>
@stop