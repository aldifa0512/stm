@extends('komite.base')

@section('title', 'Beasiswa')

@section('content')
	<?php
		$sme = $tp->semester;
	?>
	<h3 class="page-title">Beasiswa</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
		<?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li><a href="{{ url('komite/students/rombel/lihat/'. $student->kelas) }}">Kelas {{$rombel['nama']}}</a></li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
			<script type="text/javascript">
			function openInvoice() {
			    window.open("{{ url('komite/students/spp/invoice/'.$student->nisn.'/'.$tp['tp'].'/'.$tp['semester'].'/'.Session::get('no_kuitansi')) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=900,height=500");
			}
				openInvoice();
			</script>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				@if (tunggakan_total_tahun_aktif($student->nisn) == 0)
				<div class="alert alert-dismissable alert-success">
					<span><strong>Biaya sekolah pada tahun aktif telah lunas.</strong></span>
				</div>
				@endif

				<div class="panel panel-gray" data-widget='{"draggable": "false"}' >
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Beasiswa</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<form action="{{ url('komite/laporan/penyesuaian/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="proses_beasiswa" value="1">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<?php
										$size1 = 10/2;
										$size2 = 10/3;
										$style = 'width=15%';
										$style2 = 'width=35%';
									?>
									<td {{$style}}>Nama Lengkap</td>
									<td {{$style2}}><b>{{ $student->nama_lengkap }} / <u><a href="" onclick="openHistori()">Histori Pembayaran</a></u></b></td>
									<td {{$style}}>Kelas</td>
									<td {{$style2}}>{{ $rombel['nama'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>NISN</td>
									<td {{$style2}}>{{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }}</td>
									<td {{$style}}>Wali Kelas</td>
									<td {{$style2}}>{{ $rombel['pengajar'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>TP</td>
									<td {{$style2}}>{{ $tp['tp'] }}/{{ ($tp['tp']+1) }}</td>
									<td {{$style}}>Tanggal Bayar</td>
									<td {{$style2}}><div ">
										<input type="text" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-md" disabled></td>
										<input type="hidden" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-sm" required></td>
									</div>
								</tr>
							</table>
					
							<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}" >
							<input type="hidden" name="kelas" class="form-control" value="{{ $student->kelas }}" >
							<input type="hidden" size="50" name="tp" class="form-control" value="{{ $tp['tp'] }}" >

							<center><h4>DATA PEMBAYARAN SISWA PADA {{ $tp['tp'] }}/{{ ($tp['tp']+1) }}</h4></center>
							<div style="overflow-x:auto;">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="success">
                                        <th class="col-md-3"><a href=""></a></th>
                                        <th class="col-md-3"><a href="">NISN</a></th>
                                        <th class="col-md-3"><a href="">KELAS</a></th>
                                        <th class="col-md-3"><a href="">TP</a></th>
                                        <th class="col-md-3"><a href="">JANUARI</a></th>
                                        <th class="col-md-3"><a href="">FEBRUARI</a></th>
                                        <th class="col-md-3"><a href="">MARET</a></th>
										<th class="col-md-3"><a href="">APRIL</a></th>
										<th class="col-md-3"><a href="">MEI</a></th>
                                        <th class="col-md-3"><a href="">JUNI</a></th>
                                        <th class="col-md-3"><a href="">JULI</a></th>
                                        <th class="col-md-3"><a href="">AGUSTUS</a></th>
                                        <th class="col-md-3"><a href="">SEPTEMBER</a></th>
                                        <th class="col-md-3"><a href="">OKTOBER</a></th>
                                        <th class="col-md-3"><a href="">NOVEMBER</a></th>
                                        <th class="col-md-3"><a href="">DESEMBER</a></th>
                                        <th class="col-md-3"><a href="">TOTAL</a></th>
                                        <th style="width: 150px;" class="col-md-3"><a href="">CREATED AT</a></th>
										<th style="width: 150px;" class="col-md-3"><a href="">UPDATED AT</a></th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$i=1; 
								?>
									@foreach ($detail_bayar as $key => $value)
									<tr>
										<?php
										switch($key){
											case 'spp':
												echo '<td>SPP</td>';
												break;
											case 'pembangunan':
												echo '<td>PEMBANGUNAN</td>';
												break;
											case 'pramuka':
												echo '<td>PRAMUKA</td>';
												break;
											case 'lain2':
												echo '<td>LAIN-LAIN</td>';
												break;
										}
										?>
										<td>{{ $value->nisn == null ? str_pad($student->nisn, 10, '0', STR_PAD_LEFT) : str_pad($value->nisn, 10, '0', STR_PAD_LEFT) }}</td>
                                        <td>{{ $rombel['nama'] }}</td>
                                        <td>{{ $value->tp == null ? $tp['tp'] .'/'. ($tp['tp']+1) : $value->tp .'/'. ($value->tp+1) }}</td>
                                        <?php $bea_name ='bea-'; ?>
                                        <td>{{ $value->januari == -1 ? $bea_name : $value->januari }}</td>
                                        <td>{{ $value->februari == -1 ? $bea_name : $value->februari }}</td>
										<td>{{ $value->maret == -1 ? $bea_name : $value->maret }}</td>
                                        <td>{{ $value->april == -1 ? $bea_name : $value->april }}</td>
                                        <td>{{ $value->mei == -1 ? $bea_name : $value->mei }}</td>
                                        <td>{{ $value->juni == -1 ? $bea_name : $value->juni }}</td>
                                        <td class="warning">{{ $value->juli == -1 ? $bea_name : $value->juli }}</td>
                                        <td class="warning">{{ $value->agustus == -1 ? $bea_name : $value->agustus }}</td>
                                        <td class="warning">{{ $value->september == -1 ? $bea_name : $value->september }}</td>
                                        <td class="warning">{{ $value->oktober == -1 ? $bea_name : $value->oktober }}</td>
                                        <td class="warning">{{ $value->november == -1 ? $bea_name : $value->november }}</td>
                                        <td class="warning">{{ $value->desember == -1 ? $bea_name : $value->desember }}</td>
                                        <td>{{ number_format($value->total,2,',','.') }}</td>
                                        <td>{{ $value->created_at }}</td>
										<td>{{ $value->updated_at }}</td>
									</tr>
									@endforeach
								</tbody>                                
							</table>
                            </div>
                            <div style="overflow-x:auto;">
							<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tbody>
									<tr>
										<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">JENIS IURAN</th>
										<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">BESARAN</th>
										<th class="warning" style="vertical-align: middle;text-align: center;">TUNGGAKAN TP {{"-1"}}</th>
										<th class="warning" style="vertical-align: middle;text-align: center;">TUNGGAKAN TP AKTIF</th>
									</tr>
									<tr>
										<th class="warning" style="vertical-align: middle;text-align: center;">Juni - {{$thn_tunggakan-1}}/{{$thn_tunggakan}}</th>
										<th class="warning" style="vertical-align: middle;text-align: center;">Juni - {{$thn_tunggakan}}/{{$thn_tunggakan+1}}</th>
									</tr>
									<tr>
										<td>SPP</td>
										<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->spp,2,',','.') }}</span></td>
										<td>Rp. {{number_format($tunggakan_thn['spp'],2,',','.')}}</td>
										<td>Rp. {{number_format($tunggakan_bln['spp'],2,',','.')}}</td>
									</tr>
									<tr>
										<td>Pembangunan</td>
										<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->pembangunan,2,',','.') }} / Thn</td>
										<td>Rp. {{number_format($tunggakan_thn['pembangunan'],2,',','.')}}</td>
										<td>Rp. {{number_format($tunggakan_bln['pembangunan'],2,',','.')}}</td>
									</tr>
									<tr>
										<td>Pramuka </td>
										<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->pramuka,2,',','.') }}</span></td>
										<td>Rp. {{number_format($tunggakan_thn['pramuka'],2,',','.')}}</td>
										<td>Rp. {{number_format($tunggakan_bln['pramuka'],2,',','.')}}</td>
									</tr>
									<tr>
										<td>Lain-lain</td>
										<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->lain_lain,2,',','.') }}</td>
										<td>Rp. {{number_format($tunggakan_thn['lain2'],2,',','.')}}</td>
										<td>Rp. {{number_format($tunggakan_bln['lain2'],2,',','.')}}</td>
									</tr>
								</tbody>
							</table>
							</div>
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<td width="70%">
									<h4>Detail beasiswa yang akan diterima siswa (Pelunasan biaya sekolah pada tahun aktif) :</h4>
									1. Untuk pembayaran spp selama <b>{{ $bln_kosong['spp'] }}</b> bulan sebesar <b>Rp. {{ number_format($tunggakan_thn_aktif['spp'],2,',','.') }}</b><br>
									2. Untuk pembayaran pembangunan sebesar <b>Rp. {{ number_format($tunggakan_thn_aktif['pembangunan'],2,',','.') }}</b><br>
									3. Untuk pembayaran pramuka selama <b>{{ $bln_kosong['pramuka'] }}</b> bulan sebesar <b>Rp. {{ number_format($tunggakan_thn_aktif['pramuka'],2,',','.') }}</b><br>
									</td>
									<?php $total_beasiswa = $tunggakan_thn_aktif['spp'] + $tunggakan_thn_aktif['pembangunan'] + $tunggakan_thn_aktif['pramuka'] ?>
									<td width="30%" style="text-align: center;vertical-align: middle;"><h3>Rp. {{number_format($total_beasiswa,2,',','.')}}</h3>
									</td>
								</tr>
							</table>
							<?php
							if(tunggakan_total_tahun_aktif($student->nisn) == 0) 
								$disabled = 'disabled';
							else
								$disabled = '';
							?>
							<center><input type="submit" style="" class="finish btn-success btn-block btn btn-lg" value="Submit!" {{$disabled}}/></center>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@stop



@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
		// Extend the default Number object with a formatMoney() method:
		// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
		// defaults: (2, "$", ",", ".")
		Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
			places = !isNaN(places = Math.abs(places)) ? places : 2;
			symbol = symbol !== undefined ? symbol : "$";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var number = this, 
			    negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
		};

		//total biaya two ways binding
		$('#spp, #pembangunan, #pramuka, #lain2').on("keypress keyup",function(){
		  var spp = $("#spp").val();
		  var pembangunan = $("#pembangunan").val();
		  var pramuka = $("#pramuka").val();
		  var lain2 = $("#lain2").val();
		  totalBiaya = Number(spp) + Number(pembangunan) + Number(pramuka) + Number(lain2);
		  $("#total").val(totalBiaya);
		  $("#total-disabled").val(totalBiaya.formatMoney(0, "Rp "));
		});
	</script>
	<script>
	function openHistori() {
	    window.open("{{ url('komite/students/spp/histori/'.$student->nisn) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=1200,height=500");
	}
	</script>
@endsection

@section('inline-style')

@stop