@extends('komite.base')

@section('title', 'Edit Pembayaran')

@section('content')
	<?php
		$sme = $tp->semester;
	?>
	<h3 class="page-title">Edit Pembayaran</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
		<?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li><a href="{{ url('komite/students/rombel/lihat/'. $student->kelas) }}">Kelas {{$rombel['nama']}}</a></li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
			<script type="text/javascript">
			function openInvoice() {
			    window.open("{{ url('komite/students/spp/invoice/'.$student->nisn.'/'.$tp['tp'].'/'.$tp['semester'].'/'.Session::get('no_kuitansi')) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=900,height=500");
			}
				openInvoice();
			</script>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-gray" data-widget='{"draggable": "false"}' >
					<div class="panel-heading">
						<h2>Form Edit Pembayaran</h2>
					</div>
					<div class="panel-body">
						<form action="{{ url('komite/students/spp/bayar/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<center>
								<h2>Edit Pembayaran TP  {{$tp['tp']}}/{{($tp['tp']+1)}}  </h2><br/>
							</center>
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<?php
										$size1 = 10/2;
										$size2 = 10/3;
										$style = 'width=15%';
										$style2 = 'width=35%';
									?>
									<td {{$style}}>Nama Lengkap</td>
									<td {{$style2}}><b>{{ $student->nama_lengkap }} / <u><a href="" onclick="openHistori()">Histori Pembayaran</a></u></b></td>
									<td {{$style}}>Kelas</td>
									<td {{$style2}}>{{ $rombel['nama'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>NISN</td>
									<td {{$style2}}>{{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }}</td>
									<td {{$style}}>Wali Kelas</td>
									<td {{$style2}}>{{ $rombel['pengajar'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>TP</td>
									<td {{$style2}}>{{ $tp['tp'] }}/{{ ($tp['tp']+1) }}</td>
									<td {{$style}}>Tanggal Bayar</td>
									<td {{$style2}}><div ">
										<input type="text" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-md" disabled></td>
										<input type="hidden" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-sm" required></td>
									</div>
								</tr>
							</table>
							
							<div style="overflow-x:auto;">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="success">
                                        <th class="col-md-3"></th>
                                        <th class="col-md-3">NISN</th>
                                        <th class="col-md-3">KELAS</th>
                                        <th class="col-md-3">TP</th>
                                        <th class="col-md-3">JANUARI</th>
                                        <th class="col-md-3">FEBRUARI</th>
                                        <th class="col-md-3">MARET</th>
										<th class="col-md-3">APRIL</th>
										<th class="col-md-3">MEI</th>
                                        <th class="col-md-3">JUNI</th>
                                        <th class="col-md-3">JULI</th>
                                        <th class="col-md-3">AGUSTUS</th>
                                        <th class="col-md-3">SEPTEMBER</th>
                                        <th class="col-md-3">OKTOBER</th>
                                        <th class="col-md-3">NOVEMBER</th>
                                        <th class="col-md-3">DESEMBER</th>
                                        <th class="col-md-3">TOTAL</th>
                                        <th style="width: 150px;" class="col-md-3"><a href="">CREATED AT</a></th>
										<th style="width: 150px;" class="col-md-3"><a href="">UPDATED AT</a></th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$i=1; 
								?>
									@foreach ($detail_bayar as $key => $value)
									<tr>
										<?php
										switch($key){
											case 'spp':
												echo '<td>SPP</td>';
												break;
											case 'pembangunan':
												echo '<td>PEMBANGUNAN</td>';
												break;
											case 'pramuka':
												echo '<td>PRAMUKA</td>';
												break;
											case 'lain2':
												echo '<td>LAIN-LAIN</td>';
												break;
										}
										?>
										<td>{{ $value->nisn == null ? str_pad($student->nisn, 10, '0', STR_PAD_LEFT) : str_pad($value->nisn, 10, '0', STR_PAD_LEFT) }}</td>
                                        <td>{{ $rombel['nama'] }}</td>
                                        <td>{{ $value->tp == null ? $tp['tp'] .'/'. ($tp['tp']+1) : $value->tp .'/'. ($value->tp+1) }}</td>
                                        <?php $bea_name ='bea-'; ?>
                                        <td><input type="number" name="" value="{{ $value->januari == -1 ? $bea_name : $value->januari }}"></td>
                                        <td><input type="number" name="" value="{{ $value->februari == -1 ? $bea_name : $value->februari }}"></td>
										<td><input type="number" name="" value="{{ $value->maret == -1 ? $bea_name : $value->maret }}"></td>
                                        <td><input type="number" name="" value="{{ $value->april == -1 ? $bea_name : $value->april }}"></td>
                                        <td><input type="number" name="" value="{{ $value->mei == -1 ? $bea_name : $value->mei }}"></td>
                                        <td><input type="number" name="" value="{{ $value->juni == -1 ? $bea_name : $value->juni }}"></td>
                                        <td class="warning"><input type="number" name="" value="{{ $value->juli == -1 ? $bea_name : $value->juli }}"></td>
                                        <td class="warning"><input type="number" name="" value="{{ $value->agustus == -1 ? $bea_name : $value->agustus }}"></td>
                                        <td class="warning"><input type="number" name="" value="{{ $value->september == -1 ? $bea_name : $value->september }}"></td>
                                        <td class="warning"><input type="number" name="" value="{{ $value->oktober == -1 ? $bea_name : $value->oktober }}"></td>
                                        <td class="warning"><input type="number" name="" value="{{ $value->november == -1 ? $bea_name : $value->november }}"></td>
                                        <td class="warning"><input type="number" name="" value="{{ $value->desember == -1 ? $bea_name : $value->desember }}"></td>
                                        <td>{{ number_format($value->total,2,',','.') }}</td>
                                        <td>{{ $value->created_at }}</td>
										<td>{{ $value->updated_at }}</td>
									</tr>
									@endforeach
								</tbody>                                
							</table>
                            </div>
                            <br><br>
							<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}" >
							<input type="hidden" name="kelas" class="form-control" value="{{ $student->kelas }}" >
							<input type="hidden" size="50" name="tp" class="form-control" value="{{ $tp['tp'] }}" >

							<center><input type="submit" style="" class="finish btn-success btn-block btn btn-lg" value="Bayar!" /></center>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@stop



@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
		// Extend the default Number object with a formatMoney() method:
		// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
		// defaults: (2, "$", ",", ".")
		Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
			places = !isNaN(places = Math.abs(places)) ? places : 2;
			symbol = symbol !== undefined ? symbol : "$";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var number = this, 
			    negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
		};

		//total biaya two ways binding
		$('#spp, #pembangunan, #pramuka, #lain2').on("keypress keyup",function(){
		  var spp = $("#spp").val();
		  var pembangunan = $("#pembangunan").val();
		  var pramuka = $("#pramuka").val();
		  var lain2 = $("#lain2").val();
		  totalBiaya = Number(spp) + Number(pembangunan) + Number(pramuka) + Number(lain2);
		  $("#total").val(totalBiaya);
		  $("#total-disabled").val(totalBiaya.formatMoney(0, "Rp "));
		});
	</script>
	<script>
	function openHistori() {
	    window.open("{{ url('komite/students/spp/histori/'.$student->nisn) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=1200,height=500");
	}
	</script>
@endsection

@section('inline-style')

@stop