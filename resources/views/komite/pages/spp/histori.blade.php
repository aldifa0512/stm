<?php

    // Check a given name to see if it's a valid Excel worksheet name,
    // and fix if necessary
    function worksheetNameGenerator($name)
    {
        // First, strip out characters which aren't allowed
        $illegal_chars = array(':', '\\', '/', '?', '*', '[', ']');
        for ($i = 0; $i < count($illegal_chars); $i++)
            $name = str_replace($illegal_chars[$i], '', $name);
        // Now, if name is longer than 31 chars, truncate it
        if (strlen($name) > 31)
            $name = substr($name, 0, 31);
        return $name;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Histori Pembayaran - SMKN 2 PAYAKUMBUH</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="icon" href="{{asset('assets/backend/img/favicon.ico')}}" type="image/x-icon">
    <link type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet'>

     <!-- Font Awesome -->
    <link type="text/css" href="{{ asset('assets/backend/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Themify Icons -->
    <link type="text/css" href="{{ asset('assets/backend/fonts/themify-icons/themify-icons.css') }}" rel="stylesheet">
    <!-- Core CSS with all styles -->
    <link type="text/css" href="{{ asset('assets/backend/css/styles.css') }}" rel="stylesheet">

    <!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">


    <style type="text/css">
        
    </style>
</head>
<body>
    <div id="wrapper">
        <div id="layout-static">
            <div class="static-content-wrapper">
                <div class="static-content">
                    <div class="page-content">

                        <ol class="breadcrumb">
	    <li class="active"><span>Histori Pembayaran Siswa</span></li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
                            <?php
                            $bln = [4 => 'januari', 5 => 'februari', 6 => 'maret', 7 => 'april', 8 => 'mei', 9 => 'juni', 10 => 'juli', 11 => 'agustus', 12 => 'september', 13 =>'oktober', 14 => 'november', 15 => 'desember'];
                            
                            $rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();
                            ?>
							<h2>Histori Pembayaran / {{ $student->nama_lengkap }} / {{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }} / {{ $rombel['nama'] }}</h2>
                            <a style="float:right;" href="{{ url('komite/students/spp/histori/'. $student->nisn. '/export') }}" class="secondary"><i class="fa fa-download"></i> {{ worksheetNameGenerator("HP - " . $student->nama_lengkap) }}.xls</a>
						</div>

						<div class="panel-body">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="success">
										<th width="">Tanggal Bayar</th>
                                        <th width="">TP</th>
                                        <th width="">Semester</th>
                                        <th width=""><a href="{{ url('komite/students/spp/detail-histori/'.$student->nisn.'/spp') }}">SPP</a></th>
                                        <th width=""><a href="{{ url('komite/students/spp/detail-histori/'.$student->nisn.'/pembangunan') }}">Pembangunan</a></th>
                                        <th width=""><a href="{{ url('komite/students/spp/detail-histori/'.$student->nisn.'/pramuka') }}">Pramuka</a></th>
										<th width=""><a href="{{ url('komite/students/spp/detail-histori/'.$student->nisn.'/lain-lain') }}">Lain-lain</a></th>
										<th width="">Total</th>
                                        <th width="">Staff Penerima</th>
                                        <th width="">Jenis Bayar</th>
                                        <th width="">Keterangan</th>
										<th width="">Action</th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($histori as $value)
									<tr>
                                        <td>{{ str_pad($value->tgl_bayar, 2, '0', STR_PAD_LEFT) }}/{{ str_pad($value->bln_bayar, 2, '0', STR_PAD_LEFT) }}/{{ $value->thn_bayar }}</td>
										<td>{{ $value->tp }}/{{ $value->tp+1 }}</td>
                                        <td>{{ $value->semester == '2' ? '2 (Dua)' : '1 (Satu)' }}</td>
                                        <td>Rp.{{ number_format($value->spp,2,',','.') }}</td>
                                        <td>Rp.{{ number_format($value->pembangunan,2,',','.') }}</td>
										<td>Rp.{{ number_format($value->pramuka,2,',','.') }}</td>
                                        <td>Rp.{{ number_format($value->lain_lain,2,',','.') }}</td>
										<td>Rp.{{ number_format($value->nominal,2,',','.') }}</td>
                                        <td>{{ $value->oleh }}</td>
										<td><?php
                                                if($value->note != "")
                                                    echo $value->note;
                                                else
                                                    echo "-";
                                            ?>
                                        </td>
                                        <td><?php
                                                if($value->note2 != "")
                                                    echo $value->note2;
                                                else
                                                    echo "-";
                                            ?>
                                        </td>
                                        <td>
                                            <a href="{{ url('komite/students/spp/invoiceExcel/'.$student->nisn.'/' . $value->tp .'/'. $value->semester.'/'.$value->no_kwitansi) }}" class="btn btn-primary-alt btn-sm" title="Lihat Bukti Pembayaran"><i class="ti ti-eye"></i>&nbsp;Download</a>
                                        </td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
                        
                        <footer role="contentinfo">
                            <div class="clearfix">
                                <ul class="list-unstyled list-inline pull-left">
                                    <li><h6 style="margin: 0;">Dev By <a href="http://facebook.com/aldifa0512" target="_blank">Aldi Fajrin</a></h6></li>
                                </ul>
                                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Load site level scripts -->

    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

    <!-- Load jQuery -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/jquery-1.10.2.min.js') }}"></script>
    <!-- Load jQueryUI -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/jqueryui-1.10.3.min.js') }}"></script>
    <!-- Load Bootstrap -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script>
    <!-- Load Enquire -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/enquire.min.js') }}"></script>

    <!-- Load Velocity for Animated Content -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.ui.min.js') }}"></script>

    <!-- Wijet -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/wijets/wijets.js') }}"></script>

    <!-- Code Prettifier  -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/codeprettifier/prettify.js') }}"></script>
    <!-- Swith/Toggle Button -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-switch/bootstrap-switch.js') }}"></script>

    <!-- Bootstrap Tabdrop -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}"></script>

    <!-- iCheck -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/iCheck/icheck.min.js') }}"></script>

    <!-- nano scroller -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/nanoScroller/js/jquery.nanoscroller.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/backend/js/application.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/demo/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/demo/demo-switcher.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/backend/js/angular.min.js') }}"></script>
    
    <!-- End loading site level scripts -->
    @yield('page-scripts')

    @yield('inline-script')
</body>
</html>
