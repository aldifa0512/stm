@extends('komite.base')

@section('title', 'Pembayaran')

@section('content')
	<h3 class="page-title">Pembayaran</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
		<?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li><a href="{{ url('komite/students/rombel/lihat/'. $student->kelas) }}">Kelas {{$rombel['nama']}}</a></li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-blue" data-widget='{"draggable": "false"}' ng-app="SPP" ng-controller="sppCtrl">
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Pembayaran</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<form action="{{ url('komite/spp/bayar/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Lengkap</label>
								<div class="col-sm-6">
									<input type="text" name="nama_lengkap" class="form-control" value="{{ $student->nama_lengkap }} - ( {{$rombel['nama']}} )" disabled>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">NISN</label>
								<div class="col-sm-4">
									<input type="number" name="nisn" class="form-control" value="{{ $student->nisn }}" disabled>
									<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}" >
									<input type="hidden" name="kelas" class="form-control" value="{{ $student->kelas }}" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">TP</label>
								<div class="col-sm-4">
									<select class="form-control" name="tp" disabled>
									<?php 
									foreach ($tps as $tp) {
									    echo "<option>$tp->tp</option>";
									}
									?>
									</select>
									<input type="hidden" name="tp" class="form-control" value="2016" >
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Tanggal Bayar</label>
								<div class="col-sm-4">
									<input type="text" placeholder="yyyy-mm-dd" name="tanggal_bayar" class="form-control" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Bulan Tertagih</label>
								<div class="col-sm-8">
									<table>
										<tr>
										<?php
											$bln = array(	'Januari',
															'Februari',
															'Maret',
															'April',
															'Mei',
															'Juni',
															'Juli',
															'Agustus',
															'September',
															'Oktober',
															'November',
															'Desember');
											for($i = 0; $i < 12; $i++) {
												$disabled = "";
												$style = "";
												if($histori[$i+4] != 0 ){
													$disabled = "disabled";
													$style = "style=color:blue;";
												}
											?><td style="width:90px">
												<label class="checkbox">
													<input type="hidden" name="bulan[{{ $i }}]" value="0">
											    	<input type="checkbox" ng-model="bulan[{{ $i }}]"  ng-click="total()" name="bulan[{{ $i }}]" {{ $disabled }}><span {{$style}}>{{ $bln[$i] }}</span>
											    </label>
											    </td>
											<?php
												if ($i == 5) 
													echo "</tr><tr>";
											}
										?>
										</tr>
									</table>
										
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jenis Pembayaran</label>
								<div class="col-sm-3">
									<label class="checkbox">
								    	<input type="checkbox" ng-model="a" ng-click="total()" name="pembayaran[]" value="{{ $spp->first()->spp }}">SPP ( Rp.{{ $spp->first()->spp }} )
								    </label>
								    <label class="checkbox">
								    	<input type="checkbox" ng-model="b" ng-click="total()" name="pembayaran[]" value="{{ $spp->first()->pembangunan }}">Pembangunan ( Rp. {{ $spp->first()->pembangunan }} )
								    </label>
								    <label class="checkbox">
								    	<input type="checkbox" ng-model="c" ng-click="total()" name="pembayaran[]" value="{{ $spp->first()->pramuka }}">Pramuka ( Rp. {{ $spp->first()->pramuka }} )
								    </label>
								    <label class="checkbox">
								    	<input type="checkbox" ng-model="d" ng-click="total()" name="pembayaran[]" value="{{ $spp->first()->lain_lain }}">Lain-lain ( Rp. {{ $spp->first()->lain_lain }} )
								    </label>
							    </div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Total</label>
								<div class="col-sm-4">
									<input type="text" placeholder="Rp." name="total" class="form-control" value="Rp. <% count %>" disabled>
									<input type="hidden" placeholder="Rp." name="total" class="form-control" value="<% count %>" >
									<input type="hidden" name="spp" class="form-control" value="<% ca %>" >
									<input type="hidden" name="pembangunan" class="form-control" value="<% cb %>" >
									<input type="hidden" name="pramuka" class="form-control" value="<% cc %>" >
									<input type="hidden" name="lain_lain" class="form-control" value="<% cd %>" >
									<input type="hidden" name="selected_bln" class="form-control" value="<% selectedBln %>" >
									<span style="color:red;"><% msg %></span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 control-label">Note</label>
								<div class="col-sm-4">
									<textarea name="note" class="form-control"></textarea>
								</div>
							</div>
							<u><a href="" onclick="openHistori()">Lihat History Pembayaran</a></u>
						<hr/>
					</div>
					<!-- ./End panel body -->
						<div class="panel-body" style="padding: 40px 16px;">
							<center><input type="submit" style="" class="finish btn-success btn" value="Bayar!" /></center>
						</div>
						<!-- ./End panel body -->

						<!-- Panel Footer -->
						<!--<div class="panel-footer">
							<div class="row">
								<div class="col-sm-10 col-sm-offset-2">
									<a href="{//{ url('tu-user/students') }}" class="btn-default btn">Batal</a>&nbsp;&nbsp;&nbsp;
									<button class="btn-primary btn">Simpan</button>
								</div>
							</div>
						</div>-->
						<!-- ./End Panel Footer -->
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-select-1.11.2/js/bootstrap-datepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script>
		var app = angular.module('SPP', [], function($interpolateProvider){
			$interpolateProvider.startSymbol('<%');
	        $interpolateProvider.endSymbol('%>');
		});
		app.controller('sppCtrl', function($scope) {
			$scope.spp = {{ $spp->first()->spp }};
			$scope.pembangunan = {{ $spp->first()->pembangunan }};
			$scope.pramuka = {{ $spp->first()->pramuka }};
			$scope.lain_lain = {{ $spp->first()->lain_lain }};
		    $scope.a = 0;
			$scope.b = 0;
			$scope.c = 0;
			$scope.d = 0;
			$scope.bulan = [];
			
		    $scope.total = function() {

        		$scope.count = 0;
        		$scope.ca = 0;
				$scope.cb = 0;
				$scope.cc = 0;
				$scope.cd = 0;
        		//console.log($scope.bulan);
        		var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        		$scope.selectedBln = "";
        		$c = 0;
        		for ($i=0; $i<12; $i++){
        			if($scope.bulan[$i] == true){
        				$c++;
        				$scope.selectedBln += bulan[$i] + " ";
        			}
        		}

        		if($c == 0)
        			$scope.msg = "Anda harus memilih bulan tertagih.";
        		if($c!=0)
        			$scope.msg = "";

		    	if($scope.a){
    				$scope.ca = $scope.spp * $c;
        			$scope.count = $scope.spp * $c;
		    	}
        		if($scope.b){
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.count = $scope.pembangunan * $c;
        		}
        		if($scope.c){
        			$scope.cc = $scope.pramuka * $c;
        			$scope.count = $scope.pramuka * $c;
        		}
        		if($scope.d){
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.count = $scope.lain_lain * $c;
        		}
        		if($scope.a && $scope.b){
        			$scope.ca = $scope.spp * $c;
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.count = ($scope.spp + $scope.pembangunan) * $c;
        		}
        		if($scope.a && $scope.c){
        			$scope.ca = $scope.spp * $c;
        			$scope.cc = $scope.pramuka * $c;
        			$scope.count = ($scope.spp + $scope.pramuka) * $c;
        		}
        		if($scope.a && $scope.d){
        			$scope.ca = $scope.spp * $c;
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.count = ($scope.spp + $scope.lain_lain) * $c;
        		}
        		if($scope.b && $scope.c){
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.cc = $scope.pramuka * $c;
        			$scope.count = ($scope.pembangunan + $scope.pramuka) * $c;
        		}
        		if($scope.b && $scope.d){
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.count = ($scope.pembangunan + $scope.lain_lain) * $c;
        		}
        		if($scope.c && $scope.d){
        			$scope.cc = $scope.pramuka * $c;
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.count = ($scope.pramuka + $scope.lain_lain) * $c;
        		}
        		if($scope.a && $scope.b && $scope.c){
        			$scope.ca = $scope.spp * $c;
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.cc = $scope.pramuka * $c;
        			$scope.count = ($scope.spp + $scope.pembangunan + $scope.pramuka) * $c;
        		}
        		if($scope.a && $scope.b && $scope.d){
        			$scope.ca = $scope.spp * $c;
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.count = ($scope.spp + $scope.pembangunan + $scope.lain_lain) * $c;
        		}
        		if($scope.a && $scope.c && $scope.d){
        			$scope.ca = $scope.spp * $c;
        			$scope.cc = $scope.pramuka * $c;
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.count = ($scope.spp + $scope.pramuka + $scope.lain_lain) * $c;
        		}
        		if($scope.d && $scope.b && $scope.c){
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.cc = $scope.pramuka * $c;
        			$scope.count = ($scope.lain_lain + $scope.pembangunan + $scope.pramuka) * $c;
        		}
        		if($scope.a && $scope.b && $scope.c && $scope.d){
        			$scope.ca = $scope.spp * $c;
        			$scope.cb = $scope.pembangunan * $c;
        			$scope.cc = $scope.pramuka * $c;
        			$scope.cd = $scope.lain_lain * $c;
        			$scope.count = ($scope.spp + $scope.pembangunan + $scope.pramuka + $scope.lain_lain) * $c;
        		}
        		
    		}
		});
	</script>

	<script>
	function openHistori() {
	    window.open("{{ url('komite/spp/histori/'.$student->nisn) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=900,height=500");
	}
	</script>
	<script type="text/javascript">
	$(function(){
		var dataPrice = parseInt($('#package-id').find(':selected').data('price'));
		var daysTotal = parseInt($('#count-days').val());
		var billTotal = dataPrice * daysTotal;

		$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));

		$('#package-id').change(function(){

			$('.package-info').html('<i class="ti ti-info-alt"></i>&nbsp;'+$('#package-id :selected').data('notes'));
		});

		$('input[name="tanggal_bayar"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tl_ayah"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tl_ibu"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tl_wali"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
		$('input[name="tgl_masuk"]').datepicker({format: 'yyyy-mm-dd', autoclose:true,  viewMode: "year", minViewMode: "year" });
	});

	Number.prototype.formatMoney = function(c, d, t){
	var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	 };
	</script>

	<script type="text/javascript">
	</script>
@endsection

@section('inline-style')
<style type="text/css">
.package-info-wrapper {
    display: block;
    margin-top: 10px;
    background: #f6f6f6;
    padding: 7px 10px;
    font-size: 12px;
    font-style: italic;
    color: #888;
}
.package-info-wrapper strong {
	color: #000;
}
</style>
@stop