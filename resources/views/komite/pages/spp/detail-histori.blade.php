<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Detail {{ $nama_detail }} - {{ $student->nama_lengkap }} - SMKN 2 PAYAKUMBUH</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="icon" href="{{asset('assets/backend/img/favicon.ico')}}" type="image/x-icon">
    <link type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet'>

     <!-- Font Awesome -->
    <link type="text/css" href="{{ asset('assets/backend/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Themify Icons -->
    <link type="text/css" href="{{ asset('assets/backend/fonts/themify-icons/themify-icons.css') }}" rel="stylesheet">
    <!-- Core CSS with all styles -->
    <link type="text/css" href="{{ asset('assets/backend/css/styles.css') }}" rel="stylesheet">

    <!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">


    <style type="text/css">
        
    </style>
    <!--[if lt IE 10]>
    <script type="text/javascript" src="assets/js/media.match.min.js"></script>
    <script type="text/javascript" src="assets/js/respond.min.js"></script>
    <script type="text/javascript" src="assets/js/placeholder.min.js"></script>
    <![endif]-->
    <!-- The following CSS are included as plugins and can be removed if unused-->
</head>
<body>
    <div id="wrapper">
        <div id="layout-static">
            <div class="static-content-wrapper">
                <div class="static-content">
                    <div class="page-content">

                        <ol class="breadcrumb">
                        <li><a href="{{ url('komite/students/spp/histori/'.$student->nisn) }}"> Histori Bayar</a></li>
	    <li class="active"><span>Detail {{ $nama_detail }} - {{ $student->nama_lengkap }}</span></li>
	</ol>
	<div class="container-fluid">

		<!-- Listings Table -->
		<div data-widget-group="group1">
			<div class="row">
				@if (Session::has('success'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-success">
							<i class="ti ti-check"></i>&nbsp; <strong>Well Done!</strong> {{ Session::get('success') }}.
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						</div>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="col-md-12">
						<div class="alert alert-dismissable alert-danger">
							<i class="ti ti-close"></i>&nbsp; <strong>Access denied!</strong> {{ Session::get('error') }}.
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
						</div>
					</div>
				@endif
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
                            <?php
                            $bln = [4 => 'januari', 5 => 'februari', 6 => 'maret', 7 => 'april', 8 => 'mei', 9 => 'juni', 10 => 'juli', 11 => 'agustus', 12 => 'september', 13 =>'oktober', 14 => 'november', 15 => 'desember'];
                            
                            $rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();
                            ?>
							<h2>Detail {{ $nama_detail }} / {{ $student->nama_lengkap }} / {{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }} / {{ $rombel['nama'] }}</h2>
						</div>

						<div class="panel-body">
                            <div style="overflow-x:auto;">
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="success">
										<th class="col-md-3">ID</th>
                                        <th class="col-md-3">NISN</th>
                                        <th class="col-md-3">KELAS</th>
                                        <th class="col-md-3">TP</th>
                                        <th class="col-md-3">JANUARI</th>
                                        <th class="col-md-3">FEBRUARI</th>
                                        <th class="col-md-3">MARET</th>
										<th class="col-md-3">APRIL</th>
										<th class="col-md-3">MEI</th>
                                        <th class="col-md-3">JUNI</th>
                                        <th class="col-md-3">JULI</th>
                                        <th class="col-md-3">AGUSTUS</th>
                                        <th class="col-md-3">SEPTEMBER</th>
                                        <th class="col-md-3">OKTOBER</th>
                                        <th class="col-md-3">NOVEMBER</th>
                                        <th class="col-md-3">DESEMBER</th>
                                        <th class="col-md-3">TOTAL</th>
                                        <th class="col-md-3">CREATED AT</th>
										<th class="col-md-3">UPDATED AT</th>
									</tr>
								</thead>
								<tbody>
								<?php $i=1; ?>
									@foreach ($detail as $value)
									<tr>
                                        <td>{{ $value->id }}</td>
										<td>{{ $value->nisn }}</td>
                                        <td>{{ $rombel['nama'] }}</td>
                                        <td>{{ $value->tp }}</td>
                                        <?php $bea_name ='BSW'; ?>
                                        <td>{{ $value->januari == -1 ? $bea_name : $value->januari }}</td>
                                        <td>{{ $value->februari == -1 ? $bea_name : $value->februari }}</td>
                                        <td>{{ $value->maret == -1 ? $bea_name : $value->maret }}</td>
                                        <td>{{ $value->april == -1 ? $bea_name : $value->april }}</td>
                                        <td>{{ $value->mei == -1 ? $bea_name : $value->mei }}</td>
                                        <td>{{ $value->juni == -1 ? $bea_name : $value->juni }}</td>
                                        <td class="warning">{{ $value->juli == -1 ? $bea_name : $value->juli }}</td>
                                        <td class="warning">{{ $value->agustus == -1 ? $bea_name : $value->agustus }}</td>
                                        <td class="warning">{{ $value->september == -1 ? $bea_name : $value->september }}</td>
                                        <td class="warning">{{ $value->oktober == -1 ? $bea_name : $value->oktober }}</td>
                                        <td class="warning">{{ $value->november == -1 ? $bea_name : $value->november }}</td>
                                        <td class="warning">{{ $value->desember == -1 ? $bea_name : $value->desember }}</td>
                                        <td>{{ $value->total }}</td>
                                        <td>{{ $value->created_at }}</td>
										<td>{{ $value->updated_at }}</td>
									</tr>
									@endforeach
								</tbody>                                
							</table>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ./End Listings Table -->
	</div>
                        
                        <footer role="contentinfo">
                            <div class="clearfix">
                                <ul class="list-unstyled list-inline pull-left">
                                    <li><h6 style="margin: 0;">Dev By <a href="http://facebook.com/aldifa0512" target="_blank">Aldi Fajrin</a></h6></li>
                                </ul>
                                <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Load site level scripts -->

    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

    <!-- Load jQuery -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/jquery-1.10.2.min.js') }}"></script>
    <!-- Load jQueryUI -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/jqueryui-1.10.3.min.js') }}"></script>
    <!-- Load Bootstrap -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/bootstrap.min.js') }}"></script>
    <!-- Load Enquire -->
    <script type="text/javascript" src="{{ asset('assets/backend/js/enquire.min.js') }}"></script>

    <!-- Load Velocity for Animated Content -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/velocityjs/velocity.ui.min.js') }}"></script>

    <!-- Wijet -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/wijets/wijets.js') }}"></script>

    <!-- Code Prettifier  -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/codeprettifier/prettify.js') }}"></script>
    <!-- Swith/Toggle Button -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-switch/bootstrap-switch.js') }}"></script>

    <!-- Bootstrap Tabdrop -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js') }}"></script>

    <!-- iCheck -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/iCheck/icheck.min.js') }}"></script>

    <!-- nano scroller -->
    <script type="text/javascript" src="{{ asset('assets/backend/plugins/nanoScroller/js/jquery.nanoscroller.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/backend/js/application.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/demo/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/backend/demo/demo-switcher.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/backend/js/angular.min.js') }}"></script>
    
    <!-- End loading site level scripts -->
    @yield('page-scripts')

    @yield('inline-script')
</body>
</html>
