@extends('komite.base')

@section('title', 'Pembayaran')

@section('content')
	<?php
		$sme = $tp->semester;
	?>
	<h3 class="page-title">Pembayaran</h3>
	<ol class="breadcrumb">
	    <li><a href="{{ url('komite') }}">Dashboard</a></li>
		<?php
			if(isset($rombelM) && $rombelM == true){
			$rombel = App\Models\Rombel::where('id', $student->kelas)->first()->toArray();//dd($jurusan);
	    ?>
	    <li><a href="{{ url('komite/students') }}">Siswa</a></li>
	    <li class=""><a href="{{ url('komite/students/rombel') }}">Rombongan Belajar</a></li>
	    <li><a href="{{ url('komite/students/rombel/lihat/'. $student->kelas) }}">Kelas {{$rombel['nama']}}</a></li> 
	    <?php
		}elseif(isset($pasif) && $pasif == true)
			echo "<li>Siswa Tidak Aktif</li>";
		elseif(isset($recap) && $recap == true)
			echo "<li>Siswa Keseluruhan</li>";
		else
			echo "<li>Siswa Aktif</li>";
	    ?>
	</ol>

	<div class="container-fluid">
		@if (Session::has('success'))
			<div class="alert alert-dismissable alert-success">
				<i class="ti ti-check"></i>&nbsp; <strong>Well done!</strong> {{ Session::get('success') }}.
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			</div>
			<script type="text/javascript">
			function openInvoice() {
			    // window.open("{{ url('komite/students/spp/invoice/'.$student->nisn.'/' . Session::get('tpBayar').'/'.Session::get('semesterBayar').'/'.Session::get('no_kuitansi')) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=900,height=500");
			    window.open("{{ url('komite/students/spp/invoiceExcel/'.$student->nisn.'/' . Session::get('tpBayar').'/'.Session::get('semesterBayar').'/'.Session::get('no_kuitansi')) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=900,height=500");
			}
				openInvoice();
			</script>
		@endif
		<div class="row">
			<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-check"></i>&nbsp; <strong>Oh snap!</strong> {{ Session::get('error') }}.
					</div>
				@endif
				@if ($errors->has())
					<div class="alert alert-dismissable alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<i class="ti ti-close"></i>&nbsp; <strong>Oh snap!</strong>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				<div class="panel panel-gray" data-widget='{"draggable": "false"}' >
					<!-- Panel heading -->
					<div class="panel-heading">
						<h2>Form Pembayaran</h2>
					</div>
					<!-- ./End panel heading -->

						<!-- Panel body -->
					<div class="panel-body">
						<form action="{{ url('komite/students/spp/bayar/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<center>
								<h2>Pembayaran TP  {{$tp['tp']}}/{{($tp['tp']+1)}}  </h2><br/>
							</center>
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<?php
										$size1 = 10/2;
										$size2 = 10/3;
										$style = 'width=15%';
										$style2 = 'width=35%';
									?>
									<td class="{{ $student->status == 'pasif' ? 'danger' : '' }}" {{$style}}>Nama Lengkap</td>
									<td class="{{ $student->status == 'pasif' ? 'danger' : '' }}" {{$style2}}><b>{{ $student->nama_lengkap }} / <u><a href="" onclick="openHistori()">Histori Pembayaran</a></u></b></td>
									<td {{$style}}>Kelas</td>
									<td {{$style2}}>{{ $rombel['nama'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>NISN</td>
									<td {{$style2}}>{{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }}</td>
									<td {{$style}}>Wali Kelas</td>
									<td {{$style2}}>{{ $rombel['pengajar'] }}</td>
								</tr>
								<tr>
									<td {{$style}}>Angkatan</td>
									<td {{$style2}}>{{ $student->tp }}/{{ ($student->tp+1) }}</td>
									<td {{$style}}>Tanggal Bayar</td>
									<td {{$style2}}><div ">
										<input type="text" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-md" disabled></td>
										<input type="hidden" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-sm" required></td>
									</div>
								</tr>
							</table>
							Pilih Tahun Pembelajaran : 	   					
							<select class="" id="triggerTp">
   							<?php 
   							$tps = get_all_tp(); 
   							$tpAktif = get_tp_aktif();
   							$tpAktif = $tpAktif->tp;
   								foreach ($tps as $key => $value) {
   									if($student->tahunKeluar != NULL && $student->status == 'pasif'){
   										if($value->tp >= $student->tp && $value->tp <= $student->tahunKeluar){
		   									if($tpAktif == $value->tp){
		   										echo '<option value="'.$value->tp.'" selected>'.$value->tp.'/'.($value->tp + 1).'</option>';
		   									}else{
		   										echo '<option value="'.$value->tp.'">'.$value->tp.'/'.($value->tp + 1).'</option>';
		   									}
		   								}	
   									}else{
   										if($value->tp >= $student->tp){
		   									if($tpAktif == $value->tp){
		   										echo '<option value="'.$value->tp.'" selected>'.$value->tp.'/'.($value->tp + 1).'</option>';
		   									}else{
		   										echo '<option value="'.$value->tp.'">'.$value->tp.'/'.($value->tp + 1).'</option>';
		   									}
		   								}
   									}
   								}
   							?>
   							</select><br><span style="color: red;">Ini hanya untuk melihat record pembayaran siswa!!</span><br><br>
	   						<div style="overflow-x:auto;">
							<table id="tableTp" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr class="success">
                                        <?php $tunggakan = tunggakanSiswaPerTp($student->nisn, $tp->tp, $student->tp); ?>
                                        <th class="col-md-3"></th>
                                        <th class="col-md-3">TUNGGAKAN</th>
                                        <th class="col-md-3">NISN</th>
                                        <th class="col-md-3">KELAS</th>
                                        <th class="col-md-3">TP</th>
                                        <th class="col-md-3">JANUARI</th>
                                        <th class="col-md-3">FEBRUARI</th>
                                        <th class="col-md-3">MARET</th>
										<th class="col-md-3">APRIL</th>
										<th class="col-md-3">MEI</th>
                                        <th class="col-md-3">JUNI</th>
                                        <th class="col-md-3">JULI</th>
                                        <th class="col-md-3">AGUSTUS</th>
                                        <th class="col-md-3">SEPTEMBER</th>
                                        <th class="col-md-3">OKTOBER</th>
                                        <th class="col-md-3">NOVEMBER</th>
                                        <th class="col-md-3">DESEMBER</th>
                                        <th class="col-md-3">TOTAL</th>
                                        <th style="width: 150px;" class="col-md-3">CREATED AT</th>
										<th style="width: 150px;" class="col-md-3">UPDATED AT</th>
									</tr>
								</thead>
								<tbody>
								<?php 
								$i=1; 
								?>
									@foreach ($detail_bayar as $key => $value)
									<tr>
										<?php
										switch($key){
											case 'spp':
												echo '<td>SPP</td><td>Rp.'.number_format($tunggakan['spp'],2,',','.').'</td>';
												break;
											case 'pembangunan':
												echo '<td>PEMBANGUNAN</td><td>Rp.'.number_format($tunggakan['pembangunan'],2,',','.').'</td>';
												break;
											case 'pramuka':
												echo '<td>PRAMUKA</td><td>Rp.'.number_format($tunggakan['pramuka'],2,',','.').'</td>';
												break;
											case 'lain2':
												echo '<td>LAIN-LAIN</td><td>Rp.'.number_format($tunggakan['lain2'],2,',','.').'</td>';
												break;
										}
										?>
										<td>{{ $value->nisn == null ? str_pad($student->nisn, 10, '0', STR_PAD_LEFT) : str_pad($value->nisn, 10, '0', STR_PAD_LEFT) }}</td>
                                        <td>{{ $rombel['nama'] }}</td>
                                        <td>{{ $value->tp == null ? $tp['tp'] .'/'. ($tp['tp']+1) : $value->tp .'/'. ($value->tp+1) }}</td>
                                        <?php $bea_name ='bea-'; ?>
                                        <td>{{ $value->januari == -1 ? $bea_name : $value->januari }}</td>
                                        <td>{{ $value->februari == -1 ? $bea_name : $value->februari }}</td>
										<td>{{ $value->maret == -1 ? $bea_name : $value->maret }}</td>
                                        <td>{{ $value->april == -1 ? $bea_name : $value->april }}</td>
                                        <td>{{ $value->mei == -1 ? $bea_name : $value->mei }}</td>
                                        <td>{{ $value->juni == -1 ? $bea_name : $value->juni }}</td>
                                        <td class="warning">{{ $value->juli == -1 ? $bea_name : $value->juli }}</td>
                                        <td class="warning">{{ $value->agustus == -1 ? $bea_name : $value->agustus }}</td>
                                        <td class="warning">{{ $value->september == -1 ? $bea_name : $value->september }}</td>
                                        <td class="warning">{{ $value->oktober == -1 ? $bea_name : $value->oktober }}</td>
                                        <td class="warning">{{ $value->november == -1 ? $bea_name : $value->november }}</td>
                                        <td class="warning">{{ $value->desember == -1 ? $bea_name : $value->desember }}</td>
                                        <td>{{ number_format($value->total,2,',','.') }}</td>
                                        <td>{{ $value->created_at }}</td>
										<td>{{ $value->updated_at }}</td>
									</tr>
									@endforeach
								</tbody>                                
							</table>
                            </div>
                            
							<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}" >
							<input type="hidden" name="kelas" class="form-control" value="{{ $student->kelas }}" >
							<input type="hidden" size="50" name="tp" class="form-control" value="{{ $tp['tp'] }}" >
							<?php if($student->status == 'pasif'){ ?>
								<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">JENIS IURAN</th>
											<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">BESARAN</th>
											<th class="danger" style="vertical-align: middle;text-align: center;">AKUM TUNGGAKAN</th>
											<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">PEMBAYARAN</th>
										</tr>
										<tr>
											<th class="danger" style="vertical-align: middle;text-align: center;">Juni - {{$student->tahunKeluar}}/{{$student->tahunKeluar + 1}}</th>
										</tr>
										<tr>
											<td>SPP</td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->spp,2,',','.') }}</span></td>
											<td>Rp. {{number_format($tunggakan_thn['spp'],2,',','.')}}</td>
											<td><input class="form-control" type="number" id="spp" name="spp"></td>
										</tr>
										<tr>
											<td>Pembangunan</td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->pembangunan,2,',','.') }} / Thn</td>
											<td>Rp. {{number_format($tunggakan_thn['pembangunan'],2,',','.')}}</td>
											<td><input class="form-control" type="number" id="pembangunan" name="pembangunan"></td>
										</tr>
										<tr>
											<td>Pramuka </td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->pramuka,2,',','.') }}</span></td>
											<td>Rp. {{number_format($tunggakan_thn['pramuka'],2,',','.')}}</td>
											<td><input class="form-control" type="number" id="pramuka" name="pramuka"></td>
										</tr>
										<tr>
											<td>Lain-lain</td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->lain_lain,2,',','.') }}</td>
											<td>Rp. {{number_format($tunggakan_thn['lain2'],2,',','.')}}</td>
											<td>
												<input class="form-control" type="number" id="lain2-disabled" name="lain2" disabled>
												<input type="hidden" id="lain2" name="lain2">
											</td>
										</tr>
									</tbody>
								</table>
							<?php }else{ ?>
								<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">JENIS IURAN</th>
											<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">BESARAN</th>
											<th class="warning" style="vertical-align: middle;text-align: center;">AKUM TUNGGAKAN</th>
											<th colspan="2" class="warning" style="vertical-align: middle;text-align: center;">TUNGGAKAN TP AKTIF sd</th>
											<th class="warning" style="vertical-align: middle;text-align: center;" rowspan="2">PEMBAYARAN</th>
										</tr>
										<tr>
											<th class="warning" style="vertical-align: middle;text-align: center;">Juni - {{$thn_tunggakan-1}}/{{$thn_tunggakan}}</th>
											<th class="warning" style="vertical-align: middle;text-align: center;">Bulan</th>
											<th class="warning" style="vertical-align: middle;text-align: center;">Nominal</th>
										</tr>
										<tr>
											<td>SPP</td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->spp,2,',','.') }}</span></td>
											<td>Rp. {{number_format($tunggakan_thn['spp'],2,',','.')}}</td>
											<td>{{$bln[$master_spp->batas_bln_spp]}}</td>
											<td>Rp. {{number_format($tunggakan_bln['spp'],2,',','.')}}</td>
											<td><input class="form-control" type="number" id="spp" name="spp"></td>
										</tr>
										<tr>
											<td>Pembangunan</td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->pembangunan,2,',','.') }} / Thn</td>
											<td>Rp. {{number_format($tunggakan_thn['pembangunan'],2,',','.')}}</td>
											<td>{{$bln[$master_spp->batas_bln_pembangunan]}}</td>
											<td>Rp. {{number_format($tunggakan_bln['pembangunan'],2,',','.')}}</td>
											<td><input class="form-control" type="number" id="pembangunan" name="pembangunan"></td>
										</tr>
										<tr>
											<td>Pramuka </td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->pramuka,2,',','.') }}</span></td>
											<td>Rp. {{number_format($tunggakan_thn['pramuka'],2,',','.')}}</td>
											<td>{{$bln[$master_spp->batas_bln_pramuka]}}</td>
											<td>Rp. {{number_format($tunggakan_bln['pramuka'],2,',','.')}}</td>
											<td><input class="form-control" type="number" id="pramuka" name="pramuka"></td>
										</tr>
										<tr>
											<td>Lain-lain</td>
											<td><span class='masterbiaya'>Rp. {{ number_format($master_spp->lain_lain,2,',','.') }}</td>
											<td>Rp. {{number_format($tunggakan_thn['lain2'],2,',','.')}}</td>
											<td>{{$bln[$master_spp->batas_bln_lain2]}}</td>
											<td>Rp. {{number_format($tunggakan_bln['lain2'],2,',','.')}}</td>
											<td>
												<input class="form-control" type="number" id="lain2-disabled" name="lain2" disabled>
												<input type="hidden" id="lain2" name="lain2">
											</td>
										</tr>
									</tbody>
								</table>
							<?php } ?>
							
							<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								<tr>
									<td width="35%">
										Pilih Tipe Pembayaran <br>
										<select name="note" class="form-control" id="jenisBayar">
               							<?php $types = array('tu' =>'Tunai', 'al' => 'Alumni', 'be' => 'Beasiswa', 'sb' => 'Siswa Baru', 'pn' => 'Penyesuaian'); ?>
               							<?php 
               								foreach ($types as $key => $value) {
               									echo '<option value="'.$key.'">'.$value.'</option>';
               								}
               							?>
               							</select><br>
               							Pilih Tahun Pembelajaran yang dibayarkan
               							<select name="tpBayar" class="form-control" id="tpBayar">
               							<?php 
			   							$tps = get_all_tp(); 
			   							$tpAktif = get_tp_aktif();
			   							$tpAktif = $tpAktif->tp;
			   								foreach ($tps as $key => $value) {
			   									if($student->tahunKeluar != NULL && $student->status == 'pasif'){
			   										if($value->tp >= $student->tp && $value->tp <= $student->tahunKeluar){
					   									if($tpAktif == $value->tp){
					   										echo '<option value="'.$value->tp.'" selected>'.$value->tp.'/'.($value->tp + 1).'</option>';
					   									}else{
					   										echo '<option value="'.$value->tp.'">'.$value->tp.'/'.($value->tp + 1).'</option>';
					   									}
					   								}	
			   									}else{
			   										if($value->tp >= $student->tp){
					   									if($tpAktif == $value->tp){
					   										echo '<option value="'.$value->tp.'" selected>'.$value->tp.'/'.($value->tp + 1).'</option>';
					   									}else{
					   										echo '<option value="'.$value->tp.'">'.$value->tp.'/'.($value->tp + 1).'</option>';
					   									}
					   								}
			   									}
			   								}
			   							?>
               							</select>
									</td>
									<td width="35%">
										<textarea name="note2" id="note" rows="5" placeholder="Note" class="form-control"></textarea>
									</td>
									<td width="30%" rowspan="2">
										<input type="text" placeholder="" id="total-disabled" name="total" class="form-control input-lg" value="" disabled>
										<input type="hidden" placeholder="Rp." id="total" name="total" class="form-control" value="" ><br><br>
										<center>
											<!-- <input type="submit" style="" class="finish btn-success btn-block btn btn-lg" value="Bayar!" /> -->
				                            <a href="#" id="klikBayar" data-toggle="modal" data-target="#LM" class="profile" name="{{$student->nisn}}" target=""><button type="button" class="btn btn-primary btn-lg btn-block">Bayar</button></a>
										</center>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<!-- <p style="color: red"><em><b>Gunakan Case Sensitif "Beasiswa" untuk pembayaran beasiswa, "pn" untuk penyesuaian dan gunakan "sb" untuk pembayaran siswa yang baru masuk.</b></em></p> Penggunaan note diatas akan memisahkan perhitungan pembayaran dan kas yang dipegang komite -->
									</td>
								</tr>
							</table>							
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="bs-example">
	    <!-- Large modal -->
	    <form action="{{ url('komite/students/spp/bayar/'.$student->nisn) }}" method="post" id="data-siswa" class="form-horizontal" enctype="multipart/form-data" data-toggle="validator">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" placeholder="yyyy-mm-dd" name="tanggal_bayar" value="<?php echo date('Y-m-d') ?>" class="form-control input-sm" required>
			<input type="hidden" name="nisn" class="form-control" value="{{ $student->nisn }}" >
			<input type="hidden" name="kelas" class="form-control" value="{{ $student->kelas }}" >
			<input type="hidden" size="50" name="tp" class="form-control" value="{{ $tp['tp'] }}" >
			
			<input type="hidden" id="hiddenSpp" name="spp">
			<input type="hidden" id="hiddenPembangunan" name="pembangunan">
			<input type="hidden" id="hiddenPramuka" name="pramuka">
			<input type="hidden" id="hiddenLain2" name="lain2">
			<input type="hidden" id="hiddenTotal" name="total" class="form-control" value="" >
			<input type="hidden" id="hiddenTpbayar" name="tpBayar">
			<input type="hidden" id="hiddenNote" name="note">			
			<input type="hidden" id="hiddenNote2" name="note2">		
			
			<div id="LM" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
		        <div class="modal-dialog modal-lg">
		            <div class="modal-content">
		                <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border-radius: 5px;">
		                        <tbody>
		                            <tr>
		                            	<td><br></td>
		                            </tr>
		                            <tr>
		                            	<td>
		                            		<center><h3>Konfirmasi Pembayaran</h3></center>
		                            	</td>
		                            </tr>
		                            <tr>
		                                <td>
		                                    <table width="500" border="0" cellspacing="0" cellpadding="0">
		                                        <tbody>
		                                            <tr>
		                                                <td align="left" valign="top" style="padding:0px 5px 0px 5px">
		                                                    <table height="20px" width="100%" border="0" cellpadding="0" cellspacing="0">
		                                                        <tbody>
		                                                            <tr>
		                                                                <td height="10px" valign="top" style="color:#404041;font-size:13px;padding:5px 5px 0px 20px"><br>
		                                                                        <table>
		                                                                            <tbody><tr>
		                                                                                <td><strong>Nama siswa</strong></td>
		                                                                                <td>: {{ $student->nama_lengkap }}</td>
		                                                                            </tr>
		                                                                            <tr>
		                                                                                <td><strong>NISN</strong></td>
		                                                                                
		                                                                                <td>: {{ str_pad($student->nisn, 10, '0', STR_PAD_LEFT) }}</td>
		                                                                            </tr>
		                                                                            <tr>
		                                                                                <td><strong>Kelas</strong></td>
		                                                                                <td>: {{ $rombel['nama'] }}</td>
		                                                                            </tr>
		                                                                        </tbody></table>
		                                                                </td>
		                                                            </tr>
		                                                        </tbody>
		                                                    </table>
		                                                </td>
		                                                
		                                                <td align="left" valign="top" style="padding:0px 5px 0px 5px">
		                                                    <table height="" width="100%" border="0" cellpadding="3" cellspacing="3">
		                                                        <tbody>
		                                                            <tr>
		                                                                <td height="16" valign="top" style="color:#404041;font-size:13px;padding:15px 5px 0px 5px">
		                                                                    <table>
		                                                                        <tbody><tr>
		                                                                            <td><strong>TP</strong></td>
		                                                                            <td>: {{$tp['tp']}}/{{($tp['tp']+1)}}</td>
		                                                                        </tr>
		                                                                        <tr>
		                                                                            <td><strong>Semester</strong></td>
		                                                                            <td>: {{ $tp->semester}} <?php if($tp->semester == 1) echo '(satu)'; else echo '(dua)'; ?></td>
		                                                                        </tr>
		                                                                    </tbody></table>
		                                                                </td>
		                                                            </tr>
		                                                        </tbody>
		                                                    </table>
		                                                </td>
		                                            </tr>
		                                        </tbody>
		                                    </table>
		                                </td>
		                            </tr>
		                        
		                            <tr>
		                                <td style="color:#404041;font-size:12px;line-height:16px;padding:10px 16px 20px 18px">
		                                	<center>
		                                    <table width="320" border="0" cellpadding="0" cellspacing="0" style="border-radius:5px; padding-bottom:10px; border:solid 1px #e5e5e5">
		                                        <tbody>
		                                            <tr>
		                                                <td>
		                                                    <table width="300" border="0" cellspacing="0" cellpadding="0">
		                                                        <tbody>
		                                                            <tr style="border-bottom:3px;">
		                                                                <td width="15">&nbsp;
		                                                                </td>
		                                                                <td width="" align="left" valign="top" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">           
		                                                                    <strong>TANGGUNGAN</strong>
		                                                                </td>
		                                                                <td width="" align="right" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">
		                                                                    <strong>AMOUNT</strong>
		                                                                </td>
		                                                            </tr>
		                                                            <tr>
		                                                                <td width="15">&nbsp;
		                                                                </td>
		                                                                <td width="" align="left" valign="top" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">           
		                                                                    SPP
		                                                                </td>
		                                                                <td width="" align="right" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">
		                                                                    <strong><p id="konfSpp"></p></strong>
		                                                                   </td>
		                                                            </tr><tr>
		                                                                <td width="15">&nbsp;
		                                                                </td>
		                                                                <td width="" align="left" valign="top" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">           
		                                                                    Pembangunan
		                                                                </td>
		                                                                <td width="" align="right" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">
		                                                                    <strong><p id="konfPembangunan"></p></strong>
		                                                                </td>
		                                                            </tr><tr>
		                                                                <td width="15">&nbsp;
		                                                                </td>
		                                                                <td width="" align="left" valign="top" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">           
		                                                                    Pramuka
		                                                                </td>
		                                                                <td width="" align="right" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">
		                                                                    <strong><p id="konfPramuka"></p></strong></td>
		                                                            </tr>
		                                                            <tr>
		                                                                <td width="15">&nbsp;
		                                                                </td>
		                                                                <td width="" align="left" valign="top" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">           
		                                                                    Lain-lain
		                                                                </td>
		                                                                <td width="" align="right" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">
		                                                                    <strong><p id="konfLain2"></p></strong>                                                                </td>
		                                                            </tr>
		                                                            <tr>
		                                                                <td width="15">&nbsp;
		                                                                </td>
		                                                                <td width="" align="left" valign="top" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">           
		                                                                    <strong>TOTAL</strong>
		                                                                </td>
		                                                                <td width="" align="right" style="color:#404041;font-size:12px;line-height:16px;padding:5px 10px 3px 5px;border-bottom:solid 1px #e5e5e5">
		                                                                    <strong><p id="konfTotal"></p></strong>
		                                                                </td>
		                                                            </tr>
		                                                        </tbody>
		                                                    </table>
		                                                </td>
		                                            </tr>
		                                        </tbody>
		                                    </table>
		                                    </center>
		                                </td>
		                            </tr>
		                            <tr>
		                            	<td>Jenis Pembayaran : <b><span id="konfJenisBayar"></span></b></td>
		                            </tr>
		                            <tr>
		                            	<td>TP Dibayarkan : <b><span id="konfTpBayar"></span></b></td>
		                            </tr>
		                            <tr>
		                            	<td>Note : <b><span id="konfNote"></span></b></td>
		                            </tr>
		                            <tr>
		                            	<td>Apakah anda yakin ingin melanjutkan proses pembayaran ?</td>
		                            </tr>
		                            <tr><td><br></td></tr>
		                            <tr>
		                            	<td>
		                            		<span class="pull-right">
					                            <button type="submit" class="btn btn-info"><i class="fa fa-check"></i> Bayar</button>
					                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i>  Batal</button>
		                        			</span>
		                            	</td>
		                            </tr>
		                            <tr>
		                            	<td>
		                            		<br>
		                            	</td>
		                            </tr>
		                        </tbody>
		                    </table>
		            </div>
		        </div>
		    </div>
	    </form>
	</div>
	@stop



@section('page-styles')
	<!-- Code Prettifier -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/codeprettifier/prettify.css') }}" rel="stylesheet">
    <!-- iCheck -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/iCheck/skins/minimal/blue.css') }}" rel="stylesheet">
    <!-- DateRangePicker -->
    <link type="text/css" href="{{ asset('assets/backend/plugins/form-daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
@stop

@section('page-scripts')
	<!-- Datepicker -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
	 <!-- Validate Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-validation/jquery.validate.min.js') }}"></script>
	<!-- Stepy Plugin -->
	<script type="text/javascript" src="{{ asset('assets/backend/plugins/form-stepy/jquery.stepy.js') }}"></script>
@stop

@section('inline-script')
	<script type="text/javascript">
		// Extend the default Number object with a formatMoney() method:
		// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
		// defaults: (2, "$", ",", ".")
		Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
			places = !isNaN(places = Math.abs(places)) ? places : 2;
			symbol = symbol !== undefined ? symbol : "$";
			thousand = thousand || ",";
			decimal = decimal || ".";
			var number = this, 
			    negative = number < 0 ? "-" : "",
			    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
			    j = (j = i.length) > 3 ? j % 3 : 0;
			return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
		};

	//POP UP KONFIRMASI BAYAR
		$("#klikBayar").on("click",function(){
			var spp = Number($('#spp').val());
			var pembangunan = Number($('#pembangunan').val());
			var pramuka = Number($('#pramuka').val());
			var lain2 = Number($('#lain2').val());
			var total = Number($('#total').val());
			var jenisBayar = $('#jenisBayar').val();
			var tpBayar = $('#tpBayar').val();
			var note = $('#note').val();

			//Change Dislpay for jenis Bayar
			switch(jenisBayar) {
			    case 'tu':
			        var jenisBayarDisplay = 'Tunai';
			        break;
			    case 'al':
			        var jenisBayarDisplay = 'Alumni';
			        break;
			    case 'be':
			        var jenisBayarDisplay = 'Beasiswa';
			        break;
			    case 'sb':
			        var jenisBayarDisplay = 'Siswa Baru';
			        break;
			    case 'pn':
			        var jenisBayarDisplay = 'Penyesuaian';
			        break;
			}

			//Change Display for tpBayar
			var tpBayarDisplay = tpBayar + '/' + (Number(tpBayar) +1);

			$('#konfSpp').html(spp.formatMoney(0, "", ".", ","));
			$('#konfPembangunan').html(pembangunan.formatMoney(0, "", ".", ","));
			$('#konfPramuka').html(pramuka.formatMoney(0, "", ".", ","));
			$('#konfLain2').html(lain2.formatMoney(0, "", ".", ","));
			$('#konfTotal').html(total.formatMoney(0, "", ".", ","));
			$('#konfJenisBayar').html(jenisBayarDisplay );
			$('#konfTpBayar').html(tpBayarDisplay);
			$('#konfNote').html(note);

			//set Value of the hiddens field
			$('#hiddenSpp').val(Number(spp));
			$('#hiddenPembangunan').val(Number(pembangunan));
			$('#hiddenPramuka').val(Number(pramuka));
			$('#hiddenLain2').val(Number(lain2));
			$('#hiddenTotal').val(Number(total));
			$('#hiddenTotal').val(Number(total));
			$('#hiddenTpbayar').val(Number(tpBayar));
			$('#hiddenNote').val(jenisBayar);
			$('#hiddenNote2').val(note);

			console.log(spp);
			console.log(pembangunan);
			console.log(pramuka);
			console.log(lain2);
			console.log(total);
			console.log(jenisBayar);
			console.log(tpBayar);
		});

	    $('#triggerTp').on('change', function(e) {
	        var nisn = '{{$student->nisn}}';
	        var value = $('#triggerTp').val();
	            $.ajax({
	              url: '{{ url('komite/students/spp/dataBayar') }}',
	              data: {
	                    _token: '{{ csrf_token() }}',
	                    nisn: nisn,
	                    kelas: '{{$student->kelas}}',
	                    tahunMasukSiswa: '{{$student->tp}}',
	                    value: value
	                },
	              type: "POST",
	              // dataType: 'json',
	              success: function(res){
	              	console.log(res);
	              	$('#tableTp').html(res);
	              },
	              error: function(jqXHR, textStatus, errorThrown){
	                console.log( jqXHR);
	                console.log( textStatus);
	                console.log( errorThrown);
	              }
	            });      
	        });

		//total biaya two ways binding
		$('#spp, #pembangunan, #pramuka, #lain2').on("keypress keyup",function(){
		  var spp = $("#spp").val();
		  var pembangunan = $("#pembangunan").val();
		  var pramuka = $("#pramuka").val();
		  var lain2 = $("#lain2").val();
		  totalBiaya = Number(spp) + Number(pembangunan) + Number(pramuka) + Number(lain2);
		  $("#total").val(totalBiaya);
		  $("#total-disabled").val(totalBiaya.formatMoney(0, "Rp "));
		});
	</script>
	<script>
	function openHistori() {
	    window.open("{{ url('komite/students/spp/histori/'.$student->nisn) }}", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=700,left=700,width=1200,height=500");
	}
	</script>
@endsection

@section('inline-style')
<style type="text/css">
	#listings-list tr td {
		vertical-align: middle;
	}
	    .bs-example{
    	margin: 20px;
    }
</style>
@stop