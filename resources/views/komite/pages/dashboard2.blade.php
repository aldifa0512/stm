@extends('komite.base2')

@section('title', 'Dashboard')

@section('content')
<?php
//----------------------------------------------------------------------------------------------------------
//Local functions just for this page    

    function formatTp($tp){
        return ($tp . '/' . ($tp + 1));
    }

    function pilihanTahun($tps, $tpAktif, $name, $id){
        echo '<select name="' . $name . '" id="' . $id . '">';
            foreach ($tps as $key => $value) { 
                if($tpAktif == $value->tp){
                    echo '<option value="' . $value->tp . '" selected>' . formatTp($value->tp) . '</option>';    
                }else{
                    echo '<option value="' . $value->tp . '">' . formatTp($value->tp) . '</option>';    
                }
                
            }
        echo '</select>';
    }

//----------------------------------------------------------------------------------------------------------
//Local variables just for this page 

    $tpAktifObj = get_tp_aktif();
    $tpAktif = $tpAktifObj->tp;
    $tps = get_all_tp();

//----------------------------------------------------------------------------------------------------------
?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><i class="fa fa-bar-chart-o"></i> Chart Siswa dan Pembayaran</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ number_format($siswas['total']) }} org</div>
                                    <div >SISWA TOTAL 5 TAHUN TERAKHIR</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><a href="{{ url('komite/students/recap') }}" style="font-style: none;">View Details</a></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ number_format($siswas['total_aktif']) }} org</div>
                                    <div>TOTAL SISWA AKTIF {{ formatTp($tpAktif) }}</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><a href="{{ url('komite/students') }}">View Details</a></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ number_format($siswas['total_pasif']) }} org</div>
                                    <div>SISWA TIDAK AKTIF 5 TAHUN TERAKHIR</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><a href="{{ url('komite/students/pasif') }}">View Details</a></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="huge">{{ number_format($siswas['total_beda']) }} org</div>
                                    <div>RECORD SISWA TIDAK BERSTATUS</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left"><a href="{{ url('komite/siswa_nonstatus') }}"> View Details</a></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> GRAFIK PEMBAYARAN UANG SEKOLAH SISWA PADA {{ formatTp($tpAktif) }}
                            <div class="pull-right">
                                {{ pilihanTahun($tps, $tpAktif, "pilihTahunGrafikBalok", "pilihTahunGrafikBalok") }}
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12"  id="barAjax">
                                    <div id="morris-bar-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> GRFIK PENERIMAAN SISWA 10 TAHUN TERAKHIR
                            <div class="pull-right">
                                
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-area-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Chart Komposisi Siswa Terhadap Jurusan Pada Angkatan <b>{{ (formatTp($tpAktif - 2)) }}</b>
                            </div>
                            <div class="panel-body">
                                <div id="morris-donut-chart2"></div>
                                <a href="#" class="btn btn-default btn-block"><b>Dari {{ siswa_tahunke((formatTp($tpAktif - 2))) }} siswa</b></a>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Chart Komposisi Siswa Terhadap Jurusan Pada Angkatan <b>{{ (formatTp($tpAktif - 1)) }}</b>
                            </div>
                            <div class="panel-body">
                                <div id="morris-donut-chart1"></div>
                                <a href="#" class="btn btn-default btn-block"><b>Dari {{ siswa_tahunke((formatTp($tpAktif - 1))) }} siswa</b></a>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Chart Komposisi Siswa Terhadap Jurusan Pada Angkatan <b>{{ formatTp($tpAktif) }}</b>
                            </div>
                            <div class="panel-body">
                                <div id="morris-donut-chart"></div>
                                <a href="#" class="btn btn-default btn-block"><b>Dari {{ siswa_tahunke((formatTp($tpAktif))) }} siswa</b></a>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <!-- jQuery -->
    <script src="{{ asset('assets/sbadmin/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('assets/sbadmin/vendor/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/sbadmin/vendor/morrisjs/morris.min.js') }}"></script>
    <!-- <script src="{{ asset('assets/sbadmin/data/morris-data.js') }}"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/sbadmin/dist/js/sb-admin-2.js') }}"></script>
@endsection
@section('inline-script')
<script type="text/javascript">
    
    $("#pilihTahunGrafikBalok").on('keyup keypress blur change', function(e) {
            var value = $( this ).val();
            $("#grafikTahun").html(value + '/' + ( Number(value) + 1 ));
            $.ajax({
                method: 'POST',
                url: "{{ url('komite/analisis/pilihTahunGrafikBalok') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    value: value
                },
                success: function(res) {
                    var result = [];
                    for (var i = 0; i < res.length; i++) {
                        var bulanObj = {
                            y: res[i].bulan,
                            a: res[i].satu,
                            b: res[i].dua,
                            c: res[i].tiga
                        }
                        result.push(bulanObj);
                    }
                    $("#morris-bar-chart").remove();
                    $("#barAjax").html('<div id="morris-bar-chart"></div>');
                    ShowGrpah(result);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log( jqXHR);
                    console.log( textStatus);
                    console.log( errorThrown);
                }
            });
        });

    function ShowGrpah(data) {
        Morris.Bar({
            element: 'morris-bar-chart',
            data: data,
            xkey: 'y',
            ykeys: ['a', 'b', 'c'],
            labels: ['Kelas X', 'Kelas XI', 'Kelas XII']
        });
    }

    Morris.Bar({
      element: 'morris-bar-chart',
      data: [
        <?php foreach ($data_bar as $key => $value) { ?>
            {
                y: '{{ $value['bulan'] }}',
                a: {{ $value['satu'] }},
                b: {{ $value['dua'] }},
                c: {{ $value['tiga'] }}
            }, 
        <?php } ?>
      ],
      xkey: 'y',
      ykeys: ['a', 'b', 'c'],
      labels: ['Kelas X', 'Kelas XI', 'Kelas XII']
    });

    Morris.Area({
        element: 'morris-area-chart',
        data: [
        <?php foreach ($data_area as $key => $value) { ?>
            {
                tahun: '{{ $value['tahun'] }}',
                jumlah: {{ $value['jumlah'] }}
            }, 
        <?php } ?>
        ],
        xkey: 'tahun',
        ykeys: ['jumlah'],
        labels: ['Jumlah Siswa'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [
        <?php foreach ($data_jurusan as $key => $value) { ?>
            {
                label: '{{ $value['jurusan'] }} (org)',
                value: {{ $value['jumlah'] }}
            },
        <?php } ?>
        ],
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart1',
        data: [
        <?php foreach ($data_jurusan1 as $key => $value) { ?>
            {
                label: '{{ $value['jurusan'] }} (org)',
                value: {{ $value['jumlah'] }}
            },
        <?php } ?>
        ],
        resize: true
    });

    Morris.Donut({
        element: 'morris-donut-chart2',
        data: [
        <?php foreach ($data_jurusan2 as $key => $value) { ?>
            {
                label: '{{ $value['jurusan'] }} (org)',
                value: {{ $value['jumlah'] }}
            },
        <?php } ?>
        ],
        resize: true
    });
</script>
@endsection
