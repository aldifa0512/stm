CREATE TABLE IF NOT EXISTS `ajax_categories` (
  `id` int(11) NOT NULL auto_increment,
  `category` varchar(50) NOT NULL,
  `pid` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `ajax_categories`
--

INSERT INTO `ajax_categories` (`id`, `category`, `pid`) VALUES
(1, 'Tutorials', 0),
(2, 'Demos', 0),
(3, 'Entertainment', 0),
(4, 'Real Estate', 0),
(5, 'Web Development', 0),
(6, 'Browsers', 0),
(7, 'Laptop', 0),
(8, 'PHP', 1),
(9, 'jQuery', 1),
(10, 'AJAX', 1),
(11, 'CodeIgniter', 1),
(12, 'PHP demos', 2),
(13, 'jQuery demos', 2),
(14, 'Film', 3),
(15, 'Music', 3),
(16, 'Commercial', 4),
(17, 'Home', 4),
(18, 'CSS', 5),
(19, 'PHP', 5),
(20, 'FireFox', 6),
(21, 'Internet Explorer', 6),
(22, 'Safari', 6),
(23, 'Opera', 6),
(24, 'IBM', 7),
(25, 'Sony', 7),
(26, 'Dell', 7),
(27, 'HP ', 7),
(28, 'Version 4', 8),
(29, 'Version 5', 8),
(30, 'jQuery Tutorials', 9),
(31, 'jQuery Demos', 9),
(32, 'Codeigniter 1.7', 11),
(33, 'Codeigniter 2.0', 11),
(34, 'Good Demos', 12),
(35, 'Average Demos', 12),
(36, 'Good Demos', 13),
(37, 'Old', 28),
(38, 'New', 29),
(39, 'Good', 30),
(40, 'Bad', 31),
(41, 'Old', 32),
(42, 'New', 33);
