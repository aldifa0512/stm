-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: 08 Mar 2018 pada 01.54
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smk2payakumbuh2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_bayar_edit`
--

CREATE TABLE `history_bayar_edit` (
  `id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `nisn` bigint(10) UNSIGNED ZEROFILL NOT NULL,
  `no_kwitansi` varchar(50) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `tgl_bayar` int(11) NOT NULL,
  `bln_bayar` int(2) NOT NULL,
  `thn_bayar` int(4) NOT NULL,
  `bln_dibayar_spp` varchar(200) NOT NULL,
  `bln_dibayar_lain2` varchar(200) NOT NULL,
  `bln_dibayar_pembangunan` varchar(200) NOT NULL,
  `bln_dibayar_pramuka` varchar(200) NOT NULL,
  `tp` varchar(10) NOT NULL,
  `semester` int(2) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `spp` int(8) DEFAULT NULL,
  `pembangunan` int(8) DEFAULT NULL,
  `pramuka` int(8) DEFAULT NULL,
  `lain_lain` int(8) DEFAULT NULL,
  `nominal` int(10) NOT NULL,
  `note` varchar(255) NOT NULL,
  `note2` varchar(150) DEFAULT NULL,
  `oleh_id` varchar(30) NOT NULL,
  `oleh` varchar(25) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_bayar_edit`
--

INSERT INTO `history_bayar_edit` (`id`, `jurusan_id`, `kelas_id`, `nisn`, `no_kwitansi`, `nama`, `tgl_bayar`, `bln_bayar`, `thn_bayar`, `bln_dibayar_spp`, `bln_dibayar_lain2`, `bln_dibayar_pembangunan`, `bln_dibayar_pramuka`, `tp`, `semester`, `kelas`, `spp`, `pembangunan`, `pramuka`, `lain_lain`, `nominal`, `note`, `note2`, `oleh_id`, `oleh`, `updated_at`, `created_at`) VALUES
(1, 0, 0, 9992714616, '', 'TEZI DAFRI KAPUR', 3, 10, 2017, '[12]', '[]', '[]', '[12]', '2017', 1, 'XI TEI 1', 80000, 0, 1500, 0, 81500, 'tu', 'L', 'KMT-170210113021', 'Aulia Rahmi', '2018-03-08 00:49:40', '2018-03-08 00:49:40'),
(2, 0, 0, 0001898693, '', 'Febri Yandi', 3, 10, 2017, '[10,11]', '[]', '[]', '[10,11]', '2017', 1, 'X TJTL', 180000, 0, 3000, 0, 183000, 'sb', 'ppdb 2 bulan', 'KMT-170210113021', 'Aulia Rahmi', '2018-03-08 00:51:02', '2018-03-08 00:51:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history_bayar_edit`
--
ALTER TABLE `history_bayar_edit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history_bayar_edit`
--
ALTER TABLE `history_bayar_edit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
