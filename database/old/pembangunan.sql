-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 28 Mar 2017 pada 06.17
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smk2payakumbuh`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembangunan`
--

CREATE TABLE `pembangunan` (
  `id` int(10) NOT NULL,
  `nisn` bigint(10) UNSIGNED ZEROFILL NOT NULL,
  `kelas` int(2) NOT NULL,
  `tp` varchar(15) NOT NULL,
  `januari` int(10) DEFAULT NULL,
  `februari` int(10) DEFAULT NULL,
  `maret` int(10) DEFAULT NULL,
  `april` int(10) DEFAULT NULL,
  `mei` int(10) DEFAULT NULL,
  `juni` int(10) DEFAULT NULL,
  `juli` int(10) DEFAULT NULL,
  `agustus` int(10) DEFAULT NULL,
  `september` int(10) DEFAULT NULL,
  `oktober` int(10) DEFAULT NULL,
  `november` int(10) DEFAULT NULL,
  `desember` int(10) DEFAULT NULL,
  `total` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembangunan`
--

INSERT INTO `pembangunan` (`id`, `nisn`, `kelas`, `tp`, `januari`, `februari`, `maret`, `april`, `mei`, `juni`, `juli`, `agustus`, `september`, `oktober`, `november`, `desember`, `total`, `updated_at`, `created_at`) VALUES
(5, 0005979509, 37, '2016', 20000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20000, 0, '2017-01-02 14:44:52', '2016-12-30 20:07:34'),
(6, 9972739465, 38, '2016', 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2017-03-26 16:06:51', '2017-01-01 09:09:24'),
(7, 0010613080, 35, '2016', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2017-01-01 17:07:38', '2017-01-01 12:30:45'),
(8, 0003314191, 6, '2016', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2017-03-26 17:31:25', '2017-03-26 17:31:25'),
(9, 0003314191, 6, '2016', NULL, NULL, 20000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2017-03-26 17:31:46', '2017-03-26 17:31:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pembangunan`
--
ALTER TABLE `pembangunan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembangunan`
--
ALTER TABLE `pembangunan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
