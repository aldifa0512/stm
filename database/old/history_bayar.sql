-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2017 at 05:32 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smk2payakumbuh`
--

-- --------------------------------------------------------

--
-- Table structure for table `history_bayar`
--

CREATE TABLE `history_bayar` (
  `id` int(11) NOT NULL,
  `nisn` bigint(10) UNSIGNED ZEROFILL NOT NULL,
  `nama` varchar(30) NOT NULL,
  `no_kwitansi` varchar(10) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `bln_bayar` int(2) NOT NULL,
  `thn_bayar` int(4) NOT NULL,
  `bln_dibayar_spp` varchar(200) NOT NULL,
  `bln_dibayar_lain2` varchar(30) NOT NULL,
  `bln_dibayar_pembangunan` varchar(30) NOT NULL,
  `bln_dibayar_pramuka` varchar(30) NOT NULL,
  `tp` varchar(10) NOT NULL,
  `semester` int(2) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `spp` int(8) DEFAULT NULL,
  `pembangunan` int(8) DEFAULT NULL,
  `pramuka` int(8) DEFAULT NULL,
  `lain_lain` int(8) DEFAULT NULL,
  `nominal` int(10) NOT NULL,
  `note` varchar(255) NOT NULL,
  `oleh_id` varchar(30) NOT NULL,
  `oleh` varchar(25) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_bayar`
--

INSERT INTO `history_bayar` (`id`, `nisn`, `nama`, `no_kwitansi`, `tgl_bayar`, `bln_bayar`, `thn_bayar`, `bln_dibayar_spp`, `bln_dibayar_lain2`, `bln_dibayar_pembangunan`, `bln_dibayar_pramuka`, `tp`, `semester`, `kelas`, `spp`, `pembangunan`, `pramuka`, `lain_lain`, `nominal`, `note`, `oleh_id`, `oleh`, `updated_at`, `created_at`) VALUES
(1, 9972739465, 'Aan Rinaldi', '201615', '2017-04-05', 4, 2017, '[4]', '[]', '[4]', '[4]', '2016', 1, 'XII TPS', 80000, 10000, 5000, 0, 95000, 'Tes Bayar', '0', 'Aldi Fajrin', '2017-04-05 16:06:03', '2017-04-05 16:06:03'),
(2, 9972739465, 'Aan Rinaldi', '201616', '2017-04-05', 4, 2017, '[5,6,7,8,9,10,11,12,13,14]', '[]', '[5]', '[5,6,7,8,9,10,11,12,13,14,15]', '2016', 1, 'XII TPS', 800000, 190000, 55000, 0, 1045000, 'Tes Bayar', '0', 'Aldi Fajrin', '2017-04-05 16:07:32', '2017-04-05 16:07:32'),
(3, 9972739465, 'Aan Rinaldi', '201617', '2017-04-05', 4, 2017, '[15]', '[]', '[]', '[]', '2016', 1, 'XII TPS', 80000, 0, 0, 0, 80000, 'Tes Bayar', '0', 'Aldi Fajrin', '2017-04-05 16:08:15', '2017-04-05 16:08:15'),
(4, 9972615083, 'ABRAL SOPIAN', '201618', '2017-04-05', 4, 2017, '[4]', '[]', '[]', '[]', '2016', 1, 'XII TKK', 80000, 0, 0, 0, 80000, 'Tes bayar', '0', 'Aldi Fajrin', '2017-04-05 16:10:19', '2017-04-05 16:10:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history_bayar`
--
ALTER TABLE `history_bayar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_kwitansi` (`no_kwitansi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history_bayar`
--
ALTER TABLE `history_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
