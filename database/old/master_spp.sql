-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2017 at 12:32 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smk2payakumbuh`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_spp`
--

CREATE TABLE `master_spp` (
  `id` int(2) NOT NULL,
  `tp` varchar(15) NOT NULL,
  `semester` int(2) NOT NULL,
  `spp` int(8) NOT NULL,
  `pembangunan` int(8) NOT NULL,
  `pramuka` int(8) NOT NULL,
  `lain_lain` int(8) NOT NULL,
  `status` varchar(10) NOT NULL,
  `batas_bln_spp` int(2) NOT NULL,
  `batas_bln_pembangunan` int(2) NOT NULL,
  `batas_bln_pramuka` int(2) NOT NULL,
  `batas_bln_lain2` int(2) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_spp`
--

INSERT INTO `master_spp` (`id`, `tp`, `semester`, `spp`, `pembangunan`, `pramuka`, `lain_lain`, `status`, `batas_bln_spp`, `batas_bln_pembangunan`, `batas_bln_pramuka`, `batas_bln_lain2`, `updated_at`, `created_at`) VALUES
(1, '2015', 1, 80000, 200000, 5000, 0, '-', 13, 4, 9, 5, '2017-04-02 10:23:01', '0000-00-00 00:00:00'),
(2, '2016', 1, 200000, 200000, 3000, 0, 'aktif', 15, 0, 0, 0, '2017-03-28 15:40:06', '2016-10-22 13:47:01'),
(3, '2014', 0, 70000, 200000, 5000, 0, '', 0, 0, 0, 0, '2017-01-01 18:44:28', '2016-12-17 14:40:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_spp`
--
ALTER TABLE `master_spp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_spp`
--
ALTER TABLE `master_spp`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
