<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rombel extends Model
{
    protected $table = 'rombels';

    public function students()
    {
    	$this->belongsTo('App\Models\Student', 'kelas', 'id');
    }

    public function jurusan()
    {
    	$this->belongsTo('App\Models\Jurusan');
    }
}
