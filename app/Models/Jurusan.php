<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = 'jurusan';

    public function rombels()
    {
        return $this->hasMany('App\Models\Rombel', 'jurusan_id', 'id');
    }

    public function students()
    {
    	return $this->hasMany('App\Models\Student', 'jurusan', 'id');
    }
}
