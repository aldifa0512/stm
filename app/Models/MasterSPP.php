<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterSPP extends Model
{
    protected $table = 'master_spp';
}
