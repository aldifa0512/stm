<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reff extends Model
{
    protected $primaryKey = 'id_reff';
    protected $table = 'reff';
}
