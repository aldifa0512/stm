<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DropOut extends Model
{
    protected $primaryKey = 'nisn';
    protected $table = 'drop_out';
}
