<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    /*public function package()
    {
    	return $this->hasOne('\App\Models\Package', 'id', 'package_id');
    }*/

    public function jurusan()
    {
        return $this->hasOne('App\Models\Jurusan', 'id', 'id');
    }

    public function rombel()
    {
        return $this->hasOne('App\Models\Rombel', 'id', 'id');
    }

    public function dropOut()
    {
        return $this->hasOne('App\Models\dropOut', 'nisn', 'nisn');
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'students';
    protected $primaryKey = 'nisn';
}
