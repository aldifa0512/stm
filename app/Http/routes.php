<?php

use App\Models\Message;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Frontend\FrontendController@index');
//Route::get('category', 'Frontend\FrontendController@category');
Route::get('details', 'Frontend\FrontendController@details');

Route::group(['prefix' => 'category'], function(){
	Route::get('{slug}', 'Frontend\ListingsController@category');
});


Route::post('home/upload', 'HomeController@upload');

Route::controllers([
	'admin/auth' => 'Auth\AuthController',
	'app-admin/password' => 'Auth\PasswordController',
]);

Route::controllers([
	'auth-tu' => 'Auth\AuthTuController'
]);

Route::controllers([
	'auth-komite' => 'Auth\AuthKomiteController'
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {

	Route::get('/', 'Backend\BackendController@index');

	Route::group(['prefix' => 'users'], function() {
		Route::group(['prefix' => 'komite'], function(){
			Route::get('/', 'Backend\UsersController@komite_index');
			Route::get('create', 'Backend\UsersController@komite_create');
			Route::post('create', 'Backend\UsersController@komite_store');
			Route::get('edit/{id}', 'Backend\UsersController@komite_edit');
			Route::post('edit/{id}', 'Backend\UsersController@komite_update');
			Route::get('delete/{id}', 'Backend\UsersController@komite_destroy');
		});
		Route::group(['prefix' => 'tu'], function(){
			Route::get('/', 'Backend\UsersController@tu_index');
			Route::get('create', 'Backend\UsersController@tu_create');
			Route::post('create', 'Backend\UsersController@tu_store');
			Route::get('edit/{id}', 'Backend\UsersController@tu_edit');
			Route::post('edit/{id}', 'Backend\UsersController@tu_update');
			Route::get('delete/{id}', 'Backend\UsersController@tu_destroy');
		});
		Route::group(['prefix' => 'admin'], function(){
			Route::get('/', 'Backend\UsersController@admin_index');
			Route::get('create', 'Backend\UsersController@admin_create');
			Route::post('create', 'Backend\UsersController@admin_store');
			Route::get('edit/{id}', 'Backend\UsersController@admin_edit');
			Route::post('edit/{id}', 'Backend\UsersController@admin_update');
			Route::get('delete/{id}', 'Backend\UsersController@admin_destroy');
		});
	});

	Route::group(['prefix' => 'customers'], function() {
		Route::get('/', 'Backend\CustomersController@index');
		Route::get('create', 'Backend\CustomersController@create');
		Route::post('create', 'Backend\CustomersController@store');
		Route::get('edit/{id}', 'Backend\CustomersController@edit');
		Route::post('edit/{id}', 'Backend\CustomersController@update');
		Route::get('delete/{id}', 'Backend\CustomersController@destroy');
	});

	Route::group(['prefix' => 'roles'], function() {
		Route::get('/', 'Backend\RolesController@index');
		Route::get('create', 'Backend\RolesController@create');
		Route::post('create', 'Backend\RolesController@store');
		Route::get('edit/{id}', 'Backend\RolesController@edit');
		Route::post('edit/{id}', 'Backend\RolesController@update');
		Route::get('delete/{id}', 'Backend\RolesController@destroy');
	});

Route::group(['prefix' => 'rombels'], function() {
		Route::get('/', 'Backend\RombelsController@index');
		Route::get('create', 'Backend\RombelsController@create');
		Route::post('create', 'Backend\RombelsController@store');
		Route::get('edit/{id}', 'Backend\RombelsController@edit');
		Route::post('edit/{id}', 'Backend\RombelsController@update');
		Route::get('drop/{id}', 'Backend\RombelsController@drop');
		Route::get('drop/{id}/hapus', 'Backend\RombelsController@dropGet');
		Route::get('drop/{id}/batal', 'Backend\RombelsController@dropNo');
		
		Route::post('getJurusanAjax', 'Backend\RombelsController@getJurusanAjax');
		Route::post('getExistedRombelsAjax', 'Backend\RombelsController@getExistedRombelsAjax');
	});

	Route::group(['prefix' => 'spp'], function(){
		Route::get('/', 'Backend\SppController@index');
		Route::get('active/{id}', 'Backend\SppController@active');
		Route::get('create', 'Backend\SppController@create');
		Route::post('create', 'Backend\SppController@store');
		Route::get('edit/{id}', 'Backend\SppController@edit');
		Route::post('edit/{id}', 'Backend\SppController@update');
		Route::get('delete/{id}', 'Backend\SppController@delete_spp');
		Route::get('delete/{id}/Adelete', 'Backend\SppController@Adelete_spp');
		Route::get('delete/{id}/Cdelete', 'Backend\SppController@Cdelete_spp');
	});

	Route::group(['prefix' => 'import'], function(){
		Route::get('/', 'Backend\ImportController@index');
		Route::post('/', 'Backend\ImportController@import');
		Route::get('backup', 'Backend\ImportController@backup');
	});

	Route::group(['prefix' => 'jurusan'], function() {
		Route::get('/', 'Backend\JurusanController@index');

		Route::get('create', 'Backend\JurusanController@create');
		Route::post('create', 'Backend\JurusanController@store');

		Route::get('edit/{id}', 'Backend\JurusanController@edit');
		Route::post('edit/{id}', 'Backend\JurusanController@update');

		Route::get('delete/{id}', 'Backend\JurusanController@delete');
		Route::get('delete/{id}/hapus', 'Backend\JurusanController@delyes');
		Route::get('delete/{id}/batal', 'Backend\JurusanController@delno');
	});

	Route::group(['prefix' => 'setting'], function() {
		Route::get('/', 'Backend\SettingController@index');
		
		Route::group(['prefix' => 'roles'], function() {
			Route::get('/', 'Backend\SettingController@teachersRoles');
			Route::post('create', 'Backend\SettingController@teachersRolesCreate');
			Route::post('edit/{id}', 'Backend\SettingController@teachersRolesEdit');
			Route::post('delete/{id}', 'Backend\SettingController@teachersRolesDelete');
			Route::post('guruSearchRoleAjax', 'Backend\SettingController@guruSearchRoleAjax');			
		});

		Route::get('create', 'Backend\SettingController@create');
		Route::post('create', 'Backend\SettingController@store');

		Route::post('guruSearchAjax', 'Backend\SettingController@guruSearchAjax');
		
		Route::get('edit/{id}', 'Backend\SettingController@edit');
		Route::post('edit/{id}', 'Backend\SettingController@update');

		Route::get('delete/{id}', 'Backend\SettingController@delete');
		Route::get('delete/{id}/hapus', 'Backend\SettingController@delyes');
		Route::get('delete/{id}/batal', 'Backend\SettingController@delno');
	});

	Route::group(['prefix' => 'madding'], function() {
		Route::get('/', 'Backend\MaddingController@index');
		Route::get('active/{id}', 'Backend\MaddingController@active');

		Route::get('create', 'Backend\MaddingController@create');
		Route::post('create', 'Backend\MaddingController@store');

		Route::get('edit/{id}', 'Backend\MaddingController@edit');
		Route::post('edit/{id}', 'Backend\MaddingController@update');

		Route::get('delete/{id}', 'Backend\MaddingController@delete');
		Route::get('delete/{id}/hapus', 'Backend\MaddingController@delyes');
		Route::get('delete/{id}/batal', 'Backend\MaddingController@delno');
	});

	Route::group(['prefix' => 'tp'], function() {
		Route::get('/', 'Backend\TpController@index');
		Route::get('active/{id}', 'Backend\TpController@active');

		Route::get('create', 'Backend\TpController@create');
		Route::post('create', 'Backend\TpController@store');

		Route::get('edit/{id}', 'Backend\TpController@edit');
		Route::post('edit/{id}', 'Backend\TpController@update');

		Route::get('delete/{id}', 'Backend\TpController@delete');
		Route::get('delete/{id}/hapus', 'Backend\TpController@delyes');
		Route::get('delete/{id}/batal', 'Backend\TpController@delno');
	});

	
	Route::get('geo/getZone', 'Backend\BackendController@getZone');
	Route::get('/send', 'EmailController@send');

});

Route::group(['prefix' => 'tu-user', 'middleware' => 'authTu'], function() {

	// Index (Dashboard) of subscriber page
	Route::get('/', 'Administrasions\AdministrasionsController@index');
	Route::get('listing_stats', 'Administrasions\ListingsController@statistics');
	Route::get('edit_info', 'Administrasions\AdministrasionsController@edit_info');
	Route::post('edit_info/{id}', 'Administrasions\AdministrasionsController@update_info');

	Route::group(['prefix' => 'listings'], function(){
		Route::get('/', 'Administrasions\ListingsController@index');
		Route::get('create', 'Administrasions\ListingsController@create');
		Route::post('create', 'Administrasions\ListingsController@store');
		Route::get('edit/{id}', 'Administrasions\ListingsController@edit');
		Route::post('edit/{id}', 'Administrasions\ListingsController@update');
		Route::get('delete/{id}', 'Administrasions\ListingsController@destroy');

		Route::post('upload_image', 'Administrasions\ListingsController@upload_image');
		//Route::post('listing_stats', 'Administrasions\ListingsController@upload_image');


		Route::get('buy', 'Administrasions\ListingsController@buy');
		Route::post('buy', 'Administrasions\ListingsController@buy_listing_slot');
		Route::get('buy/complete', 'Administrasions\ListingsController@buyComplete');

		Route::get('renew', 'Administrasions\ListingsController@renew');
		Route::post('renew', 'Administrasions\ListingsController@renew_listing_slot');
		Route::get('renew/complete', 'Administrasions\ListingsController@renewComplete');

		Route::get('get_sub_categories', 'Administrasions\ListingsController@getSubcategory');
	});

	//Route::get('listing-wizard', 'Administrasions\ListingsController@wizard');
	Route::get('student-wizard', 'Administrasions\StudentsController@wizard');

	Route::group(['prefix' => 'students'], function(){
		Route::get('importedStudents', 'Administrasions\StudentsController@importedStudents');
		Route::post('importedStudents/{className}', 'Administrasions\StudentsController@importedStudentsbyClassName');

		Route::get('/', 'Administrasions\StudentsController@index');
		Route::post('rombelAjax', 'Administrasions\StudentsController@rombelAjax');
		
		Route::get('enroll', 'Administrasions\StudentsController@enroll');
		Route::post('enroll', 'Administrasions\StudentsController@store_enroll');

		Route::get('drop-out', 'Administrasions\StudentsController@drop_out');
		Route::post('drop-out', 'Administrasions\StudentsController@delete');
		Route::get('drop/{nisn}', 'Administrasions\StudentsController@dropSiswa');
		Route::post('drop/{nisn}', 'Administrasions\StudentsController@deleteSiswa');

		Route::get('edit/{nisn}', 'Administrasions\StudentsController@edit');
		Route::post('edit/{nisn}', 'Administrasions\StudentsController@update');

		Route::get('rombel', 'Administrasions\StudentsController@rombel');
		Route::post('rombel', 'Administrasions\StudentsController@rombelPost');
		
		Route::get('rombel/tunggakan/{kelas}', 'Administrasions\StudentsController@rombelTunggakan');
		
		Route::post('rombel/action/{id}', 'Administrasions\StudentsController@rombelAction');
		
		Route::get('rombel/lihat/{kelas}', 'Administrasions\StudentsController@rombelLihat');
		Route::get('rombel/claim/{kelas}', 'Administrasions\StudentsController@claim_siswa');
		Route::get('rombel/claim-tahun-masuk/{kelas}', 'Administrasions\StudentsController@claim_tahun');
		//Route::post('rombel', 'Administrasions\StudentsController@rombelPost');
		
		Route::post('rombel/absen/{kelas}', 'Administrasions\StudentsController@rombelAbsen');
		
		Route::post('rombel/surat/{kelas}', 'Administrasions\StudentsController@rombelSurat');

		Route::get('filter', 'Administrasions\StudentsController@get_filter');
		Route::post('filter', 'Administrasions\StudentsController@filter');
		Route::post('upload_image', 'Administrasions\StudentsController@upload_image');
		
		Route::get('pasif', 'Administrasions\StudentsController@indexPasif');
		
		Route::get('recap', 'Administrasions\StudentsController@indexRecap');
		Route::get('recap/orderby/{query}/{method}', 'Administrasions\StudentsController@queryRecap');

		Route::get('export', 'Administrasions\StudentsController@ExcelDataSiswa');
		Route::get('export/{kelas}', 'Administrasions\StudentsController@ExcelDataSiswaKelas');

		Route::get('bayar/recap', 'Administrasions\StudentsController@bayar_Recap');
		Route::get('bayar/rombel', 'Administrasions\StudentsController@bayar_rombel');

		Route::group(['prefix' => 'spp'], function(){
			Route::get('/', 'Administrasions\SPPController@index');
			Route::get('create', 'Administrasions\SPPController@create');
			Route::post('create', 'Administrasions\SPPController@store');
			Route::get('edit/{id}', 'Administrasions\SPPController@edit');
			Route::post('edit/{id}', 'Administrasions\SPPController@update');
			Route::get('delete/{id}', 'Administrasions\SPPController@destroy');

			Route::get('bayar/{id}', 'Administrasions\SPPController@bayarSPP3');
			Route::post('bayar/{id}', 'Administrasions\SPPController@bayarSPP_post3');
			//Route::post('komite_stats', 'Administrasions\SPPController@upload_image');

			Route::get('filter', 'Administrasions\StudentsController@get_filter_spp');
			Route::post('filter', 'Administrasions\StudentsController@filter_spp');
			
			Route::get('histori/{id}', 'Administrasions\SPPController@histori');
			Route::get('histori/{nisn}/export', 'Administrasions\SPPController@histori_export');

			Route::get('invoice/{id}/{tp}/{semester}/{no_kuitansi}', 'Administrasions\SPPController@invoice');
			Route::get('invoiceExcel/{id}/{tp}/{semester}/{no_kuitansi}', 'Administrasions\SPPController@invoiceExcel');
			Route::get('invoice/{nisn}/export', 'Administrasions\SPPController@invoice_export');

			Route::get('rombel/{kelas}/lap', 'Administrasions\SPPController@lapKelas');
			//Route::get('histori/{id}/export', 'Administrasions\SPPController@histori_export');

			Route::get('buy', 'Administrasions\SPPController@buy');
			Route::post('buy', 'Administrasions\SPPController@buy_komite_slot');
			Route::get('buy/complete', 'Administrasions\SPPController@buyComplete');

			Route::get('renew', 'Administrasions\SPPController@renew');
			Route::post('renew', 'Administrasions\SPPController@renew_komite_slot');
			Route::get('renew/complete', 'Administrasions\SPPController@renewComplete');

			Route::get('get_sub_categories', 'Administrasions\SPPController@getSubcategory');
		});
	});

	Route::group(['prefix' => 'laporan'], function(){
		Route::get('/', 'Administrasions\SPPController@index');
		Route::get('invoice', 'Administrasions\LaporanController@invoice');
		Route::get('invoice/cetak', 'Administrasions\LaporanController@invoice_cetak');
		Route::post('invoice/cetak', 'Administrasions\LaporanController@invoice_cetak_post');
		Route::get('invoice/{id}/{tp}/{semester}/{no_kuitansi}', 'Administrasions\LaporanController@lihat');
		Route::post('filter', 'Administrasions\LaporanController@filter');
		Route::get('penyesuaian', 'Administrasions\LaporanController@penyesuaian_list');
		Route::get('penyesuaian/{id}', 'Administrasions\LaporanController@penyesuaian');
		Route::post('penyesuaian/{id}', 'Administrasions\LaporanController@penyesuaian_post');
		Route::get('rombel', 'Administrasions\LaporanController@rombel');

		Route::group(['prefix' => 'rombel'], function(){
			Route::get('lengkap/{id}', 'Administrasions\LaporanRombelController@lap_lengkap');
			Route::get('tunggakan/{id}', 'Administrasions\LaporanRombelController@lap_tunggakan');
		});

	});

	Route::group(['prefix' => 'sub-account'], function(){
		Route::get('/', 'Administrasions\AdministrasionsController@sub_account');
		Route::get('create', 'Administrasions\AdministrasionsController@create_sub_account');
		Route::post('create', 'Administrasions\AdministrasionsController@store_sub_account');
	});
});

Route::group(['prefix' => 'komite', 'middleware' => 'authKomite'], function() {

	// Index (Dashboard) of subscriber page
	Route::get('/', 'Komite\KomiteController@index');
	
	Route::get('/siswa_nonstatus', 'Komite\KomiteController@nonstatus');
	
	Route::group(['prefix' => 'databasesBackups'], function(){
		Route::get('/', 'Komite\KomiteController@databasesBackups');
		Route::get('run', 'Komite\KomiteController@databasesBackupsRun');
		Route::get('download/{path}', 'Komite\KomiteController@databasesBackupsDownload');
		Route::get('delete/{path}', 'Komite\KomiteController@databasesBackupsDelete');
		Route::post('delete/{path}/konfirm', 'Komite\KomiteController@databasesBackupsDeleteKonfirm');
	});
	
	Route::get('listing_stats', 'Komite\ListingsController@statistics');
	Route::get('edit_info', 'Komite\KomiteController@edit_info');
	Route::post('edit_info/{id}', 'Komite\KomiteController@update_info');


	//Route::get('listing-wizard', 'Komite\ListingsController@wizard');
	Route::get('student-wizard', 'Komite\StudentsController@wizard');

	Route::group(['prefix' => 'students'], function(){
		Route::get('/', 'Komite\StudentsController@index');

		Route::get('enroll', 'Komite\StudentsController@enroll');
		Route::post('enroll', 'Komite\StudentsController@store_enroll');

		Route::get('drop-out', 'Komite\StudentsController@drop_out');
		Route::post('drop-out', 'Komite\StudentsController@delete');
		Route::get('drop/{id}', 'Komite\StudentsController@dropSiswa');
		Route::post('drop/{id}', 'Komite\StudentsController@deleteSiswa');

		Route::get('edit/{id}', 'Komite\StudentsController@edit');
		Route::post('edit/{id}', 'Komite\StudentsController@update');

		Route::get('rombel', 'Komite\StudentsController@rombel');
		Route::post('rombel', 'Komite\StudentsController@rombelPost');

		Route::get('rombel/lihat/{kelas}', 'Komite\StudentsController@rombelLihat');
		
		Route::get('rombel/tunggakan/{kelas}', 'Komite\StudentsController@rombelTunggakan');
		
		Route::post('rombel/action/{kelas}', 'Komite\StudentsController@rombelAction');
		//Route::post('rombel', 'Administrasions\StudentsController@rombelPost');
		Route::get('pasif', 'Komite\StudentsController@indexPasif');

		Route::get('filter', 'Komite\StudentsController@get_filter');
		Route::post('filter', 'Komite\StudentsController@filter');
		Route::post('upload_image', 'Komite\StudentsController@upload_image');
		Route::get('recap', 'Komite\StudentsController@indexRecap');
		Route::get('recap/orderby/{query}/{method}', 'Komite\StudentsController@queryRecap');

		Route::get('export', 'Komite\StudentsController@ExcelDataSiswa');

		Route::get('profile/{nisn}', 'Komite\StudentsController@profile');
		
		Route::get('surat-tunggakan/{nisn}', 'Komite\StudentsController@surat_tunggakan');

		Route::group(['prefix' => 'spp'], function(){
			Route::get('/', 'Komite\SPPController@index');
			Route::get('create', 'Komite\SPPController@create');
			Route::post('create', 'Komite\SPPController@store');
			Route::get('edit/{id}', 'Komite\SPPController@edit');
			Route::post('edit/{id}', 'Komite\SPPController@update');
			Route::get('delete/{id}', 'Komite\SPPController@destroy');

			Route::get('bayar/{id}', 'Komite\SPPController@bayarSPP2');
			Route::get('sukses/{id}', 'Komite\SPPController@sukses');
			Route::post('bayar/{id}', 'Komite\SPPController@bayarSPP_post2');
			Route::post('dataBayar', 'Komite\SPPController@dataBayar');
			//Route::post('komite_stats', 'Komite\SPPController@upload_image');
			
			Route::get('histori/{id}', 'Komite\SPPController@histori');
			Route::get('histori/{nisn}/export', 'Komite\SPPController@histori_export');
			Route::get('detail-histori/{nisn}/{type}', 'Komite\SPPController@detail_histori');

			Route::get('invoice/{id}/{tp}/{semester}/{no_kuitansi}', 'Komite\SPPController@invoice');
			Route::get('invoiceExcel/{id}/{tp}/{semester}/{no_kuitansi}', 'Komite\SPPController@invoiceExcel');
			Route::get('invoice/{nisn}/export', 'Komite\SPPController@invoice_export');

			Route::get('rombel/{kelas}/lap', 'Komite\SPPController@lapKelas');
			//Route::get('histori/{id}/export', 'Komite\SPPController@histori_export');

			Route::get('buy', 'Komite\SPPController@buy');
			Route::post('buy', 'Komite\SPPController@buy_komite_slot');
			Route::get('buy/complete', 'Komite\SPPController@buyComplete');

			Route::get('renew', 'Komite\SPPController@renew');
			Route::post('renew', 'Komite\SPPController@renew_komite_slot');
			Route::get('renew/complete', 'Komite\SPPController@renewComplete');

			Route::get('get_sub_categories', 'Komite\SPPController@getSubcategory');
		});

	});

	Route::group(['prefix' => 'sub-account'], function(){
		Route::get('/', 'Komite\KomiteController@sub_account');
	});

	Route::group(['prefix' => 'laporan'], function(){
		Route::get('/', 'Komite\SPPController@index');
		Route::post('ajax', 'Komite\LaporanController@ajax');
		Route::post('ajaxSuratTunggakan', 'Komite\LaporanController@ajaxSuratTunggakan');
		Route::get('invoice', 'Komite\LaporanController@invoice');
		Route::get('invoice/cetak', 'Komite\LaporanController@invoice_cetak');
		Route::post('invoice/cetak', 'Komite\LaporanController@invoice_cetak_post');
		Route::get('invoice/edit/{no_kuitansi}', 'Komite\LaporanController@invoice_edit');
		Route::post('invoice/edit/{no_kuitansi}', 'Komite\LaporanController@invoice_edit_post');
		Route::get('invoice/{id}/{tp}/{semester}/{no_kuitansi}', 'Komite\LaporanController@lihat');
		Route::post('filter', 'Komite\LaporanController@filter');
		Route::post('filter_bea', 'Komite\LaporanController@filter_bea');
		Route::get('penyesuaian', 'Komite\LaporanController@penyesuaian_list');
		Route::get('penyesuaian/{id}', 'Komite\LaporanController@penyesuaian');
		Route::post('penyesuaian/{id}', 'Komite\LaporanController@penyesuaian_post');
		Route::get('penyesuaian/edit/{id}', 'Komite\LaporanController@penyesuaianEdit');
		Route::post('penyesuaian/edit/{id}', 'Komite\LaporanController@penyesuaianEdit_post');
		Route::get('rombel', 'Komite\LaporanController@rombel');
		
		Route::get('tunggakan', 'Komite\LaporanController@tunggakan');
		Route::post('tunggakan', 'Komite\LaporanController@tunggakan_save');
		Route::get('tunggakan/edit/{id}', 'Komite\LaporanController@tunggakan_edit');
		Route::post('tunggakan/edit/{id}', 'Komite\LaporanController@tunggakan_edit_save');
		
		Route::get('cetakSuratTunggakan', 'Komite\LaporanController@suratTunggakan');
		Route::post('cetakSuratTunggakan', 'Komite\LaporanController@cetakSuratTunggakan');

		Route::group(['prefix' => 'rombel'], function(){
			Route::get('lengkap/{id}', 'Komite\LaporanRombelController@lap_lengkap');
			Route::get('tunggakan/{id}', 'Komite\LaporanRombelController@lap_tunggakan');
		});

	});

	Route::group(['prefix' => 'analisis'], function(){
		Route::get('/', 'Komite\AnalisisController@index');
		Route::post('pilihTahunGrafikBalok', 'Komite\AnalisisController@pilihTahunGrafikBalok');
	});
});
