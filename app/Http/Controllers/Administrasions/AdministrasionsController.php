<?php

namespace App\Http\Controllers\Administrasions;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\Customer;
use App\Models\Student;
use App\Models\Madding;
use App\Models\HistoriSpp;
use Carbon\Carbon;

/**
* 
*/
class AdministrasionsController extends Controller
{
	
	function __construct()
	{
		$this->middleware('authTu');
	}

	public function index()
	{
        $madding = Madding::where('status', 'aktif')->first();
        $historis = HistoriSpp::orderBy('updated_at', 'DSC')->paginate(10);
		return view('tu-user.pages.dashboard',['madding' => $madding, 'historis' => $historis]);
	}

	public function sub_account()
	{
		$tuusers = Customer::all();

		return view('tu-user.pages.account.sub-account.list', ['tuusers' => $tuusers]);
	}

    public function create_sub_account()
    {
        return view('tu-user.pages.account.create');
    }

    public function store_sub_account(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        /*if ($request->input('role') == 'choose-role' || $request->input('role') === null) {
            return redirect()->back()->with('error', 'Please select role for this user');
        }*/

        $customer = new Customer;
        $customer->customer_name = $request->input('name');
        $customer->pic_email = $request->input('email');
        $customer->password = bcrypt($request->input('password'));

        if ($customer->save()) {
            // Attach role to user
            //$customer->roles()->attach($request->input('role'));

            return redirect('tu-user/sub-account')->with('success', 'User berhasil ditambahkan.');
        }
        
    }

	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_info()
    {
        $customer = Customer::find(Auth::customer()->get()->id);
                    //var_dump($customer);die();

        if ($customer) {
            return view('tu-user.pages.account.edit', ['customer' => $customer]);
        }

        return abort(404, 'Request not found');
    }
    public function update_info(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|max:255|unique:users,email,'.$id,
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        /*if ($request->input('role') == 'choose-role' || $request->input('role') === null) {
            return redirect()->back()->with('error', 'Please select role for this user');
        }*/

        $customer = Customer::find($id);

        if ($customer) {
            $customer->customer_name = $request->input('name');
            $customer->pic_email = $request->input('email');
            $customer->password = bcrypt($request->input('password'));

            if ($customer->save()) {
                // Attach role to user
                //$customer->roles()->attach($request->input('role'));

                return redirect('tu-user/edit_info')->with('success', 'User updated success.');
            }
        }
    }
}