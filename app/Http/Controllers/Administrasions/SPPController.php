<?php

namespace App\Http\Controllers\Administrasions;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\Customer;
use App\Models\Listing;
use App\Models\Student;
use App\Models\MasterSPP;
use App\Models\Reff;
use App\Models\Rombel;
use App\Models\Pembangunan;
use App\Models\LainLain;
use App\Models\Pramuka;
use App\Models\Spp;
use App\Models\HistoriSpp;
use App\Models\Tp;
use Excel;
use Storage;
use Image;
use Setting;
use App\Models\ListingMeta;
use Session;
use App\Models\ListingCategory;

class SPPController extends Controller
{
    public function __construct()
    {
        $this->middleware('authTu');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('komite.pages.spp.list');
    }
    
    public function bayarSPP3($id)
    {
        $student = Student::find($id);
        $rombel = Rombel::find($student->kelas);
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        
        if( Spp::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $spps = Spp::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $spps = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if( Pembangunan::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $pembangunans = Pembangunan::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $pembangunans = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if( Pramuka::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $pramukas = Pramuka::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $pramukas = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if( LainLain::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $lains = LainLain::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $lains = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        $spps = array_values($spps);
        $pembangunans = array_values($pembangunans);
        $pramukas = array_values($pramukas);//dd($pramukas);
        $lains = array_values($lains);


        $Mspp = MasterSPP::where('tp', $rombel->kurikulum)->first();//dd($Mspp);
        if($student->tp == null){
            return "Pastikan <b>Tahun Kurikulum Rombel pada Admin</b> telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }
        if ($student) {
            if($student->tp == null){
                return "Pastikan <b>Tahun Masuk</b> siswa telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
            }
            return view('tu-user.pages.spp.bayar3', ['student' => $student, 'spps' => $spps, 'pembangunans' => $pembangunans, 'pramukas' => $pramukas, 'lains' => $lains, 'Mspp' => $Mspp, 'tp' => $tp,  'bln' => $bln, 'rombelM' => true]);
        }

        return abort(404, 'Request not found');
    }

    public function bayarSPP_post3($id, Request $request )
    {
        //dd($request->all());
        $rules = [
            'note' => 'max:255',
        ];

        $validation = Validator::make($request->all(), $rules);
        $bln = [4 => 'januari', 5 => 'februari', 6 => 'maret', 7 => 'april', 8 => 'mei', 9 => 'juni', 10 => 'juli', 11 => 'agustus', 12 => 'september', 13 =>'oktober', 14 => 'november', 15 => 'desember'];

        $student = Student::find($request->input('nisn'));//dd($student);
        $rombel = Rombel::find($student->kelas);
        $MasterSpp = MasterSPP::where('tp', $rombel->kurikulum)->first();
        //$pembangunan = Pembangunan::where('nisn', $student->nisn)->where('tp', $student->tp)->first()->toArray();
        //$pembangunan = array_values($pembangunan);
        $tbpem = 0;

        //------------------------------ dipisahkan dari kelompok karena cara pembayaranya sedikit berbeda, sekaligus untuk validasi
        $pembangunan = Pembangunan::where('nisn', $student->nisn)->where('tp', $student->tp)->first();
        $exist_pembangunan = Pembangunan::where('nisn', $student->nisn)->where('tp', $student->tp)->count();
        if ($exist_pembangunan != 1){ 
            $pembangunan = new Pembangunan;
            $pembangunan->nisn = $request->input("nisn");
            $pembangunan->kelas = $request->input("kelas");
            $pembangunan->tp = $request->input("tp");
        }
        //-------------------------------

        for($i=4; $i<16; $i++){
            $tbpem += $pembangunan[$i];
        }
        if($request->input('pem') > (($MasterSpp->pembangunan)- $tbpem))
            return redirect()->back()->withInput()->withErrors('Uang pembangunan melebihi jumlah yang harus dibayarkan. Sisa uang pembangunan yang harus dibayarkan adalah Rp. '.number_format((($MasterSpp->pembangunan) - $tbpem),2,',','.'));
        
        if($pembangunan->$bln[date('n')+3] != 0 && $request->input('pem') != 0){
            return redirect()->back()->withInput()->withErrors('Pembayaran pembangunan hanya dapat dilakukan sekali selama satu bulan.');
        }
        
        //cek apakah siswa sudah perna melakukan pembayaran pada TP aktif
        $spp = Spp::where('nisn', $student->nisn)->where('tp', $student->tp)->first();
        $exist_spp = Spp::where('nisn', $student->nisn)->where('tp', $student->tp)->count();
        if($exist_spp != 1){
            $spp = new Spp;
            $spp->nisn = $request->input("nisn");
            $spp->kelas = $request->input("kelas");
            $spp->tp = $request->input("tp");
        }

        $pramuka = Pramuka::where('nisn', $student->nisn)->where('tp', $student->tp)->first();
        $exist_pramuka = Pramuka::where('nisn', $student->nisn)->where('tp', $student->tp)->count();
        if($exist_pramuka != 1){
            $pramuka = new Pramuka;
            $pramuka->nisn = $request->input("nisn");
            $pramuka->kelas = $request->input("kelas");
            $pramuka->tp = $request->input("tp");
        }

        $lain = LainLain::where('nisn', $student->nisn)->where('tp', $student->tp)->first();
            $exist_lain = LainLain::where('nisn', $student->nisn)->where('tp', $student->tp)->count();
            if($exist_lain != 1){
                $lain = new LainLain;
                $lain->nisn = $request->input("nisn");
                $lain->kelas = $request->input("kelas");
                $lain->tp = $request->input("tp");
            }

        for($i=4; $i < 16; $i++){
            if($request->input('spp') == $i){
                $spp->$bln[$i] = $MasterSpp->spp;
            }
            if($request->input('pramuka') == $i){
                $pramuka->$bln[$i] = $MasterSpp->pramuka;
            }
            if($request->input('lain_lain') == $i){
                $lain->$bln[$i] = $MasterSpp->lain_lain;
            }
        }

        //pembayaran untuk pembangunan
        $pembangunan->$bln[date('n')+3] = $request->input('pem');
        //end

        //generate no kuitansi
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
        $reff = Reff::where('tp', $tp->tp)->where('semester', $tp->semester)->first();
        if($reff == null){
            $reff = new Reff;
            $reff->nama_reff = 1;
            $reff->tp = $tp->tp;
            $reff->semester = $tp->semester;
        }else{
            $reff->nama_reff = $reff->nama_reff + 1;
        }
        $no_kuitansi = $tp->tp . $tp->semester . $reff->nama_reff;

        // bulan bayar
        $bln_dibayar = array();
        array_push($bln_dibayar, $request->input('spp') == 'none' ? 0 : $request->input('spp'));
        array_push($bln_dibayar, $request->input('pem') == 0 ? 0 : (date('n')+3));
        array_push($bln_dibayar, $request->input('pramuka') == 'none' ? 0 : $request->input('pramuka'));
        array_push($bln_dibayar, $request->input('lain_lain') == 'none' ? 0 : $request->input('lain_lain'));        

        //mencatat transaksi ke history
        $histori = new HistoriSpp;
        $histori->nisn = $request->input("nisn");
        $histori->nama = $student->nama_lengkap;
        $histori->tp = $request->input("tp");
        $kelas = Rombel::where('id',$request->input("kelas"))->first();
        $histori->kelas = $kelas->nama;
        $histori->oleh = Auth::customer()->get()->customer_name;
        $histori->oleh_id = Auth::customer()->get()->customer_id;
        
        $histori->tgl_bayar = $request->input("tanggal_bayar");
        $histori->bln_bayar = date('n');
        $histori->thn_bayar = date('Y');
        $histori->bln_dibayar = json_encode($bln_dibayar);
        $histori->note = $request->input("note");
        $histori->no_kwitansi = $no_kuitansi;
        $histori->semester = $tp->semester;

        $histori->spp = $request->input('spp') == 'none' ? 0 : $MasterSpp->spp;
        $histori->pembangunan = $request->input('pem');
        $histori->pramuka = $request->input('pramuka') == 'none' ? 0 : $MasterSpp->pramuka;
        $histori->lain_lain = $request->input('lain_lain') == 'none' ? 0 : $MasterSpp->lain_lain;
        $histori->nominal = $histori->spp + $histori->pembangunan + $histori->pramuka + $histori->lain_lain;

        if($spp->save() && $pembangunan->save() && $pramuka->save() && $lain->save()){
            $histori->save();
            $reff->save();
            return redirect('tu-user/students/spp/bayar/'.$id)->with('success', 'Pembayaran berhasil disimpan.')->with('no_kuitansi', $histori->no_kwitansi);    
        }
    }

    
    public function histori($id)
    {
        $student = Student::where('nisn', $id)->first();
        $histori = HistoriSpp::where('nisn', $id)->get();
        return view('tu-user.pages.spp.histori', ['student'=> $student, 'histori' => $histori]);
    }

    public function lapKelas($kelas)
    {
        $rombel = Rombel::where('id', $kelas)->select('nama', 'pengajar')->first()->toArray();
        $filename = 'Data Keuangan Kelas - '.$rombel['nama'];
        $students = Student::where('kelas', $kelas)->select('nama_lengkap')->get();
        $cs = Student::where('kelas', $kelas)->select('nama_lengkap')->count();
        if($cs == 0)
            return redirect()->back()->withErrors('Rombel belum memiliki data siswa untuk dihitung laporan keuangannya. Silahkan isi data siswa terlebih dahulu.');
        $Mspp = MasterSPP::where('status', 'aktif')->first()->toArray();
        
        $histori=Spp::where('kelas', $kelas)->get()->toArray();//dd($histori);
        $Jhistori=Spp::where('kelas', $kelas)->get()->count();//dd($histori);

        $pem=Pembangunan::where('kelas', $kelas)->get()->toArray();//dd($histori);
        //$JPem=pembanguna::where('kelas', $kelas)->get()->count();//dd($histori);

        $pra=Pramuka::where('kelas', $kelas)->get()->toArray();//dd($histori);

        Excel::create($filename, function($excel) use ($students, $filename, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('SMKN 2 PAYAKUMBUH')
                  ->setCompany('SMKN 2 PAYAKUMBUH');

            $excel->sheet($filename, function($sheet) use ($students, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {
                //no dan nama header merging MERGING VERTICAL
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:B5');
                
                //MERGING HORIZONTAL ROW A1 FOR HEADER
                $sheet->mergeCells('A1:N1');
                $sheet->mergeCells('C4:F4');
                $sheet->mergeCells('G4:J4');
                $sheet->mergeCells('K4:N4');

                $sheet->row(1, array('DATA KEUANGAN SISWA SMKN 2 PAYAKUMBUH PER ' . date('n F Y'),'','','',
                    '',',' ,'','','','','',''));

                $sheet->row(3, array('KELAS : '. $rombel['nama'],'','','','','','','','','','','WALI KELAS : '.$rombel['pengajar'],'',''
                ));
                $sheet->row(4, array(
                     'No', 'NAMA', 
                     'BESARAN IURAN', '', '', '', 
                     'YANG TELAH DIBAYAR', '', '', '', 
                     'TUNGGAKAN', '', '', ''
                ));
                $sheet->row(5, array(
                     '',  '',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH'
                ));

                //SET ALIGNMENT CENTER FOR HEADER
                $sheet->row(4, function($row) {
                    $row->setAlignment('center');
                });
                
                $sheet->row(5, function($row) {
                    $row->setAlignment('center');
                });

                $sheet->row(1, function($row) {
                    $row->setAlignment('center');
                });

                //SETTING WIDTH CELL
                $sheet->setWidth(array(
                    'A'     =>  4,
                    'B'     =>  25,

                    'C'     =>  15,
                    'D'     =>  15,
                    'E'     =>  15,
                    'F'     =>  15,

                    'G'     =>  15,
                    'H'     =>  15,
                    'I'     =>  15,
                    'J'     =>  15,

                    'K'     =>  15,
                    'L'     =>  15,
                    'M'     =>  15,
                    'N'     =>  15
                ));

                $row = 6;
                $no = 1;
                $TotSpp = array();
                $TotPem = array();
                $TotPra = array();
                $TotBayar = array();
                $H_Spp = array();
                $H_Pem = array();
                $H_Pra = array();
                $H_Bayar = array();
                foreach ($students as $student) {
                    if($no <= $Jhistori){
                    $Pspp = $histori[$no-1]['januari'] + $histori[$no-1]['februari'] + $histori[$no-1]['maret'] + $histori[$no-1]['april'] + $histori[$no-1]['mei'] + $histori[$no-1]['juni'] + $histori[$no-1]['juli'] + $histori[$no-1]['agustus'] + $histori[$no-1]['september'] + $histori[$no-1]['oktober'] + $histori[$no-1]['november'] + $histori[$no-1]['desember'];
                    $Ppem = $pem[$no-1]['januari'] + $pem[$no-1]['februari'] + $pem[$no-1]['maret'] + $pem[$no-1]['april'] + $pem[$no-1]['mei'] + $pem[$no-1]['juni'] + $pem[$no-1]['juli'] + $pem[$no-1]['agustus'] + $pem[$no-1]['september'] + $pem[$no-1]['oktober'] + $pem[$no-1]['november'] + $pem[$no-1]['desember'];
                    $Ppra = $pra[$no-1]['januari'] + $pra[$no-1]['februari'] + $pra[$no-1]['maret'] + $pra[$no-1]['april'] + $pra[$no-1]['mei'] + $pra[$no-1]['juni'] + $pra[$no-1]['juli'] + $pra[$no-1]['agustus'] + $pra[$no-1]['september'] + $pra[$no-1]['oktober'] + $pra[$no-1]['november'] + $pra[$no-1]['desember'];
                    }else{
                        $Pspp = 0;
                        $Ppem = 0;
                        $Ppra = 0;
                    }
                    $Tspp = $Mspp['spp'] * 12;
                    $Tpem = $Mspp['pembangunan'] * 12;
                    $Tpra = $Mspp['pramuka'] * 12;
                    $Tbayar = $Tspp + $Tpem + $Tpra;
                    $Pbayar = $Pspp + $Ppem + $Ppra;
                    $Hspp = $Tspp - $Pspp;
                    $Hpem = $Tpem - $Ppem;
                    $Hpra = $Tpra - $Ppra;
                    $Hbayar = $Tbayar - $Pbayar ;
                    
                    //push telah dibaar data ke dalam array total seluruh siswa
                    array_push($TotSpp, $Pspp);
                    array_push($TotPem, $Ppem);
                    array_push($TotPra, $Ppra);
                    array_push($TotBayar, $Pbayar);
                    array_push($H_Spp, $Hspp);
                    array_push($H_Pem, $Hpem);
                    array_push($H_Pra, $Hpra);
                    array_push($H_Bayar, $Hbayar);

                    $sheet->row($row, array(
                        $no, $student->nama_lengkap, $Tspp, $Tpem, $Tpra, $Tbayar, $Pspp, $Ppem, $Ppra, $Pbayar, $Hspp, $Hpem, $Hpra, $Hbayar
                    ));
                    $row++;
                    $no++;
                }

                $no = $no - 1;
                $sheet->row(($row+1), array(
                        '', 'JUMLAH', ($Tspp * $no), ($Tpem * $no), ($Tpra * $no), ($Tbayar * $no),  array_sum($TotSpp), array_sum($TotPem), array_sum($TotPra), array_sum($TotBayar), array_sum($H_Bayar), array_sum($H_Bayar), array_sum($H_Bayar), array_sum($H_Bayar) 
                ));

                $sheet->cells('A1', function($cells) {
                    $cells->setFont(array(
                        'size'       => '14',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('L3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A4:N5', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                });

                //$sheet->setBorder('A4:N'.($row+1), 'thin');
                $sheet->setAllBorders('thin');
            });
        
        })->export('xlsx');

    }

    public function histori_export($nisn)
    {
        $student = Student::where('nisn', $nisn)->first();
        $rombel = Rombel::where('id', $student->kelas)->select('nama')->first();
        $tp = Tp::where('status', 'aktif')->first();
        $historis = HistoriSpp::where('nisn', $nisn)->where('tp', $tp->tp)->get();
        $filename = $student->nama_lengkap . " - " . $rombel->nama;

        Excel::create($filename, function($excel) use ($student, $filename, $historis, $rombel, $tp) {

            // Set the title
            $excel->setTitle($student->nama_lengkap);

            // Chain the setters
            $excel->setCreator('SMKN 2 PAYAKUMBUH')
                  ->setCompany('SMKN 2 PAYAKUMBUH');

            $excel->sheet($filename, function($sheet) use ($student, $historis, $rombel, $tp) {
                //tanggal dan uraian header merging MERGING VERTICAL
                //$sheet->setAllBorders('thin');
                $sheet->mergeCells('A5:A6');
                $sheet->mergeCells('B5:B6');
                //$sheet->mergeCells('O5:O6');
                $sheet->mergeCells('C5:C6');
                $sheet->mergeCells('D5:D6');
                
                //MERGING HORIZONTAL ROW A1 FOR HEADER
                $sheet->mergeCells('A1:I1');
                //$sheet->mergeCells('C5:N5');
                //$sheet->mergeCells('P5:T5');
                $sheet->mergeCells('E5:I5');
                
                $sheet->row(1, array(
                     'RINCIAN PEMBAYARAN', '', 
                     '', '', '', '', '', '', '', '', '', '', '', '',
                     '', '',
                     '', '', '', '', ''
                ));
                $sheet->row(2, array(
                     'NAMA', ': '. $student->nama_lengkap, 
                     '', '', '', '', '', '', '', '', '', '', '', '',
                     '', '',
                     '', '', '', '', ''
                ));
                $sheet->row(3, array(
                     'KELAS', ': '. $rombel->nama . " (TAHUN PELAJARAN " . $tp->tp .'-'. ($tp->tp+1) . ")", 
                     '', '', '', '', '', '', '', '', '', '', '', '',
                     '', 
                     '', '', '', '', ''
                ));
                $sheet->row(5, array(
                     'TANGGAL', 'NO INVOICE',
                     /*'BULAN BAYAR', '', '', '', '', '', '', '', '', '', '', '',*/
                     'TP',  'SMT',
                     'YANG DIBAYARKAN', '', '', '', ''
                ));
                $sheet->row(6, array(
                     '',  '', /*'JAN', 'FEB','MAR', 'APR', 'MEI',  'JUN','JUL',  'AGU', 'SEP',  'OKT', 'NOV', 'DES',*/
                     '','',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'LAIN-LAIN', 'JUMLAH'
                ));

                //SET ALIGNMENT CENTER FOR HEADER
                $sheet->row(1, function($row) {
                    $row->setAlignment('center');
                });

                $sheet->row(5, function($row) {
                    $row->setAlignment('center');
                });
                
                $sheet->row(6, function($row) {
                    $row->setAlignment('center');
                });

                //SETTING WIDTH CELL
                $sheet->setWidth(array(
                    'A'     =>  13,
                    'B'     =>  13,

                    'C'     =>  6,
                    'D'     =>  5,
                    'E'     =>  16,
                    'F'     =>  16,
                    'G'     =>  16,
                    'H'     =>  16,
                    'I'     =>  16,
                    // 'J'     =>  4,
                    // 'K'     =>  4,
                    // 'L'     =>  4,
                    // 'M'     =>  4,
                    // 'N'     =>  4,

                    // 'O'     =>  6,

                    // 'P'     =>  13,
                    // 'Q'     =>  13,
                    // 'R'     =>  13,
                    // 'S'     =>  13,
                    // 'T'     =>  13
                ));

                //isi
                $row = 7;
                $no = 5;
                $tspp = 0;
                $tpem = 0;
                $tpra = 0;
                $tlain = 0;
                $tjum = 0;
                foreach ($historis as $histori) {
                    $sheet->row($row, array(
                        $histori->tgl_bayar, $histori->no_kwitansi, /*'','','','','','','','','','','','',*/ (substr($histori->tp, -2).'/'. ((int)substr($histori->tp, -2) + 1)), $histori->semester, "Rp. " . number_format($histori->spp,2,',','.'), "Rp. " . number_format($histori->pembangunan,2,',','.'), "Rp. " . number_format($histori->pramuka,2,',','.'), "Rp. " . number_format($histori->lain_lain,2,',','.'), "Rp. " . number_format($histori->nominal,2,',','.')
                    ));
                    $tspp += $histori->spp;
                    $tpem += $histori->pembangunan;
                    $tpra += $histori->pramuka;
                    $tlain += $histori->lain_lain;
                    $tjum += $histori->nominal;
                    $no++;
                    $row++;
                }

                $no = $no - 1;
                $sheet->row(($row+1), array(
                    'JUMLAH', '', '', '', "Rp. " . number_format($tspp,2,',','.'), "Rp. " . number_format($tpem,2,',','.'), "Rp. " . number_format($tpra,2,',','.'), "Rp. " . number_format($tlain,2,',','.'), "Rp. " . number_format($tjum,2,',','.') 
                ));

                $sheet->row(($row+3), array(
                    '', '', '', '', '','', '', "Payakumbuh, " . date('j F Y'),'' 
                ));

                $sheet->row(($row+5), array(
                    '', '', '', '', '','', '', "a/n Bendahara Komite",'' 
                ));

                $sheet->row(($row+8), array(
                    '', '', '', '', '','', '', "Aulia Rahmi S.Pd",'' 
                ));

                $sheet->cells('A1', function($cells) {
                    $cells->setFont(array(
                        'size'       => '14',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A2:B3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A5:T6', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                }); 
                $sheet->cells('E7:I'.$row, function($cells) {
                    $cells->setAlignment('right');
                }); 
                $sheet->cells('A7:D'.$row, function($cells) {
                    $cells->setAlignment('left');
                }); 
                $sheet->cells('E'.($row+1).':I'.($row+1), function($cells) {
                    $cells->setAlignment('right');
                });   
                // $sheet->cells('A7:I14', function($cells) {
                //     $cells->setBorder('solid', 'solid', 'solid', 'solid');
                // });                
            });

        })->export('xlsx');

    }    

    public function invoice($id, $tp, $semester, $no_kuitansi)
    {
        $student = Student::where('nisn', $id)->first();
        $histori = HistoriSpp::where('nisn', $id)->where('tp', $tp)->where('semester', $semester)->where('no_kwitansi', $no_kuitansi)->first();//dd($histori);
        $Mspp = MasterSpp::where('tp', $tp)->first();
        return view('tu-user.pages.spp.invoice', ['student'=> $student, 'histori' => $histori, 'Mspp' => $Mspp]);
    }
}
