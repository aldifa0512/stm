<?php

namespace App\Http\Controllers\Administrasions;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\ExcelLibrary;
use App\Models\Student;
use App\Models\Jurusan;
use App\Models\Rombel;
use App\Models\DropOut;
use App\Models\Customer;
use App\Models\Tp;
use Validator;
use Auth;
use Storage;
use Image;
use Setting;
use Session;
use PHPExcel; 
use PHPExcel_IOFactory; 

class StudentsController extends Controller
{
//-----------------------------------------------------------------------------------------
//Get Rombels and Jurusans Functions

    public function getLoggedInUserId(){
        $tu = Customer::where('id', Auth::customer()->get()->id)->first();
        $tuId = $tu->customer_id;
        return $tuId;
    }

    public function getJurusanIdByIdTu($tuId){
        $jurusansId = Jurusan::where('tu_id', $tuId)->select('id')->get();
        return $jurusansId;
    }

    public function getRombelIdByJurusanId($jurusanId){
        $rombelsId = Rombel::where('jurusan_id', $jurusanId)->select('id')->get();
        return $rombelsId;
    }

    public function getStudentsByTuId(){
        $rombelsId = array();
        $tuId = $this->getLoggedInUserId();
        $jurusansId = $this->getJurusanIdByIdTu($tuId);

        foreach ($jurusansId as $key => $value) {
            $rombels = $this->getRombelIdByJurusanId($value->id);
            foreach ($rombels as $key2 => $value2) {
                $rombelsId[] = $value2->id;
            }
        }
        return $rombelsId;
    }

 //-----------------------------------------------------------------------------------------
   
    public function index()
    {
        $jumlahSiswa = Student::where('status', 'aktif')->count();
        $students = Student::where('status', 'aktif')->orderBy('nama_lengkap')->paginate(30);

        return view('tu-user.pages.student.list.list', [
            'students'      => $students, 
            'jumlahSiswa'   => $jumlahSiswa,
            'page_name'     => 'Siswa Aktif']);
    }
    
    public function indexPasif()
    {
        $jumlahSiswa = Student::where('status', 'pasif')->count();
        $students = Student::where('status', 'pasif')->orderBy('nisn', 'ASC')->paginate(30);

        return view('tu-user.pages.student.list.list', ['students' => $students, 'pasif' => true, 'jumlahSiswa' => $jumlahSiswa]);
    }
    public function indexRecap()
    {
        $jumlahSiswa = Student::count();
        $students = Student::orderBy('nama_lengkap')->paginate(30);

        return view('tu-user.pages.student.list.list', ['students' => $students, 'recap' => 'true', 'jumlahSiswa' => $jumlahSiswa]);
    }

    public function query($query, $method)
    {
        if ($method == "ASC") {
            $method = "DSC";
        }else{
            $method = "ASC";
        }

        $students = Student::orderBy($query, $method)->where('status', 'aktif')->paginate(30);

        return view('tu-user.pages.student.list.listorderby', ['students' => $students, 'method' => $method, 'jumlahSiswa' => $jumlahSiswa]);
    }

    public function queryRecap($query, $method)
    {
        if ($method == "ASC") {
            $method = "DSC";
        }else{
            $method = "ASC";
        }

        $students = Student::orderBy($query, $method)->paginate(30);

        return view('tu-user.pages.student.list.listorderby-recap', ['students' => $students, 'method' => $method, 'jumlahSiswa' => $jumlahSiswa]);
    }

    public function bayar_recap(){
        return ('Maaf untuk saat ini anda belum bisa melakukan penerimaan SPP');
         
        $students = Student::orderBy('nama_lengkap')->paginate(30);
        return view('tu-user.pages.spp.list', ['students' => $students, 'recap' => 'true']);
    }

//-----------------------------------------------------------------------------------------

    public function enroll()
    {
        return view('tu-user.pages.student.enroll');
    }

    public function store_enroll(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'nama_lengkap' => 'required|max:255',
            'nisn' => 'required|numeric'
            //'image' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $student = Student::find($request->input('nisn'));
        if($student)
            return redirect()->back()->withInput()->withErrors('NISN sudah ada!');

        $student = new Student;
        $student->save();
        $student->nama_lengkap = $request->input('nama_lengkap');
        $student->nisn = $request->input('nisn');
        $student->jenis_kelamin = $request->input('jenis_kelamin');
        //$student->tu_id = Auth::Tu()->get()->tu_id;
        $student->nik = $request->input('nik');
        $student->tempat_lahir = $request->input('tempat_lahir');
        $student->status = 'aktif';
        $student->tanggal_lahir = $request->input('tanggal_lahir');
        $student->agama = $request->input('agama');
        $student->alamat_jln = $request->input('alamat_jln');
        $student->rt = $request->input('rt');
        $student->rw = $request->input('rw');
        $student->tp = $request->input('thn_masuk');
        $student->dusun = $request->input('dusun');
        $student->kelurahan = $request->input('kelurahan');
        $student->kecamatan = $request->input('kecamatan');
        $student->kip = $request->input('no_kip');
        $student->kip_nama = $request->input('nama_di_kip');
        $student->kks = $request->input('nomor_kks');
        $student->akta_lahir = $request->input('no_registrasi_akta_lahir');
        $student->npsn_smp = $request->input('npsn_smp');

        $student->nama_ayah = $request->input('nama_ayah');
        $student->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $student->tl_ayah = $request->input('tl_ayah');
        $student->nama_ibu = $request->input('nama_ibu');
        $student->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $student->tl_ibu = $request->input('tl_ibu');
        $student->nama_wali = $request->input('nama_wali');
        $student->pekerjaan_wali = $request->input('pekerjaan_wali');
        $student->tl_wali = $request->input('tl_wali');
        
        $student->jenis_pendaftaran = $request->input('jenis_pendaftaran');
        $student->tgl_masuk = $request->input('tgl_masuk');
        $student->jurusan = $request->input('jurusan');
        $student->kelas = $request->input('rombel');
        $student->nis = $request->input('nis');
        $student->no_ujian_smp = $request->input('no_ujian_smp');
        $student->no_ijazah = $request->input('no_ijazah');
        $student->no_skhus = $request->input('no_skhus');

        
        if ($request->hasFile('pas_foto')) {
            //$dir = storage_path().'/app/cs/assets/';
            $dir = public_path().'/storage/app/cs/assets/';
            $file = $request->file('pas_foto');
            $file_name = preg_replace("/[^A-Z0-9._-]/i", "_", $file->getClientOriginalName());
            $thumb_admin = 'thumb-admin-'.$file_name;
            $thumb = 'thumb-'.$file_name;
            $relative_path = 'storage/app/cs/assets/'.$file_name;
            $relative_thumb_admin_path = 'storage/app/cs/assets/'.$thumb_admin;
            $relative_path = 'storage/app/cs/assets/'.$file_name;

            if (!Storage::disk('local')->exists('cs/assets')) {
                Storage::makeDirectory('cs/assets');
            }

            Image::make($request->file('pas_foto'))->save($dir . $file_name);
            Image::make($request->file('pas_foto'))->resize(150, 120)->save($dir . $thumb_admin);
            Image::make($request->file('pas_foto'))->resize(200, 200)->save($dir . $thumb);

            $student->pas_foto = json_encode([$relative_path]);
        }

        if ($student->save()) {
            $student->student_id = '28' . date('Y') . date('m') . str_pad((string)$student->id, 5, 0, STR_PAD_LEFT);
            //create_billing($student->customer_id, $student->id, 'student', $student->package->price);
            return redirect('tu-user/students'/*/edit/'. $student->id*/)->withSuccess('success', 'Student create success.');
        }
    }

//-----------------------------------------------------------------------------------------

    public function edit($id)
    {
        $student = Student::find($id);
        
        if (!$student) {
            return abort(404);
        }

        return view('tu-user.pages.student.edit', ['student' => $student]);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'nama_lengkap' => 'required|max:255',
            'nisn' => 'required|numeric'
            //'image' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $student = Student::find($id);
        $student->nama_lengkap = $request->input('nama_lengkap');
        $student->nisn = $request->input('nisn');
        $student->jenis_kelamin = $request->input('jenis_kelamin');
        //$student->tu_id = Auth::Tu()->get()->tu_id;
        $student->nik = $request->input('nik');
        $student->tempat_lahir = $request->input('tempat_lahir');
        $student->tanggal_lahir = $request->input('tanggal_lahir');
        $student->agama = $request->input('agama');
        $student->alamat_jln = $request->input('alamat_jln');
        $student->rt = $request->input('rt');
        $student->rw = $request->input('rw');
        $student->tp = $request->input('thn_masuk');
        $student->dusun = $request->input('dusun');
        $student->kelurahan = $request->input('kelurahan');
        $student->kecamatan = $request->input('kecamatan');
        $student->kip = $request->input('no_kip');
        $student->kip_nama = $request->input('nama_di_kip');
        $student->kks = $request->input('nomor_kks');
        $student->akta_lahir = $request->input('no_registrasi_akta_lahir');
        $student->npsn_smp = $request->input('npsn_smp');

        $student->nama_ayah = $request->input('nama_ayah');
        $student->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $student->tl_ayah = $request->input('tl_ayah');
        $student->nama_ibu = $request->input('nama_ibu');
        $student->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $student->tl_ibu = $request->input('tl_ibu');
        $student->nama_wali = $request->input('nama_wali');
        $student->pekerjaan_wali = $request->input('pekerjaan_wali');
        $student->tl_wali = $request->input('tl_wali');
        
        $student->jenis_pendaftaran = $request->input('jenis_pendaftaran');
        $student->tgl_masuk = $request->input('tgl_masuk');
        $student->kelas = $request->input('rombel');
        $student->jurusan = $request->input('jurusan');
        $student->nis = $request->input('nis');
        $student->no_ujian_smp = $request->input('no_ujian_smp');
        $student->no_ijazah = $request->input('no_ijazah');
        $student->no_skhus = $request->input('no_skhus'); 

        if ($request->hasFile('pas_foto')) {
            //$dir = storage_path().'/app/cs/assets/';
            $dir = public_path().'/storage/app/cs/assets/';
            $file = $request->file('pas_foto');
            $file_name = preg_replace("/[^A-Z0-9._-]/i", "_", $file->getClientOriginalName());
            $thumb_admin = 'thumb-admin-'.$file_name;
            $thumb = 'thumb-'.$file_name;
            $relative_path = 'storage/app/cs/assets/' . $student->id . '/' . $file_name;
            $relative_thumb_admin_path = 'storage/app/cs/assets/'.$thumb_admin;
            $relative_path = 'storage/app/cs/assets/'.$file_name;

            if (!Storage::disk('local')->exists('cs/assets/' . $student->id)) {
                Storage::makeDirectory('cs/assets/' . $student->id);
            }

            Image::make($request->file('pas_foto'))->save($dir . $file_name);
            Image::make($request->file('pas_foto'))->resize(150, 120)->save($dir . $thumb_admin);
            Image::make($request->file('pas_foto'))->resize(200, 200)->save($dir . $thumb);

            $student->pas_foto = json_encode([$relative_path]);
        }

        if ($student->save()) {
            return redirect('tu-user/students')->withSuccess('success', 'Data siswa berhasil diperbaharui.');
        }
    }

//-----------------------------------------------------------------------------------------

    public function drop_out()
    {
        return view('tu-user.pages.student.drop-out');
    }

    public function dropSiswa($id)
    {
        $student = Student::find($id);
        
        if (!$student) {
            return abort(404);
        }

        return view('tu-user.pages.student.drop-siswa', ['student' => $student]);
    }

    public function deleteSiswa(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'keluar_karena' => 'required|max:255',
            'tanggal_keluar' => 'required|max:255',
            'alasan' => 'required|max:255'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $dropOut = new DropOut;
        $dropOut->nisn = $request->input('nisn');
        $dropOut->keluar_karena = $request->input('keluar_karena');
        $dropOut->tanggal_keluar = $request->input('tanggal_keluar');
        $dropOut->alasan = $request->input('alasan');

        $student = Student::find($id);
        $student->status = 'pasif'; 
        $student->tahunKeluar = date('Y'); 

        if ($dropOut->save()) {
            $student->save();
            return redirect('tu-user/students/')->with('success', 'Students list has been updated');
        }
    }


//-----------------------------------------------------------------------------------------

    public function get_filter()
    {
        return view('tu-user.pages.dashboard');
    }

    public function filter(Request $request)
    {
        // if ($request->input('thn_masuk') != "None" && $request->input('nisn') !="" && $request->input('nama') !="" && $request->input('jurusan') != "select" && $request->input('status') != "None" ) {
        //     //query with all requested
        //     $thn_masuk = $request->input('thn_masuk');
        //     $nisn = $request->input('nisn');
        //     $nama = $request->input('nama');
        //     $jurusan = $request->input('jurusan');
        //     $status = $request->input('status');
        //     $students = Student::where('tgl_masuk', 'LIKE', '%'.$thn_masuk.'%')->where('nama_lengkap', 'LIKE', '%'.$nama.'%')->where('nisn', $nisn)->where('jurusan', $jurusan)->where('status', $status)->paginate(10);
        // } elseif($request->input('status') == "None" && $request->input('nisn') =="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('thn_masuk') != "None" ) {
        //     //query with only thn_masuk
        //     $search = $request->input('thn_masuk');
        //     $students = Student::where('tgl_masuk', 'LIKE', '%'.$search.'%')->paginate(10);
        // } elseif($request->input('status') != "None" && $request->input('nisn') =="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('thn_masuk') == "None" ) {
        //     //query with only status
        //     $search = $request->input('status');
        //     $students = Student::where('status', $request->input('status'))->paginate(10);
        // } elseif ($request->input('thn_masuk') == "None" && $request->input('nisn') !="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('status') == "None" ) {
        //     //query with only nisn
        //     $students = Student::where('nisn', $request->input('nisn'))->paginate(10);
        // } elseif ($request->input('thn_masuk') == "None" && $request->input('nisn') =="" && $request->input('nama') !="" && $request->input('jurusan') == "select" && $request->input('status') == "None"  ) {
        //     //query with only nama
        //     $search = $request->input('nama');
        //     $students = Student::where('nama_lengkap', 'LIKE', '%'.$search.'%')->paginate(10);
        // } elseif (/*$request->input('thn_masuk') == "None" && $request->input('nisn') =="" && $request->input('nama') =="" &&*/ $request->input('jurusan') != "select"/* && $request->input('status') == "None"  */) {
        //     //query with only jurusan
        //     $students = Student::where('jurusan', $request->input('jurusan'))->paginate(10);
        // } else{
        //     $students = getSiswa(null, 'DESC', 10);
        //     return view('tu-user.pages.student.list.list', ['students' => $students]);
        // }

        //only nisn or name
        if ($request->input('nisn') !="" ) {
            //query with only nisn
            $students = Student::where('nisn', $request->input('nisn'))->orWhere('nama_lengkap', 'LIKE', '%'.$request->input('nisn').'%')->paginate(10);
        }
        return view('tu-user.pages.student.list.list', ['students' => $students]);
    }
    public function filter_spp(Request $request)
    {
        // if ($request->input('thn_masuk') != "None" && $request->input('nisn') !="" && $request->input('nama') !="" && $request->input('jurusan') != "select" && $request->input('status') != "None" ) {
        //     //query with all requested
        //     $thn_masuk = $request->input('thn_masuk');
        //     $nisn = $request->input('nisn');
        //     $nama = $request->input('nama');
        //     $jurusan = $request->input('jurusan');
        //     $status = $request->input('status');
        //     $students = Student::where('tgl_masuk', 'LIKE', '%'.$thn_masuk.'%')->where('nama_lengkap', 'LIKE', '%'.$nama.'%')->where('nisn', $nisn)->where('jurusan', $jurusan)->where('status', $status)->paginate(10);
        // } elseif($request->input('status') == "None" && $request->input('nisn') =="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('thn_masuk') != "None" ) {
        //     //query with only thn_masuk
        //     $search = $request->input('thn_masuk');
        //     $students = Student::where('tgl_masuk', 'LIKE', '%'.$search.'%')->paginate(10);
        // } elseif($request->input('status') != "None" && $request->input('nisn') =="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('thn_masuk') == "None" ) {
        //     //query with only status
        //     $search = $request->input('status');
        //     $students = Student::where('status', $request->input('status'))->paginate(10);
        // } elseif ($request->input('thn_masuk') == "None" && $request->input('nisn') !="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('status') == "None" ) {
        //     //query with only nisn
        //     $students = Student::where('nisn', $request->input('nisn'))->paginate(10);
        // } elseif ($request->input('thn_masuk') == "None" && $request->input('nisn') =="" && $request->input('nama') !="" && $request->input('jurusan') == "select" && $request->input('status') == "None"  ) {
        //     //query with only nama
        //     $search = $request->input('nama');
        //     $students = Student::where('nama_lengkap', 'LIKE', '%'.$search.'%')->paginate(10);
        // } elseif (/*$request->input('thn_masuk') == "None" && $request->input('nisn') =="" && $request->input('nama') =="" &&*/ $request->input('jurusan') != "select"/* && $request->input('status') == "None"  */) {
        //     //query with only jurusan
        //     $students = Student::where('jurusan', $request->input('jurusan'))->paginate(10);
        // } else{
        //     $students = getSiswa(null, 'DESC', 10);
        //     return view('komite.pages.student.list.list', ['students' => $students]);
        // }

        //only nisn or name
        if ($request->input('nisn') !="" ) {
            //query with only nisn
            $students = Student::where('nisn', $request->input('nisn'))->orWhere('nama_lengkap', 'LIKE', '%'.$request->input('nisn').'%')->paginate(10);
        }

        return view('tu-user.pages.spp.list', ['students' => $students]);
    }
    //-----------------------------------------------------------------------------------------

    public function rombel()
    {
        $jurusans = Jurusan::all();
        $user = Customer::find(Auth::customer()->get()->customer_id);
        $jurusans = $user->jurusan;
        foreach ($jurusans as $key => $value) {
            $rombels = Rombel::where('jurusan_id', $value->id)->orderBy('tingkat')->get();
            foreach ($rombels as $key => $value2) {
                $jumlahSiswa = Student::where('kelas', $value2->id)->where('status', 'aktif')->count();
                $value2['jumlahSiswa'] = $jumlahSiswa;
                $value2['jurusan'] = $value->nama_jurusan;
                $value2['jenjang'] = $value->jenjang;
            }
            $value['rombels'] = $rombels;
        }
        
        return view('tu-user.pages.student.list.rombel', [
            'jurusans' => $jurusans
        ]);
    }

    public function bayar_rombel(){
        $rombels = Rombel::orderBy('jurusan_id')->get();
        return view('tu-user.pages.spp.rombel', ['rombels' => $rombels]);
    }

    public function rombelLihat($kelas)
    {
        $jumlahSiswa = Student::where('kelas', $kelas)->where('status', 'aktif')->count();
        $students = Student::where('kelas', $kelas)->where('status', 'aktif')->orderBy('nama_lengkap', "ASC")->paginate(100);

        return view('tu-user.pages.student.list.list', ['students' => $students, 'kelas' => $kelas, 'rombelM' =>"true", 'render' =>false, 'jumlahSiswa' =>$jumlahSiswa]);
    }

    public function claim_siswa($kelas){
        $kelas = Rombel::find($kelas);
        $update = Student::where('kelas', $kelas->id)->update(['jurusan' => $kelas->jurusan_id]);
        if($update)
            return redirect('/tu-user/students/rombel')->with('success', 'Jurusan siswa kelas '. $kelas->nama.' berhasil diperbaharui');
    }

    public function claim_tahun($kelas){
        $kelas = Rombel::find($kelas);
        $update = Student::where('kelas', $kelas->id)->update(['tp' => $kelas->kurikulum]);
        if($update)
            return redirect('/tu-user/students/rombel')->with('success', 'Tahun masuk siswa kelas '. $kelas->nama.' berhasil diperbaharui');
    }

    public function getTanggalCetak(){
        $bulans = Bulan_laporan();
        $hari = date('d');
        $bulan = $bulans[date('m')];
        $tahun = date('Y');

        $hasil = $hari . ' ' . $bulan . ' ' . $tahun;
        return $hasil;

    }

    public function rombelAbsen(Request $request)
    {
        $rombelId = $request->input('idRombel');
        $jurusan = $request->input('jurusan');
        $tanggalCetak = $this->getTanggalCetak();
        $rombel = Rombel::find($rombelId);
        $tpAktif = get_tp_aktif();

        if($request->input('optionsRadios') == 'siswaAktif'){
            $studentsFix = array();
            $students = Student::where('kelas', $rombelId)->where('status', 'aktif')->select('nisn', 'nama_lengkap', 'jenis_kelamin')->orderBy('nama_lengkap')->get();
            foreach ($students as $key => $value) {
                $studentsFix[] = array(
                    'nisn'          => $value->nisn,
                    'nama_lengkap'  => $value->nama_lengkap,
                    'jenis_kelamin' => $value->jenis_kelamin
                );
            }
        }else{
            $studentsFix = array();
            $batasTunggakan = $request->input('pilihBulan');
            $students = Student::where('kelas', $rombelId)->select('nisn', 'nama_lengkap', 'tp', 'jenis_kelamin')->orderBy('nama_lengkap')->get();
            foreach ($students as $key => $value) {
                $nisn = $value->nisn;
                $tahunMasukSiswa = $value->tp;
                if($request->input('pilihJenisTunggakan') == 'semesterAktif'){
                    $tunggakan = hitungTotalTunggakan($nisn, $tpAktif->tp, $tahunMasukSiswa, $batasTunggakan);
                    if($tunggakan['total'] == 0){
                        $studentsFix[] = array(
                            'nisn'          => $value->nisn,
                            'nama_lengkap'  => $value->nama_lengkap,
                            'jenis_kelamin' => $value->jenis_kelamin
                        );
                    }
                }else{
                    $tunggakan = tunggakan_tahun_aktif($nisn, $tpAktif->tp, $tahunMasukSiswa, $batasTunggakan, $batasTunggakan, $batasTunggakan, $batasTunggakan);
                    if($tunggakan['total'] == 0){
                        $studentsFix[] = array(
                            'nisn'          => $value->nisn,
                            'nama_lengkap'  => $value->nama_lengkap,
                            'jenis_kelamin' => $value->jenis_kelamin
                        );
                    }
                }
            }
        }

        $students = $studentsFix;
        $result = ExcelLibrary::cetakAbsen($students, $rombel, $tpAktif, $tanggalCetak, $jurusan);

        return $result;
        
    }
    
    //-----------------------------------------------------------------------------------------
    public function ExcelDataSiswa()
    {

    }

    public function ExcelDataSiswaKelas($kelas)
    {

    }

    public function rombelAjax(Request $request){

        switch ($request->input('action')) {
            case 'lulus':
                $result = '';
                $tpAktif = get_tp_aktif();
                $child = Tp::whereBetween('tp', [($tpAktif->tp - 3), $tpAktif->tp])->groupBy('tp')->get();
                foreach ($child as $key => $value) {
                    $result .= '<option class="addedOption" value="'.$value->tp.'">' . $value->tp . '/' . ($value->tp+1) . '</option>';
                }
                break;
            
            case 'naik':
                $result = '';
                $tingkatAtas = 1 + $request->input('rombelTingkat');
                $child = Rombel::where('tingkat', $tingkatAtas)->where('jurusan_id', $request->input('jurusanId'))->orderBy('jurusan_id')->get();
                foreach ($child as $key => $value) {
                    $result .= '<option class="addedOption" value="'.$value->id.'">' . $value->nama . '</option>';
                }
                break;

            case 'tinggal':
                $result = '';
                $tingkat = $request->input('rombelTingkat');
                $child = Rombel::where('tingkat', $tingkat)->where('jurusan_id', $request->input('jurusanId'))->orderBy('jurusan_id')->get();
                foreach ($child as $key => $value) {
                    $result .= '<option class="addedOption" value="'.$value->id.'">' . $value->nama . '</option>';
                }
                break;
        }
        return response()->json($result);
    }

    public function rombelAction(Request $request, $idRombel){
        // dd($request->all());
        
        //Validasi inputan
        if(!isset($_POST['check'])) 
            return redirect()->back()->withInput()->withErrors("Tidak ada siswa yang dipilih.");
        if($request->input('action') == null) 
            return redirect()->back()->withInput()->withErrors("Anda tidak memilih action.");

        switch ($request->input('action')) {
            case 'lulus':
                if($request->input('rombelTingkat') != $request->input('jurusanJenjang')) 
                    return redirect()->back()->withInput()->withErrors("Tidak bisa meluluskan siswa yang belum berada di tingkat akhir");
                

                $selectedStudents = $request->input('check');
                foreach ($selectedStudents as $key => $value) {
                    if($this->checkTahunMasuk($value, $request->input('actionVal'))) 
                    return redirect()->back()->withInput()->withErrors("Tidak meluluskan siswa dengan nisn " . $value . " pada " . $request->input('actionVal') . '/' . ($request->input('actionVal') + 1) . ', karena siswa diterima pada tahun setelahnya.');

                    $lulus = $this->lulus($value);
                    if(!$lulus){
                        return redirect()->back()->withInput()->withErrors("Gagal meluluskan siswa dengan NPM : " . $value);
                    }
                }
                return redirect('tu-user/students/rombel/lihat/'.$idRombel)->with('success', 'Berhasil update data siswa.');
                break;
            
            case 'naik':
                $selectedStudents = $request->input('check');
                foreach ($selectedStudents as $key => $value) {
                    $naikKelas = $this->updateKelas($request->input('actionVal'), $value);
                    if(!$naikKelas){
                       return redirect()->back()->withInput()->withErrors("Gagal meluluskan siswa dengan NPM : " . $value); 
                    }
                }
                return redirect('tu-user/students/rombel/lihat/'.$idRombel)->with('success', 'Berhasil update data siswa.');
                break;

            case 'tinggal':
                $selectedStudents = $request->input('check');
                foreach ($selectedStudents as $key => $value) {
                     $tinggalKelas = $this->updateKelas($request->input('actionVal'), $value);   
                     if(!$tinggalKelas){
                        return redirect()->back()->withInput()->withErrors("Gagal meluluskan siswa dengan NPM : " . $value);
                     }
                }
                return redirect('tu-user/students/rombel/lihat/'.$idRombel)->with('success', 'Berhasil update data siswa.');
                break;
        }
    }

    public function updateKelas($idRombel, $nisn){
        $student = get_student($nisn);
        $student->kelas = $idRombel;
        
        if($student->save()){
            return TRUE;
        }

        return FALSE;    
    }

    public function lulus($nisn){
        $dropOut = new DropOut;
        $dropOut->nisn = $nisn;
        $dropOut->keluar_karena = 'LULUS';
        $dropOut->tanggal_keluar = date('Y-m-d');
        $dropOut->alasan = 'LULUS';

        $student = Student::find($nisn);
        $student->status = 'pasif'; 

        if ($dropOut->save()) {
            $student->save();
            return true;
        }
    }

    public function checkTahunMasuk($nisn, $tahunLulus){
        $student = Student::where('nisn', $nisn)->select('tp')->first();
        if($tahunLulus >= $student->tp){
            return FALSE;
        }
        return TRUE;    
    }

    public function rombelTunggakan($rombelId)
    {   
        $result = ExcelLibrary::rombelTunggakan($rombelId);
        return $result;
    }

    public function getClassIdByClassName($className){
        $rombel = Rombel::where('nama', $className)->select('id')->first();

        if($rombel ==  NULL){
            return 0;
        }

        return $rombel->id;
    }

    public function importedStudents(){
        $jumlahSiswaTotal = 0;
        $rombels = Student::where('status', 'new_i')->groupBy('importedClass')->select('importedClass')->get();
        $existedRombels = Rombel::orderBy('nama')->select('nama')->get();
        foreach ($rombels as $key => $value) {
            $jumlahSiswa = Student::where('status', 'new_i')->where('importedClass', $value->importedClass)->count();
            $classId = $this->getClassIdByClassName($value->importedClass);

            if($classId == 0){
                $existedStudents = 0;
            }else{
                $existedStudents = Student::where('status', 'aktif')->where('kelas', $classId)->count();
            }
            
            $value['jumlahSiswa'] = $jumlahSiswa;
            $value['existedStudents'] = $existedStudents;
            $jumlahSiswaTotal += $jumlahSiswa;
        }
        return view('tu-user.pages.student.imported', [
            'rombels' => $rombels,
            'existedRombels' => $existedRombels,
            'jumlahSiswaTotal' => $jumlahSiswaTotal]);
    }

    public function getJurusanBySingkatan($singkatan){
        $jurusan = Jurusan::where('singkatan', $singkatan)->first();
        if($jurusan == NULL){
            return FALSE;
        }

        return $jurusan->id;
    }

    public function importedStudentsbyClassName(Request $request, $className){

        $className = $request->input('className');
        $singkatanJurusan = explode(' ', $className);
        $singkatanJurusan = $singkatanJurusan[1];

        $jurusanId = $this->getJurusanBySingkatan($singkatanJurusan);

        if($jurusanId == FALSE){
            return redirect()->back()->withErrors('Jurusan tidak ditemukan');
        }else{

            $classId = $this->getClassIdByClassName($className);
            if($classId == 0){
                return redirect()->back()->withErrors('ID kelas tidak dapat ditemukan');
            }else{
                $students = Student::where('importedClass', $className)->where('status', 'new_i')->select('nisn')->get();
                
                foreach ($students as $key => $value) {
                    $student = Student::find($value->nisn);
                    $student->kelas = $classId;
                    $student->jurusan = $jurusanId;
                    $student->status = 'aktif';
                    $student->save();
                }

                return redirect()->back()->withSuccess('Kelas berhasil diproses.');
            }
        }
    }
}
