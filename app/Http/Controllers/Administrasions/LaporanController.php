<?php

namespace App\Http\Controllers\Administrasions;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\HistoriSpp;
use App\Models\Student;
use App\Models\MasterSPP;
use App\Models\Rombel;
use App\Models\Customer;
use Auth;
use App\Models\Tp;
use Excel;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function invoice()
    {
        $invoices = HistoriSpp::where('oleh_id', Auth::customer()->get()->customer_id)->orderBy('no_kwitansi', 'DSC')->paginate(10);
        return view('tu-user.pages.invoice.list', ['invoices' => $invoices]);
    }

    public function invoice_cetak()
    {
        return view('tu-user.pages.laporan.cetakPerInvoice');
    }

    public function invoice_cetak_post(Request $request)
    {
        //dd(Auth::customer()->get()->customer_id);
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        $pjd = "";
        if($request->input('metode') == 'rentang_hari'){
            $from = $request->input('tanggal_rentang_dari');
            $to = $request->input('tanggal_rentang_sampai');
            $name = '';
            $pjd = $request->input('tanggal_rentang_dari') . ' : ' . $request->input('tanggal_rentang_sampai');

            $tp = Tp::where('status', 'aktif')->first();
            $historis = HistoriSpp::where('oleh_id', Auth::customer()->get()->customer_id)->where('tp', $tp->tp)->where('tgl_bayar', '>=', $from)->where('tgl_bayar', '<=', $to)->get();
            $filename = "Laporan " . $from . '-' . $to;
        }else{
            if($request->input('metode') == 'bulanan'){
                $periode = $request->input('tanggal_bulanan');
                for($i=4; $i<16; $i++){
                    if(($periode+3) == $i){                        
                        $pjd = $bln[$i];
                    }
                }
                $name = '';

                $tp = Tp::where('status', 'aktif')->first();
                $historis = HistoriSpp::where('oleh_id', Auth::customer()->get()->customer_id)->where('tp', $tp->tp)->where('bln_bayar', $periode)->get();
                $filename = "Laporan - " . $periode;
            }elseif($request->input('metode') == 'tahunan'){
                $periode = $request->input('tanggal_tahunan');
                $name = '';
                $pjd = $request->input('tanggal_tahunan');

                $tp = Tp::where('status', 'aktif')->first();
                $historis = HistoriSpp::where('oleh_id', Auth::customer()->get()->customer_id)->where('tp', $tp->tp)->where('thn_bayar', $periode)->get();
                $filename = "Laporan - " . $periode;
            }elseif($request->input('metode') == 'harian'){
                $periode = $request->input('tanggal_harian');
                $name = 'Harian';
                $pjd = $request->input('tanggal_harian');

                $tp = Tp::where('status', 'aktif')->first();
                $historis = HistoriSpp::where('oleh_id', Auth::customer()->get()->customer_id)->where('tp', $tp->tp)->where('tgl_bayar', $periode)->get();
                $filename = "Laporan - " . $periode;
            }
            
            
        }
       
        Excel::create($filename, function($excel) use ($filename, $historis, $pjd, $request) {

            // Set the title
            $excel->setTitle('filename');

            // Chain the setters
            $excel->setCreator('SMKN 2 PAYAKUMBUH')
                  ->setCompany('SMKN 2 PAYAKUMBUH');

            $excel->sheet($filename, function($sheet) use ($historis, $pjd, $request) {
                //tanggal dan uraian header merging MERGING VERTICAL
                $sheet->mergeCells('A6:A7');
                $sheet->mergeCells('B6:B7');
                $sheet->mergeCells('C6:C7');
                $sheet->mergeCells('D6:D7');
                $sheet->mergeCells('H6:H7');

                //MERGING HORIZONTAL ROW A1 FOR HEADER
                $sheet->mergeCells('A2:H2');
                $sheet->mergeCells('E6:G6');
                
                $sheet->row(2, array(
                     'DAFTAR PENERIMAAN SPP, PEMBANGUNAN, DAN PRAMUKA', '', 
                     '', '', '', '', '', '', '', '', '', '', '', '',
                     '', '',
                     '', '', '', '', ''
                ));

                $sheet->row(4, array(
                     'Periode/'. $request->input('metode') .' : ' . $pjd
                ));
                
                $sheet->row(6, array(
                     'HARI/TANGGAL', 'NO KUITANSI', 'NAMA SISWA', 
                     'KELAS',
                     'IURANG YANG DIBAYARKAN','','',
                     'KET'
                ));
                $sheet->row(7, array(
                     '', '', '', '',
                     'SPP', 'PEMBANGUNAN', 'PRAMUKA'
                ));
                $sheet->row(2, function($row) {
                    $row->setAlignment('center');
                });

                $sheet->row(6, function($row) {
                    $row->setAlignment('center');
                });
                
                $sheet->row(7, function($row) {
                    $row->setAlignment('center');
                });

                //SETTING WIDTH CELL
                $sheet->setWidth(array(
                    'A'     =>  16,
                    'B'     =>  16,

                    'C'     =>  30,
                    'D'     =>  10,
                    
                    'E'     =>  17,
                    'F'     =>  17,
                    'G'     =>  17,
                    'H'     =>  30,
                ));

                $row = 8;
                $tspp = 0;
                $tpem = 0;
                $tpra = 0;
                $tlain = 0;
                $tjum = 0;
                if(count($historis) == 0){
                        $no_data = "Tidak ada transaksi pada periode ini.";
                        $sheet->row($row, array(
                            $no_data
                        ));
                    }else{
                        foreach ($historis as $histori) {
                            $tspp += $histori->spp;
                            $tpem += $histori->pembangunan;
                            $tpra += $histori->pramuka;
                            $kelas = Rombel::where('id',$histori->kelas)->select('nama')->first();
                            $sheet->row($row, array(
                            $histori->tgl_bayar, $histori->no_kwitansi, $histori->nama, $histori->kelas, "Rp. " . number_format($histori->spp,2,',','.'), "Rp. " . number_format($histori->pembangunan,2,',','.'), "Rp. " . number_format($histori->pramuka,2,',','.'), $histori->note 
                            ));
                        $row++;
                    }
                    $tjum += $tspp +$tpem + $tpra;
                    $sheet->row(($row), array(
                        'Jumlah','','','',"Rp. " . number_format($tspp,2,',','.'), "Rp. " . number_format($tpem,2,',','.'), "Rp. " . number_format($tpra,2,',','.'), "Rp. " . number_format($tjum,2,',','.')
                        )
                    ); 
                }

                 $sheet->cells('B8:B'.$row, function($cells) {
                    $cells->setAlignment('left');
                }); 
                $sheet->cells('E8:G'.$row, function($cells) {
                    $cells->setAlignment('right');
                });
                $sheet->cells('H8:H'.($row), function($cells) {
                    $cells->setAlignment('left');
                });  
                $sheet->cells('B8:B'.$row, function($cells) {
                    $cells->setAlignment('left');
                }); 
                
                $sheet->cells('A2', function($cells) {
                    $cells->setFont(array(
                        'size'       => '14',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A4', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A6:I6', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A7:I7', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                }); 

                $sheet->cells('A'.$row.':'.'H'.$row, function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                });

                $sheet->cells('A'.($row+8), function($cells) {
                    $cells->setFont(array(
                        'size'       => '8',
                        'bold'       =>  true
                    ));
                });

                $sheet->cells('H'.$row, function($cells) {
                    $cells->setAlignment('right');
                });
                
                $sheet->row(($row+3), array(
                    'Diterima dari', ': ' . Auth::customer()->get()->customer_name, '', '', '','', "Payakumbuh, " . date('j F Y'),'' 
                ));

                $sheet->row(($row+4), array(
                    'Sejumlah', ': '. "Rp. " . number_format($tjum,2,',','.'), '', '', '','', 'Yang menerima','' 
                ));

                $sheet->row(($row+5), array(
                    '', '', '', '', '','', "Bendahara Komite",'' 
                ));

                $sheet->row(($row+8), array(
                    '', '', '', '', '','', "Drs. Asril",'' 
                ));

                $sheet->row(($row+8), array(
                    'Dicetak pada : '. date('d-m-Y h:i:s:A') 
                ));

               
                
                // $sheet->cells('A7:D'.$row, function($cells) {
                //     $cells->setAlignment('left');
                // }); 
                // $sheet->cells('E'.($row+1).':I'.($row+1), function($cells) {
                //     $cells->setAlignment('right');
                // });   
            });

        })->export('xlsx');

    }

    public function lihat($id, $tp, $semester, $no_kuitansi)
    {
        $student = Student::where('nisn', $id)->first();
        $histori = HistoriSpp::where('nisn', $id)->where('tp', $tp)->where('semester', $semester)->where('no_kwitansi', $no_kuitansi)->first();//dd($histori);
        $Mspp = MasterSpp::where('tp', $tp)->first();
        return view('tu-user.pages.invoice.view', ['student'=> $student, 'histori' => $histori, 'Mspp' => $Mspp]);
    }

    public function filter(Request $request){
        $search = $request->input('no_kuitansi');
        $invoices = HistoriSpp::where('no_kwitansi', $request->input('no_kuitansi'))->paginate(10);
        return view('tu-user.pages.invoice.list', ['invoices' => $invoices]);
    }
    
    public function penyesuaian_list()
    {
        $students = Student::orderBy('nama_lengkap', 'ASC')->paginate(10);

        return view('tu-user.pages.invoice.penyesuaian-list', ['students' => $students]);
    }
    public function penyesuaian($id)
    {
        $student = Student::find($id);
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first()->toArray();
                
        if($spps = Spp::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first() !== null)
            $spps = Spp::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $spps = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if($pembangunans = Pembangunan::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first() !== null)
            $pembangunans = Pembangunan::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $pembangunans = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if($pramukas = Pramuka::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first() !== null)
            $pramukas = Pramuka::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $pramukas = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if($lains = LainLain::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first() !== null)
            $lains = LainLain::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $lains = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        $spps = array_values($spps);
        $pembangunans = array_values($pembangunans);
        $pramukas = array_values($pramukas);//dd($pramukas);
        $lains = array_values($lains);


        $Mspp = MasterSPP::where('tp', $tp['tp'])->where('status', 'aktif')->first();//dd($Mspp);
        
        if ($student) {
            return view('tu-user.pages.laporan.penyesuaian', ['student' => $student, 'spps' => $spps, 'pembangunans' => $pembangunans, 'pramukas' => $pramukas, 'lains' => $lains, 'Mspp' => $Mspp, 'tp' => $tp, 'rombelM' => true]);
        }

        return abort(404, 'Request not found');
    }

    public function penyesuaian_post(Request $request, $id)
    {
        $rules = [
            'note' => 'max:255',
        ];

        $validation = Validator::make($request->all(), $rules);
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];

        $tp = TP::where('status', 'aktif')->first();
        $MasterSpp = MasterSPP::where('status', 'aktif')->where('tp', $tp->tp)->first();
        //dd($request->all());
        if($tp->semester == 1){
            $Error = array();
            $nom_spps = array(0,0,0,0);
            for($i=4; $i<10; $i++){
                if($request->input('spp')[$i] != "" && $request->input('spp')[$i] != "p" && $request->input('spp')[$i] != $MasterSpp->spp ){
                    $err = 'Nominal spp bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->spp;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal spp bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->spp);
                }

                if($request->input('spp')[$i] == "p")
                    $nom_spp = $MasterSpp->spp;
                else
                    $nom_spp = $request->input('spp')[$i];

                array_push($nom_spps, $nom_spp);
            }

            $nom_pems = array(0,0,0,0);
            for($i=4; $i<10; $i++){
                if($request->input('pem')[$i] != "" && $request->input('pem')[$i] != "p" && $request->input('pem')[$i] != $MasterSpp->pembangunan ){
                    $err = 'Nominal pembangunan bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->pembangunan;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal pembangunan bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->pembangunan);
                }

                if($request->input('pem')[$i] == "p")
                    $nom_pem = $MasterSpp->pembangunan;
                else
                    $nom_pem = $request->input('pem')[$i];

                array_push($nom_pems, $nom_pem);
            }

            $nom_pras = array(0,0,0,0);
            for($i=4; $i<10; $i++){
                if($request->input('pra')[$i] != "" && $request->input('pra')[$i] != "p" && $request->input('pra')[$i] != $MasterSpp->pramuka ){
                    $err = 'Nominal pramuka bulan '. $bln[$i] .'  yang harus dibayarkan adalah Rp.'.$MasterSpp->pramuka;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal pramuka bulan '. $bln[$i] .'  yang harus dibayarkan adalah Rp.'.$MasterSpp->pramuka);
                }

                if($request->input('pra')[$i] == "p")
                    $nom_pra = $MasterSpp->pramuka;
                else
                    $nom_pra = $request->input('pra')[$i];

                array_push($nom_pras, $nom_pra);
            }

            $nom_lains = array(0,0,0,0);
            for($i=4; $i<10; $i++){
                if($request->input('lain')[$i] != "" && $request->input('lain')[$i] != "p" && $request->input('lain')[$i] != $MasterSpp->lain_lain ){
                    $err = 'Nominal lain-lain bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->lain_lain;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal lain-lain bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->lain_lain);
                }

                if($request->input('lain')[$i] == "p")
                    $nom_lain = $MasterSpp->lain_lain;
                else
                    $nom_lain = $request->input('lain')[$i];

                array_push($nom_lains, $nom_lain);
            }
            //dd($nom_spps);
            if(count($Error) !== 0 )
                return redirect()->back()->withInput()->withErrors($Error);

            //if($request->total == 0)
                //return redirect()->back()->withInput()->withErrors('Nominal pembayaran harus lebih dari nol untuk bisa diproses.');

        }else{
            $Error = array();
            $nom_spps = array(0,0,0,0);
            for($i=10; $i<16; $i++){
                if($request->input('spp')[$i] != "" && $request->input('spp')[$i] != "p" && $request->input('spp')[$i] != $MasterSpp->spp ){
                    $err = 'Nominal spp bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->spp;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal spp bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->spp);
                }

                if($request->input('spp')[$i] == "p")
                    $nom_spp = $MasterSpp->spp;
                else
                    $nom_spp = $request->input('spp')[$i];

                array_push($nom_spps, $nom_spp);
            }

            $nom_pems = array(0,0,0,0);
            for($i=10; $i<16; $i++){
                if($request->input('pem')[$i] != "" && $request->input('pem')[$i] != "p" && $request->input('pem')[$i] != $MasterSpp->pembangunan ){
                    $err = 'Nominal pembangunan bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->pembangunan;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal pembangunan bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->pembangunan);
                }

                if($request->input('pem')[$i] == "p")
                    $nom_pem = $MasterSpp->pembangunan;
                else
                    $nom_pem = $request->input('pem')[$i];

                array_push($nom_pems, $nom_pem);
            }

            $nom_pras = array(0,0,0,0);
            for($i=10; $i<16; $i++){
                if($request->input('pra')[$i] != "" && $request->input('pra')[$i] != "p" && $request->input('pra')[$i] != $MasterSpp->pramuka ){
                    $err = 'Nominal pramuka bulan '. $bln[$i] .'  yang harus dibayarkan adalah Rp.'.$MasterSpp->pramuka;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal pramuka bulan '. $bln[$i] .'  yang harus dibayarkan adalah Rp.'.$MasterSpp->pramuka);
                }

                if($request->input('pra')[$i] == "p")
                    $nom_pra = $MasterSpp->pramuka;
                else
                    $nom_pra = $request->input('pra')[$i];

                array_push($nom_pras, $nom_pra);
            }

            $nom_lains = array(0,0,0,0);
            for($i=10; $i<16; $i++){
                if($request->input('lain')[$i] != "" && $request->input('lain')[$i] != "p" && $request->input('lain')[$i] != $MasterSpp->lain_lain ){
                    $err = 'Nominal lain-lain bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->lain_lain;
                    array_push($Error, $err);
                    //return redirect()->back()->withInput($request->input())->withErrors('Nominal lain-lain bulan '. $bln[$i] .' yang harus dibayarkan adalah Rp.'.$MasterSpp->lain_lain);
                }

                if($request->input('lain')[$i] == "p")
                    $nom_lain = $MasterSpp->lain_lain;
                else
                    $nom_lain = $request->input('lain')[$i];

                array_push($nom_lains, $nom_lain);
            }
            //dd($nom_spps);
            if(count($Error) !== 0 )
                return redirect()->back()->withInput()->withErrors($Error);

            //if($request->total == 0)
                //return redirect()->back()->withInput()->withErrors('Nominal pembayaran harus lebih dari nol untuk bisa diproses.');
        }

        $student = Student::find($request->input('nisn'));//dd($student);

        if ($tp->semester == 1) {
            //lakukan pembayaran semseter 1
            $spp = Spp::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();//dd($spp);
            $exist_spp = Spp::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();//dd($spp);
            if($exist_spp != 1){
                $spp = new Spp;
                $spp->nisn = $request->input("nisn");
                $spp->kelas = $request->input("kelas");
                $spp->tp = $request->input("tp");
            }
            $spp->januari = $nom_spps[4];
            $spp->februari = $nom_spps[5];
            $spp->maret = $nom_spps[6];
            $spp->april = $nom_spps[7];
            $spp->mei = $nom_spps[8];
            $spp->juni = $nom_spps[9];

            $pembangunan = Pembangunan::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();
            $exist_pembangunan = Pembangunan::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();
            if ($exist_pembangunan != 1){ 
                $pembangunan = new Pembangunan;
                $pembangunan->nisn = $request->input("nisn");
                $pembangunan->kelas = $request->input("kelas");
                $pembangunan->tp = $request->input("tp");
            }
            $pembangunan->januari = $nom_pems[4];
            $pembangunan->februari = $nom_pems[5];
            $pembangunan->maret = $nom_pems[6];
            $pembangunan->april = $nom_pems[7];
            $pembangunan->mei = $nom_pems[8];
            $pembangunan->juni = $nom_pems[9];

            $pramuka = Pramuka::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();
            $exist_pramuka = Pramuka::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();
            if($exist_pramuka != 1){
                $pramuka = new Pramuka;
                $pramuka->nisn = $request->input("nisn");
                $pramuka->kelas = $request->input("kelas");
                $pramuka->tp = $request->input("tp");
            }
            $pramuka->januari = $nom_pras[4];
            $pramuka->februari = $nom_pras[5];
            $pramuka->maret = $nom_pras[6];
            $pramuka->april = $nom_pras[7];
            $pramuka->mei = $nom_pras[8];
            $pramuka->juni = $nom_pras[9];

            $lain = LainLain::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();
            $exist_lain = LainLain::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();
            if($exist_lain != 1){
                $lain = new LainLain;
                $lain->nisn = $request->input("nisn");
                $lain->kelas = $request->input("kelas");
                $lain->tp = $request->input("tp");
            }
            $lain->januari = $nom_lains[4];
            $lain->februari = $nom_lains[5];
            $lain->maret = $nom_lains[6];
            $lain->april = $nom_lains[7];
            $lain->mei = $nom_lains[8];
            $lain->juni = $nom_lains[9];
        }else{
            //lakukan pembayaran semseter 2
            $spp = Spp::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();//dd($spp);
            $exist_spp = Spp::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();//dd($spp);
            if($exist_spp != 1){
                $spp = new Spp;
                $spp->nisn = $request->input("nisn");
                $spp->kelas = $request->input("kelas");
                $spp->tp = $request->input("tp");
            }
            $spp->juli = $nom_spps[10];
            $spp->agustus = $nom_spps[11];
            $spp->september = $nom_spps[12];
            $spp->oktober = $nom_spps[13];
            $spp->november = $nom_spps[14];
            $spp->desember = $nom_spps[15];

            $pembangunan = Pembangunan::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();
            $exist_pembangunan = Pembangunan::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();
            if ($exist_pembangunan != 1){ 
                $pembangunan = new Pembangunan;
                $pembangunan->nisn = $request->input("nisn");
                $pembangunan->kelas = $request->input("kelas");
                $pembangunan->tp = $request->input("tp");
            }
            $pembangunan->juli = $nom_pems[10];            
            $pembangunan->agustus = $nom_pems[11];
            $pembangunan->september = $nom_pems[12];
            $pembangunan->oktober = $nom_pems[13];
            $pembangunan->november = $nom_pems[14];
            $pembangunan->desember = $nom_pems[15];

            $pramuka = Pramuka::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();
            $exist_pramuka = Pramuka::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();
            if($exist_pramuka != 1){
                $pramuka = new Pramuka;
                $pramuka->nisn = $request->input("nisn");
                $pramuka->kelas = $request->input("kelas");
                $pramuka->tp = $request->input("tp");
            }
            $pramuka->juli = $nom_pras[10];
            $pramuka->agustus = $nom_pras[11];
            $pramuka->september = $nom_pras[12];
            $pramuka->oktober = $nom_pras[13];
            $pramuka->november = $nom_pras[14];
            $pramuka->desember = $nom_pras[15];

            $lain = LainLain::where('nisn', $student->nisn)->where('tp', $tp->tp)->first();
            $exist_lain = LainLain::where('nisn', $student->nisn)->where('tp', $tp->tp)->count();
            if($exist_lain != 1){
                $lain = new LainLain;
                $lain->nisn = $request->input("nisn");
                $lain->kelas = $request->input("kelas");
                $lain->tp = $request->input("tp");
            }
            $lain->juli = $nom_lains[10];
            $lain->agustus = $nom_lains[11];
            $lain->september = $nom_lains[12];
            $lain->oktober = $nom_lains[13];
            $lain->november = $nom_lains[14];
            $lain->desember = $nom_lains[15];
        }

        if($tp->semester == 1){
            $his_spp = array();
            $his_spp_bln = array();
            for ($i=4; $i <10 ; $i++) { 
                if($request->input('spp')[$i] != "" && $request->input('spp')[$i] != "p"){
                    array_push($his_spp, (int)$request->input('spp')[$i]);
                    array_push($his_spp_bln, $i);
                }
            }

            $his_pem = array();
            $his_pem_bln = array();
            for ($i=4; $i <10 ; $i++) { 
                if($request->input('pem')[$i] != "" && $request->input('pem')[$i] != "p"){
                    array_push($his_pem, (int)$request->input('pem')[$i]);
                    array_push($his_pem_bln, $i);
                }
            }

            $his_pra = array();
            $his_pra_bln = array();
            for ($i=4; $i <10 ; $i++) { 
                if($request->input('pra')[$i] != "" && $request->input('pra')[$i] != "p"){
                    array_push($his_pra, (int)$request->input('pra')[$i]);
                    array_push($his_pra_bln, $i);
                }
            }

            $his_lain = array();
            $his_lain_bln = array();
            for ($i=4; $i <10 ; $i++) { 
                if($request->input('lain')[$i] != "" && $request->input('lain')[$i] != "p"){
                    array_push($his_lain, (int)$request->input('lain')[$i]);
                    array_push($his_lain_bln, $i);
                }
            }
        }else{
            $his_spp = array();
            $his_spp_bln = array();
            for ($i=10; $i <16 ; $i++) { 
                if($request->input('spp')[$i] != "" && $request->input('spp')[$i] != "p"){
                    array_push($his_spp, (int)$request->input('spp')[$i]);
                    array_push($his_spp_bln, $i);
                }
            }

            $his_pem = array();
            $his_pem_bln = array();
            for ($i=10; $i <16 ; $i++) { 
                if($request->input('pem')[$i] != "" && $request->input('pem')[$i] != "p"){
                    array_push($his_pem, (int)$request->input('pem')[$i]);
                    array_push($his_pem_bln, $i);
                }
            }

            $his_pra = array();
            $his_pra_bln = array();
            for ($i=10; $i <16 ; $i++) { 
                if($request->input('pra')[$i] != "" && $request->input('pra')[$i] != "p"){
                    array_push($his_pra, (int)$request->input('pra')[$i]);
                    array_push($his_pra_bln, $i);
                }
            }

            $his_lain = array();
            $his_lain_bln = array();
            for ($i=10; $i <16 ; $i++) { 
                if($request->input('lain')[$i] != "" && $request->input('lain')[$i] != "p"){
                    array_push($his_lain, (int)$request->input('lain')[$i]);
                    array_push($his_lain_bln, $i);
                }
            }
        }

        //push bulan dibayarkan
        $bln_dibayar = array();
        array_push($bln_dibayar, $his_spp_bln);
        array_push($bln_dibayar, $his_pem_bln);
        array_push($bln_dibayar, $his_pra_bln);
        array_push($bln_dibayar, $his_lain_bln);//dd($bln_dibayar);

        $reff = Reff::where('tp', $tp->tp)->where('semester', $tp->semester)->first();
        if($reff == null){
            $reff = new Reff;
            $reff->nama_reff = 1;
            $reff->tp = $tp->tp;
            $reff->semester = $tp->semester;
        }else{
            $reff->nama_reff = $reff->nama_reff + 1;
        }

        $no_kuitansi = $tp->tp . $tp->semester . $reff->nama_reff;

        $histori = new HistoriSpp;
        $histori->nisn = $request->input("nisn");
        $histori->tp = $request->input("tp");
        $histori->kelas = $request->input("kelas");
        
        $histori->tgl_bayar = $request->input("tanggal_bayar");
        $histori->bln_dibayar = json_encode($bln_dibayar);
        $histori->note = $request->input("note");
        $histori->no_kwitansi = $no_kuitansi;
        $histori->semester = $tp->semester;

        $histori->spp = array_sum($his_spp);
        $histori->pembangunan = array_sum($his_pem);
        $histori->pramuka = array_sum($his_pra);
        $histori->lain_lain = array_sum($his_lain);//dd($histori->spp);
        $histori->nominal = $histori->spp + $histori->pembangunan + $histori->pramuka + $histori->lain_lain;


        if($spp->save() && $pembangunan->save() && $pramuka->save() && $lain->save()){
            $histori->save();
            $reff->save();
            return redirect('tu-user/spp/bayar/'.$id)->with('success', 'Pembayaran berhasil disimpan.')->with('no_kuitansi', $histori->no_kwitansi);    
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function rombel(Request $request)
    {
        $user = Customer::find(Auth::customer()->get()->customer_id);
        $jurusans = $user->jurusan;

        return view('tu-user.pages.laporan.list.rombel', ['jurusans' => $jurusans]);      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
