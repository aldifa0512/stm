<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Validator;
use Setting;
use App\Models\Madding;

class MaddingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maddings = Madding::orderBy('id', 'ASC')->paginate(20);

        return view('backend.pages.madding.list', ['maddings' => $maddings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.madding.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'description' => 'required|max:500'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $madding = new Madding;
        $madding->description = $request->input('description');

        $madding->save();

        return redirect('admin/madding')->with('success', 'Madding berhasil ditambahkan!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $madding = Madding::find($id);

        if ($madding) {
            return view('backend.pages.madding.edit', ['madding' => $madding]);
        }

        return abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'description' => 'required|max:500'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $madding = Madding::find($id);

        $madding->description = $request->input('description');

        $madding->save();

        return redirect('admin/madding')->with('success', 'Berhasil mengupdate madding!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $madding = Madding::find($id);

        if ($madding) {
            return view('backend.pages.madding.delete', ['madding' => $madding]);
        }

        return abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delyes($id)
    {
        $madding = Madding::find($id);

        $madding->delete();

        return redirect('admin/madding')->with('success', 'Madding berhasil dihapus !');
    }

    public function delno($id)
    {
        return redirect('admin/madding');
    }

    public function active($id)
    {  
        $Oldactive = Madding::where('status', 'aktif')->update(['status' => '-']);
        $Newactive = Madding::where('id', $id)->update(['status' => 'aktif']);
        return redirect('admin/madding')->with('success', 'Berhasil mengganti status aktif Madding !');
    }
}
