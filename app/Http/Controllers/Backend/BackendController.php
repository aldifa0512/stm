<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Madding;
use App\Models\HistoriSpp;

class BackendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $madding = Madding::where('status', 'aktif')->first();
        $historis = HistoriSpp::orderBy('updated_at', 'DSC')->paginate(10);
        return view('backend.pages.dashboard',['madding' => $madding, 'historis' => $historis]);
    }
}
