<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Validator;
use App\Models\SettingApp as Settings;
use App\Models\Gurus;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$gurus = Gurus::orderBy('namaDepan')->paginate(10);
    	return view('backend.pages.settings.setting', ['gurus' => $gurus]);
    }

    public function guruSearchAjax(Request $request){
    	$guru = Gurus::where('id', $request->input('dataId'))->first();
		return response()->json($guru);
    }

    public function teachersRoles(){
        $settings = Settings::orderBy('key')->paginate(10);
        return view('backend.pages.settings.teachersRoles', ['settings' => $settings]);
    }

    public function teachersRolesCreate(Request $request){
        $validation = Validator::make($request->all(), [
            'key' => 'required|max:255'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $setting = new Settings;

        $setting->key = $request->input('key');

        if ($setting->save()) {
            return redirect('admin/setting/roles')->with('Success', 'Role baru berhasil ditambahkan.');
        }
    }

    public function teachersRolesEdit(Request $request, $id){
        $validation = Validator::make($request->all(), [
            'key' => 'required|max:255'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $setting = Settings::find($request->input('id'));

        if ($setting->delete()) {
            return redirect('admin/setting/roles')->with('Success', 'Role berhasil dihapuskan.');
        }
    }

    public function teachersRolesDelete(Request $request, $id){
        $validation = Validator::make($request->all(), [
            'key' => 'required|max:255'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $setting = Settings::find($request->input('id'));

        if ($setting->delete()) {
            return redirect('admin/setting/roles')->with('Success', 'Role berhasil dihapuskan.');
        }
    }

    public function guruSearchRoleAjax(Request $request){
        $role = Settings::where('id', $request->input('dataId'))->first();
        return response()->json($role);   
    }

    public function teachersRolesUpdate(){
        
    }
}
