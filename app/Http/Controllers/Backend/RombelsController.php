<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use Auth;
use Storage;
use Image;
use App\Http\Requests;
use App\Models\Rombel;
use App\Models\Jurusan;
use App\Models\Student;

class RombelsController extends Controller
{
    public function index()
    {
        $jurusans = Jurusan::all();
        foreach ($jurusans as $key => $value) {
            $rombels = Rombel::where('jurusan_id', $value->id)->orderBy('tingkat')->get();
            foreach ($rombels as $key => $value2) {
                $jumlahSiswa = Student::where('kelas', $value2->id)->where('status', 'aktif')->count();
                $value2['jumlahSiswa'] = $jumlahSiswa;
                $value2['jurusan'] = $value->nama_jurusan;
                $value2['jenjang'] = $value->jenjang;
            }
            $value['rombels'] = $rombels;
        }
        
        return view('backend.pages.rombel.list', [
            'jurusans' => $jurusans
        ]);
    }

    
    public function create()
    {
        return view('backend.pages.rombel.create');
    }


    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'nama' => 'required|unique:rombels,nama',
            'jurusan' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $rombel = new Rombel;

        $rombel->nama = $request->input('nama');
        $rombel->tingkat = $request->input('tingkat');
        $rombel->kelasKe = $request->input('kelasKe');
        $rombel->jurusan_id = $request->input('jurusan');
        $rombel->pengajar = $request->input('pengajar');

        if ($rombel->save()) {
            return redirect('admin/rombels')->with('Success', 'Rombel baru berhasil ditambahkan.');
        }
    }


    public function edit($id)
    {
        $rombel = Rombel::find($id);
        $existedRombels = Rombel::where('jurusan_id', $rombel->jurusan_id)->where('tingkat', $rombel->tingkat)->get();
        if ($rombel) {
            return view('backend.pages.rombel.edit', [
                'rombel' => $rombel,
                'existedRombels' => $existedRombels
                ]);
        }
    }

    public function getJurusanAjax(Request $request){
        $jurusan = Jurusan::find($request->input('jurusanId'));
        
        return json_encode($jurusan);
    }

    public function getExistedRombelsAjax(Request $request){
        $rombels = Rombel::where('jurusan_id', $request->input('jurusanId'))->where('tingkat', $request->input('tingkat'))->get();
        
        return json_encode($rombels);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'nama' => 'required',
            'jurusan' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $rombel = Rombel::find($id);

        $rombel->nama = $request->input('nama');
        $rombel->tingkat = $request->input('tingkat');
        $rombel->kelasKe = $request->input('kelasKe');
        $rombel->jurusan_id = $request->input('jurusan');
        $rombel->pengajar = $request->input('pengajar');
        if ($rombel->save()) {
            return redirect('admin/rombels')->with('success', 'Rombel berhasil dirubah !');
        }
    }


    public function drop(Request $request, $id)
    {
        $rombel = Rombel::find($id);

        if ($rombel) {
            return view('backend.pages.rombel.delete', ['rombel' => $rombel]);
        }

        return abort(403);
    }


    public function dropGet($id)
    {
        $rombel = Rombel::find($id);

        $rombel->delete();

        return redirect('admin/rombels')->with('Success', 'Rombel berhasil dihapus !');
    }

    public function dropNo($id)
    {
        return redirect('admin/rombels');
    }
}
