<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Validator;
use Setting;
use App\Models\Jurusan;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jurusans = Jurusan::orderBy('id', 'ASC')->paginate(100);

        return view('backend.pages.jurusan.list', ['jurusans' => $jurusans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.jurusan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'nama_jurusan' => 'required|max:100'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $jurusan = new Jurusan;
        $jurusan->nama_jurusan = $request->input('nama_jurusan');
        $jurusan->tu_id = $request->input('tu');
        
        $jurusan->save();

        return redirect('admin/jurusan')->with('success', 'Jurusan berhasil ditambahkan!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurusan = Jurusan::find($id);

        if ($jurusan) {
            return view('backend.pages.jurusan.edit', ['jurusan' => $jurusan]);
        }

        return abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'nama_jurusan' => 'required|max:100'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $jurusan = Jurusan::find($id);

        $jurusan->nama_jurusan = $request->input('nama_jurusan');
        $jurusan->tu_id = $request->input('tu');

        $jurusan->save();

        return redirect('admin/jurusan')->with('success', 'Berhasil mengupdate jurusan!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $jurusan = Jurusan::find($id);

        if ($jurusan) {
            return view('backend.pages.jurusan.delete', ['jurusan' => $jurusan]);
        }

        return abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delyes($id)
    {
        $jurusan = Jurusan::find($id);

        $jurusan->delete();

        return redirect('admin/jurusan')->with('success', 'Jurusan berhasil dihapus !');
    }

    public function delno($id)
    {
        return redirect('admin/jurusan');
    }
}
