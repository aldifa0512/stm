<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MasterSPP;
use Validator;
use Auth;

class SppController extends Controller
{
    /**
     * get all un approved listings
     */
    public function index()
    {
        $spps=MasterSPP::all();
         
        return view('backend.pages.spp.list', ['spps' => $spps]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.spp.create');
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'tp' => 'required',
            'spp' => 'required',
            'pembangunan' => 'required',
            'pramuka' => 'required',
            'lain_lain' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $spp = new MasterSPP;

        $spp->tp = $request->input('tp');
        $spp->spp = $request->input('spp');
        $spp->pramuka = $request->input('pramuka');
        $spp->pembangunan = $request->input('pembangunan');
        $spp->lain_lain = $request->input('lain_lain');

        if ($spp->save()) {
            //return redirect('admin/rombels/edit/'. $listing->id)->with('success', 'Listing created successfully.');
            return redirect('admin/spp')->with('Success', 'Master SPP berhasil ditambahkan.');
        }
    }

    public function edit($id)
    {
        $spp=MasterSPP::find($id);
         
        return view('backend.pages.spp.edit', ['spp' => $spp]);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'tp' => 'required',
            'spp' => 'required',
            'pembangunan' => 'required',
            'pramuka' => 'required',
            'lain_lain' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $spp=MasterSPP::find($id);

        $spp->tp = $request->input('tp');
        $spp->spp = $request->input('spp');
        $spp->pramuka = $request->input('pramuka');
        $spp->pembangunan = $request->input('pembangunan');
        $spp->lain_lain = $request->input('lain_lain');

        if ($spp->save()) {
            //return redirect('admin/rombels/edit/'. $listing->id)->with('success', 'Listing created successfully.');
            return redirect('admin/spp')->with('Success', 'Berhasil update Master SPP.');
        }
    }

    public function delete_spp($id)
    {
        $spp = MasterSPP::find($id);

        if ($spp) {
            return view('backend.pages.spp.delete', ['spp' => $spp]);
        }

        return abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Adelete_spp($id)
    {
        $spp = MasterSPP::find($id);

        $spp->delete();

        return redirect('admin/spp')->with('Success', 'Master SPP berhasil dihapus !');
    }

    public function Cdelete_spp($id)
    {
        return redirect('admin/spp');
    }

    public function active($id)
    {  
        $Oldactive = MasterSPP::where('status', 'aktif')->update(['status' => '-']);
        $Newactive = MasterSPP::where('id', $id)->update(['status' => 'aktif']);
        return redirect('admin/spp')->with('success', 'Berhasil mengganti status aktif SPP !');
    }

}
