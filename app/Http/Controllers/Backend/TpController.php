<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Validator;
use Setting;
use App\Models\Tp;

class TpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tp = Tp::orderBy('tp', 'ASC')->paginate(20);

        return view('backend.pages.tp.list', ['tp' => $tp]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.tp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'tp' => 'required|max:100'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $tp = new Tp;
        $tp->tp = $request->input('tp');
        $tp->semester = $request->input('semester');

        $tp->save();

        return redirect('admin/tp')->with('success', 'TP berhasil ditambahkan!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tp = Tp::find($id);

        if ($tp) {
            return view('backend.pages.tp.edit', ['tp' => $tp]);
        }

        return abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'tp' => 'required|max:100'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $tp = Tp::find($id);

        $tp->tp = $request->input('tp');
        $tp->semester = $request->input('semester');

        $tp->save();

        return redirect('admin/tp')->with('success', 'Berhasil mengupdate tp!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $tp = Tp::find($id);

        if ($tp) {
            return view('backend.pages.tp.delete', ['tp' => $tp]);
        }

        return abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delyes($id)
    {
        $tp = Tp::find($id);

        $tp->delete();

        return redirect('admin/tp')->with('success', 'Tp berhasil dihapus !');
    }

    public function delno($id)
    {
        return redirect('admin/tp');
    }

    public function active($id)
    {  
        $Oldactive = Tp::where('status', 'aktif')->update(['status' => '-']);
        $Newactive = Tp::where('id', $id)->update(['status' => 'aktif']);
        return redirect('admin/tp')->with('success', 'Berhasil mengganti status aktif TP !');
    }
}
