<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Models\User;
use App\Models\Customer;
use App\Models\Komite;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tu_index()
    {
        $users = Customer::orderBy('created_at', 'DESC')->paginate(15);

        return view('backend.pages.users.list', ['users' => $users]);
    }

    public function komite_index()
    {
        $users = Komite::orderBy('created_at', 'DESC')->paginate(15);

        return view('backend.pages.users.list', ['users' => $users, 'komite' => true]);
    }

    public function admin_index()
    {
        $users = User::orderBy('created_at', 'DESC')->paginate(15);

        return view('backend.pages.users.admin.list', ['users' => $users, 'admin' => true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function komite_create()
    {
        return view('backend.pages.users.create', ['komite' => true]);
    }

    public function tu_create()
    {
        return view('backend.pages.users.create');
    }

    public function admin_create()
    {
        return view('backend.pages.users.admin.create', ['admin' => true]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function komite_store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $cek_email = Komite::all()->where('pic_email', $request->input('email'))->first();
        if($cek_email) return redirect()->back()->withInput()->withErrors('Email sudah ada !');

        $user = new Komite;
        $user->komite_id = 'KMT-' . date('ymdhis');
        $user->komite_name = $request->input('name');
        $user->pic_email = $request->input('email');
        $user->pic_phone = $request->input('telepon');
        $user->password = bcrypt($request->input('password'));

        if ($user->save()) {
            // Attach role to user
            //$user->roles()->attach($request->input('role'));
            return redirect('admin/users/komite')->with('success', 'Komite user create success.');
        }
    }

    public function tu_store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $cek_email = Customer::all()->where('pic_email', $request->input('email'))->first();
        if($cek_email) return redirect()->back()->withInput()->withErrors('Email sudah ada !');

        $user = new Customer;
        $user->customer_id = 'TU-' . date('ymdhis');
        $user->customer_name = $request->input('name');
        $user->pic_email = $request->input('email');
        $user->pic_phone = $request->input('telepon');
        $user->password = bcrypt($request->input('password'));

        if ($user->save()) {
            // Attach role to user
            //$user->roles()->attach($request->input('role'));
            return redirect('admin/users/tu')->with('success', 'Tata Usaha user create success.');
        }
    }

    public function admin_store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $cek_email = User::all()->where('email', $request->input('email'))->first();
        if($cek_email) return redirect()->back()->withInput()->withErrors('Email sudah ada !');

        $user = new User;
        //$user->komite_id = 'KMT-' . date('ymdhis');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->pic_phone = $request->input('telepon');
        $user->password = bcrypt($request->input('password'));

        if ($user->save()) {
            // Attach role to user
            // $user->roles()->attach(1);
            return redirect('admin/users/admin')->with('success', 'Administrator user create success.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function komite_edit($id)
    {
        $user = Komite::find($id);

        if ($user) {
            return view('backend.pages.users.edit', ['user' => $user, 'komite' => true]);
        }

        return abort(404, 'Request not found');
    }

    public function tu_edit($id)
    {
        $user = Customer::find($id);

        if ($user) {
            return view('backend.pages.users.edit', ['user' => $user]);
        }

        return abort(404, 'Request not found');
    }

    public function admin_edit($id)
    {
        $user = User::find($id);

        if ($user) {
            return view('backend.pages.users.admin.edit', ['user' => $user, 'admin' => true]);
        }

        return abort(404, 'Request not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function komite_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = Komite::find($id);

        if ($user) {
            $user->komite_name = $request->input('name');
            $user->password = bcrypt($request->input('password'));
            $user->pic_phone = $request->input('telepon');

            if ($user->save()) {
                // Attach role to user
                //$user->roles()->attach($request->input('role'));
                return redirect('admin/users/komite/edit/'.$user->komite_id)->with('success', 'User updated success.');
            }
        }
    }

    public function tu_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = Customer::find($id);

        if ($user) {
            $user->customer_name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->pic_phone = $request->input('telepon');

            if ($user->save()) {
                // Attach role to user
                //$user->roles()->attach($request->input('role'));
                return redirect('admin/users/tu/edit/'.$user->customer_id)->with('success', 'User updated success.');
            }
        }
    }

    public function admin_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = User::find($id);

        if ($user) {
            $user->name = $request->input('name');
            $user->password = bcrypt($request->input('password'));
            $user->pic_phone = $request->input('telepon');

            if ($user->save()) {
                // Attach role to user
                //$user->roles()->attach($request->input('role'));
                return redirect('admin/users/admin/edit/'.$user->id)->with('success', 'User updated success.');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function komite_destroy($id)
    {//die("here");
        $user = Komite::find($id);
        $user->delete();

        return redirect('admin/users/komite')->with('success', 'User berhasil dihapus !');
    }

    public function tu_destroy($id)
    {
        $user = Customer::find($id);
        $user->delete();

        return redirect('admin/users/tu')->with('success', 'User berhasil dihapus !');
    }

    public function admin_destroy($id)
    {//die("here");
        $user = User::find($id);
        $user->delete();

        return redirect('admin/users/admin')->with('success', 'User berhasil dihapus !');
    }
}
