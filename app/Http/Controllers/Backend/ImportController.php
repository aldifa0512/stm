<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Storage;
use Image;
use App\Models\Rombel;
use App\Models\Student;
use App\Models\Jurusan;
use App\Libraries\ExcelLibrary;
use PHPExcel; 
use PHPExcel_IOFactory; 


class ImportController extends Controller
{
    public function index()
    {
        return view('backend.pages.import.index');
    }

    public function import(Request $request)
    {   //dd($request->file('file'));

        $inputFileName = $request->file('file');//'./sampleData/example1.xlsx';
        echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory to identify the format<br />';
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);


        echo '<hr />';

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $results = $sheetData;//dd($results[45]);
            $count = 1;
            foreach ($results as $row) {
                if($row['E'] == null) continue;
                $student = new Student;
                $nisn = Student::where('nisn',$row['E'])->count();
                
                if($nisn >=1) continue;
                if($row['B'] == 'Nama') continue;                        

                $student->nama_lengkap = $row['B'];
                $student->nisn = $row['E'];
                // $student->kelas = $row['rombel_saat_ini'];
                $student->nipd = $row['C'];
                $student->status = 'new_import';
                $student->jenis_kelamin = $row['D'];
                //$student->tu_id = Auth::Tu[]->get[]->tu_id;
                $student->nik = $row['H'];
                $student->tempat_lahir = $row['F'];
                //$student->status = 'aktif';
                $student->tanggal_lahir = $row['G'];
                $student->agama = $row['I'];
                $student->alamat_jln = $row['J'];
                $student->rt = $row['K'];
                $student->rw = $row['L'];
                $student->dusun = $row['M'];
                $student->kelurahan = $row['N'];
                $student->kecamatan = $row['O'];
                //$student->kip = $row['kip'];
                $student->kode_pos = $row['P'];
                //$student->kip_nama = $row['kip_nama'];
                $student->jenis_tinggal = $row['Q'];
                $student->transport = $row['R'];
                $student->telepon = $row['S'];
                $student->hp = $row['T'];
                $student->email = $row['U'];
                $student->skhun = $row['V'];
                $student->penerima_kps = $row['W'];
                $student->no_kps = $row['X'];
                //$student->kks = $row['kks'];
                //$student->akta_lahir = $row['akta_lahir'];
                //$student->npsn_smp = $row['npsn_smp'];

                $student->nama_ayah = $row['Y'];
                $student->pekerjaan_ayah = $row['AB'];
                $student->tl_ayah = $row['Z'];
                $student->nik_ayah = $row['AD'];
                $student->nik_ibu = $row['AJ'];
                $student->nik_wali = $row['AP'];
                $student->penghasilan_ayah = $row['AC'];
                $student->penghasilan_ibu = $row['AI'];
                $student->penghasilan_wali = $row['AO'];
                $student->pendidikan_ayah = $row['AA'];
                $student->pendidikan_ibu = $row['AG'];
                $student->pendidikan_wali = $row['AM'];
                $student->nama_ibu = $row['AE'];
                $student->pekerjaan_ibu = $row['AH'];
                $student->tl_ibu = $row['AF'];
                $student->nama_wali = $row['AK'];
                $student->pekerjaan_wali = $row['AN'];
                $student->tl_wali = $row['AL'];
                
                //$student->jenis_pendaftaran = $row['jenis_pendaftaran'];
                //$student->tgl_masuk = $row['tgl_masuk'];
                //$student->jurusan = $row['jurusan'];
                $student->importedClass = $row['AQ'];
                $student->no_kip = $row['AU'];
                $student->nama_di_kip = $row['AV'];
                $student->penerima_kip = $row['AT'];
                //$student->nis = $row['nis'];
                $student->nomor_kks = $row['AW'];
                $student->bank = $row['AY'];
                $student->nomor_rekening_bank = $row['AZ'];
                $student->rekening_atas_nama = $row['BA'];
                $student->no_registrasi_akta_lahir = $row['AX'];
                $student->no_ujian_smp = $row['AR'];
                $student->no_ijazah = $row['AS'];
                $student->layak_pip = $row['BB'];
                $student->alasan_pip = $row['BC'];
                $student->save();
            }
        return "Berhasil";
    }
}

