<?php

namespace App\Http\Controllers\Komite;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class AnalisisController extends Controller
{
	
	function __construct()
	{
		$this->middleware('authKomite');
	}

	public function index()
	{
        $bayar = get_uang_perangkatan(get_tp_aktif());
        $bayar2 = get_uang_perbulan(get_tp_aktif());
        $data_bar = get_pembayaran_tahunke(get_tp_aktif());
		return view('komite.pages.analisis.index', [
            'data_bar' => $data_bar,
            'bayar' => $bayar,
            'bayar2' => $bayar2
            ]);
	}

	public function pilihTahunGrafikBalok(Request $request){
		$tp = getTp($request->input('value'));
        $data_bar = get_pembayaran_tahunke($tp);
        return $data_bar;
	}

}