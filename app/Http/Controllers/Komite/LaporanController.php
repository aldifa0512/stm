<?php

namespace App\Http\Controllers\Komite;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Rombel;

use App\Models\Spp;
use App\Models\Pembangunan;
use App\Models\Pramuka;
use App\Models\LainLain;

use App\Models\MasterSPP;
use App\Models\HistoriSpp;
use App\Models\HistoriSppEdit;
use App\Models\Tunggakan;
use App\Models\Jurusan;
use App\Models\Reff;
use App\Models\Tp;
use App\Models\EditKuitansi;
use App\Libraries\ExcelLibrary;
use PHPExcel; 
use PHPExcel_IOFactory; 
use Validator;
use Auth;
use PDF;

class LaporanController extends Controller
{
    public function getType($value){

        switch ($value) {
            case 'perkelas':
                $response = Rombel::orderBy('nama', 'ASC')->get();
                $child = '<div class="form-group added"><label class="control-label col-sm-2 added" for="type">Pilih Kelas</label>';
                $child .=    '<div class="col-sm-2 added">';
                $child .=    '<select name="type_value" id="type_value" class="form-control added">';
                foreach ($response as $key => $value) {
                    $child .= '<option value="'.$value->nama.'" class="added">'.$value->nama.'</option>';
                }
                $child .=     '</select></div></div>';
                
                break;
            
            case 'perjurusan':
                $response = Jurusan::all();
                $child = '<div class="form-group added"><label class="control-label col-sm-2 added" for="type">Pilih Jurusan</label>';
                $child .=    '<div class="col-sm-5 added">';
                $child .=    '<select name="type_value" id="type_value" class="form-control added">';
                foreach ($response as $key => $value) {
                    $child .= '<option value="'.$value->singkatan.'" class="added">'.$value->nama_jurusan.'</option>';
                }
                $child .=     '</select></div></div>';
                break;

            case 'semua':
                $child = '';
                break;
        }
        return response()->json($child);
    }

    public function getWaktu($value){

        switch ($value) {
            case 'tahunan':
                $tahuns = Tp::groupBy('tp')->get();
                $child = '<div class="form-group added-waktu"><label class="control-label col-sm-2 added-waktu" for="type">Pilih Tahun</label>';
                $child .=    '<div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_tahunan">';
                foreach ($tahuns as $key => $value) {
                    $child .= '<option value="'.$value->tp.'" class="added-waktu">'.$value->tp.'</option>';
                }
                $child .=    '</select></div></div>';
                
                break;
            
            case 'bulanan':
                $tahuns = Tp::groupBy('tp')->get();
                $child = '<div class="form-group added-waktu"><label class="control-label col-sm-2 added-waktu" for="type">Pilih Tahun</label>';
                $child .=    '<div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_tahunan">';
                foreach ($tahuns as $key => $value) {
                    $child .= '<option value="'.$value->tp.'" class="added-waktu">'.$value->tp.'</option>';
                }
                $child .=    '</select></div></div>';

                $bulans = Bulan_laporan();
                $child .= '<div class="form-group added-waktu"><label class="control-label col-sm-2 added-waktu" for="type">Pilih Bulan</label>';
                $child .=    '<div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_bulanan">';
                foreach ($bulans as $key => $value) {
                    $child .= '<option value="'.$key.'" class="added-waktu">'.$value.'</option>';
                }
                $child .=    '</select></div></div>';
                break;

            case 'harian':
                $tahuns = Tp::groupBy('tp')->get();
                $bulans = Bulan_laporan();
                $harians = Hari_laporan();
                $child = '<div class="form-group added-waktu"><label class="control-label col-sm-2 added-waktu" for="type">Pilih Hari</label>';
                $child .=    '<div class="col-sm-1 added-waktu"><select class="form-control" name="waktu_value_harian">';
                foreach ($harians as $key => $value) {
                    $child .= '<option value="'.$value.'" class="added-waktu">'.$value.'</option>';
                }
                $child .=    '</select></div><div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_bulanan">';
                foreach ($bulans as $key => $value) {
                    $child .= '<option value="'.$key.'" class="added-waktu">'.$value.'</option>';
                }
                $child .=    '</select></div>';
                $child .=    '<div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_tahunan">';
                foreach ($tahuns as $key => $value) {
                    $child .= '<option value="'.$value->tp.'" class="added-waktu">'.$value->tp.'</option>';
                }
                $child .=    '</select></div></div>';
                break;

            case 'rentang':
                $tahuns = Tp::groupBy('tp')->get();
                $harians = Hari_laporan();
                $child = '<div class="form-group added-waktu"><label class="control-label col-sm-2 added-waktu" for="type">Pilih Tahun</label>';
                $child .=    '<div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_tahunan">';
                foreach ($tahuns as $key => $value) {
                    $child .= '<option value="'.$value->tp.'" class="added-waktu">'.$value->tp.'</option>';
                }
                $child .=    '</select></div></div>';

                $bulans = Bulan_laporan();
                $child .= '<div class="form-group added-waktu"><label class="control-label col-sm-2 added-waktu" for="type">Hitung Dari</label>';
                $child .=    '<div class="col-sm-1 added-waktu"><select class="form-control" name="waktu_value_harian_dari">';
                foreach ($harians as $key => $value) {
                    $child .= '<option value="'.$value.'" class="added-waktu">'.$value.'</option>';
                }
                $child .=    '</select></div><div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_bulanan_dari">';
                foreach ($bulans as $key => $value) {
                    $child .= '<option value="'.$key.'" class="added-waktu">'.$value.'</option>';
                }
                $child .=    '</select></div></div>';

                $child .= '<div class="form-group added-waktu"><label class="control-label col-sm-2 added-waktu" for="type">Hitung Sampai</label>';
                $child .=    '<div class="col-sm-1 added-waktu"><select class="form-control" name="waktu_value_harian_sampai">';
                foreach ($harians as $key => $value) {
                    $child .= '<option value="'.$value.'" class="added-waktu">'.$value.'</option>';
                }
                $child .=    '</select></div><div class="col-sm-2 added-waktu"><select class="form-control" name="waktu_value_bulanan_sampai">';
                foreach ($bulans as $key => $value) {
                    $child .= '<option value="'.$key.'" class="added-waktu">'.$value.'</option>';
                }
                $child .=    '</select></div></div>';
                break;
        }
        return response()->json($child);
    }

    public function ajax(Request $request)
    {

        if($request->input('type') == 'type'){

            return $this->getType($request->input('value'));

        }elseif($request->input('type') == 'waktu'){

            return $this->getWaktu($request->input('value'));

        }elseif($request->input('type') == 'staff'){

            return $this->getStaff($request->input('value'));

        }
    }

    public function ajaxSuratTunggakan(Request $request){
        $kelas = Rombel::where('jurusan_id', $request->value)->orderBy('nama')->get();
        return json_encode($kelas);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function invoice()
    {
        $invoices = HistoriSpp::orderBy('no_kwitansi', 'DSC')->paginate(10);//dd($invoices);
        return view('komite.pages.invoice.list', ['invoices' => $invoices]);
    }

    public function invoice_cetak()
    {
        return view('komite.pages.laporan.cetakPerInvoice');
    }

    public function filter_laporan($id, $tanggal){
        if($id == 'none'){
            $laporan = HistoriSpp::where('tgl_bayar', $tanggal)->get();
        }else{
            $laporan = HistoriSpp::where('oleh_id', trim($id))->where('tgl_bayar', $tanggal)->get();
        }
        return $laporan;
    }

    public function get_typeLapName($type, $value){
        if($type == 'perjurusan'){
            return strtoupper('JUR. '.$value);
        }elseif($type == 'perkelas'){
            return strtoupper('KEL. '.$value);
        }else{
            return 'SEMUA SISWA';
        }
    }

    public function invoice_cetak_post(Request $request)
    {
        // dd($request->input('type'));
        //filter type laporan
        $jenisPembayaran = $request->input('jenisPembayaran');
        if($request->input('typeLap') == 'pembayaran'){
            switch ($request->input('type')) {
                case 'perjurusan':
                    switch ($request->input('waktu')) {
                        case 'tahunan':
                            $TI = 'SELAMA TAHUN ' . $request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'bulanan':
                            $TI = 'SELAMA BULAN ' .get_bulan($request->input('waktu_value_bulanan')) .' '.$request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'harian':
                            $TI = 'SELAMA ' . $request->input('waktu_value_harian') .' '. get_bulan($request->input('waktu_value_bulanan')) .' '.$request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('tgl_bayar', $request->input('waktu_value_harian'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('tgl_bayar', $request->input('waktu_value_harian'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'rentang':
                            $TI = 'SELAMA TAHUN ' . $request->input('waktu_value_tahunan') . ' (' .$request->input('waktu_value_harian_dari') .' '. get_bulan($request->input('waktu_value_bulanan_dari')) .' - '. $request->input('waktu_value_harian_sampai') .' '. get_bulan($request->input('waktu_value_bulanan_sampai')) . ')';
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->whereBetween('bln_bayar', [$request->input('waktu_value_bulanan_dari'), $request->input('waktu_value_bulanan_sampai')])
                                ->whereBetween('tgl_bayar', [$request->input('waktu_value_harian_dari'), $request->input('waktu_value_harian_sampai')])
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', 'LIKE', '%'.$request->input('type_value').'%')
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->whereBetween('bln_bayar', [$request->input('waktu_value_bulanan_dari'), $request->input('waktu_value_bulanan_sampai')])
                                ->whereBetween('tgl_bayar', [$request->input('waktu_value_harian_dari'), $request->input('waktu_value_harian_sampai')])
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                    }
                    break;
                
                case 'perkelas':
                    switch ($request->input('waktu')) {
                        case 'tahunan':
                            $TI = 'SELAMA TAHUN ' . $request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'bulanan':
                            $TI = 'SELAMA BULAN ' .get_bulan($request->input('waktu_value_bulanan')) .' '.$request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'harian':
                            $TI = 'SELAMA ' . $request->input('waktu_value_harian') .' '. get_bulan($request->input('waktu_value_bulanan')) .' '.$request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('tgl_bayar', $request->input('waktu_value_harian'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('tgl_bayar', $request->input('waktu_value_harian'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'rentang':
                            $TI = 'SELAMA TAHUN ' . $request->input('waktu_value_tahunan') . ' (' .$request->input('waktu_value_harian_dari') .' '. get_bulan($request->input('waktu_value_bulanan_dari')) .' - '. $request->input('waktu_value_harian_sampai') .' '. get_bulan($request->input('waktu_value_bulanan_sampai')) . ')';
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->whereBetween('bln_bayar', [$request->input('waktu_value_bulanan_dari'), $request->input('waktu_value_bulanan_sampai')])
                                ->whereBetween('tgl_bayar', [$request->input('waktu_value_harian_dari'), $request->input('waktu_value_harian_sampai')])
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('kelas', $request->input('type_value'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->whereBetween('bln_bayar', [$request->input('waktu_value_bulanan_dari'), $request->input('waktu_value_bulanan_sampai')])
                                ->whereBetween('tgl_bayar', [$request->input('waktu_value_harian_dari'), $request->input('waktu_value_harian_sampai')])
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                    }
                    break;
                
                case 'semua':
                    switch ($request->input('waktu')) {
                        case 'tahunan':
                            $TI = 'SELAMA TAHUN ' . $request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'bulanan':
                            $TI = 'SELAMA BULAN ' .get_bulan($request->input('waktu_value_bulanan')) .' '.$request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'harian':
                            $TI = 'SELAMA ' . $request->input('waktu_value_harian') .' '. get_bulan($request->input('waktu_value_bulanan')) .' '.$request->input('waktu_value_tahunan');
                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('tgl_bayar', $request->input('waktu_value_harian'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('bln_bayar', $request->input('waktu_value_bulanan'))
                                ->where('tgl_bayar', $request->input('waktu_value_harian'))
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                        
                        case 'rentang':
                            $TI = 'SELAMA TAHUN ' . $request->input('waktu_value_tahunan') . ' (' .$request->input('waktu_value_harian_dari') .' '. get_bulan($request->input('waktu_value_bulanan_dari')) .' - '. $request->input('waktu_value_harian_sampai') .' '. get_bulan($request->input('waktu_value_bulanan_sampai')) . ')';

                            if($request->input('staff') == 'semua'){
                                $hb = HistoriSpp::where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->whereBetween('bln_bayar', [$request->input('waktu_value_bulanan_dari'), $request->input('waktu_value_bulanan_sampai')])
                                ->whereBetween('tgl_bayar', [$request->input('waktu_value_harian_dari'), $request->input('waktu_value_harian_sampai')])
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }else{
                                $hb = HistoriSpp::where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->where('thn_bayar', $request->input('waktu_value_tahunan'))
                                ->whereBetween('bln_bayar', [$request->input('waktu_value_bulanan_dari'), $request->input('waktu_value_bulanan_sampai')])
                                ->whereBetween('tgl_bayar', [$request->input('waktu_value_harian_dari'), $request->input('waktu_value_harian_sampai')])
                                ->where('oleh_id', $request->input('staff'))
                                ->where('note', $jenisPembayaran)
                                ->get();
                            }
                            break;
                    }
                    break;
            }
            
            $staffName = get_user_name($request->input('staff'));
            $lapName = $this->get_typeLapName($request->input('type'), $request->input('type_value'));
            $result = ExcelLibrary::cetakPembayaran($staffName, $TI, $lapName, $hb, $jenisPembayaran );

            return $result;
        }
    }

    public function lihat($id, $tp, $semester, $no_kuitansi)
    {
        $nisn = $id;
        $student = get_student($nisn);
        $histori = HistoriSpp::where('nisn', $id)->where('tp', $tp)->where('semester', $semester)->where('no_kwitansi', $no_kuitansi)->first();//dd($histori);
        $Mspp = MasterSpp::where('tp', $tp)->first();
        return view('komite.pages.spp.invoice', ['student'=> $student, 'histori' => $histori, 'Mspp' => $Mspp]);
    }

    public function filter(Request $request){
        $search = $request->input('no_kuitansi');
        $invoices = HistoriSpp::where('no_kwitansi', $request->input('no_kuitansi'))->paginate(10);
        return view('komite.pages.invoice.list', ['invoices' => $invoices]);
    }

    public function get_tunggakan($master_biaya, $record, $bts_bln, $jml_bayar = 0, $biaya = 0, $tunggakan = 0){
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        if($bts_bln <10){
            for($i=10; $i <= 15; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
                $jml_bayar += $master_biaya;
            }
            for($i=4; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
                $jml_bayar += $master_biaya;
            }
        }else{
            for($i=10; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
                $jml_bayar += $master_biaya;
            }
        }
        $tunggakan = $jml_bayar - $biaya;
        return $tunggakan;
    }

    public function bayar_beasiswa($master_biaya, $record){
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        
        $count = 0;
        $total_beasiswa = 0;
        $bln_dibayar = array();
        for($i=4; $i <= 15; $i++){
            $b = strtolower($bln[$i]);
            if($record->$b == null || $record->$b == 0 || $record->$b == ''){
                $total_beasiswa += $master_biaya;
                $record->$b = -1;
                $bln_dibayar[] = $i;
                $count++;
            }   
        }
        return array('total_beasiswa' => $total_beasiswa, 'bln_dibayar' => $bln_dibayar);
    }

    public function hitung_kosong($record){
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        $kosong = 0;
        for ($i=4; $i < 16; $i++) { 
            $b = strtolower($bln[$i]);
            if($record->$b != null && $record->$b != '' && $record->$b != 0){
                $kosong++;
            }
        }    
        $kosong = 12 - $kosong;
        return $kosong;
    }
        
    public function get_biaya_dibayar($record, $bts_bln, $biaya = 0){
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        if($bts_bln <10){
            for($i=10; $i <= 15; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
            }
            for($i=4; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
            }
        }else{
            for($i=10; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
            }
        }
        return $biaya;
    }

    public function penyesuaian($id)
    {
        //Definisi Bulan dalam bahasa Indonesia
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];

        
        //get student's data by id
        $nisn = $id;
        $student = get_student($nisn);
        //Validasi tahun pelajaran siswa telah diiisi.
        if($student->tp == null){
            return "Pastikan <b>Tahun Dasar SPP (Tahun Kelas)</b> siswa telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }
        //get class's name by kelas id from students table
        $rombel = Rombel::find($student->kelas);
        //get Tahun Pelaran aktif, where status = aktif
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
        //Get master biaya sekolah
        $master_spp_aktif = MasterSPP::where('tp', $rombel->kurikulum)->first();
        $master_spp = MasterSPP::where('tp', $rombel->kurikulum)->first();
        //Get batas tunggakan
        $tunggakan = Tunggakan::first();
        //Validasi bahwa tahun dasar perhitungan biaya sekolah pada rombel siswa yg bersangkutan telah ditentukan pada admin page
        if($master_spp_aktif == null){
            return "Pastikan <b>Tahun Kurikulum Rombel pada Admin</b> telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }
        if($master_spp == null){
            $master_spp = new MasterSPP;
        }

        //Get data pembayaran masing2 untuk spp, pembanguna, pramuka, dan biaya lain2...
        $spp = Spp::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pembangunan = Pembangunan::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pramuka = Pramuka::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $lain2 = LainLain::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        
        //tunggakan per record/atau per tahun
        $tspp = Spp::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tpembangunan = Pembangunan::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tpramuka = Pramuka::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tlain2 = LainLain::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        //Inisialisasi var bulan dengan nilai nol tiap bulannya
        $bulan_kosong = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        
        //Inisialisasi data pembayaran tipa bulannya jika belum ada record di database
        if( $spp == null) {$spp = new Spp; $spp_null = true;}
        if( $pembangunan == null) {$pembangunan = new Pembangunan; $pembangunan_null = true;}
        if( $pramuka == null) {$pramuka = new Pramuka; $pramuka_null = true;}
        if( $lain2 == null) {$lain2 = new LainLain; $lain2_null = true;}

        //menghitung tunggakan tahunm -i dengan ini
        if( $tspp == null) {$tspp = new Spp; $tunggakan_spp = 0;}
        else{$tunggakan_spp = ($master_spp->spp * 12) - $tspp->total;}

        if( $tpembangunan == null) {$tpembangunan = new Pembangunan; $tunggakan_pembangunan = 0;}
        else{$tunggakan_pembangunan = $master_spp->pembangunan - $tpembangunan->total;}

        if( $tpramuka == null) {$tpramuka = new Pramuka; $tunggakan_pramuka = 0;}
        else{$tunggakan_pramuka = ($master_spp->pramuka * 12) - $tpramuka->total;}

        if( $tlain2 == null) {$tlain2 = new LainLain; $tunggakan_lain2 = 0;}
        else{$tunggakan_lain2 = ($master_spp->lain_lain * 12) - $tlain2->total;}

        //menghitung tunggakan tahun aktif
        $tunggakan_spp_aktif = ($master_spp_aktif->spp * 12) - $spp->total;

        $tunggakan_pembangunan_aktif = $master_spp_aktif->pembangunan - $pembangunan->total;

        $tunggakan_pramuka_aktif = ($master_spp_aktif->pramuka * 12) - $pramuka->total;

        $tunggakan_lain2_aktif = ($master_spp_aktif->lain_lain * 12) - $lain2->total;
        
        $bln_kosong_spp = $this->hitung_kosong($spp);
        $bln_kosong_pramuka = $this->hitung_kosong($pramuka);
        $bln_kosong_lain2 = $this->hitung_kosong($lain2);

        $bln_kosong = array('spp' => $bln_kosong_spp, 'pramuka' => $bln_kosong_pramuka, 'lain2' => $bln_kosong_lain2);

        $spp_dibayar =0;
        $pembangunan_dibayar =0;
        $pramuka_dibayar =0;
        $lain2_dibayar =0;
        
        $tunggakan_bln_spp = $this->get_tunggakan($master_spp->spp, $spp, $master_spp->batas_bln_spp);
        $tunggakan_bln_pembangunan = $master_spp->pembangunan - $this->get_biaya_dibayar($pembangunan, $master_spp->batas_bln_pembangunan);
        $tunggakan_bln_pramuka = $this->get_tunggakan($master_spp->pramuka, $pramuka, $master_spp->batas_bln_pramuka);
        $tunggakan_bln_lain2 = $this->get_tunggakan($master_spp->lain_lain, $lain2, $master_spp->batas_bln_lain2);

        //Check apabila nilai tunggakan sampai bulan bernilai minus (-)
        if($tunggakan_bln_spp < 0) $tunggakan_bln_spp = 0;
        if($tunggakan_bln_pembangunan < 0) $tunggakan_bln_pembangunan = 0;
        if($tunggakan_bln_pramuka < 0) $tunggakan_bln_pramuka = 0;
        if($tunggakan_bln_lain2 < 0) $tunggakan_bln_lain2 = 0;

        //Persiapan data untuk dikirim ke view
        $tahun_aktif = $tp->tp;
        $tunggakan_thn = array(
            "spp" => tunggakan_tahun_aktif_minsatu_spp($student->nisn), 
            "pembangunan" => tunggakan_tahun_aktif_minsatu_pembangunan($student->nisn), 
            "pramuka" => tunggakan_tahun_aktif_minsatu_pramuka($student->nisn), 
            "lain2" => tunggakan_tahun_aktif_minsatu_lain2($student->nisn));
        $tunggakan_thn_aktif = array("spp" => $tunggakan_spp_aktif, "pembangunan" => $tunggakan_pembangunan_aktif, "pramuka" => $tunggakan_pramuka_aktif, "lain2" => $tunggakan_lain2_aktif);
        $bln_tunggakan = $master_spp->batas_bln;
        $thn_tunggakan = $tp->tp;
        $tunggakan_bln = array(
            "spp" => tunggakan_tahun_aktif_spp($student->nisn), 
            "pembangunan" => tunggakan_tahun_aktif_pembangunan($student->nisn), 
            "pramuka" => tunggakan_tahun_aktif_pramuka($student->nisn), 
            "lain2" => tunggakan_tahun_aktif_lain2($student->nisn));
// dd($tunggakan_bln);
        //ini untuk data detail pembayarn
        $detail_bayar = array('spp' => $spp, 'pembangunan' => $pembangunan, 'pramuka' => $pramuka, 'lain2' => $lain2);
        return view('komite.pages.invoice.penyesuaian', [
            'student'       => $student, 
            "tunggakan_thn" => $tunggakan_thn, 
            "tp"            => $tp, 
            "master_spp"    => $master_spp_aktif, 
            "tunggakan_bln" => $tunggakan_bln, 
            "bln_tunggakan" => $bln_tunggakan, 
            "tunggakan_thn_aktif" => $tunggakan_thn_aktif,
            "thn_tunggakan" => $thn_tunggakan, 
            "bln_kosong"    => $bln_kosong, 
            'bln'           => $bln, 
            'tunggakan'     => $tunggakan, 
            'detail_bayar'  => $detail_bayar, 
            'rombelM'       => true]);
    }

    public function penyesuaian_post($id, Request $request )
    {

        //Definisi Bulan dalam Bahasa Indonesia
        $bln = [4 => 'januari', 5 => 'februari', 6 => 'maret', 7 => 'april', 8 => 'mei', 9 => 'juni', 10 => 'juli', 11 => 'agustus', 12 => 'september', 13 =>'oktober', 14 => 'november', 15 => 'desember'];

        //Mengambil data siswa berdasarkan input nisn
        $nisn = $request->input('nisn');
        $student = get_student($nisn);
        //Mengambil nama kelas berdasarkan id kelas yg ada di db siswa
        $rombel = Rombel::find($student->kelas);
        //get Tahun Pelaran aktif, where status = aktif
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
        //Mengambil master biaya sekolah sebagai dasar perhitungan pembayaran
        $master_spp = MasterSPP::where('tp', $rombel->kurikulum)->first();
        //Mengambil reff kuitansi yang terakhir berdasarkan tp dan semster aktif
        $reff = Reff::where('tp', $tp->tp)->where('semester', $tp->semester)->first();


        //Get data pembayaran masing2 untuk spp, pembanguna, pramuka, dan biaya lain2...
        $spp = Spp::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pembangunan = Pembangunan::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pramuka = Pramuka::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $lain2 = LainLain::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
             
        
        //Inisialisasi data pembayaran tipa bulannya jika belum ada record di database
        $exist_pembangunan = Pembangunan::where('tp', $tp->tp)->where('nisn', $student->nisn)->count();
        if ($exist_pembangunan == 0){ 
            $pembangunan = new Pembangunan;
            $pembangunan->nisn = $request->input("nisn");
            $pembangunan->kelas = $request->input("kelas");
            $pembangunan->tp = $request->input("tp");

            $pembangunan_null = true;
        }
        $exist_spp = Spp::where('tp', $tp->tp)->where('nisn', $student->nisn)->count();
        if($exist_spp == 0){
            $spp = new Spp;
            $spp->nisn = $request->input("nisn");
            $spp->kelas = $request->input("kelas");
            $spp->tp = $request->input("tp");

            $spp_null = true;
        }
        $exist_pramuka = Pramuka::where('tp', $tp->tp)->where('nisn', $student->nisn)->count();
        if($exist_pramuka == 0){
            $pramuka = new Pramuka;
            $pramuka->nisn = $request->input("nisn");
            $pramuka->kelas = $request->input("kelas");
            $pramuka->tp = $request->input("tp");

            $pramuka_null = true;
        }
        $exist_lain = LainLain::where('tp', $tp->tp)->where('nisn', $student->nisn)->count();
        if($exist_lain == 0){
            $lain2 = new LainLain;
            $lain2->nisn = $request->input("nisn");
            $lain2->kelas = $request->input("kelas");
            $lain2->tp = $request->input("tp");
            $lain2_null = true;
        }

        //bayar beasiswa
        $bayar_beasiswa_spp = $this->bayar_beasiswa($master_spp->spp, $spp);
        $bayar_beasiswa_pembangunan = $this->bayar_beasiswa($master_spp->pembangunan, $pembangunan);
        $bayar_beasiswa_pramuka = $this->bayar_beasiswa($master_spp->pramuka, $pramuka);
        $bayar_beasiswa_lain2 = $this->bayar_beasiswa($master_spp->lain_lain, $lain2);

        $total_beasiswa_spp  = $bayar_beasiswa_spp['total_beasiswa'];
        $total_beasiswa_pembangunan = $master_spp->pembangunan - $this->get_biaya_dibayar($pembangunan, $master_spp->batas_bln_pembangunan);
        $total_beasiswa_pramuka  = $bayar_beasiswa_pramuka['total_beasiswa'];
        $total_beasiswa_lain2  = $bayar_beasiswa_lain2['total_beasiswa'];
   
        $bln_dibayar_spp = $bayar_beasiswa_spp['bln_dibayar'];
        $bln_dibayar_pramuka = $bayar_beasiswa_pramuka['bln_dibayar'];
        $bln_dibayar_lain2 = $bayar_beasiswa_lain2['bln_dibayar'];

        // Check reff,  jika tidak ada maka akan dibuat record baru
        if($reff == null){
            $reff = new Reff;
            $reff->nama_reff = 1;
            $reff->tp = $tp->tp;
            $reff->semester = $tp->semester;
        }else{
            //Jika ada maka reff selanjutnya ditambahkan 1
            $reff->nama_reff = $reff->nama_reff + 1;
        }
        //Menentukan no kuitansi baru
        $no_kuitansi = $tp->tp . $tp->semester . $reff->nama_reff;

        //mencatat transaksi ke history
        $histori = new HistoriSpp;
        $histori->nisn = $request->input("nisn");
        $histori->nama = $student->nama_lengkap;
        $histori->tp = $request->input("tp");
        $kelas = Rombel::where('id',$request->input("kelas"))->first();
        $histori->kelas = $kelas->nama;
        $histori->oleh = Auth::komite()->get()->komite_name;
        $histori->oleh_id = Auth::komite()->get()->komite_id;
        
        $histori->tgl_bayar = date('d');
        $histori->bln_bayar = date('n');
        $histori->thn_bayar = date('Y');
        $histori->bln_dibayar_spp = json_encode($bln_dibayar_spp);
        // $histori->bln_dibayar_pembangunan = json_encode($bln_dibayar_pembangunan);
        $histori->bln_dibayar_pramuka = json_encode($bln_dibayar_pramuka);
        $histori->bln_dibayar_lain2 = json_encode($bln_dibayar_lain2);
        $histori->note = 'Beasiswa';
        $histori->no_kwitansi = $no_kuitansi;
        $histori->semester = $tp->semester;

        $histori->spp = $total_beasiswa_spp;
        $histori->pembangunan = $total_beasiswa_pembangunan;
        $histori->pramuka = $total_beasiswa_pramuka;
        $histori->lain_lain = $total_beasiswa_lain2;
        $histori->nominal = $total_beasiswa_spp + $total_beasiswa_pembangunan + $total_beasiswa_pramuka + $total_beasiswa_lain2;

        //Menghitung Total biaya yang telah dibayar
        $spp_total = 0 ;
        for($i=4; $i <= 15; $i++){
            $spp_total += $spp->$bln[$i];
        }

        $spp->total = ($master_spp->spp * 12);
        $pembangunan->total = $master_spp->pembangunan;
        $pramuka->total = ($master_spp->pramuka * 12);
        $lain2->total = ($master_spp->lain_lain * 12);
        // die('Berhasil');
        if($spp->save() && $pembangunan->save() && $pramuka->save() && $lain2->save()){
            $histori->save();
            $reff->save();
            return redirect('komite/laporan/penyesuaian/'.$id)->with('success', 'Pembayaran beasiswa berhasil disimpan.')->with('no_kuitansi', $histori->no_kwitansi);    
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function rombel(Request $request)
    {
        $rombels = Rombel::all();

        return view('komite.pages.laporan.list.rombel', ['rombels' => $rombels]);      
    }

    public function tunggakan(){
        $tunggakan = MasterSpp::orderBy('tp')->get();
        $tungg = Tunggakan::first();
        $tp = Tp::all();
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];

        return view('komite.pages.laporan.list.tunggakan', [
            'tunggakan' => $tunggakan,
            'tungg' => $tungg, 
            'bln' => $bln, 
            'tp' => $tp]);   
    }

    public function tunggakan_save(Request $request){
        $tunggakan = Tunggakan::find(1);//Masih hardcode

        $tunggakan->spp = $request->input('tungg_spp');
        $tunggakan->pembangunan = $request->input('tungg_pembangunan');
        $tunggakan->pramuka = $request->input('tungg_pramuka');
        $tunggakan->lain2 = $request->input('tungg_lain2');

        $tunggakan->thn_spp = $request->input('thn_spp');
        $tunggakan->thn_pembangunan = $request->input('thn_pembangunan');
        $tunggakan->thn_pramuka = $request->input('thn_pramuka');
        $tunggakan->thn_lain2 = $request->input('thn_lain2');

        $tunggakan->save();

        return redirect('komite/laporan/tunggakan')->with('success', 'Berhasil mengupdate tunggakan!');
    }

    public function tunggakan_edit($id){
        $tunggakan = MasterSpp::find($id);
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];

        if ($tunggakan) {
            return view('komite.pages.laporan.list.tunggakan-edit', ['tunggakan' => $tunggakan, 'bln' => $bln]);
        }

        return abort(403);
    }

    public function tunggakan_edit_save(Request $request, $id){
        $tunggakan = MasterSpp::find($id);

        $tunggakan->batas_bln_spp = $request->input('tungg_spp');
        $tunggakan->batas_bln_pembangunan = $request->input('tungg_pembangunan');
        $tunggakan->batas_bln_pramuka = $request->input('tungg_pramuka');
        $tunggakan->batas_bln_lain2 = $request->input('tungg_lain2');

        $tunggakan->save();

        return redirect('komite/laporan/tunggakan/edit/'. $id)->with('success', 'Berhasil mengupdate batas bulan tunggakan!');
    }

    public function suratTunggakan()
    { 
        $jumlahSiswa = Student::count();
        $jurusan = Jurusan::all();
        $students = Student::orderBy('nama_lengkap')->paginate(30);

        return view('komite.pages.laporan.filterSuratTunggakan', [
            'students' => $students, 
            'recap' => 'true', 
            'jumlahSiswa' => $jumlahSiswa,
            'jurusan'    => $jurusan]);
    }

    public function hitungTotalTunggakan($nisn, $tpAktif, $tahunMasukSiswa, $batasTunggakan){
        $tunggakanAktif = tunggakan_tahun_aktif($nisn, $tpAktif, $tahunMasukSiswa, $batasTunggakan, $batasTunggakan, $batasTunggakan, $batasTunggakan);
            $tunggakanMinsatu = hitungAkumTunggTotal($tpAktif, $tahunMasukSiswa, $nisn);

        $totalSpp = $tunggakanAktif['spp'] + $tunggakanMinsatu['spp'];
        $totalPembangunan = $tunggakanAktif['pembangunan'] + $tunggakanMinsatu['pembangunan'];
        $totalPramuka = $tunggakanAktif['pramuka'] + $tunggakanMinsatu['pramuka'];
        $totalLain2 = $tunggakanAktif['lain2'] + $tunggakanMinsatu['lain2'];
        $total = $tunggakanAktif['total'] + $tunggakanMinsatu['total'];
        $totalTunggakan = array(
            'spp' => $totalSpp,
            'pembangunan' => $totalPembangunan,
            'pramuka' => $totalPramuka,
            'lain2' => $totalLain2,
            'total' => $total
            );

        return $totalTunggakan;
    }

    public function cetakSuratTunggakan(Request $request)
    {
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];

        $rules = [
            'jurusan'  => 'required',
            'latarBelakangSurat'  => 'required',
            'tahunPelajaran'  => 'required',
            'batasPelunasan'  => 'required',
            'tanggalCetak'  => 'required',
            'suratOleh'  => 'required',
            'kelas'   => 'required'];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }
        
        $students = Student::where('kelas', $_POST['kelas'])->select('nama_lengkap', 'nisn', 'tp')->orderBy('nama_lengkap')->get();
        $tpAktif = get_tp_aktif();
        $rombel = $rombel = Rombel::find($_POST['kelas']);
        $bulanTunggakan = $bln[$_POST['batasTunggakan']] . ' ' . date('Y') ; 

        //--------------------------------------------------------------------------------------
        

        if (count($students) == 0) {
            return redirect()->back()->withInput()->withErrors('Tidak ada siswa di kelas ' . $rombel['nama']);
        }
        //--------------------------------------------------------------------------------------

        $data = array(
            'nomorSurat' => $_POST['nomorSurat'],
            'sifatSurat' => $_POST['sifatSurat'],
            'lampiranSurat' => $_POST['lampiranSurat'],
            'perihalSurat' => $_POST['perihalSurat'],
            'latarBelakangSurat' => $_POST['latarBelakangSurat'],
            'tahunPelajaranSurat' => $_POST['tahunPelajaran'],
            'batasBayar' => $_POST['batasPelunasan'],
            'tanggalSurat' => $_POST['tanggalCetak'],
            'suratOleh' => $_POST['suratOleh']
            );

        foreach ($students as $key => $value) {
            $nisn = $value->nisn;
            $tahunMasukSiswa = $value->tp;
            $tunggakan = $this->hitungTotalTunggakan($nisn, $tpAktif->tp, $tahunMasukSiswa, $_POST['batasTunggakan']);

            $data['students'][] = array(
                'nisn' => $value->nisn,
                'nama_lengkap' => $value->nama_lengkap,
                'bulanTunggakan' => $bulanTunggakan,
                'tunggakan' => array(
                    'spp' => $tunggakan['spp'],
                    'pembangunan' => $tunggakan['pembangunan'],
                    'pramuka' => $tunggakan['pramuka'],
                    'lain2' => $tunggakan['lain2'],
                    'total' => $tunggakan['total']
                    )
                );
        }
        // dd($data);

        $pdf = PDF::loadView('komite.pages.laporan.surattunggakan', ['data' =>$data]);
        return $pdf->download('ST-' . $rombel->nama . '.pdf');
    }

    public function invoice_edit($noKuitansi){
        $kuitansi = HistoriSpp::where('no_kwitansi',$noKuitansi)->first();

        if(!$kuitansi){
            return redirect()->back()->withInput()->withErrors("Kuitansi Tidak Ditemukan");
        }

        $kuitansi = $kuitansi->toArray();

        $student = Student::find($kuitansi['nisn']);
        $spp = Spp::where('nisn', $kuitansi['nisn'])->where('tp', $kuitansi['tp'])->first()->toArray();
        $pembangunan = Pembangunan::where('nisn', $kuitansi['nisn'])->where('tp', $kuitansi['tp'])->first()->toArray();
        $pramuka = Pramuka::where('nisn', $kuitansi['nisn'])->where('tp', $kuitansi['tp'])->first()->toArray();
        $lain2 = LainLain::where('nisn', $kuitansi['nisn'])->where('tp', $kuitansi['tp'])->first()->toArray();

        $data = array(
            'kuitansi'   => $kuitansi,
            'spp'   => $spp,
            'pembangunan'   => $pembangunan,
            'pramuka'   => $pramuka,
            'lain2'   => $lain2
            );
        return view('komite.pages.invoice.editKuitansi', ['data'    => $data]);        
    }

    public function invoice_edit_post(Request $request){
        $rules = [
            'k-bln_dibayar_spp'  => 'required',
            'k-bln_dibayar_lain2'  => 'required',
            'k-bln_dibayar_pembangunan'  => 'required',
            'k-bln_dibayar_pramuka'  => 'required',

            'k-spp'  => 'integer',
            'k-pembangunan'  => 'integer',
            'k-pramuka'  => 'integer',
            'k-lain_lain'  => 'integer',
            'k-nominal'  => 'integer',

            's-januari'  => 'integer',
            's-februari'  => 'integer',
            's-maret'  => 'integer',
            's-april'  => 'integer',
            's-mei'  => 'integer',
            's-juni'  => 'integer',
            's-juli'  => 'integer',
            's-agustus'  => 'integer',
            's-september'  => 'integer',
            's-oktober'  => 'integer',
            's-november'  => 'integer',
            's-desember'  => 'integer',
            's-total'  => 'integer',

            'pe-januari'  => 'integer',
            'pe-februari'  => 'integer',
            'pe-maret'  => 'integer',
            'pe-april'  => 'integer',
            'pe-mei'  => 'integer',
            'pe-juni'  => 'integer',
            'pe-juli'  => 'integer',
            'pe-agustus'  => 'integer',
            'pe-september'  => 'integer',
            'pe-oktober'  => 'integer',
            'pe-november'  => 'integer',
            'pe-desember'  => 'integer',
            'pe-total'  => 'integer',

            'pr-januari'  => 'integer',
            'pr-februari'  => 'integer',
            'pr-maret'  => 'integer',
            'pr-april'  => 'integer',
            'pr-mei'  => 'integer',
            'pr-juni'  => 'integer',
            'pr-juli'  => 'integer',
            'pr-agustus'  => 'integer',
            'pr-september'  => 'integer',
            'pr-oktober'  => 'integer',
            'pr-november'  => 'integer',
            'pr-desember'  => 'integer',
            'pr-total'  => 'integer',

            'l-januari'  => 'integer',
            'l-februari'  => 'integer',
            'l-maret'  => 'integer',
            'l-april'  => 'integer',
            'l-mei'  => 'integer',
            'l-juni'  => 'integer',
            'l-juli'  => 'integer',
            'l-agustus'  => 'integer',
            'l-september'  => 'integer',
            'l-oktober'  => 'integer',
            'l-november'  => 'integer',
            'l-desember'  => 'integer',
            'l-total'  => 'integer',
        ];

        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        // Cek hapus atau hanya edit
        $kuitansiLama = HistoriSpp::where('no_kwitansi',$_POST['k-no_kwitansi'])->first();
        $tipe_edit = 'Hanya Edit';
        if(isset($_POST['hapus'])){
            $tipe_edit = 'Edit Dan Atau Hapus';
        }

        $edit_kuitansi = new EditKuitansi;
        $edit_kuitansi->no_kuitansi = $kuitansiLama->no_kwitansi;
        $edit_kuitansi->tipe_edit = $tipe_edit;
        $edit_kuitansi->deskripsi_edit = $tipe_edit;
        // -------------------------------------------------------------------------------

        //mencatat transaksi ke history
        $historiEdit = new HistoriSppEdit;
        
        $historiEdit->nisn = $kuitansiLama->nisn;
        
        $historiEdit->nama = $kuitansiLama->nama;
        
        $historiEdit->tp = $kuitansiLama->tp;
        
        $historiEdit->kelas = $kuitansiLama->kelas;
        
        $historiEdit->oleh = $kuitansiLama->oleh;
        $historiEdit->oleh_id = $kuitansiLama->oleh_id;
        
        $historiEdit->tgl_bayar = $kuitansiLama->tgl_bayar;
        $historiEdit->bln_bayar = $kuitansiLama->bln_bayar;
        $historiEdit->thn_bayar = $kuitansiLama->thn_bayar;

        $historiEdit->bln_dibayar_spp = $kuitansiLama->bln_dibayar_spp;
        $historiEdit->bln_dibayar_pembangunan = $kuitansiLama->bln_dibayar_pembangunan;
        $historiEdit->bln_dibayar_pramuka = $kuitansiLama->bln_dibayar_pramuka;
        $historiEdit->bln_dibayar_lain2 = $kuitansiLama->bln_dibayar_lain2;
        
        $historiEdit->note = $kuitansiLama->note;
        $historiEdit->note2 = $kuitansiLama->note2;
        $historiEdit->no_kwitansi = $kuitansiLama->no_kwitansi;
        
        $historiEdit->semester = $kuitansiLama->semester;

        $historiEdit->spp = $kuitansiLama->spp;
        $historiEdit->pembangunan = $kuitansiLama->pembangunan;
        $historiEdit->pramuka = $kuitansiLama->pramuka;
        $historiEdit->lain_lain = $kuitansiLama->lain_lain;
        $historiEdit->nominal = $kuitansiLama->nominal;
        // -------------------------------------------------------------------------------


        $kuitansi = HistoriSpp::find($request->input('k-id'));
        $spp = Spp::find($request->input('s-id'));
        $pembangunan = Pembangunan::find($request->input('pe-id'));
        $pramuka = Pramuka::find($request->input('pr-id'));
        $lain2 = LainLain::find($request->input('l-id'));
        
        $kuitansi->bln_dibayar_spp = str_replace(' ', '', $request->input('k-bln_dibayar_spp'));
        $kuitansi->bln_dibayar_lain2 = str_replace(' ', '', $request->input('k-bln_dibayar_lain2'));
        $kuitansi->bln_dibayar_pembangunan = str_replace(' ', '', $request->input('k-bln_dibayar_pembangunan'));
        $kuitansi->bln_dibayar_pramuka = str_replace(' ', '', $request->input('k-bln_dibayar_pramuka'));

        $kuitansi->spp = $request->input('k-spp');
        $kuitansi->pembangunan = $request->input('k-pembangunan');
        $kuitansi->pramuka = $request->input('k-pramuka');
        $kuitansi->lain_lain = $request->input('k-lain_lain');
        $kuitansi->nominal = $request->input('k-nominal');

        $total_kuitansi = 
            intval($request->input('k-spp')) +
            intval($request->input('k-pembangunan')) +
            intval($request->input('k-pramuka')) +
            intval($request->input('k-lain_lain'));

        $total_spp = 
            intval($request->input('s-januari'))+
            intval($request->input('s-februari'))+
            intval($request->input('s-maret'))+
            intval($request->input('s-april'))+
            intval($request->input('s-mei'))+
            intval($request->input('s-juni'))+
            intval($request->input('s-juli'))+
            intval($request->input('s-agustus'))+
            intval($request->input('s-september'))+
            intval($request->input('s-oktober'))+
            intval($request->input('s-november'))+
            intval($request->input('s-desember'));

        $total_pembangunan = 
            intval($request->input('pe-januari'))+
            intval($request->input('pe-februari'))+
            intval($request->input('pe-maret'))+
            intval($request->input('pe-april'))+
            intval($request->input('pe-mei'))+
            intval($request->input('pe-juni'))+
            intval($request->input('pe-juli'))+
            intval($request->input('pe-agustus'))+
            intval($request->input('pe-september'))+
            intval($request->input('pe-oktober'))+
            intval($request->input('pe-november'))+
            intval($request->input('pe-desember'));

        $total_pramuka = 
            intval($request->input('pr-januari'))+
            intval($request->input('pr-februari'))+
            intval($request->input('pr-maret'))+
            intval($request->input('pr-april'))+
            intval($request->input('pr-mei'))+
            intval($request->input('pr-juni'))+
            intval($request->input('pr-juli'))+
            intval($request->input('pr-agustus'))+
            intval($request->input('pr-september'))+
            intval($request->input('pr-oktober'))+
            intval($request->input('pr-november'))+
            intval($request->input('pr-desember'));

        $total_lain2 = 
            intval($request->input('l-januari'))+
            intval($request->input('l-februari'))+
            intval($request->input('l-maret'))+
            intval($request->input('l-april'))+
            intval($request->input('l-mei'))+
            intval($request->input('l-juni'))+
            intval($request->input('l-juli'))+
            intval($request->input('l-agustus'))+
            intval($request->input('l-september'))+
            intval($request->input('l-oktober'))+
            intval($request->input('l-november'))+
            intval($request->input('l-desember'));
    
        if($total_kuitansi !== intval($request->input('k-nominal')) && $total_kuitansi !== 0) 
            return redirect()->back()->withInput()->withErrors("Jumlah Total Kuitansi Tidak Sesuai.");
        if($total_spp !== intval($request->input('s-total')) && $total_spp !== 0) 
            return redirect()->back()->withInput()->withErrors("Jumlah Total SPP Tidak Sesuai.");
        if($total_pembangunan !== intval($request->input('pe-total')) && $total_pembangunan !== 0) 
            return redirect()->back()->withInput()->withErrors("Jumlah Total Pembangunan Tidak Sesuai.");
        if($total_pramuka !== intval($request->input('pr-total')) && $total_pramuka !== 0) 
            return redirect()->back()->withInput()->withErrors("Jumlah Total Pramuka Tidak Sesuai.");
        if($total_lain2 !== intval($request->input('l-total')) && $total_lain2 !== 0) 
            return redirect()->back()->withInput()->withErrors("Jumlah Total Lain-lain Tidak Sesuai.");
        
        $spp->januari = $request->input('s-januari');
        $spp->februari = $request->input('s-februari');
        $spp->maret = $request->input('s-maret');
        $spp->april = $request->input('s-april');
        $spp->mei = $request->input('s-mei');
        $spp->juni = $request->input('s-juni');
        $spp->juli = $request->input('s-juli');
        $spp->september = $request->input('s-september');
        $spp->oktober = $request->input('s-oktober');
        $spp->november = $request->input('s-november');
        $spp->desember = $request->input('s-desember');
        $spp->total = $request->input('s-total');

        $pembangunan->januari = $request->input('pe-januari');
        $pembangunan->februari = $request->input('pe-februari');
        $pembangunan->maret = $request->input('pe-maret');
        $pembangunan->april = $request->input('pe-april');
        $pembangunan->mei = $request->input('pe-mei');
        $pembangunan->juni = $request->input('pe-juni');
        $pembangunan->juli = $request->input('pe-juli');
        $pembangunan->september = $request->input('pe-september');
        $pembangunan->oktober = $request->input('pe-oktober');
        $pembangunan->november = $request->input('pe-november');
        $pembangunan->desember = $request->input('pe-desember');
        $pembangunan->total = $request->input('pe-total');

        $pramuka->januari = $request->input('pr-januari');
        $pramuka->februari = $request->input('pr-februari');
        $pramuka->maret = $request->input('pr-maret');
        $pramuka->april = $request->input('pr-april');
        $pramuka->mei = $request->input('pr-mei');
        $pramuka->juni = $request->input('pr-juni');
        $pramuka->juli = $request->input('pr-juli');
        $pramuka->september = $request->input('pr-september');
        $pramuka->oktober = $request->input('pr-oktober');
        $pramuka->november = $request->input('pr-november');
        $pramuka->desember = $request->input('pr-desember');
        $pramuka->total = $request->input('pr-total');

        $lain2->januari = $request->input('l-januari');
        $lain2->februari = $request->input('l-februari');
        $lain2->maret = $request->input('l-maret');
        $lain2->april = $request->input('l-april');
        $lain2->mei = $request->input('l-mei');
        $lain2->juni = $request->input('l-juni');
        $lain2->juli = $request->input('l-juli');
        $lain2->september = $request->input('l-september');
        $lain2->oktober = $request->input('l-oktober');
        $lain2->november = $request->input('l-november');
        $lain2->desember = $request->input('l-desember');
        $lain2->total = $request->input('l-total');

        if(!$historiEdit->save()){
            return redirect()->back()->withInput()->withErrors("Gagal Step 1 Edit/Hapus Kuitansi - [SaveKuitansiLama()-Failed]");
        }
        if(!$edit_kuitansi->save()){
            return redirect()->back()->withInput()->withErrors("Gagal Step 2 Edit/Hapus Kuitansi - [SaveDataEditKuitansi()-Failed]");
        }

        if($kuitansi->save()){
            if(!$spp->save()){
                return redirect()->back()->withInput()->withErrors("Terjadi gangguan saat menyimpan data Spp");  
            }
            if(!$pembangunan->save()){
                return redirect()->back()->withInput()->withErrors("Terjadi gangguan saat menyimpan data Pembangunan");  
            }
            if(!$pramuka->save()){
                return redirect()->back()->withInput()->withErrors("Terjadi gangguan saat menyimpan data Pramuka");  
            }
            if(!$lain2->save()){
                return redirect()->back()->withInput()->withErrors("Terjadi gangguan saat menyimpan data Lain-lain");  
            }

            if(isset($_POST['hapus'])){
                if($kuitansi->delete()){
                    return redirect('komite/laporan/invoice/')->with('success', 'Kuitansi berhasil dihapus');                     
                }
            }

            return redirect('komite/laporan/invoice/edit/' . $request->input('k-no_kwitansi'))->with('success', 'Kuitansi berhasil dirubah'); 
        }else{
            return redirect()->back()->withInput()->withErrors("Terjadi gangguan saat menyimpan data kuitansi");  
        }
    }
}
