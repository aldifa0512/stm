<?php

namespace App\Http\Controllers\Komite;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\ExcelLibrary;
use App\Models\Ad;
use App\Models\Student;
use App\Models\DropOut;
use App\Models\Rombel;
use Validator;
use Auth;
use Storage;
use Image;
use App\Models\Package;
use Setting;
use Session;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Models\Jurusan;

class StudentsController extends Controller
{
//-----------------------------------------------------------------------------------------

    public function index()
    {
        $jumlahSiswa = Student::where('status', 'aktif')->count();
        $students = Student::where('status', 'aktif')->orderBy('nama_lengkap')->paginate(30);

        return view('komite.pages.student.list.list', [
            'students'      => $students, 
            'jumlahSiswa'   => $jumlahSiswa,
            'page_name'     => 'Siswa Aktif']);
    }
    
    public function indexPasif()
    {
        $jumlahSiswa = Student::where('status', 'pasif')->count();
        $students = Student::where('status', 'pasif')->orderBy('nisn', 'ASC')->paginate(30);

        return view('komite.pages.student.list.list', ['students' => $students, 'pasif' => true, 'jumlahSiswa' => $jumlahSiswa]);
    }
    public function indexRecap()
    {
        $jumlahSiswa = Student::count();
        $students = Student::orderBy('nama_lengkap')->paginate(30);

        return view('komite.pages.student.list.list', ['students' => $students, 'recap' => 'true', 'jumlahSiswa' => $jumlahSiswa]);
    }

    public function query($query, $method)
    {
        if ($method == "ASC") {
            $method = "DSC";
        }else{
            $method = "ASC";
        }

        $students = Student::orderBy($query, $method)->where('status', 'aktif')->paginate(30);

        return view('komite.pages.student.list.listorderby', ['students' => $students, 'method' => $method, 'jumlahSiswa' => $jumlahSiswa]);
    }

    public function queryRecap($query, $method)
    {
        if ($method == "ASC") {
            $method = "DSC";
        }else{
            $method = "ASC";
        }

        $students = Student::orderBy($query, $method)->paginate(30);

        return view('komite.pages.student.list.listorderby-recap', ['students' => $students, 'method' => $method, 'jumlahSiswa' => $jumlahSiswa]);
    }

    public function profile($nisn){
        $student = Student::find($nisn);
        $jumlahSiswa = '';

        if($student){
            return view('komite.pages.student.list.profile', ['student' => $student, 'jumlahSiswa' => $jumlahSiswa]);
        }
    }

//-----------------------------------------------------------------------------------------

    public function enroll()
    {
        return view('komite.pages.student.enroll');
    }

    public function store_enroll(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'nama_lengkap' => 'required|max:255',
            'nisn' => 'required|max:255',
            'jenis_pendaftaran' => 'required|max:255'
            //'image' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $student = new Student;
        $student->save();
        $student->nama_lengkap = $request->input('nama_lengkap');
        $student->nisn = $request->input('nisn');
        $student->jenis_kelamin = $request->input('jenis_kelamin');
        //$student->tu_id = Auth::Tu()->get()->tu_id;
        $student->nik = $request->input('nik');
        $student->tempat_lahir = $request->input('tempat_lahir');
        $student->status = 'aktif';
        $student->tanggal_lahir = $request->input('tanggal_lahir');
        $student->agama = $request->input('agama');
        $student->alamat_jln = $request->input('alamat_jln');
        $student->rt = $request->input('rt');
        $student->rw = $request->input('rw');
        $student->dusun = $request->input('dusun');
        $student->kelurahan = $request->input('kelurahan');
        $student->kecamatan = $request->input('kecamatan');
        $student->kip = $request->input('kip');
        $student->kip_nama = $request->input('kip_nama');
        $student->kks = $request->input('kks');
        $student->akta_lahir = $request->input('akta_lahir');
        $student->npsn_smp = $request->input('npsn_smp');

        $student->nama_ayah = $request->input('nama_ayah');
        $student->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $student->tl_ayah = $request->input('tl_ayah');
        $student->nama_ibu = $request->input('nama_ibu');
        $student->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $student->tl_ibu = $request->input('tl_ibu');
        $student->nama_wali = $request->input('nama_wali');
        $student->pekerjaan_wali = $request->input('pekerjaan_wali');
        $student->tl_wali = $request->input('tl_wali');
        
        $student->jenis_pendaftaran = $request->input('jenis_pendaftaran');
        $student->tgl_masuk = $request->input('tgl_masuk');
        $student->jurusan = $request->input('jurusan');
        $student->nis = $request->input('nis');
        $student->no_ujian_smp = $request->input('no_ujian_smp');
        $student->no_ijazah = $request->input('no_ijazah');
        $student->no_skhus = $request->input('no_skhus');

        
        if ($request->hasFile('pas_foto')) {
            //$dir = storage_path().'/app/cs/assets/';
            $dir = public_path().'/storage/app/cs/assets/';
            $file = $request->file('image');
            $file_name = preg_replace("/[^A-Z0-9._-]/i", "_", $file->getClientOriginalName());
            $thumb_admin = 'thumb-admin-'.$file_name;
            $thumb = 'thumb-'.$file_name;
            $relative_path = 'storage/app/cs/assets/'.$file_name;
            $relative_thumb_admin_path = 'storage/app/cs/assets/'.$thumb_admin;
            $relative_path = 'storage/app/cs/assets/'.$file_name;

            if (!Storage::disk('local')->exists('cs/assets')) {
                Storage::makeDirectory('cs/assets');
            }

            Image::make($request->file('image'))->save($dir . $file_name);
            Image::make($request->file('image'))->resize(150, 120)->save($dir . $thumb_admin);
            Image::make($request->file('image'))->resize(200, 200)->save($dir . $thumb);

            $student->assets = json_encode([$relative_path]);
        }

        if ($student->save()) {
            $student->student_id = '28' . date('Y') . date('m') . str_pad((string)$student->id, 5, 0, STR_PAD_LEFT);
            //create_billing($student->customer_id, $student->id, 'student', $student->package->price);
            return redirect('komite/students'/*/edit/'. $student->id*/)->withSuccess('success', 'Student create success.');
        }
    }

//-----------------------------------------------------------------------------------------

    public function edit($id)
    {
        $student = Student::find($id);
        
        if (!$student) {
            return abort(404);
        }

        return view('komite.pages.student.edit', ['student' => $student]);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'nama_lengkap' => 'required|max:255',
            'nisn' => 'required|max:255',
            'jenis_pendaftaran' => 'required|max:255'
            //'image' => 'required'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $student = Student::find($id);
        $student->nama_lengkap = $request->input('nama_lengkap');
        $student->nisn = $request->input('nisn');
        $student->jenis_kelamin = $request->input('jenis_kelamin');
        //$student->tu_id = Auth::Tu()->get()->tu_id;
        $student->nik = $request->input('nik');
        $student->tempat_lahir = $request->input('tempat_lahir');
        $student->tanggal_lahir = $request->input('tanggal_lahir');
        $student->agama = $request->input('agama');
        $student->alamat_jln = $request->input('alamat_jln');
        $student->rt = $request->input('rt');
        $student->rw = $request->input('rw');
        $student->dusun = $request->input('dusun');
        $student->kelurahan = $request->input('kelurahan');
        $student->kecamatan = $request->input('kecamatan');
        $student->kip = $request->input('kip');
        $student->kip_nama = $request->input('kip_nama');
        $student->kks = $request->input('kks');
        $student->akta_lahir = $request->input('akta_lahir');
        $student->npsn_smp = $request->input('npsn_smp');

        $student->nama_ayah = $request->input('nama_ayah');
        $student->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $student->tl_ayah = $request->input('tl_ayah');
        $student->nama_ibu = $request->input('nama_ibu');
        $student->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $student->tl_ibu = $request->input('tl_ibu');
        $student->nama_wali = $request->input('nama_wali');
        $student->pekerjaan_wali = $request->input('pekerjaan_wali');
        $student->tl_wali = $request->input('tl_wali');
        
        $student->jenis_pendaftaran = $request->input('jenis_pendaftaran');
        $student->tgl_masuk = $request->input('tgl_masuk');
        $student->jurusan = $request->input('jurusan');
        $student->nis = $request->input('nis');
        $student->no_ujian_smp = $request->input('no_ujian_smp');
        $student->no_ijazah = $request->input('no_ijazah');
        $student->no_skhus = $request->input('no_skhus'); 

        if ($request->hasFile('pas_foto')) {
            //$dir = storage_path().'/app/cs/assets/';
            $dir = public_path().'/storage/app/cs/assets/';
            $file = $request->file('pas_foto');
            $file_name = preg_replace("/[^A-Z0-9._-]/i", "_", $file->getClientOriginalName());
            $thumb_admin = 'thumb-admin-'.$file_name;
            $thumb = 'thumb-'.$file_name;
            $relative_path = 'storage/app/cs/assets/' . $student->id . '/' . $file_name;
            $relative_thumb_admin_path = 'storage/app/cs/assets/'.$thumb_admin;
            $relative_path = 'storage/app/cs/assets/'.$file_name;

            if (!Storage::disk('local')->exists('cs/assets/' . $student->id)) {
                Storage::makeDirectory('cs/assets/' . $student->id);
            }

            Image::make($request->file('pas_foto'))->save($dir . $file_name);
            Image::make($request->file('pas_foto'))->resize(150, 120)->save($dir . $thumb_admin);
            Image::make($request->file('pas_foto'))->resize(200, 200)->save($dir . $thumb);

            $student->pas_foto = json_encode([$relative_path]);
        }

        if ($student->save()) {
            //$history->save();
            //$history_old->save();
            return redirect('komite/students')->withSuccess('success', 'Ads create success.');
        }
    }

//-----------------------------------------------------------------------------------------

    public function drop_out()
    {
        return view('komite.pages.student.drop-out');
    }

    public function dropSiswa($id)
    {
        $student = Student::find($id);
        
        if (!$student) {
            return abort(404);
        }

        return view('komite.pages.student.drop-siswa', ['student' => $student]);
    }

    public function deleteSiswa(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'keluar_karena' => 'required|max:255',
            'tanggal_keluar' => 'required|max:255',
            'alasan' => 'required|max:255'
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $dropOut = new DropOut;
        $dropOut->nisn = $request->input('nisn');
        $dropOut->keluar_karena = $request->input('keluar_karena');
        $dropOut->tanggal_keluar = $request->input('tanggal_keluar');
        $dropOut->alasan = $request->input('alasan');

        $student = Student::find($id);
        $student->status = 'pasif'; 

        if ($dropOut->save()) {
            $student->save();
            return redirect('komite/students/')->withSuccess('success', 'Students list has been updated');
        }
    }


//-----------------------------------------------------------------------------------------

    public function get_filter()
    {
        return view('komite.pages.dashboard');
    }

    public function filter(Request $request)
    {
        // if ($request->input('thn_masuk') != "None" && $request->input('nisn') !="" && $request->input('nama') !="" && $request->input('jurusan') != "select" && $request->input('status') != "None" ) {
        //     //query with all requested
        //     $thn_masuk = $request->input('thn_masuk');
        //     $nisn = $request->input('nisn');
        //     $nama = $request->input('nama');
        //     $jurusan = $request->input('jurusan');
        //     $status = $request->input('status');
        //     $students = Student::where('tgl_masuk', 'LIKE', '%'.$thn_masuk.'%')->where('nama_lengkap', 'LIKE', '%'.$nama.'%')->where('nisn', $nisn)->where('jurusan', $jurusan)->where('status', $status)->paginate(10);
        // } elseif($request->input('status') == "None" && $request->input('nisn') =="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('thn_masuk') != "None" ) {
        //     //query with only thn_masuk
        //     $search = $request->input('thn_masuk');
        //     $students = Student::where('tgl_masuk', 'LIKE', '%'.$search.'%')->paginate(10);
        // } elseif($request->input('status') != "None" && $request->input('nisn') =="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('thn_masuk') == "None" ) {
        //     //query with only status
        //     $search = $request->input('status');
        //     $students = Student::where('status', $request->input('status'))->paginate(10);
        // } elseif ($request->input('thn_masuk') == "None" && $request->input('nisn') !="" && $request->input('nama') =="" && $request->input('jurusan') == "select" && $request->input('status') == "None" ) {
        //     //query with only nisn
        //     $students = Student::where('nisn', $request->input('nisn'))->paginate(10);
        // } elseif ($request->input('thn_masuk') == "None" && $request->input('nisn') =="" && $request->input('nama') !="" && $request->input('jurusan') == "select" && $request->input('status') == "None"  ) {
        //     //query with only nama
        //     $search = $request->input('nama');
        //     $students = Student::where('nama_lengkap', 'LIKE', '%'.$search.'%')->paginate(10);
        // } elseif (/*$request->input('thn_masuk') == "None" && $request->input('nisn') =="" && $request->input('nama') =="" &&*/ $request->input('jurusan') != "select"/* && $request->input('status') == "None"  */) {
        //     //query with only jurusan
        //     $students = Student::where('jurusan', $request->input('jurusan'))->paginate(10);
        // } else{
        //     $students = getSiswa(null, 'DESC', 10);
        //     return view('komite.pages.student.list.list', ['students' => $students]);
        // }

        //only nisn or name
        if ($request->input('nisn') == "" ) {
            $students = Student::where('status', 'aktif')->orderBy('nama_lengkap')->paginate(30);
        }else{
            //query with only nisn
            $students = Student::where('nisn', $request->input('nisn'))->orWhere('nama_lengkap', 'LIKE', '%'.$request->input('nisn').'%')->get();
        }

        return view('komite.pages.student.list.list', ['students' => $students, 'render' =>false]);
    }

    
    //-----------------------------------------------------------------------------------------

    public function rombel()
    {
        $jurusans = Jurusan::all();
        foreach ($jurusans as $key => $value) {
            $rombels = Rombel::where('jurusan_id', $value->id)->orderBy('tingkat')->get();
            foreach ($rombels as $key => $value2) {
                $jumlahSiswa = Student::where('kelas', $value2->id)->where('status', 'aktif')->count();
                $value2['jumlahSiswa'] = $jumlahSiswa;
                $value2['jurusan'] = $value->nama_jurusan;
                $value2['jenjang'] = $value->jenjang;
            }
            $value['rombels'] = $rombels;
        }
        
        return view('komite.pages.student.list.rombel', [
            'jurusans' => $jurusans
        ]);
    }

    public function rombelLihat($kelas)
    {
        $jumlahSiswa = Student::where('kelas', $kelas)->where('status', 'aktif')->count();
        $students = Student::where('kelas', $kelas)->where('status', 'aktif')->orderBy('nama_lengkap', "ASC")->paginate(100);

        return view('komite.pages.student.list.list', ['students' => $students, 'kelas' => $kelas, 'rombelM' =>"true", 'render' =>false, 'jumlahSiswa' =>$jumlahSiswa]);
    }
    //-----------------------------------------------------------------------------------------
    public function ExcelDataSiswa()
    {
        $filename = '';
        $title = '' ;
        $description = '' ;
        $students = Student::all();
        $tgl_awal = '4';
        $tgl_akhir = '5';
        $bulan = '9';
        $thn = '2016';
        Excel::create('Filename', function($excel) use ($students) {

            // Set the title
            $excel->setTitle('Our new awesome title');

            // Chain the setters
            $excel->setCreator('Maatwebsite')
                  ->setCompany('Maatwebsite');
            $excel->sheet('Sheetname', function($sheet) use ($students) {
                $sheet->setWidth(array(
                    'A'     =>  4,
                    'B'     =>  13,
                    'C'     =>  28,
                    'D'     =>  3,

                    'E'     =>  3,
                    'F'     =>  3,
                    'G'     =>  3,
                    'H'     =>  3,

                    'I'     =>  3,
                    'J'     =>  3,
                    'K'     =>  3,
                    'L'     =>  3,

                    'M'     =>  3,
                    'N'     =>  3,
                    'O'     =>  3,
                    'P'     =>  3,

                    'Q'     =>  3,
                    'R'     =>  3,
                    'S'     =>  3,
                    'T'     =>  3,

                    'U'     =>  3,
                    'V'     =>  3,
                    'W'     =>  3,
                    'X'     =>  3
                ));

                $sheet->mergeCells('A1:A4');
                $sheet->mergeCells('B1:B4');
                $sheet->mergeCells('C1:C4');
                $sheet->mergeCells('D1:D4');

                $sheet->mergeCells('E1:H1');
                $sheet->mergeCells('I1:L1');
                $sheet->mergeCells('M1:P1');
                $sheet->mergeCells('Q1:T1');
                $sheet->mergeCells('U1:X1');

                $sheet->mergeCells('E2:H2');
                $sheet->mergeCells('I2:L2');
                $sheet->mergeCells('M2:P2');
                $sheet->mergeCells('Q2:T2');
                $sheet->mergeCells('U2:X2');

                $sheet->mergeCells('E3:H3');
                $sheet->mergeCells('I3:L3');
                $sheet->mergeCells('M3:P3');
                $sheet->mergeCells('Q3:T3');
                $sheet->mergeCells('U3:X3');
                $sheet->row(1, array(
                     'No', 'NISN', 'Nama Siswa', 'L/P', 
                     'HARI SENIN', '', '', '', 
                     'HARI SELASA', '', '', '', 
                     'HARI RABU', '', '', '', 
                     'HARI KAMIS', '', '', '',
                     'HARI JUMAT', '', '', ''
                ))->setFontBold(true);
                $sheet->row(2, array(
                     '',  '', '', '',
                     'TGL',  '', '', '',
                     'TGL',  '', '', '',
                     'TGL',  '', '', '',
                     'TGL',  '', '', '',
                     'TGL',  '', '', ''
                ));
                $sheet->row(3, array(
                     '', '', '', '', 
                     'PELAJARAN KE', '', '', '',
                     'PELAJARAN KE', '', '', '',
                     'PELAJARAN KE', '', '', '',
                     'PELAJARAN KE', '', '', '',
                     'PELAJARAN KE', '', '', ''
                ));
                $sheet->row(4, array(
                    '', '', '', '', 
                    '1', '2', '3', '4',
                    '1', '2', '3', '4',
                    '1', '2', '3', '4',
                    '1', '2', '3', '4',
                    '1', '2', '3', '4'
                ));
                $row = 5;
                $no = 1;
                foreach ($students as $student) {
                    $sheet->row($row, array(
                        $no, $student->nisn, $student->nama_lengkap, $student->jenis_kelamin,
                    ));
                    $row++;
                    $no++;
                }
                $right_corner = "X".$row; 
                $sheet->setBorder('A1:'.$right_corner, 'thin');
            });
        })->export('xlsx');
    }
    public function surat_tunggakan($nisn){
        $student = Student::find($nisn);

        // First, strip out characters which aren't allowed
        $illegal_chars = array(':', '\\', '/', '?', '*', '[', ']');
        for ($i = 0; $i < count($illegal_chars); $i++)
            $name = str_replace($illegal_chars[$i], '', $student->nama_lengkap);
        // Now, if name is longer than 31 chars, truncate it
        if (strlen($name) > 31)
            $name = substr($name, 0, 31);
        
        $filename = 'SRT-TGKN ' . $name;
        Excel::create($filename, function($excel) use ($nisn, $filename, $student) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('SMKN 2 PAYAKUMBUH')
                  ->setCompany('SMKN 2 PAYAKUMBUH');

            $excel->sheet($filename, function($sheet) use ($nisn, $filename, $student) {
                //no dan nama header merging MERGING VERTICAL
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:B5');
                
                //MERGING HORIZONTAL ROW A1 FOR HEADER
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('C4:F4');
                $sheet->mergeCells('G4:J4');
                $sheet->mergeCells('K4:N4');

                $sheet->row(1, array('SURAT PEMBERITAHUAN TUNGGAKAN -- ' . $student->nama_lengkap,'','','',
                    '',',' ,'','','','','',''));
                $sheet->row(1, function($row) {
                    $row->setAlignment('center');
                });
                $sheet->cells('A1', function($cells) {
                    $cells->setFont(array(
                        'size'       => '14',
                        'bold'       =>  true
                    ));
                });

                $sheet->row(3, array('Nomor'));
                $sheet->row(4, array('SIfat'));
                $sheet->row(5, array('Lampiran'));
                $sheet->row(6, array('Perihal'));

                $sheet->row(8, array('Kepada: Yth Bapak/Ibu Wali Murid'));
                $sheet->row(9, array('', 'Di Tempat,'));

                $sheet->row(11, array('Sehubungan dengan akan berakhirnya pelajaran 2016-2017, maka kami selaku bendaha sekolah memberitahukan bahwa siswa yang namanya tersebut dibawah ini :'));


            });
        
        })->export('xlsx');

    }

    public function rombelAction(Request $request){
        // dd($_POST);
        $idRombel = $_POST['idRombel'];
        $idJurusan = $_POST['idJurusan'];

        if(!isset($_POST['check'])) 
            return redirect()->back()->withInput()->withErrors("Tidak ada siswa yang dipilih.");
        
        if($_POST['action'] == 'LULUS'){
            foreach ($_POST['check'] as $key => $value) {
                if($this->lulus($value) != TRUE) 
                    return redirect()->back()->withInput()->withErrors("Gagal merubah kelas siswa dengan NPM : " . $value);
            }
        }else{
            foreach ($_POST['check'] as $key => $value) {
                if($this->naikKelas($idRombel, $value) != TRUE)
                    return redirect()->back()->withInput()->withErrors("Gagal merubah kelas siswa dengan NPM : " . $value);
            }
        }

        return redirect('komite/students/rombel/lihat/'.$idRombel)->with('success', 'Berhasil merubah kelas siswa.');
    }

    public function naikKelas($idRombel, $nisn){
        $student = get_student($nisn);
        $idNewRombel = getNaikKelasId($idRombel);

        $student->kelas = $idNewRombel;
        
        if($student->save()){
            return TRUE;
        }

        return FALSE;    
    }

    public function lulus($nisn){
        $dropOut = new DropOut;
        $dropOut->nisn = $nisn;
        $dropOut->keluar_karena = 'LULUS';
        $dropOut->tanggal_keluar = date('Y-m-d');
        $dropOut->alasan = 'LULUS';

        $student = Student::find($nisn);
        $student->status = 'pasif'; 

        if ($dropOut->save()) {
            $student->save();
            return true;
        }
    }

    public function rombelTunggakan($rombelId)
    {   
        $result = ExcelLibrary::rombelTunggakan($rombelId);
        return $result;
    }
}
