<?php

namespace App\Http\Controllers\Komite;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\Customer;
use App\Models\Listing;
use App\Models\Student;
use App\Models\MasterSPP;
use App\Models\Reff;
use App\Models\Rombel;
use App\Models\Pembangunan;
use App\Models\LainLain;
use App\Models\Pramuka;
use App\Models\Spp;
use App\Models\Tunggakan;
use App\Models\HistoriSpp;
use App\Models\Tp;
use App\Libraries\ExcelLibrary;
use App\Models\ListingMeta;
use App\Models\ListingCategory;
use Excel;
use Storage;
use Image;
use PHPExcel; 
use PHPExcel_IOFactory; 
use Setting;
use Session;

class SPPController extends Controller
{
    public function __construct()
    {
        $this->middleware('authKomite');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('komite.pages.spp.list');
    }
    
    public function bayarSPP3($id)
    {
        $student = Student::find($id);
        $rombel = Rombel::find($student->kelas);
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        
        if( Spp::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $spps = Spp::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $spps = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if( Pembangunan::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $pembangunans = Pembangunan::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $pembangunans = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if( Pramuka::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $pramukas = Pramuka::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $pramukas = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        if( LainLain::where('tp', $student->tp)->where('nisn', $student->nisn)->first() !== null)
            $lains = LainLain::where('tp', $tp['tp'])->where('nisn', $student->nisn)->first()->toArray();
        else
            $lains = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        $spps = array_values($spps);
        $pembangunans = array_values($pembangunans);
        $pramukas = array_values($pramukas);//dd($pramukas);
        $lains = array_values($lains);


        $Mspp = get_master_biaya($student->nisn);dd($Mspp);//MasterSPP::where('tp', $rombel->kurikulum)->first();//dd($Mspp);
        if($Mspp == null){
            return "Pastikan <b>Tahun Kurikulum Rombel pada Admin</b> telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }
        if ($student) {
            if($student->tp == null){
                return "Pastikan <b>Tahun Masuk</b> siswa telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
            }
            return view('komite.pages.spp.bayar3', ['student' => $student, 'spps' => $spps, 'pembangunans' => $pembangunans, 'pramukas' => $pramukas, 'lains' => $lains, 'Mspp' => $Mspp, 'tp' => $tp,  'bln' => $bln, 'rombelM' => true]);
        }

        return abort(404, 'Request not found');
    }

    //-------------------------------------------------------------------------------------------
    //Bayar SPP GET form 2

    public function get_tunggakan($master_biaya, $record, $bts_bln, $jml_bayar = 0, $biaya = 0, $tunggakan = 0){
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        if($bts_bln <10){
            for($i=10; $i <= 15; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
                $jml_bayar += $master_biaya;
            }
            for($i=4; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
                $jml_bayar += $master_biaya;
            }
        }else{
            for($i=10; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
                $jml_bayar += $master_biaya;
            }
        }
        $tunggakan = $jml_bayar - $biaya;
        return $tunggakan;
    }

    public function get_biaya_dibayar($record, $bts_bln, $biaya = 0){
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
        if($bts_bln <10){
            for($i=10; $i <= 15; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
            }
            for($i=4; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
            }
        }else{
            for($i=10; $i <= $bts_bln; $i++){
                $b = strtolower($bln[$i]);
                $biaya += $record->$b;
            }
        }
        return $biaya;
    }

    public function bayarSPP2($id)
    {
        //Definisi Bulan dalam bahasa Indonesia
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];

        
        //get student's data by id
        $student = Student::find($id);
        //Validasi tahun pelajaran siswa telah diiisi.
        if($student->tp == null){
            return "Pastikan <b>Tahun Dasar SPP (Tahun Kelas)</b> siswa telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }
        //get class's name by kelas id from students table
        $rombel = Rombel::find($student->kelas);
        //get Tahun Pelaran aktif, where status = aktif
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
        //Get master biaya sekolah
        $master_spp = get_master_biaya($student->nisn);//MasterSPP::where('tp', $rombel->kurikulum)->first();
        //Get batas tunggakan
        $tunggakan = Tunggakan::first();
        //Validasi bahwa tahun dasar perhitungan biaya sekolah pada rombel siswa yg bersangkutan telah ditentukan pada admin page
        if($master_spp == null){
            return "Pastikan <b>Tahun Kurikulum Rombel pada Admin</b> telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }

        //Get data pembayaran masing2 untuk spp, pembanguna, pramuka, dan biaya lain2...
        $spp = Spp::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pembangunan = Pembangunan::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pramuka = Pramuka::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $lain2 = LainLain::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        
        //tunggakan per record/atau per tahun
        $tspp = Spp::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tpembangunan = Pembangunan::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tpramuka = Pramuka::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tlain2 = LainLain::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        // var_dump($tp->tp-1);die();
        //Inisialisasi var bulan dengan nilai nol tiap bulannya
        $bulan_kosong = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        
        //Inisialisasi data pembayaran tipa bulannya jika belum ada record di database
        if( $spp == null) {$spp = new Spp; $spp_null = true;}
        if( $pembangunan == null) {$pembangunan = new Pembangunan; $pembangunan_null = true;}
        if( $pramuka == null) {$pramuka = new Pramuka; $pramuka_null = true;}
        if( $lain2 == null) {$lain2 = new LainLain; $lain2_null = true;}

        //menghitung tunggakan tahun dengan ini
        if( $tspp == null) {$tspp = new Spp; $tunggakan_spp = 0;}
        else{$tunggakan_spp = ($master_spp->spp * 12) - $tspp->total;}

        if( $tpembangunan == null) {$tpembangunan = new Pembangunan; $tunggakan_pembangunan = 0;}
        else{$tunggakan_pembangunan = $master_spp->pembangunan - $tpembangunan->total;}

        if( $tpramuka == null) {$tpramuka = new Pramuka; $tunggakan_pramuka = 0;}
        else{$tunggakan_pramuka = ($master_spp->pramuka * 12) - $tpramuka->total;}

        if( $tlain2 == null) {$tlain2 = new LainLain; $tunggakan_lain2 = 0;}
        else{$tunggakan_lain2 = ($master_spp->lain_lain * 12) - $tlain2->total;}
               
        $spp_dibayar =0;
        $pembangunan_dibayar =0;
        $pramuka_dibayar =0;
        $lain2_dibayar =0;
        
        $tunggakan_bln_spp = $this->get_tunggakan($master_spp->spp, $spp, $master_spp->batas_bln_spp);
        $tunggakan_bln_pembangunan = $master_spp->pembangunan - $this->get_biaya_dibayar($pembangunan, $master_spp->batas_bln_pembangunan);
        $tunggakan_bln_pramuka = $this->get_tunggakan($master_spp->pramuka, $pramuka, $master_spp->batas_bln_pramuka);
        $tunggakan_bln_lain2 = $this->get_tunggakan($master_spp->lain_lain, $lain2, $master_spp->batas_bln_lain2);
        // var_dump($spp_dibayar);die();
        //Check apabila nilai tunggakan sampai bulan bernilai minus (-)
        if($tunggakan_bln_spp < 0) $tunggakan_bln_spp = 0;
        if($tunggakan_bln_pembangunan < 0) $tunggakan_bln_pembangunan = 0;
        if($tunggakan_bln_pramuka < 0) $tunggakan_bln_pramuka = 0;
        if($tunggakan_bln_lain2 < 0) $tunggakan_bln_lain2 = 0;

        //Persiapan data untuk dikirim ke view
        $tahun_aktif = $tp->tp;
        $tunggakan_thn = array("spp" => $tunggakan_spp, "pembangunan" => $tunggakan_pembangunan, "pramuka" => $tunggakan_pramuka, "lain2" => $tunggakan_lain2);
        $bln_tunggakan = get_batas_tunggakan_biaya_sekolah($student->nisn);
        $thn_tunggakan = $tp->tp;//dd($bln_tunggakan);
        $tunggakan_bln = array(
            "spp" => tunggakan_tahun_aktif_spp($student->nisn, $bln_tunggakan['spp']), 
            "pembangunan" => tunggakan_tahun_aktif_pembangunan($student->nisn, $bln_tunggakan['pembangunan'], $tp->tp, $student->tp), 
            "pramuka" => tunggakan_tahun_aktif_pramuka($student->nisn, $bln_tunggakan['pramuka']), 
            "lain2" => tunggakan_tahun_aktif_lain2($student->nisn, $bln_tunggakan['lain2']));

        $detail_bayar = array('spp' => $spp, 'pembangunan' => $pembangunan, 'pramuka' => $pramuka, 'lain2' => $lain2);

        $tunggakanMinsatu = hitungAkumTunggTotal($tp->tp, $student->tp, $student->nisn);
        // dd($tunggakan_thn);
        return view('komite.pages.spp.bayar2', [
            'student'       => $student, 
            "tunggakan_thn" => $tunggakanMinsatu, 
            "tp"            => $tp, 
            "master_spp"    => $master_spp, 
            "tunggakan_bln" => $tunggakan_bln, 
            "bln_tunggakan" => $bln_tunggakan, 
            "thn_tunggakan" => $thn_tunggakan, 
            'bln'           => $bln, 
            // 'tunggakan'     => $tunggakan, 
            'detail_bayar'  => $detail_bayar, 
            'rombelM'       => true]);
    }


    public function checkBayarTunggakan($tpAktif, $tpBayar){
        if($tpAktif != $tpBayar){
            return TRUE;
        }
        return FALSE;
    }

    public function getDataBayarPerTahun($tp, $nisn, $kelas, $typeBayar){
        switch ($typeBayar) {
            case 'spp':
                $dataBayar = Spp::where('tp', $tp)->where('nisn', $nisn)->first();    
                if(count($dataBayar) == 0){
                    $dataBayar = new Spp;
                    $dataBayar->nisn = $nisn;
                    $dataBayar->kelas = $kelas;
                    $dataBayar->tp = $tp;

                    $bulan = Bulan_kecil();
                    foreach ($bulan as $key => $value) {
                        $dataBayar->$value = 0;
                    }
                    $dataBayar->total = 0;
                }
                break;
            
            case 'pembangunan':
                $dataBayar = Pembangunan::where('tp', $tp)->where('nisn', $nisn)->first();
                if(count($dataBayar) == 0){
                    $dataBayar = new Pembangunan;
                    $dataBayar->nisn = $nisn;
                    $dataBayar->kelas = $kelas;
                    $dataBayar->tp = $tp;

                    $bulan = Bulan_kecil();
                    foreach ($bulan as $key => $value) {
                        $dataBayar->$value = 0;
                    }
                    $dataBayar->total = 0;
                }
                break;

            case 'pramuka':
                $dataBayar = Pramuka::where('tp', $tp)->where('nisn', $nisn)->first();
                if(count($dataBayar) == 0){
                    $dataBayar = new Pramuka;
                    $dataBayar->nisn = $nisn;
                    $dataBayar->kelas = $kelas;
                    $dataBayar->tp = $tp;

                    $bulan = Bulan_kecil();
                    foreach ($bulan as $key => $value) {
                        $dataBayar->$value = 0;
                    }
                    $dataBayar->total = 0;
                }
                break;

            case 'lain2':
                $dataBayar = LainLain::where('tp', $tp)->where('nisn', $nisn)->first();
                if(count($dataBayar) == 0){
                    $dataBayar = new LainLain;
                    $dataBayar->nisn = $nisn;
                    $dataBayar->kelas = $kelas;
                    $dataBayar->tp = $tp;

                    $bulan = Bulan_kecil();
                    foreach ($bulan as $key => $value) {
                        $dataBayar->$value = 0;
                    }
                    $dataBayar->total = 0;
                }
                break;

            default:
                $dataBayar = null;
                break;
        }

        return $dataBayar;
    }

    public function checkTelahLunas($dataValidateLunas){
        $data = $dataValidateLunas;
            
        foreach ($data as $key => $value) {
            if($value['inputBayar'] > $value['totalTunggakan'] && $value['totalTunggakan'] == 0) {
                return array('return' => TRUE, 'typeBayar' => $key);
            }
            return array('return' => FALSE);
        }
    }

    public function checkLebihBayarTotal($dataValidateLunas){
        $data = $dataValidateLunas;
            
        foreach ($data as $key => $value) {
            if($value['inputBayar'] > $value['totalTunggakan']) {
                return array('return' => TRUE, 'typeBayar' => $key, 'totalTunggakan' => $value['totalTunggakan']);
            }
            return array('return' => FALSE);
        }
    }

    public function checkKurangBayar($dataValidateLunas){
        $data = $dataValidateLunas;
            
        foreach ($data as $key => $value) {
            if($key == 'pembangunan') continue;
            if($value['inputBayar'] != 0 && $value['inputBayar'] < $value['harusBayar']) {
                return array('return' => TRUE, 'typeBayar' => $key, 'harusBayar' => $value['harusBayar']);
            }
            return array('return' => FALSE);
        }
    }

    public function checkLebihBayar($dataValidateLunas){
        $data = $dataValidateLunas;
            
        foreach ($data as $key => $value) {
            if($key == 'pembangunan') continue;
            $lebihBayar = $value['inputBayar'] % $value['harusBayar'];
            if( $lebihBayar != 0) {
                return array('return' => TRUE, 'typeBayar' => $key, 'lebihBayar' => $lebihBayar);
            }
            return array('return' => FALSE);
        }
    }

    public function getJumlahBulanBayar($inputBayar, $harusBayar, $typeBayar){
        if($typeBayar == 'pembangunan') {
            if($inputBayar > 0){
                return ['jml_bulan' => 1, 'dibayarkan' => $inputBayar];    
            }elseif($inputBayar == 0){
                return 0;
            }
            
        }

        if($harusBayar == 0) {
            return 0;
        }
        
        $jbb = $inputBayar / $harusBayar;
        $jumlahBulanBayar = ['jml_bulan' => $jbb, 'dibayarkan' => $harusBayar];    
        return $jumlahBulanBayar;
    }

    public function getBulanYangDibayar($jumlahBulanBayar, $recordBayar){
        $rbArray = $recordBayar->toArray();
        $rb = array();
        $numkey = 0;

        foreach ( $rbArray as $key => $value) {
            $rb[] = [
                'assoc_key' => $key,
                'numeric_key' => $numkey,
                'value' => $value
            ];

            $numkey++;
        }

        $bulanDibayar = array();
        for ($i = 0; $i < $jumlahBulanBayar['jml_bulan']; $i++) {
            // i = 10 bulan juli ==> semester I
            // 1 = 4 bulan januari ==> semester II
            
            for($i=10; $i <= 15; $i++){
                if ($rb[$i]['value'] == null){
                    $bulanDibayar[] = [
                        'bulan_num' => $rb[$i]['numeric_key'],
                        'bulan' => $rb[$i]['assoc_key'],
                        'dibayarkan' => $jumlahBulanBayar['dibayarkan']
                    ];
                }
            }
            for($i=4; $i <= 9; $i++){
                if ($rb[$i]['value'] == null){
                    $bulanDibayar[] = [
                        'bulan_num' => $rb[$i]['numeric_key'],
                        'bulan' => $rb[$i]['assoc_key'],
                        'dibayarkan' => $jumlahBulanBayar['dibayarkan']
                    ];   
                }
            }    

            $bulanDibayar = array_splice($bulanDibayar, 0, $jumlahBulanBayar['jml_bulan']);
            return $bulanDibayar;
        }
        
    }

    public function bayarDariBulanBayar($jumlahBulanBayar, $recordBayar){
        $bulanDibayar = $this->getBulanYangDibayar($jumlahBulanBayar, $recordBayar);
        
        return $bulanDibayar;

    }   

    public function hitungTotalBayarPertahun($recordBayar){
        $rb = array();
        $numkey = 0;
        
        foreach ($recordBayar->toArray() as $key => $value) {
            $rb[] = [
                'assoc_key' => $key,
                'numeric_key' => $numkey,
                'value' => $value
            ];

            $numkey++;
        }

        $bulanDibayar = array();
        // i = 10 bulan juli ==> semester I
        // 1 = 4 bulan januari ==> semester II
        
        for($i=10; $i <= 15; $i++){
            if ($rb[$i]['value'] != null){
                $bulanDibayar[] = [
                    'bulan' => $rb[$i]['assoc_key'],
                    'dibayarkan' => $rb[$i]['value']
                ];
            }
        }
        for($i=4; $i <= 9; $i++){
            if ($rb[$i]['value'] != null){
                $bulanDibayar[] = [
                    'bulan' => $rb[$i]['assoc_key'],
                    'dibayarkan' => $rb[$i]['value']
                ];   
            }
        }
        $totalBayar = 0;
        foreach ($bulanDibayar as $key => $value) {
            $totalBayar += $value['dibayarkan'];
        }

        return $totalBayar;
    }

    public function bayarSPP_post2($id, Request $request )
    {
        // dd($request->all());
        
        $bln = Bulan_kecil();
        $rules = [
            'note'  => 'max:255',
            'spp'   => 'numeric',
            'pembangunan' =>'numeric',
            'pramuka'=> 'numeric',
            'lain2' => 'numeric',
            'total' => 'numeric'];
        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        //Mengambil data siswa berdasarkan input nisn
        $student = get_student($request->input('nisn'));
        //Mengambil nama kelas berdasarkan id kelas yg ada di db siswa
        $rombel = Rombel::find($student->kelas);
        //get Tahun Pelaran aktif, where status = aktif
        $tpAktifObj = get_tp_aktif();

        $tpAktif = $tpAktifObj->tp;
        $semesterAktif = $tpAktifObj->semester;
        
        $tpBayar = $_POST['tpBayar'];
        
        if($this->checkBayarTunggakan($tpAktif, $tpBayar)){
            $bayarTunggakan = TRUE;
            $semesterBayar = 2;
            $tp = Tp::where('tp', $request->input('tpBayar'))->select('tp', 'semester')->first();
        }else{
            $bayarTunggakan = FALSE;
            $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
            $semesterBayar = $tp->semester;
        }
        //Mengambil master biaya sekolah sebagai dasar perhitungan pembayaran
        // $master_spp = MasterSPP::where('tp', $rombel->kurikulum)->first();
        $master_spp = get_wajib_bayar($request->input('nisn'));
        //Mengambil reff kuitansi yang terakhir berdasarkan tp dan semster aktif
        if($bayarTunggakan){
            $reff = Reff::where('tp', $tp->tp)->where('semester', $semesterBayar)->first();
        }else{
            $reff = Reff::where('tp', $tp->tp)->where('semester', $semesterBayar)->first();
        }

        //Get data pembayaran masing2 untuk spp, pembanguna, pramuka, dan biaya lain2...
        $spp = $this->getDataBayarPerTahun($tp->tp, $student->nisn, $request->input("nisn"), 'spp');
        $pembangunan = $this->getDataBayarPerTahun($tp->tp, $student->nisn, $request->input("nisn"), 'pembangunan');
        $pramuka = $this->getDataBayarPerTahun($tp->tp, $student->nisn, $request->input("nisn"), 'pramuka');
        $lain2 = $this->getDataBayarPerTahun($tp->tp, $student->nisn, $request->input("nisn"), 'lain2');
        
        //Data Tunggakan per tahun ke -i
        $tunggakan_spp = ($master_spp['spp'] * 12) - $spp->total;
        $tunggakan_pembangunan = $master_spp['pembangunan'] - $pembangunan->total;
        $tunggakan_pramuka = ($master_spp['pramuka'] * 12) - $pramuka->total;
        $tunggakan_lain2 = $master_spp['lain2'] - $lain2->total;

        //---------------------------------------------------------------------------------------
        //validasi nilai inputan bayaran yg melebihi nilai tunggakan
        if(($request->input('spp')+$request->input('pembangunan')+$request->input('pramuka')+$request->input('lain2')) == 0) 
            return redirect()->back()->withInput()->withErrors("Pembayaran tidak bisa diproses jika total bayar = Rp. 0,-");
        if($request->input('pembangunan') > 0 && $request->input('tpBayar') != $student->tp) 
            return redirect()->back()->withInput()->withErrors("Biaya Pembangunan hanya dapat dibayarkan pada tahun ajaran pertama pada saat siswa masuk, yaitu TP " . $student->tp . '/' . ($student->tp+1) . '.');

        // array per typeBayar ['inputBayar', 'totalTunggakan', 'typeBayar']
        $dataValidateLunas = array(
            'spp' => [
                'inputBayar' => $request->input('spp'),
                'harusBayar' =>$master_spp['spp'],
                'totalTunggakan' => $tunggakan_spp
            ],
            'pembangunan' => [
                'inputBayar' => $request->input('pembangunan'),
                'harusBayar' =>$master_spp['pembangunan'],
                'totalTunggakan' => $tunggakan_pembangunan
            ],
            'pramuka' => [
                'inputBayar' => $request->input('pramuka'),
                'harusBayar' =>$master_spp['pramuka'],
                'totalTunggakan' => $tunggakan_pramuka
            ],
            'lain2' => [
                'inputBayar' => $request->input('lain2'),
                'harusBayar' =>$master_spp['lain2'],
                'totalTunggakan' => $tunggakan_lain2
            ]
        );

        // Validasi--------------------------------------------------------------------

        $validateLunas = $this->checkTelahLunas($dataValidateLunas);
        $validateKurangBayar = $this->checkKurangBayar($dataValidateLunas);
        $validateLebihBayar = $this->checkLebihBayar($dataValidateLunas);
        $validateLebihBayarTotal = $this->checkLebihBayarTotal($dataValidateLunas);
        
        if($validateLunas['return'] == TRUE) {
            return redirect()->back()->withInput()->withErrors("Tunggakan " . $validateLunas['typeBayar'] . " pada tahun ajaran " . $tpBayar . '/' . ($tpBayar+1) . ' telah lunas.');        
        }
        if($validateLebihBayarTotal['return'] == TRUE){ 
            return redirect()->back()->withInput()->withErrors("Nilai inputan " . $validateLebihBayarTotal['typeBayar'] . " melebihi tunggakan yang harus dibayar, yaitu sebesar = Rp. " . number_format($validateLebihBayarTotal['totalTunggakan'],2,',','.'));
        }
        if($validateKurangBayar['return'] == TRUE){ 
            return redirect()->back()->withInput()->withErrors("Nilai inputan " . $validateKurangBayar['typeBayar'] . " setidaknya harus = Rp. " . number_format($validateKurangBayar['harusBayar'],2,',','.'));
        }
        if($validateLebihBayar['return'] == TRUE){ 
            return redirect()->back()->withInput()->withErrors("Nilai inputan " . $validateLebihBayar['typeBayar'] . " lebih = Rp. " . number_format($validateLebihBayar['lebihBayar'],2,',','.'));
        }

        // Validasi--------------------------------------------------------------------

        //Persiapan data untuk disimpan di database spp, pembayaran, pramuka dan biaya lain-lain
        $jumlahBulanBayar = [
            'spp' => $this->getJumlahBulanBayar($request->input('spp'), $master_spp['spp'], 'spp'),
            'pembangunan' => $this->getJumlahBulanBayar($request->input('pembangunan'), $master_spp['pembangunan'], 'pembangunan'),
            'pramuka' => $this->getJumlahBulanBayar($request->input('pramuka'), $master_spp['pramuka'], 'pramuka'),
            'lain2' => $this->getJumlahBulanBayar($request->input('lain2'), $master_spp['lain2'], 'lain2')
        ];

        $dibayarkan = [
            'spp' => $this->bayarDariBulanBayar($jumlahBulanBayar['spp'], $spp),
            'pembangunan' => $this->bayarDariBulanBayar($jumlahBulanBayar['pembangunan'], $pembangunan),
            'pramuka' => $this->bayarDariBulanBayar($jumlahBulanBayar['pramuka'], $pramuka),
            'lain2' => $this->bayarDariBulanBayar($jumlahBulanBayar['lain2'], $lain2),
        ];

        $bulanDibayarkan = array(
            'spp' => array(), 
            'pembangunan' => array(), 
            'pramuka' => array(), 
            'lain2' => array() 
        );

        $uangDibayarkan = [
            'spp' => 0,
            'pembangunan' => 0,
            'pramuka' => 0,
            'lain2' => 0
        ];
        
        foreach ($dibayarkan as $key => $value) {
            if($value == null) continue;
            
            switch ($key) {
                case 'spp':
                    foreach ($value as $a => $val) {
                        $spp->$val['bulan'] = $val['dibayarkan'];
                        $bulanDibayarkan['spp'][] = $val['bulan_num'];
                        $uangDibayarkan['spp'] += $val['dibayarkan'];
                    }
                    break;

                case 'pembangunan':
                    foreach ($value as $a => $val) {
                        $pembangunan->$val['bulan'] = $val['dibayarkan'];
                        $bulanDibayarkan['pembangunan'][] = $val['bulan_num'];
                        $uangDibayarkan['pembangunan'] += $val['dibayarkan'];
                    }
                    break;

                case 'pramuka':
                    foreach ($value as $a => $val) {
                        $pramuka->$val['bulan'] = $val['dibayarkan'];
                        $bulanDibayarkan['pramuka'][] = $val['bulan_num'];
                        $uangDibayarkan['pramuka'] += $val['dibayarkan'];
                    }
                    break;

                case 'lain2':
                    foreach ($value as $a => $val) {
                        $lain2->$val['bulan'] = $val['dibayarkan'];
                        $bulanDibayarkan['lain2'][] = $val['bulan_num'];
                        $uangDibayarkan['lain2'] += $val['dibayarkan'];
                    }
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        $spp->total = $this->hitungTotalBayarPertahun($spp);
        $pembangunan->total = $this->hitungTotalBayarPertahun($pembangunan);
        $pramuka->total = $this->hitungTotalBayarPertahun($pramuka);
        $lain2->total = $this->hitungTotalBayarPertahun($lain2);

        // Check reff,  jika tidak ada maka akan dibuat record baru
        if($reff == null){
            $reff = new Reff;
            $reff->nama_reff = 1;
            $reff->tp = $tp->tp;
            if($bayarTunggakan){
                $reff->semester = $semesterBayar;
            }else{
                $reff->semester = $tp->semester;
            }
        }else{
            //Jika ada maka reff selanjutnya ditambahkan 1
            $reff->nama_reff = $reff->nama_reff + 1;
        }
        //Menentukan no kuitansi baru
        if($bayarTunggakan){
            $no_kuitansi = $tp->tp . $semesterBayar . $reff->nama_reff;
        }else{
            $no_kuitansi = $tp->tp . $tp->semester . $reff->nama_reff;
        }

        //mencatat transaksi ke history
        $histori = new HistoriSpp;
        
        $histori->nisn = $request->input("nisn");
        
        $histori->nama = $student->nama_lengkap;
        
        $histori->tp = $tp->tp;
        
        $kelas = Rombel::where('id',$request->input("kelas"))->first();
        $histori->kelas = $kelas->nama;
        
        $histori->oleh = Auth::komite()->get()->komite_name;
        $histori->oleh_id = Auth::komite()->get()->komite_id;
        
        $histori->tgl_bayar = date('d');
        $histori->bln_bayar = date('n');
        $histori->thn_bayar = date('Y');

        $histori->bln_dibayar_spp = json_encode($bulanDibayarkan['spp']);
        $histori->bln_dibayar_pembangunan = json_encode($bulanDibayarkan['pembangunan']);
        $histori->bln_dibayar_pramuka = json_encode($bulanDibayarkan['pramuka']);
        $histori->bln_dibayar_lain2 = json_encode($bulanDibayarkan['lain2']);
        
        $histori->note = $request->input("note");
        $histori->note2 = $request->input("note2");
        $histori->no_kwitansi = $no_kuitansi;
        
        $histori->semester = $semesterAktif;//$semesterBayar;    

        $histori->spp = $uangDibayarkan['spp'];
        $histori->pembangunan = $uangDibayarkan['pembangunan'];
        $histori->pramuka = $uangDibayarkan['pramuka'];
        $histori->lain_lain = $uangDibayarkan['lain2'];
        $histori->nominal = $uangDibayarkan['spp'] + $uangDibayarkan['pembangunan'] + $uangDibayarkan['pramuka'] + $uangDibayarkan['lain2'];
        
        if($histori->save()){
            if($reff->save()){
                if(!$spp->save()){
                    return redirect()->back()->withInput()->withErrors("Gagal menyimpan data SPP.");            
                }
                if(!$pembangunan->save()){
                    return redirect()->back()->withInput()->withErrors("Gagal menyimpan data Pembangunan.");            
                }
                if(!$pramuka->save()){
                    return redirect()->back()->withInput()->withErrors("Gagal menyimpan data Pramuka.");
                }   
                if(!$lain2->save()){
                    return redirect()->back()->withInput()->withErrors("Gagal menyimpan data Biaya Lain-lain.");
                }   

                return redirect('komite/students/spp/sukses/'.$id)->with('success', 'Pembayaran berhasil disimpan.')->with('no_kuitansi', $histori->no_kwitansi)->with('semesterBayar', $semesterAktif)->with('tpBayar', $tpBayar); 
            }else{
                return redirect()->back()->withInput()->withErrors("Terjadi gangguan saat menyimpan nomor reff kuitansi. Periksa kembali data bayar dan histori kuitansi.");            
            }
        }else{
            return redirect()->back()->withInput()->withErrors("Terjadi gangguan saat menyimpan data kuitansi. Periksa kembali data bayar dan histori kuitansi.");            
        }        
    }

    public function sukses($id){
        if(Session::get('tpBayar') == null || Session::get('semesterBayar') == null || Session::get('no_kuitansi') == null){
            return "Halaman sudah tidak berlaku lagi.";
        }
        $url = url('komite/students/spp/invoiceExcel/'.$id.'/' . Session::get('tpBayar').'/'.Session::get('semesterBayar').'/'.Session::get('no_kuitansi'));
        $urlBayar = url('komite/students/spp/bayar/'.$id);
        return "<script>
                    history.pushState(null, null, location.href);
                    window.onpopstate = function () {
                        history.go(1);
                    };
                </script>
                <script type='text/javascript'>
                function openInvoice() {
                    window.open('" . $url . "', '', '');
                }
                    openInvoice();
                </script>
                <h3>Transaksi Berhasil disimpan !</h3><br>
                <a href='" . $url . "'>Download Bukti Transaksi</a><br><br>
                <a href='" . $urlBayar . "'>Kembali</a><br>
                <a href='" . url('komite/students') . "'>Kembali Ke Daftar Siswa Aktif</a>";
    }

    public function return_bulan($array, $tp){
        $bln = Bulan();
        $bln_dibayar = json_decode($array);
        if(count($bln_dibayar > 0) && $bln_dibayar != NULL){
            $lastEl = array_values(array_slice($bln_dibayar, -1))[0];
            $firstEl = $bln_dibayar[0];
            if($lastEl == $firstEl) {
                return $bln[$firstEl] . ' ' . $tp . '/' . ($tp+1);
            }else{
                return ($bln[$firstEl] . ' ' . $tp . '/' . ($tp+1) . ' - ' . $bln[$lastEl] . ' ' . $tp . '/' . ($tp+1));
            }
        }elseif(count($bln_dibayar == 0)){
            return '-';
        }
    }

    public function pembangunanKe($nisn, $tp, $nomPem){
        $tp = Tp::where('tp', $tp)->orderBy('tp')->first();
        $record = get_record($nisn, 'pembangunan', $tp)->toArray();
        $a = 0;
        $bayarKe = 0;
        if($nomPem != null && $nomPem != 0 && $nomPem != ''){
            foreach ($record as $key => $value) {
                if($a > 3 && $a < 16){
                    if($value != null && $value != 0 && $value != ''){
                        $bayarKe++;
                    }            
                }
                $a++;
            }    
        }
        
        $hrufRomawi = array(1 => 'I', 2 => 'II', 3 => 'III', 4 => 'IV', 5 => 'V', 6 => 'VI', 7 => 'VII', 8 => 'VIII', 9 => 'IX', 10 => 'X', 11 => 'XI', 12 => 'XII');
        $ke = '';
        foreach ($hrufRomawi as $key => $value) {
            if($key == $bayarKe){
                $ke = $value;
            }
        }
        $bayarKe = 'Pembayaran ke ' . $ke;
        if($ke == ''){
            $bayarKe = '-';
        }
        return $bayarKe;
    }
    public function potongStringBulanBayar($stringBulanBayar){
        if ($stringBulanBayar == "-"){
            return "-";
        }

        $stringBulan = explode('-', $stringBulanBayar);

        if(count($stringBulan) == 2){
            return trim($stringBulan[0]) . " - \n" . trim($stringBulan[1]);    
        }
        
        return trim($stringBulan[0]);
    }   

    public function hitungSisaTunggakan($nisn)
    {
        //Definisi Bulan dalam bahasa Indonesia
        $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];

        
        //get student's data by id
        $student = Student::find($nisn);
        //Validasi tahun pelajaran siswa telah diiisi.
        if($student->tp == null){
            return "Pastikan <b>Tahun Dasar SPP (Tahun Kelas)</b> siswa telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }
        //get class's name by kelas id from students table
        $rombel = Rombel::find($student->kelas);
        //get Tahun Pelaran aktif, where status = aktif
        $tp = Tp::where('status', 'aktif')->select('tp', 'semester')->first();
        //Get master biaya sekolah
        $master_spp = get_master_biaya($student->nisn);//MasterSPP::where('tp', $rombel->kurikulum)->first();
        //Get batas tunggakan
        $tunggakan = Tunggakan::first();
        //Validasi bahwa tahun dasar perhitungan biaya sekolah pada rombel siswa yg bersangkutan telah ditentukan pada admin page
        if($master_spp == null){
            return "Pastikan <b>Tahun Kurikulum Rombel pada Admin</b> telah diisi. <a href='".url('tu-user/students/bayar/recap')."'>Kembali</a>";
        }

        //Get data pembayaran masing2 untuk spp, pembanguna, pramuka, dan biaya lain2...
        $spp = Spp::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pembangunan = Pembangunan::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $pramuka = Pramuka::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        $lain2 = LainLain::where('tp', $tp->tp)->where('nisn', $student->nisn)->first();
        
        //tunggakan per record/atau per tahun
        $tspp = Spp::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tpembangunan = Pembangunan::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tpramuka = Pramuka::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        $tlain2 = LainLain::where('tp', ($tp->tp-1))->where('nisn', $student->nisn)->first();
        // var_dump($tp->tp-1);die();
        //Inisialisasi var bulan dengan nilai nol tiap bulannya
        $bulan_kosong = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        
        //Inisialisasi data pembayaran tipa bulannya jika belum ada record di database
        if( $spp == null) {$spp = new Spp; $spp_null = true;}
        if( $pembangunan == null) {$pembangunan = new Pembangunan; $pembangunan_null = true;}
        if( $pramuka == null) {$pramuka = new Pramuka; $pramuka_null = true;}
        if( $lain2 == null) {$lain2 = new LainLain; $lain2_null = true;}

        //menghitung tunggakan tahun dengan ini
        if( $tspp == null) {$tspp = new Spp; $tunggakan_spp = 0;}
        else{$tunggakan_spp = ($master_spp->spp * 12) - $tspp->total;}

        if( $tpembangunan == null) {$tpembangunan = new Pembangunan; $tunggakan_pembangunan = 0;}
        else{$tunggakan_pembangunan = $master_spp->pembangunan - $tpembangunan->total;}

        if( $tpramuka == null) {$tpramuka = new Pramuka; $tunggakan_pramuka = 0;}
        else{$tunggakan_pramuka = ($master_spp->pramuka * 12) - $tpramuka->total;}

        if( $tlain2 == null) {$tlain2 = new LainLain; $tunggakan_lain2 = 0;}
        else{$tunggakan_lain2 = ($master_spp->lain_lain * 12) - $tlain2->total;}
               
        $spp_dibayar =0;
        $pembangunan_dibayar =0;
        $pramuka_dibayar =0;
        $lain2_dibayar =0;
        
        $tunggakan_bln_spp = $this->get_tunggakan($master_spp->spp, $spp, $master_spp->batas_bln_spp);
        $tunggakan_bln_pembangunan = $master_spp->pembangunan - $this->get_biaya_dibayar($pembangunan, $master_spp->batas_bln_pembangunan);
        $tunggakan_bln_pramuka = $this->get_tunggakan($master_spp->pramuka, $pramuka, $master_spp->batas_bln_pramuka);
        $tunggakan_bln_lain2 = $this->get_tunggakan($master_spp->lain_lain, $lain2, $master_spp->batas_bln_lain2);
        // var_dump($spp_dibayar);die();
        //Check apabila nilai tunggakan sampai bulan bernilai minus (-)
        if($tunggakan_bln_spp < 0) $tunggakan_bln_spp = 0;
        if($tunggakan_bln_pembangunan < 0) $tunggakan_bln_pembangunan = 0;
        if($tunggakan_bln_pramuka < 0) $tunggakan_bln_pramuka = 0;
        if($tunggakan_bln_lain2 < 0) $tunggakan_bln_lain2 = 0;

        //Persiapan data untuk dikirim ke view
        $bln_tunggakan = get_batas_tunggakan_biaya_sekolah($student->nisn);
        $tunggakan_thn = hitungAkumTunggTotal($tp->tp, $student->tp, $student->nisn);
        $tunggakan_bln = array(
            "spp" => tunggakan_tahun_aktif_spp($student->nisn, $bln_tunggakan['spp']), 
            "pembangunan" => tunggakan_tahun_aktif_pembangunan($student->nisn, $bln_tunggakan['pembangunan'], $tp->tp, $student->tp), 
            "pramuka" => tunggakan_tahun_aktif_pramuka($student->nisn, $bln_tunggakan['pramuka']), 
            "lain2" => tunggakan_tahun_aktif_lain2($student->nisn, $bln_tunggakan['lain2'])
        );

        $tspp = $tunggakan_thn['spp'] + $tunggakan_bln['spp'];
        $tpem = $tunggakan_thn['pembangunan'] + $tunggakan_bln['pembangunan'];
        $tpra = $tunggakan_thn['pramuka'] + $tunggakan_bln['pramuka'];
        $tlain = $tunggakan_thn['lain2'] + $tunggakan_bln['lain2'];
        $ttotal = $tspp + $tpem + $tpra + $tlain;

        $total_tunggakan = array(
            "spp" => $tspp,
            "pembangunan" => $tpem,
            "pramuka" => $tpra,
            "lain2" => $tlain,
            "total" => $ttotal
        );

        return $total_tunggakan;
    }

    public function invoiceExcel($id, $tp, $semester, $no_kuitansi){
        $total_tunggakan = $this->hitungSisaTunggakan($id);
        $objPHPExcel = new PHPExcel(); 
        $bord = $objPHPExcel->Gen_PHPExcel_Style_Border();
        $fill = $objPHPExcel->Gen_PHPExcel_Style_Fill();
        $align = $objPHPExcel->Gen_PHPExcel_Style_Align();
        $objPHPExcel->getProperties()->setCreator("SMKN 2 PAYAKUMBUH")
                                     ->setLastModifiedBy("SMKN 2 PAYAKUMBUH")
                                     ->setTitle("SMKN 2 PAYAKUMBUH")
                                     ->setSubject("SMKN 2 PAYAKUMBUH")
                                     ->setDescription("SMKN 2 PAYAKUMBUH")
                                     ->setKeywords("SMKN 2 PAYAKUMBUH")
                                     ->setCategory("SMKN 2 PAYAKUMBUH");

        // create style
        $border = array(
            'style' => $bord::BORDER_THIN,
            'color' => array('rgb'=>'000')
        );
        $mergeStyle = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            ),
            'font' => array(
                'size' => 14,
            )
        );
        $contentStyle = array(
            'alignment' => array(
                'vertical' => $align::VERTICAL_CENTER,
            ),
            'borders' => array(
                'bottom' => $border,
                'left' => $border,
                'top' => $border,
                'right' => $border,
            ),
        );
        $center = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $header = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_RIGHT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        ); 
        $rightTop = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_LEFT,
                'vertical' => $align::VERTICAL_TOP,
            )
        ); 
        $smallFont = array(
            'font' => array(
                'size' => 8,
            )
        );
        $fontFace = array(
            'font' => array(
                'size' => 8,
                'name' => 'verdana',
            )
        );
        //-----------------------------------------------------------
        $histori = HistoriSpp::where('nisn', $id)
                    ->where('tp', $tp)
                    ->where('semester', $semester)
                    ->where('no_kwitansi', $no_kuitansi)
                    ->first();
        
        $noKuitansi = $histori->no_kwitansi;
        $tanggalKuitansi = str_pad($histori->tgl_bayar, 2, '0', STR_PAD_LEFT) . '/' . str_pad($histori->bln_bayar, 2, '0', STR_PAD_LEFT) . '/' . $histori->thn_bayar;
        
        $namaSiswa = $histori->nama;
        $nisn = str_pad($histori->nisn, 10, '0', STR_PAD_LEFT);
        $kelas = $histori->kelas;
        $tpBayar = $histori->thn_bayar . '/' . ($histori->thn_bayar + 1);
        $semester = $histori->semester == 1 ? $histori->semester . ' (satu)' : $histori->semester . ' (dua)'; 
        
        $bulanBayarSpp = $this->return_bulan($histori->bln_dibayar_spp, $histori->tp);
        $bulanBayarPramuka = $this->return_bulan($histori->bln_dibayar_pramuka, $histori->tp);
        $bulanBayarLain2 = $this->return_bulan($histori->bln_dibayar_lain2, $histori->tp);
        $bulanBayarPembangunan = $this->pembangunanKe($histori->nisn, $histori->tp, $histori->pembangunan );
        
        $nomSpp = $histori->spp != 0 ? number_format($histori->spp,2,',','.')  : '-';
        $nomPembangunan = $histori->pembangunan != 0 ? number_format($histori->pembangunan,2,',','.')  : '-';
        $nomPramuka = $histori->pramuka != 0 ? number_format($histori->pramuka,2,',','.')  : '-';
        $nomLain2 = $histori->lain2 != 0 ? number_format($histori->lain2,2,',','.')  : '-';
        
        $ttspp = $total_tunggakan['spp'] != 0 ? number_format($total_tunggakan['spp'],2,',','.')  : '-';
        $ttpem = $total_tunggakan['pembangunan'] != 0 ? number_format($total_tunggakan['pembangunan'],2,',','.')  : '-';
        $ttpra = $total_tunggakan['pramuka'] != 0 ? number_format($total_tunggakan['pramuka'],2,',','.')  : '-';
        $ttlain = $total_tunggakan['lain2'] != 0 ? number_format($total_tunggakan['lain2'],2,',','.')  : '-';
        $tttotal = $total_tunggakan['total'] != 0 ? number_format($total_tunggakan['total'],2,',','.')  : '-';

        $totalBayar = number_format($histori->nominal,2,',','.');
        $note = '*' . $histori->note . ($histori->note2 != '' ? ' / ' . $histori->note2 : '');
        $penerima = $histori->oleh;
        
        //-----------------------------------------------------------
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', ' SMKN 2 PAYAKUMBUH')
                    ->setCellValue('E1', ' No Kuitansi')
                    ->setCellValue('F1', ' : ' . $noKuitansi)
                    ->setCellValue('E2', ' Tanggal')
                    ->setCellValue('F2', ' : ' . $tanggalKuitansi);
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A1:C2');
        $objPHPExcel->getActiveSheet()->getStyle('A1:C2')->applyFromArray( $mergeStyle );
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
        
        //-----------------------------------------------------------
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A3', 'BUKTI PEMBAYARAN');
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A3:G3');
        $objPHPExcel->getActiveSheet()->getStyle('A3:G3')->applyFromArray( $center );
        //-----------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A4', 'Nama')
                    ->setCellValue('A5', 'Nisn')
                    ->setCellValue('A6', 'Kelas')
                    ->setCellValue('B4', ' : ' . $namaSiswa)
                    ->setCellValue('B5', ' : ' . $nisn)
                    ->setCellValue('B6', ' : ' . $kelas);
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('E4', 'TP')
                    ->setCellValue('E5', 'Semester')
                    ->setCellValue('F4', ' : ' . $tpBayar)
                    ->setCellValue('F5', ' : ' . $semester);
        //-----------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', 'Tanggungan')
                    ->setCellValue('B7', 'Bulan Dibayar')
                    ->setCellValue('E7', 'Nominal')
                    ->setCellValue('F7', 'Sisa Iuran');
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B7:D7')
                    ->mergeCells('F7:G7');
        $objPHPExcel->getActiveSheet()->getStyle('A7:G7')->applyFromArray( $header );
        //-----------------------------------------------------------
        
        $objPHPExcel->getActiveSheet()->getRowDimension('8')->setRowHeight(30);
        $objPHPExcel->getActiveSheet()->getRowDimension('9')->setRowHeight(30);
        $objPHPExcel
                  ->getActiveSheet()
                  ->getStyle('B8:D11')
                  ->getAlignment()
                  ->setWrapText(true);

        //-----------------------------------------------------------

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A8', 'SPP')
                    ->setCellValue('B8', $this->potongStringBulanBayar($bulanBayarSpp))
                    ->setCellValue('E8', $nomSpp)
                    ->setCellValue('F8', $ttspp);
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A9', 'Pramuka')
                    ->setCellValue('B9', $this->potongStringBulanBayar($bulanBayarPramuka))
                    ->setCellValue('E9', $nomPramuka)
                    ->setCellValue('F9', $ttpra);
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A10', 'Pembangunan')
                    ->setCellValue('B10', $bulanBayarPembangunan)
                    ->setCellValue('E10', $nomPembangunan)
                    ->setCellValue('F10', $ttpem);
        // $objPHPExcel->setActiveSheetIndex(0)
        //             ->setCellValue('A11', 'Lain-lain')
        //             ->setCellValue('B11', $bulanBayarLain2)
        //             ->setCellValue('E11', $nomLain2)
        //             ->setCellValue('F11', $ttlain);
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A11', 'TOTAL')
                    ->setCellValue('E11', $totalBayar)
                    ->setCellValue('F11', $tttotal);
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B8:D8')
                    ->mergeCells('F8:G8')
                    ->mergeCells('B9:D9')
                    ->mergeCells('F9:G9')
                    ->mergeCells('B10:D10')
                    ->mergeCells('F10:G10')
                    ->mergeCells('B11:D11')
                    ->mergeCells('F11:G11')
                    ->mergeCells('A12:D12')
                    ->mergeCells('F12:G12');
        $objPHPExcel->getActiveSheet()->getStyle('E8:F12')->applyFromArray( $right );
        //-----------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A13', $note)
                    ->setCellValue('F13', 'Diterima oleh')
                    ->setCellValue('F15', $penerima);
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A13:D15');
        $objPHPExcel->getActiveSheet()->getStyle('A13:D15')->applyFromArray( $contentStyle );
        $objPHPExcel->getActiveSheet()->getStyle('A13')->applyFromArray( $rightTop );
        //-----------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A16', 'Lembar 1 Arsip')
                    ->setCellValue('A17', 'Lembar 2 Siswa/Penyetor');       
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A16:D16')    
                    ->mergeCells('A17:D17');     
        $objPHPExcel->getActiveSheet()->getStyle('A16:A17')->applyFromArray( $smallFont );
        $objPHPExcel->getActiveSheet()->getStyle('A1:G17')->applyFromArray( $fontFace );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
        //-----------------------------------------------------------
        $letters = array();
        foreach (range('A', 'G') as $char) {
            $letters[] = $char;
        }
        for($i = 7 ; $i <= 11; $i++){
            foreach ($letters as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$i.':'.$value.$i)->applyFromArray( $contentStyle );
            }
        }
        $objPHPExcel->getActiveSheet()
                    ->getPageMargins()
                    ->setTop(0.748031)
                    ->setRight(0.11811)
                    ->setLeft(0.11811)
                    ->setBottom(0.354331);
        //-----------------------------------------------------------

        // Set nama supaya tidak terlalu panjang
        $filename = 'Inv - ' . ExcelLibrary::worksheetNameGenerator($namaSiswa);
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('BUKTI PEMBAYARAN');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"'); // file name of excel
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    public function histori($id)
    {
        $student = Student::where('nisn', $id)->first();
        $histori = HistoriSpp::where('nisn', $id)->get();
        return view('komite.pages.spp.histori', ['student'=> $student, 'histori' => $histori]);
    }

    public function detail_histori($nisn, $type)
    {
        if($type == 'spp') $detail = Spp::where('nisn', $nisn)->get();
        if($type == 'pembangunan') $detail = Pembangunan::where('nisn', $nisn)->get();
        if($type == 'pramuka') $detail = Pramuka::where('nisn', $nisn)->get();
        if($type == 'lain-lain') $detail = LainLain::where('nisn', $nisn)->get();

        $student = Student::where('nisn', $nisn)->first();

        return view('komite.pages.spp.detail-histori', [
            'student'       => $student, 
            'nama_detail'   => $type,
            'detail' => $detail]);
    }

    public function lapKelas($kelas)
    {
        $rombel = Rombel::where('id', $kelas)->select('nama', 'pengajar')->first()->toArray();
        $filename = 'Data Keuangan Kelas - '.$rombel['nama'];
        $students = Student::where('kelas', $kelas)->select('nama_lengkap')->get();
        $cs = Student::where('kelas', $kelas)->select('nama_lengkap')->count();
        if($cs == 0)
            return redirect()->back()->withErrors('Rombel belum memiliki data siswa untuk dihitung laporan keuangannya. Silahkan isi data siswa terlebih dahulu.');
        $Mspp = MasterSPP::where('status', 'aktif')->first()->toArray();
        
        $histori=Spp::where('kelas', $kelas)->get()->toArray();//dd($histori);
        $Jhistori=Spp::where('kelas', $kelas)->get()->count();//dd($histori);

        $pem=Pembangunan::where('kelas', $kelas)->get()->toArray();//dd($histori);
        //$JPem=pembanguna::where('kelas', $kelas)->get()->count();//dd($histori);

        $pra=Pramuka::where('kelas', $kelas)->get()->toArray();//dd($histori);

        Excel::create($filename, function($excel) use ($students, $filename, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('SMKN 2 PAYAKUMBUH')
                  ->setCompany('SMKN 2 PAYAKUMBUH');

            $excel->sheet($filename, function($sheet) use ($students, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {
                //no dan nama header merging MERGING VERTICAL
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:B5');
                
                //MERGING HORIZONTAL ROW A1 FOR HEADER
                $sheet->mergeCells('A1:N1');
                $sheet->mergeCells('C4:F4');
                $sheet->mergeCells('G4:J4');
                $sheet->mergeCells('K4:N4');

                $sheet->row(1, array('DATA KEUANGAN SISWA SMKN 2 PAYAKUMBUH PER ' . date('n F Y'),'','','',
                    '',',' ,'','','','','',''));

                $sheet->row(3, array('KELAS : '. $rombel['nama'],'','','','','','','','','','','WALI KELAS : '.$rombel['pengajar'],'',''
                ));
                $sheet->row(4, array(
                     'No', 'NAMA', 
                     'BESARAN IURAN', '', '', '', 
                     'YANG TELAH DIBAYAR', '', '', '', 
                     'TUNGGAKAN', '', '', ''
                ));
                $sheet->row(5, array(
                     '',  '',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH'
                ));

                //SET ALIGNMENT CENTER FOR HEADER
                $sheet->row(4, function($row) {
                    $row->setAlignment('center');
                });
                
                $sheet->row(5, function($row) {
                    $row->setAlignment('center');
                });

                $sheet->row(1, function($row) {
                    $row->setAlignment('center');
                });

                //SETTING WIDTH CELL
                $sheet->setWidth(array(
                    'A'     =>  4,
                    'B'     =>  25,

                    'C'     =>  15,
                    'D'     =>  15,
                    'E'     =>  15,
                    'F'     =>  15,

                    'G'     =>  15,
                    'H'     =>  15,
                    'I'     =>  15,
                    'J'     =>  15,

                    'K'     =>  15,
                    'L'     =>  15,
                    'M'     =>  15,
                    'N'     =>  15
                ));

                $row = 6;
                $no = 1;
                $TotSpp = array();
                $TotPem = array();
                $TotPra = array();
                $TotBayar = array();
                $H_Spp = array();
                $H_Pem = array();
                $H_Pra = array();
                $H_Bayar = array();
                foreach ($students as $student) {
                    if($no <= $Jhistori){
                    $Pspp = $histori[$no-1]['januari'] + $histori[$no-1]['februari'] + $histori[$no-1]['maret'] + $histori[$no-1]['april'] + $histori[$no-1]['mei'] + $histori[$no-1]['juni'] + $histori[$no-1]['juli'] + $histori[$no-1]['agustus'] + $histori[$no-1]['september'] + $histori[$no-1]['oktober'] + $histori[$no-1]['november'] + $histori[$no-1]['desember'];
                    $Ppem = $pem[$no-1]['januari'] + $pem[$no-1]['februari'] + $pem[$no-1]['maret'] + $pem[$no-1]['april'] + $pem[$no-1]['mei'] + $pem[$no-1]['juni'] + $pem[$no-1]['juli'] + $pem[$no-1]['agustus'] + $pem[$no-1]['september'] + $pem[$no-1]['oktober'] + $pem[$no-1]['november'] + $pem[$no-1]['desember'];
                    $Ppra = $pra[$no-1]['januari'] + $pra[$no-1]['februari'] + $pra[$no-1]['maret'] + $pra[$no-1]['april'] + $pra[$no-1]['mei'] + $pra[$no-1]['juni'] + $pra[$no-1]['juli'] + $pra[$no-1]['agustus'] + $pra[$no-1]['september'] + $pra[$no-1]['oktober'] + $pra[$no-1]['november'] + $pra[$no-1]['desember'];
                    }else{
                        $Pspp = 0;
                        $Ppem = 0;
                        $Ppra = 0;
                    }
                    $Tspp = $Mspp['spp'] * 12;
                    $Tpem = $Mspp['pembangunan'] * 12;
                    $Tpra = $Mspp['pramuka'] * 12;
                    $Tbayar = $Tspp + $Tpem + $Tpra;
                    $Pbayar = $Pspp + $Ppem + $Ppra;
                    $Hspp = $Tspp - $Pspp;
                    $Hpem = $Tpem - $Ppem;
                    $Hpra = $Tpra - $Ppra;
                    $Hbayar = $Tbayar - $Pbayar ;
                    
                    //push telah dibaar data ke dalam array total seluruh siswa
                    array_push($TotSpp, $Pspp);
                    array_push($TotPem, $Ppem);
                    array_push($TotPra, $Ppra);
                    array_push($TotBayar, $Pbayar);
                    array_push($H_Spp, $Hspp);
                    array_push($H_Pem, $Hpem);
                    array_push($H_Pra, $Hpra);
                    array_push($H_Bayar, $Hbayar);

                    $sheet->row($row, array(
                        $no, $student->nama_lengkap, $Tspp, $Tpem, $Tpra, $Tbayar, $Pspp, $Ppem, $Ppra, $Pbayar, $Hspp, $Hpem, $Hpra, $Hbayar
                    ));
                    $row++;
                    $no++;
                }

                $no = $no - 1;
                $sheet->row(($row+1), array(
                        '', 'JUMLAH', ($Tspp * $no), ($Tpem * $no), ($Tpra * $no), ($Tbayar * $no),  array_sum($TotSpp), array_sum($TotPem), array_sum($TotPra), array_sum($TotBayar), array_sum($H_Bayar), array_sum($H_Bayar), array_sum($H_Bayar), array_sum($H_Bayar) 
                ));

                $sheet->cells('A1', function($cells) {
                    $cells->setFont(array(
                        'size'       => '14',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('L3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A4:N5', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                });

                //$sheet->setBorder('A4:N'.($row+1), 'thin');
                $sheet->setAllBorders('thin');
            });
        
        })->export('xlsx');

    }

    public function histori_export($nisn)
    {
        $historis = get_histori_bayar($nisn, $tahun = null);
        $nama_siswa = strtoupper(get_student_name($nisn));

        $result = ExcelLibrary::historyPembayaran($historis, $nama_siswa);
        return $result;
        
    }    

    public function invoice($id, $tp, $semester, $no_kuitansi)
    {
        $student = Student::where('nisn', $id)->first();
        $histori = HistoriSpp::where('nisn', $id)->where('tp', $tp)->where('semester', $semester)->where('no_kwitansi', $no_kuitansi)->first();//dd($histori);
        $Mspp = MasterSpp::where('tp', $tp)->first();
        return view('komite.pages.spp.invoice', ['student'=> $student, 'histori' => $histori, 'Mspp' => $Mspp]);
    }

    public function dataBayar(Request $request){
        //Get data pembayaran masing2 untuk spp, pembanguna, pramuka, dan biaya lain2...
        $spp = Spp::where('tp', $_POST['value'])->where('nisn', $_POST['nisn'])->first();
        $pembangunan = Pembangunan::where('tp', $_POST['value'])->where('nisn', $_POST['nisn'])->first();
        $pramuka = Pramuka::where('tp', $_POST['value'])->where('nisn', $_POST['nisn'])->first();
        $lain2 = LainLain::where('tp', $_POST['value'])->where('nisn', $_POST['nisn'])->first();
        
        //Inisialisasi data pembayaran tipa bulannya jika belum ada record di database
        if( $spp == null) {$spp = new Spp; $spp_null = true;}
        if( $pembangunan == null) {$pembangunan = new Pembangunan; $pembangunan_null = true;}
        if( $pramuka == null) {$pramuka = new Pramuka; $pramuka_null = true;}
        if( $lain2 == null) {$lain2 = new LainLain; $lain2_null = true;}

        $rombel = Rombel::where('id', $_POST['kelas'])->first()->toArray();//dd($jurusan);

        $detail_bayar = array('spp' => $spp, 'pembangunan' => $pembangunan, 'pramuka' => $pramuka, 'lain2' => $lain2);//dd($detail_bayar);

        $tahunMasuk = Student::where('nisn', $_POST['nisn'])->select('tp')->first();
        $tahunMasuk = $tahunMasuk->tp;
        if($_POST['value'] < $tahunMasuk){
            return '<span style="color:red"><b>Record siswa dimulai dari tahun ajaran ' . $tahunMasuk . '/' . $tahunMasuk . '</b></span>';
        }
        $bea_name = '';
        $tunggakan = tunggakanSiswaPerTp($_POST['nisn'], $_POST['value'], $_POST['tahunMasukSiswa']);

        $result = '<table id="tableTp" class="table table-striped table-bordered" cellspacing="0" width="100%">';
        $tps = get_all_tp(); 
        $tpAktif = get_tp_aktif();
        $tpAktif = $tpAktif->tp;
        $result .= '<thead>
                    <tr class="success">
                    <th class="col-md-3">
                    <th style="width:1000px;">TUNGGAKAN</th>';
                            

        $result .='</th>
                        <th class="col-md-3">NISN</th>
                        <th class="col-md-3">KELAS</th>
                        <th class="col-md-3">TP</th>
                        <th class="col-md-3">JANUARI</th>
                        <th class="col-md-3">FEBRUARI</th>
                        <th class="col-md-3">MARET</th>
                        <th class="col-md-3">APRIL</th>
                        <th class="col-md-3">MEI</th>
                        <th class="col-md-3">JUNI</th>
                        <th class="col-md-3">JULI</th>
                        <th class="col-md-3">AGUSTUS</th>
                        <th class="col-md-3">SEPTEMBER</th>
                        <th class="col-md-3">OKTOBER</th>
                        <th class="col-md-3">NOVEMBER</th>
                        <th class="col-md-3">DESEMBER</th>
                        <th class="col-md-3">TOTAL</th>
                        <th style="width: 150px;" class="col-md-3">CREATED AT</th>
                        <th style="width: 150px;" class="col-md-3">UPDATED AT</th>
                    </tr>
                </thead>
                <tbody>';
        $i=1; 
            foreach ($detail_bayar as $key => $value){
            $result .= '<tr>';
                switch($key){
                    case 'spp':
                        $result .= '<td>SPP</td><td>Rp.'.number_format($tunggakan['spp'],2,',','.').'</td>';
                        break;
                    case 'pembangunan':
                        $result .= '<td>PEMBANGUNAN</td><td>Rp.'.number_format($tunggakan['pembangunan'],2,',','.').'</td>';
                        break;
                    case 'pramuka':
                        $result .= '<td>PRAMUKA</td><td>Rp.'.number_format($tunggakan['pramuka'],2,',','.').'</td>';
                        break;
                    case 'lain2':
                        $result .= '<td>LAIN-LAIN</td><td>Rp.'.number_format($tunggakan['lain2'],2,',','.').'</td>';
                        break;
                }

        $result .= '<td>'. str_pad($_POST['nisn'], 10, '0', STR_PAD_LEFT) .'</td>';
        $result .= '<td>'. $rombel['nama'] .'</td>';
        $result .= '<td>'. $_POST['value'] .'/'. ($_POST['value']+1) .'</td>';
        $result .= '<td>'. $value->januari .'</td>';
        $result .= '<td>'. $value->februari .'</td>';
        $result .= '<td>'. $value->maret .'</td>';
        $result .= '<td>'. $value->april .'</td>';
        $result .= '<td>'. $value->mei .'</td>';
        $result .= '<td>'. $value->juni .'</td>';
        $result .= '<td class="warning">'. $value->juli .'</td>';
        $result .= '<td class="warning">'. $value->agustus .'</td>';
        $result .= '<td class="warning">'. $value->september .'</td>';
        $result .= '<td class="warning">'. $value->oktober .'</td>';
        $result .= '<td class="warning">'. $value->november .'</td>';
        $result .= '<td class="warning">'. $value->desember.'</td>';
        $result .= '<td>'. number_format($value->total,2,',','.') .'</td>';
        $result .= '<td>'. $value->created_at .'</td>';
        $result .= '<td>'. $value->updated_at .'</td>';
        $result .= '</tr>';
                }
        $result .= '</tbody></table>';        
        // $result = 'Berhasil';
        return $result;
    }

    public function rombelTunggakan($rombelId)
    {
        dd($rombelId);
    }
}
