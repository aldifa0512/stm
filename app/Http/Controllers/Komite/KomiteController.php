<?php

namespace App\Http\Controllers\Komite;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\HistoriSpp;
use App\Models\Komite;
use App\Models\Student;
use App\Models\Madding;
use Auth;
use Validator;
use Storage;

class KomiteController extends Controller
{
	
	function __construct()
	{
		$this->middleware('authKomite');
	}

	public function index()
	{
        $siswas = array(
            'total'     => siswa_5thn_awal(),
            'total_aktif'     => siswa_aktif(),
            'total_pasif'     => get_siswa_pasif_min5(),
            'total_beda'     => siswa_status_beda());
        $data_area = get_siswa_tiap_tahun();
        $data_bar = get_pembayaran_tahunke(get_tp_aktif());
        $data_jurusan = get_jurusan_tiap_tahun(get_tp_aktif()->tp);
        $data_jurusan1 = get_jurusan_tiap_tahun(get_tp_aktif()->tp - 1);
        $data_jurusan2 = get_jurusan_tiap_tahun(get_tp_aktif()->tp - 2);
		$madding = Madding::where('status', 'aktif')->first();
		$historis = HistoriSpp::orderBy('updated_at', 'DESC')->paginate(10);
		return view('komite.pages.dashboard2', [
            'madding' => $madding, 
            'historis' => $historis, 
            'siswas' => $siswas, 
            'data_bar' => $data_bar,
            'data_area' => $data_area,
            'data_jurusan' => $data_jurusan,
            'data_jurusan1' => $data_jurusan1,
            'data_jurusan2' => $data_jurusan2]);
	}


    public function nonstatus(){
        $siswas = get_siswa_status_beda();
        return view('komite.pages.nonstatus', ['siswas' => $siswas]);
    }

	public function edit_info()
    {
        $customer = Komite::find(Auth::komite()->get()->komite_id);
        
        if ($customer) {
            return view('komite.pages.account.edit', ['customer' => $customer]);
        }

        return abort(404, 'Request not found');
    }
    public function update_info(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$id,
            'password' => 'required|confirmed|min:6',
            ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $customer = Komite::find($id);

        if ($customer) {
            $customer->komite_name = $request->input('name');
            $customer->pic_email = $request->input('email');
            $customer->password = bcrypt($request->input('password'));

            if ($customer->save()) {
                return redirect('komite/edit_info')->with('success', 'User updated success.');
            }
        }
    }

    public function databasesBackups(){
        $directory = public_path().'/assets/backups' ;
        $files = scandir($directory);
        return view('komite.pages.laporan.databasesBackups', ['files' => $files]);
    }

    public function databasesBackupsRun(){
        $backup = shell_exec('cd .. & BackupMysql.bat');
        // $backup = shell_exec('cd .. & dir');
        if($backup !== null){
            return redirect('komite/databasesBackups')->with('success', 'Database berhasil di backup, silahkan download pada tabel dibawah.');
        }else{
            var_dump($backup);die();
            return redirect('komite/databasesBackups')->with('error', 'Gangguan saat membackup database.');
        }
        
    }

    public function databasesBackupsDownload($path){
        $pathToFile = public_path().'/assets/backups/' . $path ;
        return response()->download($pathToFile);
    }

    public function databasesBackupsDelete($path){
        echo "<form action='" . url('komite/databasesBackups/delete/' . $path . '/konfirm') ."' method='post'>";
        echo "<input type='hidden' name='_token' value='" . csrf_token() . "'>";
        echo "<input type='hidden' name='path' value='" . $path . "'>";
        echo "Apakah anda yakin ingin menghapus file ini ? <br>";
        echo "Iya<input type='radio' name='delete' value='iya' checked='checked'><br/><br/>";
        echo "Tidak<input type='radio' name='delete' value='tidak'><br/><br/>";
        echo "<input type='submit' name='submit' value='submit'>";
        echo "</form>";
        
    }

    public function databasesBackupsDeleteKonfirm(Request $request, $path){
        if( $request->input('delete') == 'iya'){
            $pathToFile = public_path().'/assets/backups/' . $path ;
            $delteFile = unlink($pathToFile);
            if($delteFile)
                return redirect('komite/databasesBackups')->with('success', 'Backup file deleted success.');
        }else{
            return redirect('komite/databasesBackups');
        }
    }
}