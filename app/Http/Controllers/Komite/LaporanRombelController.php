<?php

namespace App\Http\Controllers\Komite;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\Customer;
use App\Models\Listing;
use App\Models\Student;
use App\Models\MasterSPP;
use App\Models\Reff;
use App\Models\Rombel;
use App\Models\Pembangunan;
use App\Models\LainLain;
use App\Models\Pramuka;
use App\Models\Spp;
use App\Models\HistoriSpp;
use App\Models\Tp;
use Excel;
use Storage;
use Image;
use Setting;
use App\Models\ListingMeta;
use Session;
use App\Models\ListingCategory;

class LaporanRombelController extends Controller
{
    public function lap_lengkap($kelas)
    {
        $rombel = Rombel::where('id', $kelas)->select('nama', 'pengajar')->first()->toArray();
        $filename = 'Data Keuangan Kelas - '.$rombel['nama'];
        $students = Student::where('kelas', $kelas)->select('nama_lengkap')->get();
        $cs = Student::where('kelas', $kelas)->select('nama_lengkap')->count();
        foreach ($students as $student) {
            if($student->tp == null)
                 return redirect('komite/laporan/rombel')->withErrors('Data tahun masuk siswa belum diisi seluruhnya. Silahkan isi data tahun masuk siswa terlebih dahulu.');
        }
        if($cs == 0)
            return redirect('komite/laporan/rombel')->withErrors('Rombel belum memiliki data siswa untuk dihitung laporan keuangannya. Silahkan isi data siswa terlebih dahulu.');
        $Mspp = MasterSPP::where('status', 'aktif')->first()->toArray();
        
        $histori=Spp::where('kelas', $kelas)->get()->toArray();//dd($histori);
        $Jhistori=Spp::where('kelas', $kelas)->get()->count();//dd($histori);

        $pem=Pembangunan::where('kelas', $kelas)->get()->toArray();//dd($histori);
        //$JPem=pembanguna::where('kelas', $kelas)->get()->count();//dd($histori);

        $pra=Pramuka::where('kelas', $kelas)->get()->toArray();//dd($histori);

        Excel::create($filename, function($excel) use ($students, $filename, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('SMKN 2 PAYAKUMBUH')
                  ->setCompany('SMKN 2 PAYAKUMBUH');

            $excel->sheet($filename, function($sheet) use ($students, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {
                //no dan nama header merging MERGING VERTICAL
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:B5');
                
                //MERGING HORIZONTAL ROW A1 FOR HEADER
                $sheet->mergeCells('A1:N1');
                $sheet->mergeCells('C4:F4');
                $sheet->mergeCells('G4:J4');
                $sheet->mergeCells('K4:N4');

                $sheet->row(1, array('DATA KEUANGAN SISWA SMKN 2 PAYAKUMBUH PER ' . date('j-n-Y'),'','','',
                    '',',' ,'','','','','',''));

                $sheet->row(3, array('KELAS : '. $rombel['nama'],'','','','','','','','','','','WALI KELAS : '.$rombel['pengajar'],'',''
                ));
                $sheet->row(4, array(
                     'No', 'NAMA', 
                     'BESARAN IURAN', '', '', '', 
                     'YANG TELAH DIBAYAR', '', '', '', 
                     'TUNGGAKAN', '', '', ''
                ));
                $sheet->row(5, array(
                     '',  '',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH'
                ));

                //SET ALIGNMENT CENTER FOR HEADER
                $sheet->row(4, function($row) {
                    $row->setAlignment('center');
                });
                
                $sheet->row(5, function($row) {
                    $row->setAlignment('center');
                });

                $sheet->row(1, function($row) {
                    $row->setAlignment('center');
                });

                //SETTING WIDTH CELL
                $sheet->setWidth(array(
                    'A'     =>  4,
                    'B'     =>  25,

                    'C'     =>  15,
                    'D'     =>  15,
                    'E'     =>  15,
                    'F'     =>  15,

                    'G'     =>  15,
                    'H'     =>  15,
                    'I'     =>  15,
                    'J'     =>  15,

                    'K'     =>  15,
                    'L'     =>  15,
                    'M'     =>  15,
                    'N'     =>  15
                ));

                $row = 6;
                $no = 1;
                $TotSpp = array();
                $TotPem = array();
                $TotPra = array();
                $TotBayar = array();
                $H_Spp = array();
                $H_Pem = array();
                $H_Pra = array();
                $H_Bayar = array();
                foreach ($students as $student) {
                    if($no <= $Jhistori){
                    $Pspp = $histori[$no-1]['januari'] + $histori[$no-1]['februari'] + $histori[$no-1]['maret'] + $histori[$no-1]['april'] + $histori[$no-1]['mei'] + $histori[$no-1]['juni'] + $histori[$no-1]['juli'] + $histori[$no-1]['agustus'] + $histori[$no-1]['september'] + $histori[$no-1]['oktober'] + $histori[$no-1]['november'] + $histori[$no-1]['desember'];
                    $Ppem = $pem[$no-1]['januari'] + $pem[$no-1]['februari'] + $pem[$no-1]['maret'] + $pem[$no-1]['april'] + $pem[$no-1]['mei'] + $pem[$no-1]['juni'] + $pem[$no-1]['juli'] + $pem[$no-1]['agustus'] + $pem[$no-1]['september'] + $pem[$no-1]['oktober'] + $pem[$no-1]['november'] + $pem[$no-1]['desember'];
                    $Ppra = $pra[$no-1]['januari'] + $pra[$no-1]['februari'] + $pra[$no-1]['maret'] + $pra[$no-1]['april'] + $pra[$no-1]['mei'] + $pra[$no-1]['juni'] + $pra[$no-1]['juli'] + $pra[$no-1]['agustus'] + $pra[$no-1]['september'] + $pra[$no-1]['oktober'] + $pra[$no-1]['november'] + $pra[$no-1]['desember'];
                    }else{
                        $Pspp = 0;
                        $Ppem = 0;
                        $Ppra = 0;
                    }

                    $Mspp = MasterSPP::where('tp', $student->tp)->first()->toArray();
                    $Tspp = $Mspp['spp'] * 12;
                    $Tpem = $Mspp['pembangunan'];
                    $Tpra = $Mspp['pramuka'] * 12;
                    $Tbayar = $Tspp + $Tpem + $Tpra;
                    $Pbayar = $Pspp + $Ppem + $Ppra;
                    $Hspp = $Tspp - $Pspp;
                    $Hpem = $Tpem - $Ppem;
                    $Hpra = $Tpra - $Ppra;
                    $Hbayar = $Tbayar - $Pbayar ;
                    
                    //push telah dibaar data ke dalam array total seluruh siswa
                    array_push($TotSpp, $Pspp);
                    array_push($TotPem, $Ppem);
                    array_push($TotPra, $Ppra);
                    array_push($TotBayar, $Pbayar);
                    array_push($H_Spp, $Hspp);
                    array_push($H_Pem, $Hpem);
                    array_push($H_Pra, $Hpra);
                    array_push($H_Bayar, $Hbayar);

                    $sheet->row($row, array(
                        $no, $student->nama_lengkap, $Tspp, $Tpem, $Tpra, $Tbayar, $Pspp, $Ppem, $Ppra, $Pbayar, $Hspp, $Hpem, $Hpra, $Hbayar
                    ));
                    $row++;
                    $no++;
                }

                $no = $no - 1;
                $sheet->row(($row+1), array(
                        '', 'JUMLAH', ($Tspp * $no), ($Tpem * $no), ($Tpra * $no), ($Tbayar * $no),  array_sum($TotSpp), array_sum($TotPem), array_sum($TotPra), array_sum($TotBayar), array_sum($H_Bayar), array_sum($H_Bayar), array_sum($H_Bayar), array_sum($H_Bayar) 
                ));

                $sheet->cells('A1', function($cells) {
                    $cells->setFont(array(
                        'size'       => '14',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('L3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A4:N5', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                });

                //$sheet->setBorder('A4:N'.($row+1), 'thin');
                $sheet->setAllBorders('thin');
            });
        
        })->export('xlsx');

    }

    public function lap_tunggakan($kelas)
    {
        $rombel = Rombel::where('id', $kelas)->select('nama', 'pengajar')->first()->toArray();
        $filename = 'Data Tunggakan Kelas - '.$rombel['nama'];
        $students = Student::where('kelas', $kelas)->select('nama_lengkap')->get();
        $cs = Student::where('kelas', $kelas)->select('nama_lengkap')->count();
        foreach ($students as $student) {
            if($student->tp == null)
                 return redirect('komite/laporan/rombel')->withErrors('Data tahun masuk siswa belum diisi seluruhnya. Silahkan isi data tahun masuk siswa terlebih dahulu.');
        }
        if($cs == 0)
            return redirect('komite/laporan/rombel')->withErrors('Rombel belum memiliki data siswa untuk dihitung tunggakannya. Silahkan isi data siswa terlebih dahulu.');
        $Mspp = MasterSPP::where('status', 'aktif')->first()->toArray();
        
        $histori=Spp::where('kelas', $kelas)->get()->toArray();//dd($histori);
        $Jhistori=Spp::where('kelas', $kelas)->get()->count();//dd($histori);

        $pem=Pembangunan::where('kelas', $kelas)->get()->toArray();//dd($histori);
        //$JPem=pembanguna::where('kelas', $kelas)->get()->count();//dd($histori);

        $pra=Pramuka::where('kelas', $kelas)->get()->toArray();//dd($histori);

        Excel::create($filename, function($excel) use ($students, $filename, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('SMKN 2 PAYAKUMBUH')
                  ->setCompany('SMKN 2 PAYAKUMBUH');

            $excel->sheet($filename, function($sheet) use ($students, $Mspp, $histori, $pem, $pra, $Jhistori, $rombel) {
                //no dan nama header merging MERGING VERTICAL
                $sheet->mergeCells('A4:A5');
                $sheet->mergeCells('B4:B5');
                
                //MERGING HORIZONTAL ROW A1 FOR HEADER
                $sheet->mergeCells('A1:F1');
                $sheet->mergeCells('C4:F4');
                $sheet->mergeCells('G4:J4');
                $sheet->mergeCells('K4:N4');

                $sheet->row(1, array('DAFTAR TUNGGAKAN PER ' . date('j-n-Y'),'','','',
                    '',',' ,'','','','','',''));

                $sheet->row(3, array('KELAS : '. $rombel['nama'],'','','','WALI KELAS : '.$rombel['pengajar']
                ));
                $sheet->row(4, array(
                     'No', 'NAMA', 
                     'TUNGGAKAN', '', '', '', 
                     '', '', '', '', 
                     '', '', '', ''
                ));
                $sheet->row(5, array(
                     '',  '',
                     'SPP',  'PEMBANGUNAN', 'PRAMUKA', 'JUMLAH',
                     '',  '', '', '',
                     '',  '', '', ''
                ));

                //SET ALIGNMENT CENTER FOR HEADER
                $sheet->row(4, function($row) {
                    $row->setAlignment('center');
                });
                
                $sheet->row(5, function($row) {
                    $row->setAlignment('center');
                });

                $sheet->row(1, function($row) {
                    $row->setAlignment('center');
                });

                //SETTING WIDTH CELL
                $sheet->setWidth(array(
                    'A'     =>  4,
                    'B'     =>  25,

                    'C'     =>  15,
                    'D'     =>  15,
                    'E'     =>  15,
                    'F'     =>  15,

                    'G'     =>  15,
                    'H'     =>  15,
                    'I'     =>  15,
                    'J'     =>  15,

                    'K'     =>  15,
                    'L'     =>  15,
                    'M'     =>  15,
                    'N'     =>  15
                ));

                $row = 6;
                $no = 1;
                $TotSpp = array();
                $TotPem = array();
                $TotPra = array();
                $TotBayar = array();
                $H_Spp = array();
                $H_Pem = array();
                $H_Pra = array();
                $H_Bayar = array();
                foreach ($students as $student) {
                    if($no <= $Jhistori){
                    $Pspp = $histori[$no-1]['januari'] + $histori[$no-1]['februari'] + $histori[$no-1]['maret'] + $histori[$no-1]['april'] + $histori[$no-1]['mei'] + $histori[$no-1]['juni'] + $histori[$no-1]['juli'] + $histori[$no-1]['agustus'] + $histori[$no-1]['september'] + $histori[$no-1]['oktober'] + $histori[$no-1]['november'] + $histori[$no-1]['desember'];
                    $Ppem = $pem[$no-1]['januari'] + $pem[$no-1]['februari'] + $pem[$no-1]['maret'] + $pem[$no-1]['april'] + $pem[$no-1]['mei'] + $pem[$no-1]['juni'] + $pem[$no-1]['juli'] + $pem[$no-1]['agustus'] + $pem[$no-1]['september'] + $pem[$no-1]['oktober'] + $pem[$no-1]['november'] + $pem[$no-1]['desember'];
                    $Ppra = $pra[$no-1]['januari'] + $pra[$no-1]['februari'] + $pra[$no-1]['maret'] + $pra[$no-1]['april'] + $pra[$no-1]['mei'] + $pra[$no-1]['juni'] + $pra[$no-1]['juli'] + $pra[$no-1]['agustus'] + $pra[$no-1]['september'] + $pra[$no-1]['oktober'] + $pra[$no-1]['november'] + $pra[$no-1]['desember'];
                    }else{
                        $Pspp = 0;
                        $Ppem = 0;
                        $Ppra = 0;
                    }
                    $Mspp = MasterSPP::where('tp', $student->tp)->first()->toArray();
                    $Tspp = $Mspp['spp'] * 12;
                    $Tpem = $Mspp['pembangunan'];
                    $Tpra = $Mspp['pramuka'] * 12;
                    $Tbayar = $Tspp + $Tpem + $Tpra;
                    $Pbayar = $Pspp + $Ppem + $Ppra;
                    $Hspp = $Tspp - $Pspp;
                    $Hpem = $Tpem - $Ppem;
                    $Hpra = $Tpra - $Ppra;
                    $Hbayar = $Tbayar - $Pbayar ;
                    
                    //push telah dibaar data ke dalam array total seluruh siswa
                    array_push($TotSpp, $Pspp);
                    array_push($TotPem, $Ppem);
                    array_push($TotPra, $Ppra);
                    array_push($TotBayar, $Pbayar);
                    array_push($H_Spp, $Hspp);
                    array_push($H_Pem, $Hpem);
                    array_push($H_Pra, $Hpra);
                    array_push($H_Bayar, $Hbayar);

                    $sheet->row($row, array(
                        $no, $student->nama_lengkap, /*$Tspp, $Tpem, $Tpra, $Tbayar, $Pspp, $Ppem, $Ppra, $Pbayar,*/ $Hspp, $Hpem, $Hpra, $Hbayar
                    ));
                    $row++;
                    $no++;
                }

                $no = $no - 1;
                $sheet->row(($row+1), array(
                        '', 'JUMLAH', /*($Tspp * $no), ($Tpem * $no), ($Tpra * $no), ($Tbayar * $no),  array_sum($TotSpp), array_sum($TotPem), array_sum($TotPra), array_sum($TotBayar),*/ array_sum($H_Spp), array_sum($H_Pem), array_sum($H_Pra), array_sum($H_Bayar) 
                ));

                $sheet->row(($row+3), array(
                    '', '','', '', "Payakumbuh, " . date('j F Y'),'' 
                ));

                $sheet->row(($row+5), array(
                    '', '','', '', "a/n Bendahara Komite",'' 
                ));

                $sheet->row(($row+8), array(
                    '', '', '','', "Aulia Rahmi S.Pd",'' 
                ));

                $sheet->cells('A1', function($cells) {
                    $cells->setFont(array(
                        'size'       => '14',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A3:F3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('L3', function($cells) {
                    $cells->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('A4:N5', function($cells) {
                    $cells->setFont(array(
                        'size'       => '11',
                        'bold'       =>  true
                    ));
                });

                //$sheet->setBorder('A4:N'.($row+1), 'thin');
                $sheet->setAllBorders('thin');
            });
        
        })->export('xlsx');

    }

}
