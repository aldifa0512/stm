<?php
namespace App\Libraries;

use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Models\Student;
use App\Models\Jurusan;
use App\Models\Rombel;

class ExcelLibrary
{
    public static function worksheetNameGenerator($name)
    {
        // First, strip out characters which aren't allowed
        $illegal_chars = array(':', '\\', '/', '?', '*', '[', ']');
        for ($i = 0; $i < count($illegal_chars); $i++)
            $name = str_replace($illegal_chars[$i], '', $name);
        // Now, if name is longer than 31 chars, truncate it
        if (strlen($name) > 31)
            $name = substr($name, 0, 31);
        return $name;
    }

    public static function fontCenter($size = 18, $type = 'Calibri', $bold = FALSE){
        $style = array(
            'font' => array(
                'bold'  => $bold,
                'size'  => $size,
                'name'  => $type
            ),
            'alignment' => array(
                'horizontal' => 'center',
                'vertical' => 'center',
            )
        );
        return $style;
    }

    public static function cetakAbsen($students, $rombel, $tpAktif, $tanggalCetak, $jurusan){
    	$objPHPExcel = new PHPExcel(); 
        $bord = $objPHPExcel->Gen_PHPExcel_Style_Border();
        $fill = $objPHPExcel->Gen_PHPExcel_Style_Fill();
        $align = $objPHPExcel->Gen_PHPExcel_Style_Align();
        $drawing = $objPHPExcel->Gen_PHPExcel_Worksheet_Drawing();
        $objPHPExcel->getProperties()->setCreator("SMKN 2 PAYAKUMBUH")
                                     ->setLastModifiedBy('')
                                     ->setTitle('ABSEN ' . $rombel->nama)
                                     ->setSubject('ABSEN ' . $rombel->nama)
                                     ->setDescription('ABSEN ' . $rombel->nama)
                                     ->setKeywords('ABSEN ' . $rombel->nama)
                                     ->setCategory('ABSEN ' . $rombel->nama);
        // create style
        $default_border = array(
            'style' => $bord::BORDER_THIN,
            'color' => array('rgb'=>'#000000')
        );
        $h1 = self::fontCenter(16, 'Times New Roman', TRUE);
        $h2 = self::fontCenter(18, 'Times New Roman', TRUE);
        $h3 = self::fontCenter(13, 'Times New Roman', TRUE);
        $h4 = self::fontCenter(15, 'Times New Roman', TRUE);
        $h5 = self::fontCenter(11, 'Times New Roman', FALSE);
        $h6 = self::fontCenter(10, 'Times New Roman', FALSE);
        $h7 = self::fontCenter(14, 'Times New Roman', TRUE);
        $h8 = self::fontCenter(13, null, TRUE);
        $borderBottom = array(
            'borders' => array(
                'bottom' => array(
                            'style' => $bord::BORDER_THICK,
                            'color' => array('rgb'=>'#000000')
                        )
            )
        );
        $content = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            )
        );
        $center = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $centerBold = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'size' => 13
            )
        );
        $bold = array(
            'font' => array(
                'bold' => true
            )
        );
        $left = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_LEFT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_RIGHT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );

        // Create Header
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', 'PEMERINTAH PROVINSI SUMATERA BARAT')
                    ->setCellValue('A3', 'DINAS PENDIDIKAN')
                    ->setCellValue('A4', 'SEKOLAH MENENGAH KEJURUAN NEGERI 2 PAYAKUMBUH')
                    ->setCellValue('A5', 'KOTA PAYAKUMBUH')
                    ->setCellValue('A6', 'Jln. Soekarno Hatta / Anggrek I Payakumbuh Telp/Fax No: (0752)92123')
                    ->setCellValue('A7', 'Email: smkn2_pyk@yahoo.co.id, Website: www.smkn2pyk.sch.id')
                    ->setCellValue('A8', 'DAFTAR HADIR PESERTA DIKLAT')
                    ->setCellValue('A9', 'TAHUN DIKLAT ' . $tpAktif->tp . '/' . ($tpAktif->tp + 1));
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A2:AB2')
                    ->mergeCells('A3:AB3')
                    ->mergeCells('A4:AB4')
                    ->mergeCells('A5:AB5')
                    ->mergeCells('A6:AB6')
                    ->mergeCells('A7:AB7')
                    ->mergeCells('A8:AB8')
                    ->mergeCells('A9:AB9');
        $objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray( $h1 );
        $objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray( $h2 );
        $objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray( $h3 );
        $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray( $h4 );
        $objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray( $h5 );
        $objPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray( $h6 );
        $objPHPExcel->getActiveSheet()->getStyle('A8:A9')->applyFromArray( $h7 );
        $objPHPExcel->getActiveSheet()->getStyle('A10:AC11')->applyFromArray( $h8 );
        $objPHPExcel->getActiveSheet()->getStyle('A10:AC11')->applyFromArray( $left );
        $objPHPExcel->getActiveSheet()->getStyle('A7:AB7')->applyFromArray( $borderBottom );
        $objPHPExcel->getActiveSheet()->getStyle('A3:AB9')->applyFromArray( $center );
        //-------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A10', 'Program Keahlian')
                    ->setCellValue('D10', ': ' . $jurusan )
                    ->setCellValue('A11', 'Kelas')
                    ->setCellValue('D11', ': ' . $rombel->nama );
        //-------------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A12', 'NO')
                    ->setCellValue('B12', 'NISN')
                    ->setCellValue('C12', 'NAMA PESERTA')
                    ->setCellValue('D12', 'L/P')
                    ->setCellValue('E12', 'SENIN')
                    ->setCellValue('I12', 'SELASA')
                    ->setCellValue('N12', 'RABU')
                    ->setCellValue('S12', 'KAMIS')
                    ->setCellValue('Y12', 'JUMAT');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('E13', 'tgl:')
                    ->setCellValue('I13', 'tgl:')
                    ->setCellValue('N13', 'tgl:')
                    ->setCellValue('S13', 'tgl:')
                    ->setCellValue('Y13', 'tgl:');

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A12:A16')
                    ->mergeCells('B12:B16')
                    ->mergeCells('C12:C16')
                    ->mergeCells('D12:D16');

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('E12:H12')
                    ->mergeCells('I12:M12')
                    ->mergeCells('N12:R12')
                    ->mergeCells('S12:X12')
                    ->mergeCells('Y12:AB12');

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('E13:H13')
                    ->mergeCells('I13:M13')
                    ->mergeCells('N13:R13')
                    ->mergeCells('S13:X13')
                    ->mergeCells('Y13:AB13');
        $objPHPExcel->getActiveSheet()->getStyle('A12:AB12')->applyFromArray( $center );
        //-------------------------------------------------------------------------------
        $letters = array();
        foreach (range('E', 'Z') as $char) {
            $letters[] = $char;
        }
        $letters3 = array();
        foreach (range('A', 'Z') as $char) {
            $letters3[] = $char;
        }

        foreach ($letters as $key => $value) {
            $objPHPExcel->getActiveSheet()->mergeCells($value.'14:'.$value.'16');
        }
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('AA14:AA16')
                    ->mergeCells('AB14:AB16');
        
        for($i = 12; $i <17; $i++){
            foreach ($letters3 as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$i)->applyFromArray( $content );
            }
            $objPHPExcel->getActiveSheet()->getStyle('AA'.$i)->applyFromArray( $content );
            $objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray( $content );
        }
        //-------------------------------------------------------------------------------
        for ($col = ord('E'); $col <= ord('Z'); $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($col))->setWidth(3);
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(3);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(3);
        //-------------------------------------------------------------------------------
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(33);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
        //-------------------------------------------------------------------------------
        
        $letters2 = array();
        foreach (range('A', 'Z') as $char) {
            $letters2[] = $char;
        }

        $dataku= $students;
        $i = 0;
        $urut =0;
        foreach($dataku as $key => $value)
        {
            $urut=$i+17;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$urut, $i+1)
                    ->setCellValue('B'.$urut, nisn_format($value['nisn']))
                    ->setCellValue('C'.$urut, strtoupper ($value['nama_lengkap']))
                    ->setCellValue('D'.$urut, strtoupper ($value['jenis_kelamin']));

            
            $objPHPExcel->getActiveSheet()->getStyle('A' . $urut)->applyFromArray( $left );
            $objPHPExcel->getActiveSheet()->getStyle('B' . $urut)->applyFromArray( $left );
            $objPHPExcel->getActiveSheet()->getStyle('C' . $urut)->applyFromArray( $left );
            $objPHPExcel->getActiveSheet()->getStyle('D' . $urut)->applyFromArray( $left );
            $i++;

            foreach ($letters2 as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$urut)->applyFromArray( $content );
            }
            $objPHPExcel->getActiveSheet()->getStyle('AA'.$urut)->applyFromArray( $content );
            $objPHPExcel->getActiveSheet()->getStyle('AB'.$urut)->applyFromArray( $content );
        }
        //-------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $urut, 'JUMLAH HADIR')
                    ->setCellValue('A' . ($urut + 1), 'PARAF GURU');
        
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('A' . $urut . ':D' . $urut)
                    ->mergeCells('A' . ($urut + 1) . ':D' . ($urut + 3));

        foreach ($letters as $key => $value) {
            $objPHPExcel->getActiveSheet()->mergeCells($value . ($urut + 1) . ':' . $value . ($urut + 3));
        }
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('AA' . ($urut + 1) . ':AA' . ($urut + 3))
                    ->mergeCells('AB' . ($urut + 1) . ':AB' . ($urut + 3));

        for($i = $urut; $i < ($urut + 4); $i++){
            foreach ($letters3 as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$i)->applyFromArray( $content );
            }
            $objPHPExcel->getActiveSheet()->getStyle('AA'.$i)->applyFromArray( $content );
            $objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray( $content );
        }

        $objPHPExcel->getActiveSheet()->getStyle('A' . $urut . ':A' . ($urut + 4))->applyFromArray( $center );
        $objPHPExcel->getActiveSheet()->getStyle('A' . $urut . ':A' . ($urut + 4))->applyFromArray( $bold );
        //-------------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('P' . ($urut + 6), 'Payakumbuh, ')
                    ->setCellValue('X' . ($urut + 6), $tpAktif->tp)
                    ->setCellValue('B' . ($urut + 7), 'Kaprog : ..........................')
                    ->setCellValue('P' . ($urut + 7), 'Wali Kelas')
                    ->setCellValue('B' . ($urut + 11), '(                                                  )')
                    ->setCellValue('P' . ($urut + 11), '(                                                  )')
                    ->setCellValue('B' . ($urut + 12), 'NIP : ')
                    ->setCellValue('P' . ($urut + 12), 'NIP : ');        
        
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('X' . ($urut + 6) . ':AB' . ($urut + 6));
        $objPHPExcel->getActiveSheet()->getStyle('X' . ($urut + 6) . ':AB' . ($urut + 6))->applyFromArray( $left );
        $objPHPExcel->getActiveSheet()->getStyle('P' . ($urut + 7))->applyFromArray( $bold );
        //-------------------------------------------------------------------------------
        $objDrawing = $drawing;
        $objDrawing->setName('Logo Sumatera Barat');
        $objDrawing->setDescription('Logo Sumatera Barat');
        //Path to signature .jpg file
        $signature = public_path() . '/assets/backend/img/sumbar.png';    
        $objDrawing->setPath($signature);
        $objDrawing->setOffsetX(0);                     //setOffsetX works properly
        $objDrawing->setOffsetY(0);                     //setOffsetY works properly
        $objDrawing->setCoordinates('B2');             //set image to cell
        $objDrawing->setWidth(120);  
        $objDrawing->setHeight(100);                     //signature height  
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());  //save
        //-------------------------------------------------------------------------------

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('ABSEN - ' . $rombel->nama);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ABSEN - ' . $rombel->nama . '.xls"'); // file name of excel
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public static function getTanggalCetak(){
        $bulans = Bulan_laporan();
        $hari = date('d');
        $bulan = $bulans[date('m')];
        $tahun = date('Y');

        $hasil = $hari . ' ' . $bulan . ' ' . $tahun;
        return $hasil;

    }

    public static function rombelTunggakan($rombelId)
    {
        $tunggakan = array();
        $tpAktif = get_tp_aktif();
        $bulans = Bulan();
        $students = Student::where('kelas', $rombelId)->where('status', 'aktif')->select('nisn', 'nama_lengkap', 'tp')->orderBy('nama_lengkap')->get();
        $rombel = Rombel::find($rombelId);
        $tanggalCetak = self::getTanggalCetak();

        $sppTotal = 0;
        $pembangunanTotal = 0;
        $pramukaTotal = 0;
        $lain2Total = 0;
        $totalVertical = 0;

        $sppMinTotal = 0;
        $pembangunanMinTotal = 0;
        $pramukaMinTotal = 0;
        $lain2MinTotal = 0;
        $totalMinVertical = 0;

        $sppAMTotal = 0;
        $pembangunanAMTotal = 0;
        $pramukaAMTotal = 0;
        $lain2AMTotal = 0;
        $totalAMVertical = 0;

        $tunggakanKelas = 0;
        $totalAktifdanMin = array();
        foreach ($students as $key => $value) {
            $nisn = $value->nisn;
            $batasTunggakan = get_batas_tunggakan_biaya_sekolah($value->nisn);
            if($batasTunggakan == FALSE){
                return redirect()->back()->withErrors('Batas Tunggakan ' . $value->nama_lengkap . ' dengan nisn ' . nisn_format($value->nisn) . ' tidak dapat dihitung.');
            }
            $tunggakanAktif = tunggakan_tahun_aktif($nisn, $tpAktif->tp, $value->tp, $batasTunggakan['spp'], $batasTunggakan['pembangunan'], $batasTunggakan['pramuka'], $batasTunggakan['lain2']);
            $tunggakanMinsatu = hitungAkumTunggTotal($tpAktif->tp, $value->tp, $value->nisn);
            $totalTunggakan = $tunggakanAktif['total'] + $tunggakanMinsatu['total'];
            $totalAktifdanMin = array(
                'spp' => $tunggakanAktif['spp'] + $tunggakanMinsatu['spp'],
                'pembangunan' => $tunggakanAktif['pembangunan'] + $tunggakanMinsatu['pembangunan'],
                'pramuka' => $tunggakanAktif['pramuka'] + $tunggakanMinsatu['pramuka'],
                'lain2' => $tunggakanAktif['lain2'] + $tunggakanMinsatu['lain2']
                );

            $sppTotal += $tunggakanAktif['spp'];
            $pembangunanTotal += $tunggakanAktif['pembangunan'];
            $pramukaTotal += $tunggakanAktif['pramuka'];
            $lain2Total += $tunggakanAktif['lain2'];
            $totalVertical += $tunggakanAktif['total'];

            $sppMinTotal += $tunggakanMinsatu['spp'];
            $pembangunanMinTotal += $tunggakanMinsatu['pembangunan'];
            $pramukaMinTotal += $tunggakanMinsatu['pramuka'];
            $lain2MinTotal += $tunggakanMinsatu['lain2'];
            $totalMinVertical += $tunggakanMinsatu['total'];

            $sppAMTotal += $totalAktifdanMin['spp'];
            $pembangunanAMTotal += $totalAktifdanMin['pembangunan'];
            $pramukaAMTotal += $totalAktifdanMin['pramuka'];
            $lain2AMTotal += $totalAktifdanMin['lain2'];

            $tunggakanKelas += $totalTunggakan;
            $tunggakan[$value->nisn] = array(
                'nisn' => $value->nisn,
                'nama_lengkap' => $value->nama_lengkap,
                'tunggakan' => $tunggakanAktif,
                'tunggakan-1' => $tunggakanMinsatu,
                'tunggakanTot' => $totalAktifdanMin,
                'total' => $totalTunggakan
                );
        }

        // dd($tunggakan);
        //-----------------------------------------------------------------------------

        $objPHPExcel = new PHPExcel(); 
        $bord = $objPHPExcel->Gen_PHPExcel_Style_Border();
        $fill = $objPHPExcel->Gen_PHPExcel_Style_Fill();
        $align = $objPHPExcel->Gen_PHPExcel_Style_Align();
        $objPHPExcel->getProperties()->setCreator("SMKN 2 PAYAKUMBUH")
                                     ->setLastModifiedBy("SMKN 2 PAYAKUMBUH")
                                     ->setTitle("SMKN 2 PAYAKUMBUH")
                                     ->setSubject("SMKN 2 PAYAKUMBUH")
                                     ->setDescription("SMKN 2 PAYAKUMBUH")
                                     ->setKeywords("SMKN 2 PAYAKUMBUH")
                                     ->setCategory("SMKN 2 PAYAKUMBUH");
        // create style
        $default_border = array(
            'style' => $bord::BORDER_THIN,
            'color' => array('rgb'=>'#000000')
        );
        $style_header = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
            'fill' => array(
                'type' => $fill::FILL_SOLID,
                'color' => array('rgb'=>'E1E0F7'),
            ),
            'font' => array(
                'bold' => true,
                'size' => 10,
            ),
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $content = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            )
        );
        $center = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $centerBold = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true,
                'size' => 13
            )
        );
        $left = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_LEFT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_RIGHT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $style_border = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            ),
        );

        // Create Header
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B2', 'DATA TUNGGAKAN KELAS ' . $rombel->nama)
                    ->setCellValue('B3', 'TERHITUNG HINGGA ' . strtoupper($bulans[$batasTunggakan['spp']]) . ' ' . $tpAktif->tp . '/' . ($tpAktif->tp + 1));
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B2:S2')
                    ->mergeCells('B3:S3');
        $objPHPExcel->getActiveSheet()->getStyle('B2:O3')->applyFromArray( $centerBold );
        //-------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B4', 'Dikeluarkan pada ' . $tanggalCetak)
                    ->setCellValue('B5', 'Wali Kelas : ' . $rombel->pengajar);
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B4:S4')
                    ->mergeCells('B5:S5');
        $objPHPExcel->getActiveSheet()->getStyle('B4:S5')->applyFromArray( $right );
        // $objPHPExcel->getActiveSheet()->getStyle('B5:55')->applyFromArray( $right );
        //-------------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B6', 'NO')
                    ->setCellValue('C6', 'NISN')
                    ->setCellValue('D6', 'NAMA')
                    ->setCellValue('E6', 'TUNGGAKAN AKTIF')
                    ->setCellValue('J6', 'TUNGGAKAN -1')
                    ->setCellValue('O6', 'JUMLAH TUNGGAKAN')
                    ->setCellValue('S6', 'TOTAL ');
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('E7', 'SPP')
                    ->setCellValue('F7', 'PEMBANGUNAN')
                    ->setCellValue('G7', 'PRAMUKA')
                    ->setCellValue('H7', 'LAIN-LAIN')
                    ->setCellValue('I7', 'TOTAL')
                    ->setCellValue('J7', 'SPP')
                    ->setCellValue('K7', 'PEMBANGUNAN')
                    ->setCellValue('L7', 'PRAMUKA')
                    ->setCellValue('M7', 'LAIN-LAIN')
                    ->setCellValue('N7', 'TOTAL')
                    ->setCellValue('O7', 'SPP')
                    ->setCellValue('P7', 'PEMBANGUNAN')
                    ->setCellValue('Q7', 'PRAMUKA')
                    ->setCellValue('R7', 'LAIN-LAIN');

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('E6:I6')
                    ->mergeCells('B6:B7')
                    ->mergeCells('C6:C7')
                    ->mergeCells('D6:D7')
                    ->mergeCells('J6:N6')
                    ->mergeCells('O6:R6')
                    ->mergeCells('S6:S7');

        $letters = array();
        foreach (range('B', 'S') as $char) {
            $letters[] = $char;
        }

        for($i = 6 ; $i <= 7; $i++){
            foreach ($letters as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$i.':'.$value.$i)->applyFromArray( $style_header );
            }
        }

        for ($col = ord('B'); $col <= ord('S'); $col++)
        {
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($col))->setWidth(15);
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(33);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        //-------------------------------------------------------------------------------
        
        $dataku= $tunggakan;
        $i = 0;
        $urut =0;
        foreach($dataku as $key => $value)
        {
            $urut=$i+8;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$urut, $i+1)
                    ->setCellValue('C'.$urut, nisn_format($value['nisn']))
                    ->setCellValue('D'.$urut, strtoupper ($value['nama_lengkap']))
                    ->setCellValue('E'.$urut, $value['tunggakan']['spp'])
                    ->setCellValue('F'.$urut, $value['tunggakan']['pembangunan'])
                    ->setCellValue('G'.$urut, $value['tunggakan']['pramuka'])
                    ->setCellValue('H'.$urut, $value['tunggakan']['lain2'])
                    ->setCellValue('I'.$urut, $value['tunggakan']['total'])
                    ->setCellValue('J'.$urut, $value['tunggakan-1']['spp'])
                    ->setCellValue('K'.$urut, $value['tunggakan-1']['pembangunan'])
                    ->setCellValue('L'.$urut, $value['tunggakan-1']['pramuka'])
                    ->setCellValue('M'.$urut, $value['tunggakan-1']['lain2'])
                    ->setCellValue('N'.$urut, $value['tunggakan-1']['total'])
                    ->setCellValue('O'.$urut, $value['tunggakanTot']['spp'])
                    ->setCellValue('P'.$urut, $value['tunggakanTot']['pembangunan'])
                    ->setCellValue('Q'.$urut, $value['tunggakanTot']['pramuka'])
                    ->setCellValue('R'.$urut, $value['tunggakanTot']['lain2'])
                    ->setCellValue('S'.$urut, $value['total']);

            
            $objPHPExcel->getActiveSheet()->getStyle('B' . $urut)->applyFromArray( $left );
            $objPHPExcel->getActiveSheet()->getStyle('C' . $urut)->applyFromArray( $left );
            $objPHPExcel->getActiveSheet()->getStyle('D' . $urut)->applyFromArray( $left );

            $objPHPExcel->getActiveSheet()->getStyle('E' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('F' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('G' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('H' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('I' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('J' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('K' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('L' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('M' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('N' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('O' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('P' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('Q' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('R' . $urut)->applyFromArray( $right );
            $objPHPExcel->getActiveSheet()->getStyle('S' . $urut)->applyFromArray( $right );
            $i++;

            foreach ($letters as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$urut.':'.$value.$urut)->applyFromArray( $content );
            }
        }
        //-------------------------------------------------------------------------

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.($urut + 1), 'JUMLAH')
                    ->setCellValue('E'.($urut + 1), rupiah($sppTotal))
                    ->setCellValue('F'.($urut + 1), rupiah($pembangunanTotal))
                    ->setCellValue('G'.($urut + 1), rupiah($pramukaTotal))
                    ->setCellValue('H'.($urut + 1), rupiah($lain2Total))
                    ->setCellValue('I'.($urut + 1), rupiah($totalVertical))
                    ->setCellValue('J'.($urut + 1), rupiah($sppMinTotal))
                    ->setCellValue('K'.($urut + 1), rupiah($pembangunanMinTotal))
                    ->setCellValue('L'.($urut + 1), rupiah($pramukaMinTotal))
                    ->setCellValue('M'.($urut + 1), rupiah($lain2MinTotal))
                    ->setCellValue('N'.($urut + 1), rupiah($totalMinVertical))
                    ->setCellValue('O'.($urut + 1), rupiah($sppAMTotal))
                    ->setCellValue('P'.($urut + 1), rupiah($pembangunanAMTotal))
                    ->setCellValue('Q'.($urut + 1), rupiah($pramukaAMTotal))
                    ->setCellValue('R'.($urut + 1), rupiah($lain2AMTotal))
                    ->setCellValue('S'.($urut + 1), rupiah($tunggakanKelas));

        foreach ($letters as $key => $value) {
            $objPHPExcel->getActiveSheet()->getStyle($value.($urut + 1).':'.$value.($urut + 1))->applyFromArray( $content );
            $objPHPExcel->getActiveSheet()->getStyle($value.($urut + 1).':'.$value.($urut + 1))->applyFromArray( $right );
        }
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B' . ($urut + 1) . ':D' . ($urut + 1) );
        $objPHPExcel->getActiveSheet()->getStyle('B' . ($urut + 1))->applyFromArray( $center );
        //-------------------------------------------------------------------------
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B' . ($urut + 3), 'Catatan : ')
                    ->setCellValue('B' . ($urut + 4), 'Data tunggakan ini sesuai dengan data keuangan komite,')
                    ->setCellValue('B' . ($urut + 5), 'dapat dijadikan bahan pertimbangan bagi pihak')
                    ->setCellValue('B' . ($urut + 6), 'yang berkepentingan.');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('E' . ($urut + 4), 'Tertanda,')
                    ->setCellValue('E' . ($urut + 6), 'Bendahara Komite');

        $objPHPExcel->getActiveSheet()
                    ->mergeCells('E' . ($urut + 4) . ':R' . ($urut + 4) )
                    ->mergeCells('E' . ($urut + 6) . ':R' . ($urut + 6) );

        $objPHPExcel->getActiveSheet()->getStyle('E' . ($urut + 4) . ':R' . ($urut + 6))->applyFromArray( $right );


        //-------------------------------------------------------------------------------


        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('TUNGG - ' . $rombel->nama);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="TUNGG - ' . $rombel->nama . '.xls"'); // file name of excel
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public static function historyPembayaran($historis, $namaSiswa){
        $objPHPExcel = new PHPExcel();
        $bord = $objPHPExcel->Gen_PHPExcel_Style_Border();
        $fill = $objPHPExcel->Gen_PHPExcel_Style_Fill();
        $align = $objPHPExcel->Gen_PHPExcel_Style_Align();
        $objPHPExcel->getProperties()->setCreator("SMKN 2 PAYAKUMBUH")
                                     ->setLastModifiedBy(date('Y-m-d H:i:s'))
                                     ->setTitle(self::worksheetNameGenerator("HP - " . $namaSiswa))
                                     ->setSubject(self::worksheetNameGenerator("HP - " . $namaSiswa))
                                     ->setDescription(self::worksheetNameGenerator("HP - " . $namaSiswa))
                                     ->setKeywords(self::worksheetNameGenerator("HP - " . $namaSiswa))
                                     ->setCategory(self::worksheetNameGenerator("HP - " . $namaSiswa));
        // create style
        $default_border = array(
            'style' => $bord::BORDER_THIN,
            'color' => array('rgb'=>'#000000')
        );
        $h1 = self::fontCenter(16, 'Times New Roman', TRUE);
        $h2 = self::fontCenter(18, 'Times New Roman', TRUE);
        $h3 = self::fontCenter(13, 'Times New Roman', TRUE);
        $h4 = self::fontCenter(15, 'Times New Roman', TRUE);
        $h5 = self::fontCenter(11, 'Times New Roman', FALSE);
        $h6 = self::fontCenter(10, 'Times New Roman', FALSE);
        $h7 = self::fontCenter(14, 'Times New Roman', TRUE);
        $h8 = self::fontCenter(13, null, TRUE);
        $borderBottom = array(
            'borders' => array(
                'bottom' => array(
                            'style' => $bord::BORDER_THICK,
                            'color' => array('rgb'=>'#000000')
                        )
            )
        );
        $content = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            )
        );
        $center = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $centerBold = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true
            )
        );
        $bold = array(
            'font' => array(
                'bold' => true
            )
        );
        $left = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_LEFT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_RIGHT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );

        //-----------------------------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B3', 'HISTORI PEMBAYARAN ')
                    ->setCellValue('B4', $namaSiswa);
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B3:O3')
                    ->mergeCells('B4:O4');
        $objPHPExcel->getActiveSheet()->getStyle('B3:O4')->applyFromArray( $h8 );
        //-----------------------------------------------------------------------------------------------         
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B6', 'NO')
                    ->setCellValue('C6', 'NISN')
                    ->setCellValue('D6', 'NAMA')
                    ->setCellValue('E6', 'NO KWITANSI')
                    ->setCellValue('F6', 'SPP')
                    ->setCellValue('G6', 'PEMBANGUNAN')
                    ->setCellValue('H6', 'PRAMUKA')
                    ->setCellValue('I6', 'LAIN-LAIN')
                    ->setCellValue('J6', 'TOTAL')
                    ->setCellValue('K6', 'NOTE')
                    ->setCellValue('L6', 'KETERANGAN')
                    ->setCellValue('M6', 'TP')
                    ->setCellValue('N6', 'KELAS')
                    ->setCellValue('O6', 'CREATED AT');
        $objPHPExcel->getActiveSheet()->getStyle('B6:O6')->applyFromArray( $content );

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(22);
        //-----------------------------------------------------------------------------------------------         
        $dataku= $historis;
        $firststyle='B5';
        $urut =0;
        for($i=0;$i<count($dataku);$i++)
        {
            $urut=$i+7;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$urut, $i+1 .'')
                    ->setCellValue('C'.$urut, ''.nisn_format($dataku[$i]->nisn))
                    ->setCellValue('D'.$urut, $dataku[$i]->nama)
                    ->setCellValue('E'.$urut, $dataku[$i]->no_kwitansi)
                    ->setCellValue('F'.$urut, rupiah($dataku[$i]->spp))
                    ->setCellValue('G'.$urut, rupiah($dataku[$i]->pembangunan))
                    ->setCellValue('H'.$urut, rupiah($dataku[$i]->pramuka))
                    ->setCellValue('I'.$urut, rupiah($dataku[$i]->lain_lain))
                    ->setCellValue('J'.$urut, rupiah($dataku[$i]->nominal))
                    ->setCellValue('K'.$urut, $dataku[$i]->note)
                    ->setCellValue('L'.$urut, $dataku[$i]->note2)
                    ->setCellValue('M'.$urut, $dataku[$i]->tp . '/' . ($dataku[$i]->tp + 1))
                    ->setCellValue('N'.$urut, $dataku[$i]->kelas)
                    ->setCellValue('O'.$urut, $dataku[$i]->created_at)
                    ->getStyle('B'.$urut.':'.'O'.$urut)->applyFromArray( $content );
            $laststyle='O'.$urut;
        }
        $letters = array();
        foreach (range('B', 'O') as $char) {
            $letters[] = $char;
        }
        
        for($i = 6 ; $i <= $urut; $i++){
            foreach ($letters as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$i.':'.$value.$i)->applyFromArray( $content );
                $objPHPExcel->getActiveSheet()->getStyle('B' . $i . ':' . 'B' . $i)->applyFromArray( $left );
                $objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':' . 'C' . $i)->applyFromArray( $left );
                $objPHPExcel->getActiveSheet()->getStyle('D' . $i . ':' . 'D' . $i)->applyFromArray( $left );
                $objPHPExcel->getActiveSheet()->getStyle('E' . $i . ':' . 'E' . $i)->applyFromArray( $left );
                $objPHPExcel->getActiveSheet()->getStyle('F' . $i . ':' . 'J' . $i)->applyFromArray( $right );
                $objPHPExcel->getActiveSheet()->getStyle('K' . $i . ':' . 'O' . $i)->applyFromArray( $left );
            }
        }
        $objPHPExcel->getActiveSheet()->getStyle('B6:O6')->applyFromArray( $centerBold );
        //-----------------------------------------------------------------------------------------------         
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B' . ($urut + 2), 'Dicetak pada : ' . date('Y-m-d H:i:s'));
        //-----------------------------------------------------------------------------------------------         

        $objPHPExcel->getActiveSheet()->setTitle(self::worksheetNameGenerator("HP - " . $namaSiswa));
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . self::worksheetNameGenerator("HP - " . $namaSiswa) . '.xls"'); // file name of excel
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public static function cetakPembayaran($staffName, $TI, $lapName, $hb, $jenisPembayaran ){
        
        $objPHPExcel = new PHPExcel(); 
        $bord = $objPHPExcel->Gen_PHPExcel_Style_Border();
        $fill = $objPHPExcel->Gen_PHPExcel_Style_Fill();
        $align = $objPHPExcel->Gen_PHPExcel_Style_Align();
        $objPHPExcel->getProperties()->setCreator("SMKN 2 PAYAKUMBUH")
                                     ->setLastModifiedBy("SMKN 2 PAYAKUMBUH")
                                     ->setTitle("SMKN 2 PAYAKUMBUH")
                                     ->setSubject("SMKN 2 PAYAKUMBUH")
                                     ->setDescription("SMKN 2 PAYAKUMBUH")
                                     ->setKeywords("SMKN 2 PAYAKUMBUH")
                                     ->setCategory("SMKN 2 PAYAKUMBUH");
        // create style
        $default_border = array(
            'style' => $bord::BORDER_THIN,
            'color' => array('rgb'=>'#000000')
        );
        $h1 = self::fontCenter(16, 'Times New Roman', TRUE);
        $h2 = self::fontCenter(18, 'Times New Roman', TRUE);
        $h3 = self::fontCenter(13, 'Times New Roman', TRUE);
        $h4 = self::fontCenter(15, 'Times New Roman', TRUE);
        $h5 = self::fontCenter(11, 'Times New Roman', FALSE);
        $h6 = self::fontCenter(10, 'Times New Roman', FALSE);
        $h7 = self::fontCenter(14, 'Times New Roman', TRUE);
        $h8 = self::fontCenter(13, null, TRUE);
        $borderBottom = array(
            'borders' => array(
                'bottom' => array(
                            'style' => $bord::BORDER_THICK,
                            'color' => array('rgb'=>'#000000')
                        )
            )
        );
        $content = array(
            'borders' => array(
                'bottom' => $default_border,
                'left' => $default_border,
                'top' => $default_border,
                'right' => $default_border,
            )
        );
        $center = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $centerBold = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_CENTER,
                'vertical' => $align::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' => true
            )
        );
        $bold = array(
            'font' => array(
                'bold' => true
            )
        );
        $left = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_LEFT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );
        $right = array(
            'alignment' => array(
                'horizontal' => $align::HORIZONTAL_RIGHT,
                'vertical' => $align::VERTICAL_CENTER,
            )
        );

        //----------------------------------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B3', 'DATA PENERIMAAN ' . $staffName)
                    ->setCellValue('B4', $TI);
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B3:L3')
                    ->mergeCells('B4:L4');
        $objPHPExcel->getActiveSheet()->getStyle('B3:L4')->applyFromArray( $h8 );
        //----------------------------------------------------------------------------------------------------         
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B5', $lapName)
                    ->setCellValue('E5', 'IURAN')
                    ->setCellValue('C6', 'NISN')
                    ->setCellValue('D6', 'NAMA')
                    ->setCellValue('E6', 'KELAS')
                    ->setCellValue('L6', 'KETERANGAN');
        $objPHPExcel->getActiveSheet()
                    ->mergeCells('B5:E5')
                    ->mergeCells('F5:L5');
        $objPHPExcel->getActiveSheet()->getStyle('B5:L5')->applyFromArray( $centerBold );
        //----------------------------------------------------------------------------------------------------
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B6', 'NO')
                    ->setCellValue('F6', 'NO KUITANSI')
                    ->setCellValue('G6', 'TANGGAL')
                    ->setCellValue('H6', 'SPP')
                    ->setCellValue('I6', 'SPP')
                    ->setCellValue('I6', 'PRAMUKA')
                    ->setCellValue('J6', 'PEMBANGUNAN')
                    ->setCellValue('K6', 'JUMLAH');
        $objPHPExcel->getActiveSheet()->getStyle('B6:L6')->applyFromArray( $centerBold );
        //----------------------------------------------------------------------------------------------------
        $dataku= $hb;
        $urut =0;
        for($i=0;$i<count($dataku);$i++)
        {
            $urut=$i+7;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$urut, $i+1 .'')
                    ->setCellValue('C'.$urut, ''.nisn_format($dataku[$i]->nisn))
                    ->setCellValue('D'.$urut, $dataku[$i]->nama)
                    ->setCellValue('E'.$urut, $dataku[$i]->kelas)
                    ->setCellValue('F'.$urut, $dataku[$i]->no_kwitansi)
                    ->setCellValue('G'.$urut, $dataku[$i]->created_at)
                    ->setCellValue('H'.$urut, (int)$dataku[$i]->spp)
                    ->setCellValue('I'.$urut, (int)$dataku[$i]->pramuka)
                    ->setCellValue('J'.$urut, (int)$dataku[$i]->pembangunan)
                    ->setCellValue('K'.$urut, (int)$dataku[$i]->nominal)
                    ->setCellValue('L'.$urut, $dataku[$i]->note2)
                    ->getStyle('B'.$urut.':'.'L'.$urut)->applyFromArray( $content );
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(7);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(24);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(16);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(40);

        $objPHPExcel->getActiveSheet()->getStyle('B7:G'.$urut)->applyFromArray( $left );
        $objPHPExcel->getActiveSheet()->getStyle('H7:K'.$urut)->applyFromArray( $right );
        $objPHPExcel->getActiveSheet()->getStyle('L'.$urut)->applyFromArray( $left );
        
        $letters = array();
        foreach (range('B', 'L') as $char) {
            $letters[] = $char;
        }
        $endLopp = 5 + count($hb);
        for($i = 5 ; $i <= $urut; $i++){
            foreach ($letters as $key => $value) {
                $objPHPExcel->getActiveSheet()->getStyle($value.$i.':'.$value.$i)->applyFromArray( $content );
            }
        } 
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('DATA PENERIMAAN');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="DATA PENERIMAAN.xls"'); // file name of excel
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}