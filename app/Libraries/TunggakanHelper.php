<?php

use App\Models\Komite;
use App\Models\Customer;
use App\Models\Student;
use App\Models\Jurusan;
use App\Models\Rombel;
use App\Models\HistoriSpp;
use App\Models\MasterSPP;
use App\Models\Tp;
use App\Models\Spp;
use App\Models\Pembangunan;
use App\Models\Pramuka;
use App\Models\LainLain;

//----------------------------------------------------------------------------------------
//Fungsi untuk mendapatkan jumlah biaya sekolah yang harus dibayarkan pertahunnya
//

function nisn_format($nisn){
	return str_pad($nisn, 10, '0', STR_PAD_LEFT);
}


function get_student($nisn){
	$student = Student::find($nisn);
	return $student;
}

function get_student_name($nisn){
	$student = get_student($nisn);
	return $student->nama_lengkap;
}

function get_class_name($nisn){
	$student = get_student($nisn);
	if($student == NULL){
		return 'NISN tidak dapat ditemukan.';
	}else{
		$class_name = Rombel::where('id', $student->kelas)->select('nama')->first();
		if($class_name == NULL){
			return "Belum ada kelas";
		}
		return $class_name->nama;
	}
}

function getStudentJenjangJurusan($nisn){
	$student = get_student($nisn);
	$jenjangJurusan = Jurusan::where('id', $student->jurusan)->select('jenjang')->first();
	return $jenjangJurusan->jenjang;
}

function getTahunKeluarSiswa($nisn){
	$tahunKeluar = Student::where('nisn', $nisn)->where('status', 'pasif')->select('tahunKeluar')->first();
	if($tahunKeluar == null) return NULL;
	return $tahunKeluar->tahunKeluar;
}

function get_jurusan_name($id_jurusan){
	$jurusan_name = Jurusan::where('id', $id_jurusan)->select('nama_jurusan')->first();
	return $jurusan_name->nama_jurusan;
}

function get_wajib_bayar_spp($nisn){
	$master_biaya = get_master_biaya($nisn);
	return (12 * $master_biaya->spp);	
}

function get_wajib_bayar_pembangunan($nisn){
	$master_biaya = get_master_biaya($nisn);
	return $master_biaya->pembangunan;	
}

function get_wajib_bayar_pramuka($nisn){
	$master_biaya = get_master_biaya($nisn);
	return (12 * $master_biaya->pramuka);	
}

function get_wajib_bayar_lain2($nisn){
	$master_biaya = get_master_biaya($nisn);
	return (12 * $master_biaya->lain_lain);	
}

function get_wajib_bayar($nisn){
	$master_biaya = get_master_biaya($nisn);
	$masterBiaya = array(
		'spp' => $master_biaya->spp,	
		'pembangunan' => $master_biaya->pembangunan,	
		'pramuka' => $master_biaya->pramuka,	
		'lain2' => $master_biaya->lain_lain);

	return $masterBiaya;	
}

//----------------------------------------------------------------------------------------
//Mendapatkan data tahun aktif dan semacamnya
//

function get_all_tp(){
    $tp = Tp::groupBy('tp')->get();
	return $tp;
}

function getTp($tp){
    $tp = Tp::where('tp', $tp)->first();
	return $tp;
}

function get_tp_aktif(){
    $tp = Tp::where('status', 'aktif')->first();
	return $tp;
}

function get_tahun_sekarang(){
	return date('Y');
}

function get_tp_minsatu(){
    $tp_aktif = Tp::where('status', 'aktif')->first();
    $tp_minsatu = Tp::where('tp', ($tp_aktif->tp-1))->first();
	return $tp_minsatu;
}

//----------------------------------------------------------------------------------------
//Mendapatkan data biaya sekolah dan semacamnya
//

function get_rombel_byid($nisn){
	$student = Student::find($nisn);
	$rombel = Rombel::find($student->kelas);
	return $rombel;
}

function get_tahun_master_biaya($nisn){
	$student = Student::find($nisn);
	return $student->tp;
}

function get_master_biaya($nisn){
	$tahun_master_biaya = get_tahun_master_biaya($nisn);
	$master_biaya = MasterSPP::where('tp', $tahun_master_biaya)->first();
	if($master_biaya ==  NULL){
		return FALSE;
	}
	return $master_biaya;
}

function get_batas_tunggakan_biaya_sekolah($nisn){
	$master_biaya = get_master_biaya($nisn);
	
	if($master_biaya == FALSE){
		return FALSE;
	}

	$batas_tunggakan = array(
		'spp'		=> $master_biaya->batas_bln_spp,
		'pembangunan'=> $master_biaya->batas_bln_pembangunan,
		'pramuka'	=> $master_biaya->batas_bln_pramuka,
		'lain2'		=> $master_biaya->batas_bln_lain2
		);
	return $batas_tunggakan;
}

function getBastasTunggakanByIdTunggakan($idMasterBiaya){
	$masterBiaya = MasterSPP::find($idMasterBiaya);
	$batasTunggakan = $masterBiaya->batas_bln_spp;
	return $batasTunggakan;
}

//----------------------------------------------------------------------------------------
//Return bulan
//

function Bulan(){
    $bln = [4 => 'Januari', 5 => 'Februari', 6 => 'Maret', 7 => 'April', 8 => 'Mei', 9 => 'Juni', 10 => 'Juli', 11 => 'Agustus', 12 => 'September', 13 =>'Oktober', 14 => 'November', 15 => 'Desember'];
	return $bln;
}

function Bulan_singkatan(){
    $bln = [4 => 'JAN', 5 => 'FEB', 6 => 'MAR', 7 => 'APR', 8 => 'MEI', 9 => 'JUN', 10 => 'JUL', 11 => 'AGU', 12 => 'SEP', 13 =>'OKT', 14 => 'NOV', 15 => 'DES'];
	return $bln;
}

function Bulan_semester(){
    $bln = [4 => 'JUL', 5 => 'AGU', 6 => 'SEP', 7 => 'OKT', 8 => 'NOV', 9 => 'DES', 10 => 'JAN', 11 => 'FEB', 12 => 'MAR', 13 =>'APR', 14 => 'MEI', 15 => 'JUN'];
	return $bln;
}

function Bulan_laporan(){
    $bln = ['01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' =>'Oktober', '11' => 'November', '12' => 'Desember'];
	return $bln;
}

function Hari_laporan(){
	$hari = [ '01', '02', '03', '04', '05', 
			 '06', '07', '08', '09', '10', 
			 '11', '12', '13', '14', '15', 
			 '16', '17', '18', '19', '20', 
			 '20', '21', '22', '23', '24', 
			 '25', '26', '27', '28', '29', 
			 '30', '31' ];
	return  $hari;
}

function Bulan_kecil(){
    $bln = [4 => 'januari', 5 => 'februari', 6 => 'maret', 7 => 'april', 8 => 'mei', 9 => 'juni', 10 => 'juli', 11 => 'agustus', 12 => 'september', 13 =>'oktober', 14 => 'november', 15 => 'desember'];
	return $bln;
}

function get_bulan($bln_index){
	foreach (Bulan_laporan() as $key => $value) {
		if($key == $bln_index) return strtoupper($value);
	}
}

function get_bulan_noup($bln_index){
	foreach (Bulan_laporan() as $key => $value) {
		if($key == $bln_index) return $value;
	}
}

function rupiah($nominal){
	$rupiah = number_format( $nominal, 0 , '' , '.' ) . ' ';
	return $rupiah;
}

//----------------------------------------------------------------------------------------
//Menghitung tunggakan tahunan
//

function get_record($nisn, $type, $tp){
	switch ($type) {
		case 'spp':
			$record = Spp::where('tp', $tp->tp)->where('nisn', $nisn)->first();
			if($record == null) $record = new Spp;
			break;
		case 'pembangunan':
			$record = Pembangunan::where('tp', $tp->tp)->where('nisn', $nisn)->first();
			if($record == null) $record = new Pembangunan;
			break;
		case 'pramuka':
			$record = Pramuka::where('tp', $tp->tp)->where('nisn', $nisn)->first();
			if($record == null) $record = new Pramuka;
			break;
		case 'lain2':
			$record = LainLain::where('tp', $tp->tp)->where('nisn', $nisn)->first();
			if($record == null) $record = new LainLain;
			break;
	}
	return $record;
}

//Dihitung dengan cara melooping samua bulan yang ada di database. kemudian total bayar setahun dikurangi hasil looping

function tunggakanPerTp($tp, $type, $nisn, $sampai = 9, $tahunMasukSiswa){
	if($type == 'pembangunan'){
		$bln = Bulan_kecil();
		$tp = getTp($tp);
		$record = get_record($nisn, $type, $tp);
		$master_biaya = get_master_biaya($nisn);
		$master_pembangunan = $master_biaya->pembangunan;
		$wajib_bayar_setahun = get_wajib_bayar_pembangunan($nisn);
		$dibayar = 0;
		$beasiswa = false;
	    if($tp->tp == $tahunMasukSiswa){
		    if($sampai < 10){
			    for($i=10; $i <= 15; $i++){
			    	$b = $bln[$i];
			    	if($record->$b == -1){
			    		$value = 0;
			    		$beasiswa = true;
			    	}else {
			    		$value = $record->$b;
			    	}
			        $dibayar += $value;
			    }
			    for($i=4; $i <= $sampai; $i++){
			    	$b = $bln[$i];
			    	if($record->$b == -1){
			    		$value = 0;
			    		$beasiswa = true;
			    	}else {
			    		$value = $record->$b;
			    	}
			        $dibayar += $value;
			    }
			}else{
				for($i=10; $i <= $sampai; $i++){
			    	$b = $bln[$i];
			    	if($record->$b == -1){
			    		$value = 0;
			    		$beasiswa = true;
			    	}else {
			    		$value = $record->$b;
			    	}
			        $dibayar += $value;
			    }
			}
		}else{
			return 0;
		}
		    

	    if($beasiswa){
	    	return 0;
	    }

	    $tunggakan = $wajib_bayar_setahun - $dibayar;
	    return $tunggakan;
	}else{
		$bln = Bulan_kecil();
		$tp = getTp($tp);
		$record = get_record($nisn, $type, $tp);
		$master_biaya = get_master_biaya($nisn);
		if($type == 'spp'){
			$master_spp = $master_biaya->spp;
			$wajib_bayar_setahun = get_wajib_bayar_spp($nisn);
		}elseif($type == 'pramuka'){
			$master_spp = $master_biaya->pramuka;
			$wajib_bayar_setahun = get_wajib_bayar_pramuka($nisn);
		}elseif($type == 'lain2'){
			$master_spp = $master_biaya->lain_lain;
			$wajib_bayar_setahun = get_wajib_bayar_lain2($nisn);
		}
		$dibayar = 0;
		$jml_bayar = 0;
		if($sampai <10){
		    for($i=10; $i <= 15; $i++){
		    	$b = $bln[$i];
		    	if($record->$b == -1){
		    		$value = $master_spp;
		    	}else {
		    		$value = $record->$b;
		    	}
	            $jml_bayar += $master_spp;
		        $dibayar += $value;
		    }
		    for($i=4; $i <= $sampai; $i++){
		    	$b = $bln[$i];
		    	if($record->$b == -1){
		    		$value = $master_spp;
		    	}else {
		    		$value = $record->$b;
		    	}
	            $jml_bayar += $master_spp;
		        $dibayar += $value;
		    }
		}else{
			for($i=10; $i <= $sampai; $i++){
		    	$b = $bln[$i];
		    	if($record->$b == -1){
		    		$value = $master_spp;
		    	}else {
		    		$value = $record->$b;
		    	}
	            $jml_bayar += $master_spp;
		        $dibayar += $value;
		    }
		}

	    // $tunggakan = $wajib_bayar_setahun - $dibayar;
	    $tunggakan = $jml_bayar - $dibayar;
	    return $tunggakan;
	}
}

function checkNullMasterBiaya($masterBiaya){
	if($masterBiaya == NULL) return 1;
}

function tunggakan_tahun_aktif_spp($nisn, $sampai = 9){
	$bln = Bulan_kecil();
	$tp = get_tp_aktif();
	$record = get_record($nisn, 'spp', $tp);
	$master_biaya = get_master_biaya($nisn);
	
	$checkMasterBiaya = checkNullMasterBiaya($master_biaya);
	if($checkMasterBiaya) return 0;

	$master_spp = $master_biaya->spp;
	$wajib_bayar_setahun = get_wajib_bayar_spp($nisn);
	$dibayar = 0;
	$jml_bayar = 0;
	if($sampai <10){
	    for($i=10; $i <= 15; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_spp;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_spp;
	        $dibayar += $value;
	    }
	    for($i=4; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_spp;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_spp;
	        $dibayar += $value;
	    }
	}else{
		for($i=10; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_spp;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_spp;
	        $dibayar += $value;
	    }
	}

    // $tunggakan = $wajib_bayar_setahun - $dibayar;
    $tunggakan = $jml_bayar - $dibayar;
    return $tunggakan;
}

function tunggakan_tahun_aktif_pembangunan($nisn, $sampai = 9, $tpAktif, $tahunMasukSiswa){
	$bln = Bulan_kecil();
	$tp = get_tp_aktif();
	$record = get_record($nisn, 'pembangunan', $tp);
	$master_biaya = get_master_biaya($nisn);
	
	$checkMasterBiaya = checkNullMasterBiaya($master_biaya);
	if($checkMasterBiaya) return 0;
	
	$master_pembangunan = $master_biaya->pembangunan;
	$wajib_bayar_setahun = get_wajib_bayar_pembangunan($nisn);
	$dibayar = 0;
	$beasiswa = false;
	if($tpAktif == $tahunMasukSiswa){
	    if($sampai <10){
		    for($i=10; $i <= 15; $i++){
		    	$b = $bln[$i];
		    	if($record->$b == -1){
		    		$value = 0;
		    		$beasiswa = true;
		    	}else {
		    		$value = $record->$b;
		    	}
		        $dibayar += $value;
		    }
		    for($i=4; $i <= $sampai; $i++){
		    	$b = $bln[$i];
		    	if($record->$b == -1){
		    		$value = 0;
		    		$beasiswa = true;
		    	}else {
		    		$value = $record->$b;
		    	}
		        $dibayar += $value;
		    }
		}else{
			for($i=10; $i <= $sampai; $i++){
		    	$b = $bln[$i];
		    	if($record->$b == -1){
		    		$value = 0;
		    		$beasiswa = true;
		    	}else {
		    		$value = $record->$b;
		    	}
		        $dibayar += $value;
		    }
		}
	}else{
		return 0;
	}
	    

    if($beasiswa){
    	return 0;
    }

    $tunggakan = $wajib_bayar_setahun - $dibayar;
    return $tunggakan;
}

function tunggakan_tahun_aktif_pramuka($nisn, $sampai = 9){
	$bln = Bulan_kecil();
	$tp = get_tp_aktif();
	$record = get_record($nisn, 'pramuka', $tp);
	$master_biaya = get_master_biaya($nisn);
	
	$checkMasterBiaya = checkNullMasterBiaya($master_biaya);
	if($checkMasterBiaya) return 0;
	
	$master_pramuka = $master_biaya->pramuka;
	$wajib_bayar_setahun = get_wajib_bayar_pramuka($nisn);
	$dibayar = 0;
    $jml_bayar = 0;
	if($sampai <10){
	    for($i=10; $i <= 15; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_pramuka;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_pramuka;
	        $dibayar += $value;
	    }
	    for($i=4; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_pramuka;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_pramuka;
	        $dibayar += $value;
	    }
	}else{
		for($i=10; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_pramuka;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_pramuka;
	        $dibayar += $value;
	    }
	}

   	// $tunggakan = $wajib_bayar_setahun - $dibayar;
    $tunggakan = $jml_bayar - $dibayar;
    return $tunggakan;
}

function tunggakan_tahun_aktif_lain2($nisn, $sampai = 9){
	$bln = Bulan_kecil();
	$tp = get_tp_aktif();
	$record = get_record($nisn, 'lain2', $tp);
	$master_biaya = get_master_biaya($nisn);
	
	$checkMasterBiaya = checkNullMasterBiaya($master_biaya);
	if($checkMasterBiaya) return 0;
	
	$master_lain2 = $master_biaya->lain_lain;
	$wajib_bayar_setahun = get_wajib_bayar_lain2($nisn);
	$dibayar = 0;
    $jml_bayar = 0;
	if($sampai <10){
	    for($i=10; $i <= 15; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_lain2;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_lain2;
	        $dibayar += $value;
	    }
	    for($i=4; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_lain2;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_lain2;
	        $dibayar += $value;
	    }
	}else{
		for($i=10; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_lain2;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_lain2;
	        $dibayar += $value;
	    }
	}

    // $tunggakan = $wajib_bayar_setahun - $dibayar;
    $tunggakan = $jml_bayar - $dibayar;
    return $tunggakan;
}

function tunggakan_tahun_aktif($nisn, $tpAktif, $tahunMasukSiswa, $sampaiSpp, $sampaiPembangunan, $sampaiPramuka, $sampaiLain2){
	$spp = tunggakan_tahun_aktif_spp($nisn, $sampaiSpp);
	$pembangunan = tunggakan_tahun_aktif_pembangunan($nisn, $sampaiPembangunan, $tpAktif, $tahunMasukSiswa );
	$pramuka = tunggakan_tahun_aktif_pramuka($nisn, $sampaiPramuka);
	$lain2 = tunggakan_tahun_aktif_lain2($nisn, $sampaiLain2);
	$total = $spp + $pembangunan + $pramuka + $lain2;
	$tunggakan = array(
		'spp' => $spp,
		'pembangunan' => $pembangunan, 
		'pramuka' => $pramuka,
		'lain2' => $lain2,
		'total' => $total
		);

	return $tunggakan;
}

function tunggakan_tahun_aktif_minsatu($nisn, $tpAktif, $tahunMasukSiswa, $sampaiSpp, $sampaiPembangunan, $sampaiPramuka, $sampaiLain2){
	$spp = tunggakan_tahun_aktif_minsatu_spp($nisn, $sampaiSpp);
	$pembangunan = tunggakan_tahun_aktif_pembangunan($nisn, $sampaiPembangunan, $tpAktif, $tahunMasukSiswa);
	$pramuka = tunggakan_tahun_aktif_minsatu_pramuka($nisn, $sampaiPramuka);
	$lain2 = tunggakan_tahun_aktif_minsatu_lain2($nisn, $sampaiLain2);
	$total = $spp + $pembangunan + $pramuka + $lain2;;
	$tunggakan = array(
		'spp' => $spp,
		'pembangunan' => $pembangunan, 
		'pramuka' => $pramuka,
		'lain2' => $lain2,
		'total' => $total
		);

	return $tunggakan;
}

function tunggakan_tahun_aktif_minsatu_spp($nisn, $sampai = 9){
	$bln = Bulan_kecil();
	$tp = get_tp_minsatu();
	$record = get_record($nisn, 'spp', $tp);
	$master_biaya = get_master_biaya($nisn);
	$master_spp = $master_biaya->spp;
	$wajib_bayar_setahun = get_wajib_bayar_spp($nisn);
	$dibayar = 0;
    $jml_bayar = 0;
	if($sampai <10){
	    for($i=10; $i <= 15; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_spp;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_spp;
	        $dibayar += $value;
	    }
	    for($i=4; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_spp;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_spp;
	        $dibayar += $value;
	    }
	}else{
		for($i=10; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_spp;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_spp;
	        $dibayar += $value;
	    }
	}

    // $tunggakan = $wajib_bayar_setahun - $dibayar;
    $tunggakan = $jml_bayar - $dibayar;
    return $tunggakan;
}

function tunggakan_tahun_aktif_minsatu_pembangunan($nisn, $sampai = 9){
	$bln = Bulan_kecil();
	$tp = get_tp_minsatu();
	$record = get_record($nisn, 'pembangunan', $tp);
	$master_biaya = get_master_biaya($nisn);
	$master_pembangunan = $master_biaya->pembangunan;
	$wajib_bayar_setahun = get_wajib_bayar_pembangunan($nisn);
	$dibayar = 0;
	$beasiswa = false;
    if($sampai <10){
	    for($i=10; $i <= 15; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = 0;
	    		$beasiswa = true;
	    	}else {
	    		$value = $record->$b;
	    	}
	        $dibayar += $value;
	    }
	    for($i=4; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = 0;
	    		$beasiswa = true;
	    	}else {
	    		$value = $record->$b;
	    	}
	        $dibayar += $value;
	    }
	}else{
		for($i=10; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = 0;
	    		$beasiswa = true;
	    	}else {
	    		$value = $record->$b;
	    	}
	        $dibayar += $value;
	    }
	}

    $tunggakan = $wajib_bayar_setahun - $dibayar;
    return $tunggakan;
}

function tunggakan_tahun_aktif_minsatu_pramuka($nisn, $sampai = 9){
	$bln = Bulan_kecil();
	$tp = get_tp_minsatu();
	$record = get_record($nisn, 'pramuka', $tp);
	$master_biaya = get_master_biaya($nisn);
	$master_pramuka = $master_biaya->pramuka;
	$wajib_bayar_setahun = get_wajib_bayar_pramuka($nisn);
	$dibayar = 0;
    $jml_bayar = 0;
	if($sampai <10){
	    for($i=10; $i <= 15; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_pramuka;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_pramuka;
	        $dibayar += $value;
	    }
	    for($i=4; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_pramuka;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_pramuka;
	        $dibayar += $value;
	    }
	}else{
		for($i=10; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_pramuka;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_pramuka;
	        $dibayar += $value;
	    }
	}

    // $tunggakan = $wajib_bayar_setahun - $dibayar;
    $tunggakan = $jml_bayar - $dibayar;
    return $tunggakan;
}

function tunggakan_tahun_aktif_minsatu_lain2($nisn, $sampai = 9){
	$bln = Bulan_kecil();
	$tp = get_tp_minsatu();
	$record = get_record($nisn, 'lain2', $tp);
	$master_biaya = get_master_biaya($nisn);
	$master_lain2 = $master_biaya->lain_lain;
	$wajib_bayar_setahun = get_wajib_bayar_lain2($nisn);
	$dibayar = 0;
    $jml_bayar = 0;
	if($sampai <10){
	    for($i=10; $i <= 15; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_lain2;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_lain2;
	        $dibayar += $value;
	    }
	    for($i=4; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_lain2;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_lain2;
	        $dibayar += $value;
	    }
	}else{
		for($i=10; $i <= $sampai; $i++){
	    	$b = $bln[$i];
	    	if($record->$b == -1){
	    		$value = $master_lain2;
	    	}else {
	    		$value = $record->$b;
	    	}
            $jml_bayar += $master_lain2;
	        $dibayar += $value;
	    }
	}

    // $tunggakan = $wajib_bayar_setahun - $dibayar;
    $tunggakan = $jml_bayar - $dibayar;
    return $tunggakan;
}

function tunggakan_total_tahun_aktif($nisn, $sampai = 9, $tpAktif, $tahunMasukSiswa){
	$total_tunggakan_spp = tunggakan_tahun_aktif_spp($nisn, $sampai, $tpAktif, $tahunMasukSiswa);	
	$total_tunggakan_pembangunan = tunggakan_tahun_aktif_pembangunan($nisn, $sampai, $tpAktif, $tahunMasukSiswa);	
	$total_tunggakan_pramuka = tunggakan_tahun_aktif_pramuka($nisn, $sampai, $tpAktif, $tahunMasukSiswa);	
	$total_tunggakan_lain2 = tunggakan_tahun_aktif_lain2($nisn, $sampai, $tpAktif, $tahunMasukSiswa);	
	return ($total_tunggakan_spp + $total_tunggakan_pembangunan + $total_tunggakan_pramuka + $total_tunggakan_lain2);
}

function tunggakan_sampai_bulan($n){
	
}

function spp_beasiswa_tahun_aktif($nisn){
	$bln = Bulan_kecil();
	$tp = get_tp_aktif();
	$record = get_record($nisn, 'spp', $tp);
	$master_biaya = get_master_biaya($nisn);
	$master_spp = $master_biaya->spp;
	$wajib_bayar_setahun = get_wajib_bayar_spp($nisn);
	$dibayar = 0;
    for($i=4; $i <= 15; $i++){
    	$b = $bln[$i];
    	if($record->$b == -1){
    		$value = $master_spp;
    	}else {
    		$value = $record->$b;
    	}
        $dibayar += $value;
    }

    $tunggakan = $wajib_bayar_setahun - $dibayar;
    return $tunggakan;
}


	//ini dari komite konctroller
    //jumlah siswa dengan status 'aktif' pada database
    function siswa($jumlah_tahun = 5){
        $tahun_aktif =get_tahun_sekarang();
        $tahun_keen = $tahun_aktif - $jumlah_tahun;
        $jumlahSiswa = Student::whereBetween('tp', [$tahun_keen, $tahun_aktif])->count();
        return $jumlahSiswa;
    }

    function siswa_5thn_awal(){
        $tahun_aktif =get_tahun_sekarang();
        $tahun_min5 = $tahun_aktif - 5;
        $jumlahSiswa = Student::whereBetween('tp', [$tahun_min5, $tahun_aktif])->count();
        return $jumlahSiswa;
    }

    function siswa_aktif(){
        $jumlahSiswa = Student::where('status', 'aktif')->count();
        return $jumlahSiswa;
    }

    function siswa_pasif(){
        $jumlahSiswa = Student::where('status', 'pasif')->count();
        return $jumlahSiswa;
    }

    function siswa_tahun_null(){
        $jumlahSiswa = Student::where('tp', NULL)->count();
        return $jumlahSiswa;
    }

    function siswa_status_beda(){
        $jumlahSiswa = Student::where('status', '!=', 'aktif')->where('status', '!=', 'pasif')->count();
        return $jumlahSiswa;
    }

    function get_siswa(){
        $jumlahSiswa = Student::all();
        return $jumlahSiswa;
    }

    function get_siswa_aktif(){
        $jumlahSiswa = Student::where('status', 'aktif')->orderBy('nama_lengkap', 'ASC')->get();
        return $jumlahSiswa;
    }

    function get_siswa_pasif(){
        $jumlahSiswa = Student::where('status', 'pasif')->orderBy('nama_lengkap', 'ASC')->get();
        return $jumlahSiswa;
    }

    function get_siswa_pasif_min5(){
    	$tahun_aktif = get_tahun_sekarang();
        $tahun_min5 = $tahun_aktif - 5;
        $jumlahSiswa = Student::where('status', 'pasif')->whereBetween('tp', [$tahun_min5, $tahun_aktif])->count();
        return $jumlahSiswa;
    }

    function get_siswa_status_beda(){
        $jumlahSiswa = Student::where('status', '!=', 'aktif')->where('status', '!=', 'pasif')->orderBy('nama_lengkap', 'ASC')->get();
        return $jumlahSiswa;
    }

    function get_tahun_dari_siswa(){
    	$tahun_aktif = get_tahun_sekarang();
        $tahun_min10 = $tahun_aktif - 10;
        $tahuns = Tp::whereBetween('tp', [$tahun_min10, $tahun_aktif])->groupBy('tp')->select('tp')->get();
        $tahun = array();
        foreach ($tahuns as $key => $value) {
        	$tahun[] = $value->tp; 
        }
        return $tahun;
    }

    function siswa_tahunke($tahunke){
        $jumlahSiswa = Student::where('tp', $tahunke)->count();
        return $jumlahSiswa;
    }

    function get_siswa_tiap_tahun(){
    	$tahuns = get_tahun_dari_siswa();
    	$datas = array();
    	if(count($tahuns) < 10){
	    	for($i = (10 - count($tahuns)); $i > 0; $i-- ) {
	    		$tahun = $tahuns[0] - $i;
	    		$datas[] = array(
	    			'tahun' => $tahun . '/' . ($tahun + 1),
	    			'jumlah' => 0);
	    	}
	    }
    	foreach ($tahuns as $key => $value) {
    		$data = Student::where('tp', $value)->count();
    		$datas[] = array(
    			'tahun' => $value . '/' . ($value + 1),
    			'jumlah' => $data);
    	}
    	return $datas;
    }

    function get_jurusan_tahunke($tahunke, $jurusan_id){
        $jurusans = Student::where('jurusan', $jurusan_id)->where('tp', $tahunke)->count();
        return $jurusans;
    }

    function get_jurusans(){
		$jurusan_ids = Jurusan::select('id', 'singkatan')->get();
		$jurusans = array();
		foreach ($jurusan_ids as $key => $value) {
        	$jurusans[$value->id] = $value->singkatan; 
        }
		return $jurusans;
	}

    function get_jurusan_tiap_tahun($tahunke){
    	$jurusans = get_jurusans();
    	$datas = array();
    	foreach ($jurusans as $key => $value) {
    		$datas[] = array(
    			'jumlah' => get_jurusan_tahunke($tahunke, $key),
    			'jurusan' => $value);
    	}
    	return $datas;
    }

    function get_pembayaran_tahunke($tahunke){
    	$bln = Bulan_singkatan();
    	$datas = array();

    	for($i = 10; $i < 16; $i++){
    		$satu = HistoriSpp::where('thn_bayar', ($tahunke->tp))
	    		->where('bln_bayar', ($i-3))
	    		->where('kelas', 'REGEXP', 'X ')
	    		->count();
	    	$dua = HistoriSpp::where('thn_bayar', ($tahunke->tp))
	    		->where('bln_bayar', ($i-3))
	    		->where('kelas', 'REGEXP', 'XI ')
	    		->count();
	    	$tiga = HistoriSpp::where('thn_bayar', ($tahunke->tp))
	    		->where('bln_bayar', ($i-3))
	    		->where('kelas', 'REGEXP', 'XII ')
	    		->count();
	    	$datas[] = array(
    			'bulan' => $bln[$i],
    			'satu' => $satu,
    			'dua' => $dua,
    			'tiga' => $tiga);
	    }

    	for($i = 4; $i < 10; $i++){
    		$satu = HistoriSpp::where('thn_bayar', ($tahunke->tp) + 1)
	    		->where('bln_bayar', ($i-3))
	    		->where('kelas', 'REGEXP', 'X ')
	    		->count();
	    	$dua = HistoriSpp::where('thn_bayar', ($tahunke->tp) + 1)
	    		->where('bln_bayar', ($i-3))
	    		->where('kelas', 'REGEXP', 'XI ')
	    		->count();
	    	$tiga = HistoriSpp::where('thn_bayar', ($tahunke->tp) + 1)
	    		->where('bln_bayar', ($i-3))
	    		->where('kelas', 'REGEXP', 'XII ')
	    		->count();
	    	$datas[] = array(
    			'bulan' => $bln[$i],
    			'satu' => $satu,
    			'dua' => $dua,
    			'tiga' => $tiga);
	    }
        return $datas;
    }

	//--------------------------------------------------------------------------------
    
    function get_total_uang_masuk($tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_total_uang_bea($tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'be')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_total_uang_tunai($tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'tu')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_total_uang_alumni($tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'al')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_total_uang_siswa_baru($tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'sb')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_total_uang_penyesuaian($tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'pn')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_total_uang_kas_komite($tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', '!=', 'be')
		->where('note', '!=', 'al')
		->where('note', '!=', 'tu')
		->where('note', '!=', 'sb')
		->where('note', '!=', 'pn')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    //---------------------------------------------------------------------------

	function get_uang_masuk($kelas, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
	    ->where('kelas', 'REGEXP', $kelas.' ')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_bea($kelas, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'be')
	    ->where('kelas', 'REGEXP', $kelas.' ')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_siswa_tunai($kelas, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'tu')
	    ->where('kelas', 'REGEXP', $kelas.' ')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_siswa_alumni($kelas, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'al')
	    ->where('kelas', 'REGEXP', $kelas.' ')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_siswa_baru($kelas, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'sb')
	    ->where('kelas', 'REGEXP', $kelas.' ')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_penyesuaian($kelas, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'pn')
	    ->where('kelas', 'REGEXP', $kelas.' ')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_kas_komite($kelas, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', '!=', 'be')
		->where('note', '!=', 'al')
		->where('note', '!=', 'tu')
		->where('note', '!=', 'sb')
		->where('note', '!=', 'pn')
	    ->where('kelas', 'REGEXP', $kelas.' ')
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    //--------------------------------------------------------------------------------------

    function get_uang_masuk_bulanan($bulan, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
	    ->where('bln_bayar', $bulan)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_bea_bulanan($bulan, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'be')
	    ->where('bln_bayar', $bulan)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_siswa_baru_bulanan($bulan, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'sb')
	    ->where('bln_bayar', $bulan)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_alumni_bulanan($bulan, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'al')
	    ->where('bln_bayar', $bulan)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_tunai_bulanan($bulan, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'tu')
	    ->where('bln_bayar', $bulan)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    function get_uang_penyesuaian_bulanan($bulan, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', 'pn')
	    ->where('bln_bayar', $bulan)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

     function get_uang_kas_komite_bulanan($bulan, $tahunke){
		$datas = HistoriSpp::where('thn_bayar', $tahunke->tp)
		->where('note', '!=', 'be')
		->where('note', '!=', 'al')
		->where('note', '!=', 'tu')
		->where('note', '!=', 'sb')
		->where('note', '!=', 'pn')
	    ->where('bln_bayar', $bulan)
		->select('nominal')
		->get();

		$total = 0;
		foreach ($datas as $key => $value) {
			$total += $value->nominal;
		}
        return $total;
    }

    //--------------------------------------------------------------------------------------------------

       function get_uang_perangkatan($tahunke){
    	$kelas = array("X", "XI", "XII");
    	
    	$detail = array();
    	foreach ($kelas as $key => $value) {
    		$datas = array(
	            'total'     => get_uang_masuk($value, $tahunke),
	            'bea'     => get_uang_bea($value, $tahunke),
	            'sb'     => get_uang_siswa_baru($value, $tahunke),
	            'al'     => get_uang_siswa_alumni($value, $tahunke),
	            'tu'     => get_uang_siswa_tunai($value, $tahunke),
	            'komite'     => get_uang_kas_komite($value, $tahunke),
	            'pn'      =>get_uang_penyesuaian($value, $tahunke));
    		$detail[$value] = $datas;
    	}
    	
    	$total = $detail['X']['total'] + $detail['XI']['total'] + $detail['XII']['total'];
    	$bea = $detail['X']['bea'] + $detail['XI']['bea'] + $detail['XII']['bea'];
    	$sb = $detail['X']['sb'] + $detail['XI']['sb'] + $detail['XII']['sb'];
    	$al = $detail['X']['al'] + $detail['XI']['al'] + $detail['XII']['al'];
    	$tu = $detail['X']['tu'] + $detail['XI']['tu'] + $detail['XII']['tu'];
    	$komite = $detail['X']['komite'] + $detail['XI']['komite'] + $detail['XII']['komite'];
    	$pn = $detail['X']['pn'] + $detail['XI']['pn'] + $detail['XII']['pn'];
    	$upj = array(
    		'detail' => $detail,
    		'total'     => $total,
            'bea'     => $bea,
            'sb'     => $sb,
            'al'     => $al,
            'tu'     => $tu,
            'komite'     => $komite,
            'pn'      => $pn
    		);


    	return $upj;
    }

    function get_uang_perbulan($tahunke){
    	$bulan = Bulan_laporan();
    	
    	$detail = array();
    	foreach ($bulan as $key => $value) {
    		$datas = array(
	            'total'     => get_uang_masuk_bulanan($key, $tahunke),
	            'bea'     => get_uang_bea_bulanan($key, $tahunke),
	            'sb'     => get_uang_siswa_baru_bulanan($key, $tahunke),
	            'al'     => get_uang_alumni_bulanan($key, $tahunke),
	            'tu'     => get_uang_tunai_bulanan($key, $tahunke),
	            'komite'     => get_uang_kas_komite_bulanan($key, $tahunke),
	            'pn'      =>get_uang_penyesuaian_bulanan($key, $tahunke));
    		$detail[$value] = $datas;
    	}

    	$total = 0;
    	$bea = 0;
    	$sb = 0;
    	$al = 0;
    	$tu = 0;
    	$komite = 0;
    	$pn = 0;

    	foreach ($detail as $value => $value) {
    		$total += $detail[$value]['total'];
	    	$bea += $detail[$value]['bea'];
	    	$sb += $detail[$value]['sb'];
	    	$al += $detail[$value]['al'];
	    	$tu += $detail[$value]['tu'];
	    	$komite += $detail[$value]['komite'];
	    	$pn += $detail[$value]['pn'];
    	}
    	
    	
    	$upj = array(
    		'detail' => $detail,
    		'total'     => $total,
            'bea'     => $bea,
            'sb'     => $sb,
            'al'     => $al,
            'tu'     => $tu,
            'komite'     => $komite,
            'pn'      => $pn
    		);


    	return $upj;
    }

    //---------------------------------------------------------------------------

    function get_histori_bayar($nisn, $tahun = null){
    	if($tahun != null){
        	$historis = HistoriSpp::where('nisn', $nisn)->where('tp', $tahun)->get();
    	}else{
        	$historis = HistoriSpp::where('nisn', $nisn)->get();
    	}
    	return $historis;        
    }
    
    //---------------------------------------------------------------------------

    function tahunMasukSiswa($nisn){
    	$tahunMasuk = Student::where('nisn', $nisn)->select('tp')->first();
    	return $tahunMasuk->tp;
    }

    function tunggakanSiswaPerTp($nisn, $tp, $tahunMasukSiswa){
    	$tunggakan = array(
    		'spp' => tunggakanPerTp($tp, 'spp', $nisn, 9, $tahunMasukSiswa),
    		'pembangunan' => tunggakanPerTp($tp, 'pembangunan', $nisn, 9, $tahunMasukSiswa),
    		'pramuka' => tunggakanPerTp($tp, 'pramuka', $nisn, 9, $tahunMasukSiswa),
    		'lain2' => tunggakanPerTp($tp, 'lain2', $nisn, 9, $tahunMasukSiswa)
    		);
    	return $tunggakan;
    }

    function tunggakanSiswaTotal($nisn){
    	$tahunMasuk = tahunMasukSiswa($nisn);

    }

    //---------------------------------------------------------------------------

    function getRecordAkumulasi($nisn, $type, $tahunMasuk, $tp){
		$jumlahTahunAkum = $tp - $tahunMasuk;
		switch ($type) {
			case 'spp':
				$record = Spp::whereBetween('tp', [$tahunMasuk, $tp])->where('nisn', $nisn)->get();
				$jumlahRecord = count($record);

				if($jumlahRecord < $jumlahTahunAkum){
					$selisihKurang = $jumlahTahunAkum - $jumlahRecord;
					for ($i=0; $i < $selisihKurang; $i++) { 
						$record[] = new Spp;
					}
				}
				break;
			case 'pembangunan':
				$record = Pembangunan::whereBetween('tp', [$tahunMasuk, $tp])->where('nisn', $nisn)->get();
				$jumlahRecord = count($record);

				if($jumlahRecord < $jumlahTahunAkum){
					$selisihKurang = $jumlahTahunAkum - $jumlahRecord;
					for ($i=0; $i < $selisihKurang; $i++) { 
						$record[] = new Pembangunan;
					}
				}
				break;
			case 'pramuka':
				$record = Pramuka::whereBetween('tp', [$tahunMasuk, $tp])->where('nisn', $nisn)->get();
				$jumlahRecord = count($record);

				if($jumlahRecord < $jumlahTahunAkum){
					$selisihKurang = $jumlahTahunAkum - $jumlahRecord;
					for ($i=0; $i < $selisihKurang; $i++) { 
						$record[] = new Pramuka;
					}
				}
				break;
			case 'lain2':
				$record = LainLain::whereBetween('tp', [$tahunMasuk, $tp])->where('nisn', $nisn)->get();
				$jumlahRecord = count($record);

				if($jumlahRecord < $jumlahTahunAkum){
					$selisihKurang = $jumlahTahunAkum - $jumlahRecord;
					for ($i=0; $i < $selisihKurang; $i++) { 
						$record[] = new LainLain;
					}
				}
				break;
		}
		
		// dd($record);
		return $record;
	}

	function tunggakanAkumulasiMinsatu($nisn, $tahunMasuk, $tp){
    	$tp = $tp - 1;
    	$tahunMasuk = $tahunMasuk - 1;
    	$spp = getRecordAkumulasi($nisn, 'spp', $tahunMasuk, $tp);
    	$pembangunan = getRecordAkumulasi($nisn, 'pembangunan', $tahunMasuk, $tp);
    	$pramuka = getRecordAkumulasi($nisn, 'pramuka', $tahunMasuk, $tp);
    	$lain2 = getRecordAkumulasi($nisn, 'lain2', $tahunMasuk, $tp);

    	$tunggakan = array(
    		'spp' => hitungAkumTungg($spp, $tp, 'spp', $nisn),
    		'pembangunan' => hitungAkumTungg($pembangunan, $tp, 'pembangunan', $nisn),
    		'pramuka' => hitungAkumTungg($pramuka, $tp, 'pramuka', $nisn),
    		'lain2' => hitungAkumTungg($lain2, $tp, 'lain2', $nisn)
    	);
    	return $tunggakan;
    }

    //----------------------------------------------------------------------------

    function getTahunAkumTunggakan($tpAktif, $tahunMasukSiswa, $nisn){
    	$tahunTunggakan = array();
    	for ($i = ($tpAktif - 1); $i >= $tahunMasukSiswa; $i--) { 
    		$tahunTunggakan[] = $i;
    	}
    	return $tahunTunggakan;
    }

    function getAkumTunggakan($tpAktif, $tahunMasukSiswa, $nisn){
    	$akumTunggakan = array(
    		'spp' => array(),
    		'pembangunan' => array(),
    		'pramuka' => array(),
    		'lain2' => array()
    		);
    	$tahunTunggakan = getTahunAkumTunggakan($tpAktif, $tahunMasukSiswa, $nisn);
    	foreach ($tahunTunggakan as $key => $value) {
    		$tpObj = getTp($value);
    		$akumTunggakan['spp'][$value] = get_record($nisn, 'spp', $tpObj);    		
    		$akumTunggakan['pembangunan'][$value] = get_record($nisn, 'pembangunan', $tpObj);    		
    		$akumTunggakan['pramuka'][$value] = get_record($nisn, 'pramuka', $tpObj);    		
    		$akumTunggakan['lain2'][$value] = get_record($nisn, 'lain2', $tpObj);    		
    	}
    	return $akumTunggakan;
    }

    function hitungAkumTunggTotal($tpAktif, $tahunMasukSiswa, $nisn){
    	$akumTunggakan = getAkumTunggakan($tpAktif, $tahunMasukSiswa, $nisn);
    	$spp = hitungAkumTungg($tpAktif, $tahunMasukSiswa, $akumTunggakan['spp'], 'spp', $nisn);
    	$pembangunan = hitungAkumTungg($tpAktif, $tahunMasukSiswa, $akumTunggakan['pembangunan'], 'pembangunan', $nisn);
    	$pramuka = hitungAkumTungg($tpAktif, $tahunMasukSiswa, $akumTunggakan['pramuka'], 'pramuka', $nisn);
    	$lain2 = hitungAkumTungg($tpAktif, $tahunMasukSiswa, $akumTunggakan['lain2'], 'lain2', $nisn);
    	$total = $spp + $pembangunan + $pramuka + $lain2;

    	$tunggakan = array(
    		'spp' => $spp,
    		'pembangunan' => $pembangunan,
    		'pramuka' => $pramuka,
    		'lain2' => $lain2,
    		'total' => $total
    	);
    	return $tunggakan;
    }

    function hitungAkumTungg($tpAktif, $tahunMasukSiswa, $record, $type, $nisn){
		$tunggakan = 0;
		$jenjangJurusan = getStudentJenjangJurusan($nisn);
		$tahunKeluar = getTahunKeluarSiswa($nisn);
		if($type == 'pembangunan'){
			foreach ($record as $key => $value) {
				if($tahunMasukSiswa == $key){
					$tunggakan += tunggakanPerTp($key, $type, $nisn, 9, $tahunMasukSiswa);
				}
			}
		}else{
			if($tahunKeluar == NULL){
				foreach ($record as $key => $value) {
					$tunggakan += tunggakanPerTp($key, $type, $nisn, 9, $tahunMasukSiswa);
				}
			}else{
				foreach ($record as $key => $value) {
					if($key <= $tahunKeluar){
						$tunggakan += tunggakanPerTp($key, $type, $nisn, 9, $tahunMasukSiswa);
					}
				}
			}
		}	
		return $tunggakan;
	}

	//--------------------------------------------------------------------------------------

	function getRombelById($idKelas){
		$rombel = Rombel::find($idKelas);
		return $rombel;
	}

	function getJurusanById($idJurusan){
		$jurusan = Jurusan::find($idJurusan);
		return $jurusan;
	}
 
 	function getNaikKelas($idKelas){
 		$rombel = getRombelById($idKelas);
 		$jurusan = getJurusanById($rombel->jurusan_id);
 		$tingkat = $rombel->tingkat;
 		$tingkatAtas = $tingkat + 1;
 		$idJurusan = $rombel->jurusan_id;
	 	$namaKelas = explode(' ',$rombel->nama);//dd($namaNaikKelas);
	 	if(count($namaKelas) == 3){
	 		$namaKelasAtas = getAngkaRomawiKelas($tingkatAtas) . ' ' . $namaKelas[1] . ' ' . $namaKelas[2];
 			$naikKelas = Rombel::where('jurusan_id', $idJurusan)
 								->where('tingkat', $tingkatAtas)
 								->where('nama', $namaKelasAtas)
 								->first();
	 		if($naikKelas == NULL){
	 			$naikKelas = createNewRombel($tingkatAtas, $jurusan, $namaKelas[2]);
	 			return $naikKelas->nama;
	 		}
 		}else{
 			$naikKelas = Rombel::where('jurusan_id', $idJurusan)->where('tingkat', $tingkatAtas)->first();
 			if($naikKelas == NULL){
	 			$naikKelas = createNewRombel($tingkatAtas, $jurusan);
	 			return $naikKelas->nama;
	 		}
 		}

 		return $naikKelas->nama;
 	}

 	function getAngkaRomawiKelas($tingkat){
 		$kelasRomawi = array(
 			1 => 'X',
 			2 => 'XI',
 			3 => 'XII',
 			4 => 'XIII',
 			5 => 'XIV'
 			);
 		return $kelasRomawi[$tingkat];
 	}

 	function getNaikKelasId($idKelas){
 		$rombel = getRombelById($idKelas);
 		$jurusan = getJurusanById($rombel->jurusan_id);
 		$tingkat = $rombel->tingkat;
 		$tingkatAtas = $tingkat + 1;
 		$idJurusan = $rombel->jurusan_id;
	 	$namaKelas = explode(' ',$rombel->nama);//dd($namaNaikKelas);
	 	if(count($namaKelas) == 3){
	 		$namaKelasAtas = getAngkaRomawiKelas($tingkatAtas) . ' ' . $namaKelas[1] . ' ' . $namaKelas[2];
 			$naikKelas = Rombel::where('jurusan_id', $idJurusan)
 								->where('tingkat', $tingkatAtas)
 								->where('nama', $namaKelasAtas)
 								->first();
	 		if($naikKelas == NULL){
	 			$naikKelas = createNewRombel($tingkatAtas, $jurusan, $namaKelas[2]);
	 			return $naikKelas->id;
	 		}
 		}else{
 			$naikKelas = Rombel::where('jurusan_id', $idJurusan)->where('tingkat', $tingkatAtas)->first();
 			if($naikKelas == NULL){
	 			$naikKelas = createNewRombel($tingkatAtas, $jurusan);
	 			return $naikKelas->id;
	 		}
 		}

 		return $naikKelas->id;
 	}

 	function createNewRombel($tingkat, $jurusan, $kelasKe = null){
 		$kelasRomawi = array(
 			1 => 'X',
 			2 => 'XI',
 			3 => 'XII',
 			4 => 'XIII',
 			5 => 'XIV'
 			);
 		if($kelasKe != NULL)
 			$namaRombel = $kelasRomawi[$tingkat] . ' ' . $jurusan->singkatan . ' ' . $kelasKe;
 		else
 			$namaRombel = $kelasRomawi[$tingkat] . ' ' . $jurusan->singkatan;

 		$newRombel = new Rombel;
 		$newRombel->nama = $namaRombel;
 		$newRombel->tingkat = $tingkat;
 		$newRombel->jurusan_id = $jurusan->id;
 		$newRombel->kurikulum = '';
 		$newRombel->pengajar = '';
 		$newRombel->save();

 		return $newRombel;
 	}

//-----------------------------------------------------------------------------------------------

 	function hitungTotalTunggakan($nisn, $tpAktif, $tahunMasukSiswa, $batasTunggakan){
        $tunggakanAktif = tunggakan_tahun_aktif($nisn, $tpAktif, $tahunMasukSiswa, $batasTunggakan, $batasTunggakan, $batasTunggakan, $batasTunggakan);
            $tunggakanMinsatu = hitungAkumTunggTotal($tpAktif, $tahunMasukSiswa, $nisn);

        $totalSpp = $tunggakanAktif['spp'] + $tunggakanMinsatu['spp'];
        $totalPembangunan = $tunggakanAktif['pembangunan'] + $tunggakanMinsatu['pembangunan'];
        $totalPramuka = $tunggakanAktif['pramuka'] + $tunggakanMinsatu['pramuka'];
        $totalLain2 = $tunggakanAktif['lain2'] + $tunggakanMinsatu['lain2'];
        $total = $tunggakanAktif['total'] + $tunggakanMinsatu['total'];
        $totalTunggakan = array(
            'spp' => $totalSpp,
            'pembangunan' => $totalPembangunan,
            'pramuka' => $totalPramuka,
            'lain2' => $totalLain2,
            'total' => $total
            );

        return $totalTunggakan;
    }


//--------------------------------------------------------------------------------------------

    function get_user_name($id){
        $komite = Komite::where('komite_id', $id)->select('komite_name')->first();
        $tu = Customer::where('customer_id', $id)->select('customer_name')->first();
        if($komite){
            return strtoupper($komite->komite_name);
        }elseif($tu){
            return strtoupper($tu->customer_name);
        }else{
            return '';
        }   
    }

    function checkExistedClassByName($className){
    	$rombel = Rombel::where('nama', $className)->select('nama')->first();
    	if($rombel) {
    		return '<span style="color:black">Kelas telah tersedia</span>';
    	}else{
    		return '<span style="color:red">Kelas belum tersedia</span>';
    	}


    }